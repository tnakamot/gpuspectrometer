#ifndef DBBC3SPECTROMETER_H
#define DBBC3SPECTROMETER_H

#include "datarecipients/datarecipient.hxx"

class UDPVDIFReceiver;

class DBBC3Spectrometer : public DataRecipient {

    public:
        /** C'stor
         * \param Tint_sec integration time in seconds
         * \param port UDP port for listening for VDIF data
         */
        DBBC3Spectrometer(float Tint_sec, int port);

        /** D'stor */
        ~DBBC3Spectrometer();

    public:
        /** Iface of DataRecipient */
        void takeData(void* data, size_t nbytes, double w, struct timeval);

    public:
        /** Start data capture and processing */
        bool start();

        /** Stop data capture and processing */
        bool stop();

    private:
        UDPVDIFReceiver* udpRx;
        int udp_port;
        size_t vdif_framesize;
        size_t vdif_payloadsize;
        size_t vdif_framespersec;
        int vdif_Nbufs;
        size_t vdif_bufsize;
        float vdif_Tint;
}

#endif
