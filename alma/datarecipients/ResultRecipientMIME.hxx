/**
 *
 * \class ResultRecipientMIME
 *
 * Implements the ResultRecipient interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 * Output data are written in MIME format as defined in the internal ICD (ALMA-64.00.00.00-70.44.00.00-A-ICD).
 * The MIME message contains a global header, an a 'multipart/mixed' to which new results are appended.
 *
 * The spectral results set in MIME format created by the spectrometer is structured as:
 *
 * - MIME header
 * - MIME multipart level 1 as 'multipart/mixed'
 *   - dataHeader.xml as 'text/xml'
 *   - MIME multipart level 2 as 'multipart/related'
 *     - <ip_name>/desc.xml as 'text/xml'
 *     - <ip_name>/flags.bin as 'application/octet-stream'
 *     - <ip_name>/actualTimes.bin as 'application/octet-stream'
 *     - <ip_name>/actualDurations.bin as 'application/octet-stream'
 *     - <ip_name>/autoData.bin as 'application/octet-stream'
 *   - MIME multipart level 2 as 'multipart/related'
 *     - <ip_name>/desc.xml as 'text/xml'
 *     - <ip_name>/flags.bin as 'application/octet-stream'
 *     ...
 *
 * where <ip_name> consists of <observationId/antennaId/baseBandName/<%05d(ipNr)>
 * in which ipNr starts from 1 and increments by 1 for each added 'multipart/related'.
 *
 * The MIME file(s) from several spectrometer instances are later combined i
 * the ASC server into a final output file in ALMA BDF format.
 */

#ifndef RESULTRECIPIENTMIME_HXX
#define RESULTRECIPIENTMIME_HXX

#include "datarecipients/ResultRecipient_iface.hxx"

#include <string>

#include <stdlib.h>
#include <sys/time.h>

class MCObservation;

class ResultRecipientMIME : public ResultRecipient
{

    public:
        /** C'stor with defaults */
        ResultRecipientMIME(std::string outputPath);

        /** D'stor, blocks until reasonably sure all data were written */
        ~ResultRecipientMIME();

    public:
        /** Open the MIME file */
        void open();

        /** Copy certain needed MIME/BDF infos from MCObservation; if used then call this before open() */
        void configureFrom(const MCObservation*);

        /** Block until all results were written */
        void sync();

        /** Safely close the MIME file */
        void close();

    public:

        /** Callback-like function. Accepts a SpectralResultSet object. Apply data reordering and calibrations and store the data. */
        void takeResult(const void* resultset);

    private:
        void* pimpl;
};

#endif // RESULTRECIPIENTMIME_HXX

