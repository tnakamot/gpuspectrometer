/**
 *
 * \class ResultRecipientPlain
 *
 * Writes results into a plain file. No headers.
 * Can plot with GNUPlot with commands such as:
 * >> set logscale y
 * >> plot '/tmp/xyz987654321_1_BB_1.plain' binary record=(524288)  format='%float' using 1 with lines
 * >> plot '/tmp/xyz987654321_1_BB_1.plain' binary record=(1048576) format='%float' using 1 with lines
 * >> plot '/tmp/xyz987654321_1_BB_1.plain' binary record=(2097152) format='%float' using 1 with lines
 *
 */

#include "datarecipients/ResultRecipientPlain.hxx"
#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx" // class MCConfiguration
#include "mcwrapping/mc_observation.hxx"   // class MCObservation (has observation metadata and another copy of MCConfiguration)
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include <stdlib.h>
#include <sys/time.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

#define lockscope  // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes

//////////////////////////////////////////////////////////////////////////////////////////////////////

class ResultRecipientPlain_impl;
class ResultRecipientPlain_impl : private boost::noncopyable
{
    friend class ResultRecipientPlain;

    boost::mutex mutex; /// a mutex to serialize access where needed

    std::string root; /// root for 'Content-Location:' e.g. "xyz987654321/1/BB_1/"
    int nQueued;      /// number of results not yet fully written out
    boost::condition_variable alldoneCond; /// signaled when no results are pending and relatively save to quit

    std::string outfilepath;
    std::string outfilename;
    std::ofstream outfile;
    struct timespec first_timestamp;

    /** C'stor() */
    ResultRecipientPlain_impl() {
        nQueued = 0;
        first_timestamp.tv_sec = 0;
        first_timestamp.tv_nsec = 0;
    }

};

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** C'stor */
ResultRecipientPlain::ResultRecipientPlain(std::string outputPath) : ResultRecipient(outputPath)
{
    ResultRecipientPlain_impl* impl = new ResultRecipientPlain_impl;
    impl->outfilepath = outputPath;
    impl->outfilename = "/tmp/ResultRecipientPlain.plain";
    impl->root = "defaultObs/1/BB_1/";
    pimpl = (void*)impl;
}

/** D'stor, blocks until reasonably sure all data were written */
ResultRecipientPlain::~ResultRecipientPlain()
{
    this->close();
    delete (ResultRecipientPlain_impl*)pimpl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Copy certain needed MIME/BDF infos from MCObservation; if used then call this before open() */
void ResultRecipientPlain::configureFrom(const MCObservation* obs)
{
    ResultRecipientPlain_impl* impl = (ResultRecipientPlain_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mutex);
        std::string of = obs->observationId + "_" + obs->antennaId + "_" + obs->baseBandName + ".plain";
        std::replace(of.begin(), of.end(), ':', '_');
        std::replace(of.begin(), of.end(), '*', '_');
        std::replace(of.begin(), of.end(), '?', '_');
        std::replace(of.begin(), of.end(), '/', '_');
        impl->outfilename = impl->outfilepath + of;
        impl->root = obs->observationId + "/" + obs->antennaId + "/" + obs->baseBandName + "/";
        impl->first_timestamp.tv_sec = 0;
        impl->first_timestamp.tv_nsec = 0;
    }
}

/** Open the BDF file */
void ResultRecipientPlain::open()
{
    ResultRecipientPlain_impl* impl = (ResultRecipientPlain_impl*)pimpl;

    impl->outfile.open(impl->outfilename.c_str());
    L_(linfo) << "ResultRecipientPlain initialized output to file " << impl->outfilename;
}

/** Block until all results were written */
void ResultRecipientPlain::sync()
{
    if (pimpl == NULL) {
        return;
    }

    // Wait for all dispatched writers to finish
    ResultRecipientPlain_impl* impl = (ResultRecipientPlain_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mutex);
        while (impl->nQueued > 0) {
            impl->alldoneCond.wait(mmlock);
        }
    }
}

/** Safely close the BDF file*/
void ResultRecipientPlain::close()
{
    if (pimpl == NULL) {
        return;
    }

    // Wait for all dispatched writers to finish
    sync();

    // Close output
    ResultRecipientPlain_impl* impl = (ResultRecipientPlain_impl*)pimpl;
    if (impl->outfile.is_open()) {
        impl->outfile.close();
        L_(linfo) << "ResultRecipientPlain closed output file " << impl->outfilename;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Write data to a file as-is, immediately, no background thread. Does not append! */
void ResultRecipientPlain::takeResult(const void* resultset)
{
    ResultRecipientPlain_impl* impl = (ResultRecipientPlain_impl*)pimpl;
    const SpectralResultSet* res = (const SpectralResultSet*)resultset;

    if (res->bytesinfinalspectra > 0) {
        impl->outfile.seekp(0);
        impl->outfile.write((char*)res->finalspectra, res->bytesinfinalspectra);
    } else {
        impl->outfile.seekp(0);
        for (int n = 0; n < res->nspectra; n++) {
            impl->outfile.write((char*)res->spectra[n], res->bytesperspectrum);
        }
    }

    if (impl->first_timestamp.tv_sec == 0) {
        impl->first_timestamp = res->timestamp;
    }
    float dT = (res->timestamp.tv_sec - impl->first_timestamp.tv_sec) + 1e-9*(res->timestamp.tv_nsec - impl->first_timestamp.tv_nsec);
    L_(linfo) << "ResultRecipientPlain dispatched " << (res->nspectra * res->bytesperspectrum) << "-byte raw and "
              << res->bytesinfinalspectra << "-byte postprocessed data of time dT=" << std::fixed << std::setprecision(4) << dT << "s to output writer";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
