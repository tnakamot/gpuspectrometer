
#include "datarecipients/SpectralResultSetManager.hxx"
#include "datarecipients/SpectralResultSet.hxx"    // class SpectralResultSet
#include "mcwrapping/mc_configuration.hxx"         // class MCConfiguration
#include "mcwrapping/mc_observation.hxx"           // class MCObservation
#include "drxp/DRXPInfoFrame.hxx"                  // class DRXPInfoFrame

#include <stdint.h>
#include <iostream>
#include <iomanip>

static void test1()
{
    const int N_48MS_FRAMES = 140;

    // Config to test
    MCConfiguration cfg;
    cfg.createTestconfig2();

    // Modify the config into a switching -type observation
    double Tdwell = 100e-3, Ttrans = 100e-3;
    //double Tdwell = 48e-3, Ttrans = 48e-3;
    //double Tdwell = 5e-3, Ttrans = 5e-3;
    cfg.singleConfiguration.timeSpec.switching.clear();
    for (int p=0; p<2; p++) {
        XSD::switching_t sw(p, Tdwell, Ttrans);
        cfg.singleConfiguration.timeSpec.switching.push_back(sw);
    }
    cfg.singleConfiguration.timeSpec.integrationDuration = 2*(Tdwell + Ttrans);
    cfg.singleConfiguration.timeSpec.channelAverageDuration = 2*(Tdwell + Ttrans);

    // Starting time: 1000.0 sec, stop 1005.0
    cfg.procConfig.requestedStartTime.tv_sec = 1000;
    cfg.procConfig.requestedStartTime.tv_nsec = 0;
    cfg.procConfig.requestedStopTime.tv_sec = 1005;
    cfg.procConfig.requestedStopTime.tv_nsec = 0;
    cfg.updateDerivedParams();
    if (!cfg.valid()) {
        std::cout << "Config used for test thinks it is not valid! Stopping.\n\nConfig was:" << cfg << "\n";
        return;
    }

    // DRXP data starting time: 999.008 sec
    DRXPInfoFrame frame;
    frame.swRxTime_UTC.tv_sec = 999;
    frame.swRxTime_UTC.tv_nsec = 8e-3 * 1e9;
    frame.hwTickCount_48ms = 0;

    // Use the SpectralResultSetManager to look up storages for a series of DRXP frames
    SpectralResultSetManager mgr;
    int completed_count = 0;
    int processeable_msec_count = 0;
    std::cout << "Testing segmentation of an integrationDuration of " << std::fixed << std::setprecision(4) << (cfg.singleConfiguration.timeSpec.integrationDuration*1e3) << "ms and 48ms frames\n";
    for (int n=0; n<N_48MS_FRAMES; n++) {

        std::cout << "------- Frame " << n << " -------\n";
        const int64_t frame_ms = mgr.getMillisecSince(&frame.swRxTime_UTC, &cfg.procConfig.requestedStartTime);

        // Get all integrations possible during this frame
        int offset_ms = 0;
        while (offset_ms < 48) {

            SpectralResultSet* prs;
            int curr_swpos, integ_use_ms, transit_use_ms;

            std::cout << "Obs from " << std::fixed << std::setprecision(4) << (cfg.procConfig.requestedStartTime.tv_sec + cfg.procConfig.requestedStartTime.tv_nsec*1e-9) << "s ";
            std::cout << "to " << std::fixed << std::setprecision(4) << (cfg.procConfig.requestedStopTime.tv_sec + cfg.procConfig.requestedStopTime.tv_nsec*1e-9) << "s\n";
            std::cout << "Frame of " << std::fixed << std::setprecision(4) << (frame.swRxTime_UTC.tv_sec + frame.swRxTime_UTC.tv_nsec*1e-9) << "s check at +" << offset_ms << "ms\n";

            bool valid = mgr.lookupSpectralResultSet(&frame, &offset_ms, &cfg, &prs, &curr_swpos, &integ_use_ms, &transit_use_ms);

            std::cout << "  lookup : ok=" << valid << ", usable from offset_ms=" << offset_ms << ", "
                      << "cycle=" << ((prs != NULL) ? (prs->id) : (-1)) << ", "
                      << "swpos=" << curr_swpos << ", frame left "
                      << "rdwell=" << integ_use_ms << "ms "
                      << "rtrsit=" << transit_use_ms << "ms\n";

            // Frame contributes to no SpectralResultSet?
            if (!valid) {
                std::cout << "  ** ignore, out of obs range\n";
                break;
            } else {
                assert(prs != NULL);
            }

            // Frame contributes to the results set 'prs'
            // (FFT processing, accumulate 'integ_use_ms' worth of data found at offset into current frame of 'offset_ms')

            if (integ_use_ms == 0) {
                processeable_msec_count += transit_use_ms;
                prs->total_msec += transit_use_ms;
                std::cout << "  ** transition during " << (frame_ms + offset_ms) << "..." << (frame_ms + offset_ms + transit_use_ms) << "ms from obs start, "
                          << " now res[" << prs->id << "].total_msec=" << prs->total_msec  << "\n";
            } else {
                processeable_msec_count += integ_use_ms;
                prs->total_msec += integ_use_ms;
                prs->integrated_msec += integ_use_ms;
                std::cout << "  ** dwell+accu during " << (frame_ms + offset_ms) << "..." << (frame_ms + offset_ms + integ_use_ms) << "ms from obs start, "
                          << " now res[" << prs->id << "].total_msec=" << prs->total_msec  << "\n";
            }

            //if (prs->integrated_msec >= cfg.singleConfiguration.procConfig.Tswitchingdwell_msec) { // --> setCompleted(prs) but creation of new obj when last transition time starts!
            if (prs->total_msec >= cfg.procConfig.Tswitchingcycle_msec) { // --> setCompleted(prs) at end of cycle, avoid creation of unnecessary new obj
                std::cout << "  ++ cycle=" << prs->id << " appears ready, integrated " << prs->integrated_msec << "ms wallclock " << prs->total_msec << "ms "
                          << " >= switching cycle " << cfg.procConfig.Tswitchingcycle_msec << "ms\n";
                completed_count++;
                prs->lock.unlock();
                mgr.setCompleted(prs->id);
            } else {
                prs->lock.unlock();
            }

            // Next accumulation in this frame if any
            if (integ_use_ms == 0) {
                offset_ms += transit_use_ms;
            } else {
                offset_ms += integ_use_ms;
            }
        }

        // Next DRXP frame (timestamp)
        mgr.incTimespecMillisecs(&frame.swRxTime_UTC, 48);
        frame.hwTickCount_48ms++;

        // Spectral write-out
        SpectralResultSet* prsout = mgr.getNextCompleted();
        while (prsout != NULL) {
            // postProcessor.process(prsout);
            // prsout->setPostprocessed();
            // outputWriter.takeData(prsout);
            prsout->lock.unlock();
            mgr.releaseCompleted(prsout->id);
            prsout = mgr.getNextCompleted();
        }
    }

    int completed_expect = (processeable_msec_count*1e-3) /  cfg.singleConfiguration.timeSpec.integrationDuration;
    std::cout << "\nTest run finished!\n";
    std::cout << "Had " << N_48MS_FRAMES*48 << "ms of data, " << processeable_msec_count << "ms were in range of observation.\n";
    std::cout << "Got " << completed_count << " completed out of " << completed_expect << " expected SpectralResultSet's.\n\n";
}

static void test2()
{
    std::cout << "Starting test for 99ms/100ms integration rounding issue\n";

    MCConfiguration cfg100ms;
    cfg100ms.createTestconfig1();
    cfg100ms.singleConfiguration.timeSpec.integrationDuration = 100e-3;
    cfg100ms.singleConfiguration.timeSpec.channelAverageDuration = 100e-3;
    cfg100ms.singleConfiguration.timeSpec.switching[0].dwell_sec = 100e-3;
    cfg100ms.procConfig.requestedStartTime.tv_sec = 1000;
    cfg100ms.procConfig.requestedStartTime.tv_nsec = 0;
    cfg100ms.updateDerivedParams();

    DRXPInfoFrame frame;
    frame.swRxTime_UTC.tv_sec = 999;
    frame.swRxTime_UTC.tv_nsec = 8e-3 * 1e9;
    frame.hwTickCount_48ms = 0;

    for (int pps_align_offset_ms=0; pps_align_offset_ms<480; pps_align_offset_ms+=1) {

        // Start of frame series
        frame.swRxTime_UTC.tv_sec = cfg100ms.procConfig.requestedStartTime.tv_sec - 1;
        frame.swRxTime_UTC.tv_nsec = pps_align_offset_ms * 1000000UL;
        frame.hwTickCount_48ms = 13;

        // Keeping track
        SpectralResultSetManager m;
        struct timespec first_timestamp;
        int prev_ap = -1;
        memset(&first_timestamp, 0, sizeof(first_timestamp));
        std::cout << "\n" << "Checking offset-to-1PPS of " << pps_align_offset_ms << "ms...\n";

        // Check 126 frames; LCM(48ms,1000ms)=6000ms = 125 x 48ms plus one extra frame
        for (int n=0; n<126; n++) {

            int offset_ms = 0;
            while (offset_ms < 48) {
                SpectralResultSet* prs;
                int curr_swpos, integ_use_ms, transit_use_ms;
                bool valid = m.lookupSpectralResultSet(&frame, &offset_ms, &cfg100ms, &prs, &curr_swpos, &integ_use_ms, &transit_use_ms);
                if (!valid) { break; }
                if (integ_use_ms == 0) {
                    prs->total_msec += transit_use_ms;
                } else {
                    prs->total_msec += integ_use_ms;
                    prs->integrated_msec += integ_use_ms;
                }
                //std::cout << " res[" << prs->id << "].total_msec=" << prs->total_msec << "\n";
                if (prs->total_msec >= cfg100ms.procConfig.Tswitchingcycle_msec) {
                    // "takeData():"
                    if (first_timestamp.tv_sec == 0) { first_timestamp = prs->timestamp; }
                    float dT = (prs->timestamp.tv_sec - first_timestamp.tv_sec) + 1e-9*(prs->timestamp.tv_nsec - first_timestamp.tv_nsec);
                    int dT_ms_fract = int(1000*dT) % 1000;
                    if (prev_ap != -1) {
                        if (prs->id != (prev_ap+1)) {
                            std::cout << "with pps-offset " << pps_align_offset_ms << "ms and frame #" << n << " got AP gap between AP#" << prev_ap << " and #" << prs->id << "\n";
                        }
                    }
                    prev_ap = prs->id;
                    // with 100ms int the dT_ms_fract should be multiple of 100ns
                    //std::cout << "with pps-offset " << pps_align_offset_ms << "ms and frame #" << n
                    //          << " finished AP #" << prs->id << " time dT="  << std::fixed << std::setprecision(5) << dT << " dT_ms_fract=" << dT_ms_fract << "\n";
                    prs->lock.unlock();
                    m.setCompleted(prs->id);
                } else {
                    prs->lock.unlock();
                }
                offset_ms += (integ_use_ms == 0) ? transit_use_ms : integ_use_ms;
            }

            // Next DRXP frame (timestamp)
            m.incTimespecMillisecs(&frame.swRxTime_UTC, 48);
            frame.hwTickCount_48ms++;

            // Spectral write-out
            SpectralResultSet* prsout = m.getNextCompleted();
            while (prsout != NULL) {
                // postProcessor.process(prsout);
                // psrout->setPostprocessed();
                // outputWriter.takeData(prsout);
                std::cout << "dump id " << prsout->id << "\n";
                prsout->lock.unlock();
                m.releaseCompleted(prsout->id);
                prsout = m.getNextCompleted();
            }
        }

        std::cout << "Checking for any leftover APs at end of testing of offset-to-1PPS of " << pps_align_offset_ms << "ms\n";
        SpectralResultSet* r;
        int nincomplete = 0;
        while ((r = m.first()) != NULL) {
            std::cout << "   incomplete spectrum with ID#" << r->id << " with " << r->total_msec << "ms of data\n";
            m.setCompleted(r->id);
            r->lock.unlock();
            m.releaseCompleted(r->id);
            nincomplete++;
        }
        std::cout << "   " << nincomplete << " incomplete spectra out of max. 1 incomplete expected, result : " << ((nincomplete<=1) ? "PASS" : "FAIL") << "\n";
    }
}

static void test3()
{
    const int N_48MS_FRAMES = 40;

    std::cout << "Starting test for manager deadline writeout\n";

    MCConfiguration cfg100ms;
    cfg100ms.createTestconfig1();
    cfg100ms.singleConfiguration.timeSpec.integrationDuration = 100e-3;
    cfg100ms.singleConfiguration.timeSpec.channelAverageDuration = 100e-3;
    cfg100ms.singleConfiguration.timeSpec.switching[0].dwell_sec = 100e-3;
    cfg100ms.procConfig.requestedStartTime.tv_sec = 1000;
    cfg100ms.procConfig.requestedStartTime.tv_nsec = 0;
    cfg100ms.updateDerivedParams();

    DRXPInfoFrame frame;
    frame.swRxTime_UTC.tv_sec = cfg100ms.procConfig.requestedStartTime.tv_sec;
    frame.swRxTime_UTC.tv_nsec = 0;
    frame.hwTickCount_48ms = 0;

    SpectralResultSetManager mgr;

    for (int n=0; n<N_48MS_FRAMES; n++) {

        // On-demand creation of SpectralResultSet(s)
        int offset_ms = 0, latest_id = 0;
        while (offset_ms < 48) {
            SpectralResultSet* prs;
            int curr_swpos, integ_use_ms, transit_use_ms;
            bool valid = mgr.lookupSpectralResultSet(&frame, &offset_ms, &cfg100ms, &prs, &curr_swpos, &integ_use_ms, &transit_use_ms);
            if (!valid) { break; }
            offset_ms += (integ_use_ms == 0) ? transit_use_ms : integ_use_ms;
            latest_id = prs->id;
            prs->lock.unlock();
            // Don't mark SpectralResultSet as completed (simulate partial data loss)
        }

        // Increment DRXP time
        mgr.incTimespecMillisecs(&frame.swRxTime_UTC, 48);
        frame.hwTickCount_48ms++;

        // Mark some spectra amidst incomplete spectra as completed
        if ((latest_id == 0) || (latest_id == 6) || (latest_id == 7) || (latest_id == 12)) {
            if (!mgr.isCompleted(latest_id)) {
                mgr.setCompleted(latest_id);
            }
        }

        // Check whether deadline reached for something
        SpectralResultSet* prsout = mgr.getNextCompleted();
        while (prsout != NULL) {
            // postProcessor.process(prsout);
            // psrout->setPostprocessed();
            // outputWriter.takeData(prsout);
            prsout->lock.unlock();
            mgr.releaseCompleted(prsout->id);
            prsout = mgr.getNextCompleted();
        }
    }
}

int main(int argc, char** argv)
{
    //test1(); // Segmentation of an integrationDuration (AP) over multiple DRXP frames, or multiple APs in one DRP frame
    test2(); // Try to reproduce 99ms/100ms integration rounding issue (failed so far)
    test3(); // Manager deadline writeout

    return 0;
}
