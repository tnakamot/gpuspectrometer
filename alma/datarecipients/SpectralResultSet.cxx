/**
 *
 * \class SpectralResultSet
 *
 * Container for spectral result details, and wrapper to pointers where spectra are stored in memory.
 *
 */

#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <iostream>
#include <string.h>
#include <sys/time.h>

/** Initialize as empty */
SpectralResultSet::SpectralResultSet()
{
    user_owned_arrays = true;

    id = 0;
    total_msec = 0;
    integrated_msec = 0;
    memset(&timestamp, 0, sizeof(timestamp));
    spectra = NULL;
    bytesperspectrum = 0;
    nspectra = 0;
    weight = 0;

    memset(&histogram_X, 0, sizeof(histogram_X));
    memset(&histogram_Y, 0, sizeof(histogram_Y));
    histogram_is_set = false;

    cfg = NULL;
    obs = NULL;

    finalspectra = NULL;
    bytesinfinalspectra = 0;
    postprocessed = false;
}

/** Copy c'stor; copy data into own arrays */
SpectralResultSet::SpectralResultSet(const SpectralResultSet& rhs)
{
    boost::unique_lock<boost::recursive_mutex> mutex(rhs.opslock);
    //boost::unique_lock<boost::mutex> mutex(rhs.opslock);

    user_owned_arrays = false;
    id = rhs.id;
    timestamp = rhs.timestamp;
    total_msec = rhs.total_msec;
    integrated_msec = rhs.integrated_msec;
    bytesperspectrum = rhs.bytesperspectrum;
    bytesinfinalspectra = rhs.bytesinfinalspectra;
    nspectra = rhs.nspectra;
    weight = rhs.weight;
    cfg = rhs.cfg;
    histogram_is_set = rhs.histogram_is_set;
    postprocessed = rhs.postprocessed;

    // Create new arrays to hold a copy of raw spectra
    allocate(rhs.nspectra, rhs.bytesperspectrum, rhs.bytesinfinalspectra);

    // Copy the histogram data
    storeHistogram((hist_t*)rhs.histogram_X, (hist_t*)rhs.histogram_Y);

    // Copy the raw and the calibrated post-processed spectra
    if (nspectra > 0) {
        for (int s = 0; s < nspectra; s++) {
            memcpy(spectra[s], rhs.spectra[s], bytesperspectrum);
        }
    }
    if (bytesinfinalspectra > 0) {
        memcpy(finalspectra, rhs.finalspectra, bytesinfinalspectra);
    }
}

/** Copy c'stor; copy data into own arrays */
SpectralResultSet::SpectralResultSet(const SpectralResultSet& rhs, bool copyRaw)
{
    boost::unique_lock<boost::recursive_mutex> mutex(rhs.opslock);
    //boost::unique_lock<boost::mutex> mutex(rhs.lock);

    user_owned_arrays = false;
    id = rhs.id;
    timestamp = rhs.timestamp;
    total_msec = rhs.total_msec;
    integrated_msec = rhs.integrated_msec;
    bytesinfinalspectra = rhs.bytesinfinalspectra;
    weight = rhs.weight;
    cfg = rhs.cfg;
    postprocessed = rhs.postprocessed;

    // Allocate data buffers
    if (copyRaw) {
        bytesperspectrum = rhs.bytesperspectrum;
        nspectra = rhs.nspectra;
    } else {
        bytesperspectrum = 0;
        nspectra = 0;
    }
    allocate(nspectra, bytesperspectrum, bytesinfinalspectra);

    // Copy the histogram data
    storeHistogram((hist_t*)rhs.histogram_X, (hist_t*)rhs.histogram_Y);
    histogram_is_set = rhs.histogram_is_set;

    // Copy the raw and the calibrated post-processed spectra
    if (copyRaw && (nspectra > 0)) {
        for (int s = 0; s < nspectra; s++) {
            memcpy(spectra[s], rhs.spectra[s], bytesperspectrum);
        }
    }
    if (bytesinfinalspectra > 0) {
        memcpy(finalspectra, rhs.finalspectra, bytesinfinalspectra);
    }
}

/** D'stor */
SpectralResultSet::~SpectralResultSet()
{
    deallocate();
    user_owned_arrays = true;
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Allocate */
void SpectralResultSet::allocate(int nspec, size_t bytesperspec, size_t bytesinfinal)
{
    boost::unique_lock<boost::recursive_mutex> mutex(opslock);
    //boost::unique_lock<boost::mutex> mutex(lock);

    user_owned_arrays = false;
    histogram_is_set = false;
    nspectra = nspec;
    bytesperspectrum = bytesperspec;
    if (nspectra > 0) {
        spectra = new float*[nspectra];
        for (int s=0; s<nspectra; s++) {
            spectra[s] = new float[1+bytesperspectrum/sizeof(float)];
            memset(spectra[s], 0, bytesperspectrum);
        }
    } else {
        spectra = NULL;
    }
    if (bytesinfinal > 0) {
        bytesinfinalspectra = bytesinfinal;
        finalspectra = new float[1+bytesinfinalspectra/sizeof(float)];
        memset(finalspectra, 0, bytesinfinalspectra);
    }
}

/** Deallocate */
void SpectralResultSet::deallocate()
{
    deallocateRaw();
    deallocatePostprocessed();
    user_owned_arrays = true;
}

/** Deallocate only the data used for raw spectra */
void SpectralResultSet::deallocateRaw()
{
    boost::unique_lock<boost::recursive_mutex> mutex(opslock);
    //boost::unique_lock<boost::mutex> mutex(lock);

    histogram_is_set = false;
    if (user_owned_arrays) {
        return;
    }
    if (nspectra > 0) {
        if (spectra != NULL) {
            for (int s=0; s<nspectra; s++) {
                if (spectra[s] != NULL) {
                    delete[] spectra[s];
                }
                spectra[s] = NULL;
            }
            delete[] spectra;
        }
    }
    bytesperspectrum = 0;
    spectra = NULL;
    nspectra = 0;
}

/** Deallocate only the flat storage used for post-processed spectra */
void SpectralResultSet::deallocatePostprocessed()
{
    boost::unique_lock<boost::recursive_mutex> mutex(opslock);
    //boost::unique_lock<boost::mutex> mutex(lock);

    if (bytesinfinalspectra > 0) {
        delete[] finalspectra;
       finalspectra = NULL;
    }
    bytesinfinalspectra = 0;
    finalspectra = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Store a copy of X, Y histograms */
void SpectralResultSet::storeHistogram(const hist_t* X_8bin, const hist_t* Y_8bin)
{
    if (X_8bin) {
        memcpy(histogram_X, X_8bin, sizeof(histogram_X));
        histogram_is_set = true;
    }
    if (Y_8bin) {
        memcpy(histogram_Y, Y_8bin, sizeof(histogram_Y));
        histogram_is_set = true;
    }
}

/** Create dummy data */
void SpectralResultSet::createDummyData(int out_nspectra, size_t out_bytesperspectrum)
{
    // Fake histogram; eval ' round(exp(-[-7,-5,-3,-1,1,3,5,7].^2/(2*(7/3)^2)) * 1000) '
    const hist_t histDummy[8] = { 11, 101, 438, 912, 912, 438, 101, 11 };
    storeHistogram(histDummy, histDummy);

    // Fake infos
    struct timeval tv;
    gettimeofday(&tv, NULL);
    timestamp.tv_sec = tv.tv_sec;
    timestamp.tv_nsec = 1e3*tv.tv_usec;

    // Fake spectral data
    weight = 1.0f;
    integrated_msec = 48;
    total_msec = 2*integrated_msec; // dwell + transition
    allocate(out_nspectra, out_bytesperspectrum, 0);
    for (int s = 0; s < nspectra; s++) {
        for (size_t n = 0; n < out_bytesperspectrum/sizeof(float); n++) {
            spectra[s][n] = (float)(n % 123);
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
