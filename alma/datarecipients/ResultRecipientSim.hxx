/**
 *
 * \class ResultRecipientSim
 *
 * Simulates the ResultRecipient interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 * Ignores the input data and reads a MIME/BDF file instead, and sends out its contents.
 *
 */

#ifndef RESULTRECIPIENTSIM_HXX
#define RESULTRECIPIENTSIM_HXX

#include "datarecipients/ResultRecipient_iface.hxx"

#include <string>

#include <stdlib.h>
#include <sys/time.h>

class MCObservation;

class ResultRecipientSim : public ResultRecipient
{

    public:

        /** C'stor with defaults */
        ResultRecipientSim(std::string outputPath);

        /** D'stor, blocks until reasonably sure all data were written */
        ~ResultRecipientSim();

    public:
        /** Open the MIME file */
        void open();

        /** Copy certain needed MIME/SIM infos from MCObservation; if used then call this before open() */
        void configureFrom(const MCObservation*);

        /** Block until all results were written */
        void sync();

        /** Safely close the MIME file */
        void close();

    public:
        void setSourceMIME(std::string inputfile);

    public:

        /** Pretend to take a spectral dataset and store it in MIME format. Actually store the contents of a fixed test file instead */
        void takeResult(const void* resultset);

    private:
        void* pimpl;
};

#endif // RESULTRECIPIENTSIM_HXX
