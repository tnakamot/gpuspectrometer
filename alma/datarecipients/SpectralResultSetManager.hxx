/**
 *
 * \class SpectralResultSetManager
 *
 * Container for a working array of incomplete SpectralResultSet objects.
 * Assists in time-based lookup of SpectralResultSet objects and switching positions.
 *
 */

#ifndef SPECTRALRESULTSETMANAGER_HXX
#define SPECTRALRESULTSETMANAGER_HXX

#include <map>
#include <set>

#include <time.h>
#include <sys/time.h>
#include <stdint.h>

#include <boost/thread/mutex.hpp>

class MCConfiguration;
class SpectralResultSet;
class DRXPInfoFrame;

class SpectralResultSetManager {

    public:
        /** C'stor to initialize as empty */
        SpectralResultSetManager();

        /** D'stor */
        ~SpectralResultSetManager();

    public:
        /**
         * Look up and return the (ongoing/incomplete) result set that mmatches the DRXP frame timestamp
         * plus offset into the DRXP frame. Unwinds the frame timestap+offset relative to observation startTimeUTC
         * and the series of position switching cycles from startTimeUTC until the given DRXP frame timestap+offset.
         *
         * I.e. finds first k for which "T = sum { pos[n]:dwell pos[n]:transition ; n=0..k } >= t_frame + t_offset"
         * The dwell and transition times are those given in the MCConfiguration::timeSpec switching plan.
         *
         * \param[in] frame DRXP frame info with contained timestamp
         * \param[inout] offset_ms time offset in milliseconds (0..47) from start of DRXP frame for which to look up SpectralResultSet
         * \param[in] cfg an MCConfiguration with the position switching time specifications to use
         * \param[out] rs the SpectralResultSet(s) into which data of the frame should accumulate into; is returned with mutex lock held
         * \param[out] swpos the switching position; swpos = k modulo #positions in MCConfiguration::timeSpec
         * \param[out] residual_dwell_ms the residual dwell time availble in current frame starting from offset_ms during which to integrate DRXP data
         * \param[out] residual_transition_ms the residual time in current frame starting from offset_ms during which to ignore DRXP data
         * \return True if lookup was successful
         */
        bool lookupSpectralResultSet(
            const DRXPInfoFrame* frame, int* offset_ms, const MCConfiguration* cfg,
            SpectralResultSet** rs, int* swpos, int* residual_dwell_ms, int* transition_ms);

        /** \return True if given 48ms frame has data that contributes to actual spectral integration in any 'dwell' */
        bool isFrameDataIntegratable(const DRXPInfoFrame* frame, const MCConfiguration* cfg) const;

        /** \return Spectral result set object that has the given ID, or NULL of not found */
        SpectralResultSet* getSpectralResultSet(const int id) const;

        /** Mark a result set as completed */
        bool setCompleted(const int id);

        /** Remove the result set from internal storage and delete the object */
        bool releaseCompleted(const int id);

        /** \return True if spectral result set with given id is already marked as completed */
        bool isCompleted(const int id) const;

        /** \return Pointer to the oldest SpectralResultSet that can/must now be processed externally, or NULL of no suitable found. */
        SpectralResultSet* getNextCompleted();

        /** \return First SpectralResultSet in the internal bookkeeping, or NULL if none. The result set is returned with mutex (SpectralResultSet::lock) locked. */
        SpectralResultSet* first();

        /** Remove and deallocate all contained SpectralResultSet objects */
        void clear();

        /** \return the time difference in msec */
        int64_t getMillisecSince(const struct timespec* arg, const struct timespec* ref) const;

        /** Increment timespec by milliseconds */
        void incTimespecMillisecs(struct timespec* arg, int64_t ms);

    private:
        mutable boost::mutex storagelock;
        std::map<int,SpectralResultSet*> storage;
        std::set<int> completedAPs;
        std::set<int> completed;
        int tail_id;
        int head_id;

};

#endif // SPECTRALRESULTSETMANAGER_HXX
