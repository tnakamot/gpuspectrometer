/**
 *
 * \class SpectralResultSetManager
 *
 * Container for a working array of incomplete SpectralResultSet objects.
 * Assists in time-based lookup of SpectralResultSet objects and switching positions.
 *
 */

#include "mcwrapping/mc_configuration.hxx"          // class MCConfiguration
#include "datarecipients/SpectralResultSet.hxx"     // class SpectralResultSet
#include "datarecipients/SpectralResultSetManager.hxx"
#include "drxp/DRXPInfoFrame.hxx"                   // class DRXPInfoFrame
#include "xsd_datatypes/xsd_datatypes.hxx"          // all XSD types
#include "helper/logger.h"                          // L_(<loglevel>)
#include "global_defs.hxx"                          // DRXP_DT_MSEC

#include <stdint.h>
#include <iostream>
#include <iomanip>

#ifndef MIN
    #define MIN(a,b) (((a)<(b))?(a):(b))
    #define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#define MAX_UNWRITTEN_QUEUE 64

#define lockscope

/** C'stor to initialize as empty */
SpectralResultSetManager::SpectralResultSetManager()
{
    storage.clear();
    completedAPs.clear();
    tail_id = -1;
    head_id = -1;
}

/** D'stor */
SpectralResultSetManager::~SpectralResultSetManager()
{
    boost::mutex::scoped_lock lock(storagelock);
    if (storage.size() > 0) {
        L_(linfo) << "SpectralResultSetManager d'stor had " << storage.size() << " remaining result(s) before closing";
        for (std::map<int,SpectralResultSet*>::iterator it=storage.begin(); it != storage.end(); it++) {
            it->second->lock.lock();
            it->second->lock.unlock();
            delete it->second;
            it->second = NULL;
        }
        storage.clear();
        completedAPs.clear();
    }
}

/** Remove and deallocate all contained SpectralResultSet objects */
void SpectralResultSetManager::clear()
{
    boost::mutex::scoped_lock lock(storagelock);
    for (std::map<int,SpectralResultSet*>::iterator it=storage.begin(); it != storage.end(); it++) {
        it->second->lock.lock();
        it->second->lock.unlock();
        delete it->second;
        it->second = NULL;
    }
    storage.clear();
    completedAPs.clear();
    tail_id = -1;
    head_id = -1;
}

int64_t SpectralResultSetManager::getMillisecSince(const struct timespec* arg, const struct timespec* ref) const
{
    int64_t dt_msec = 1000*((int64_t)arg->tv_sec - ref->tv_sec);
    dt_msec += ((int64_t)arg->tv_nsec - ref->tv_nsec + 1000000/2)/1000000; // rounded to closest millisec
    return dt_msec;
}

void SpectralResultSetManager::incTimespecMillisecs(struct timespec* arg, int64_t ms)
{
    int64_t s = ms / 1000;
    arg->tv_sec += s;
    arg->tv_nsec += (ms - 1000*s) * 1000000;
    while (arg->tv_nsec >= 1000000000) {
        arg->tv_nsec -= 1000000000;
        arg->tv_sec++;
    }
    while (arg->tv_nsec < 0) {
        arg->tv_nsec += 1000000000;
        arg->tv_sec--;
    }
}

/** \return First SpectralResultSet in the internal bookkeeping, or NULL if none. The result set is returned with mutex (SpectralResultSet::lock) locked. */
SpectralResultSet* SpectralResultSetManager::first()
{
    boost::mutex::scoped_lock lock(storagelock);
    if (storage.size() <= 0) {
        return NULL;
    }
    SpectralResultSet* rs = storage.begin()->second;
    rs->lock.lock();
    return rs;
}

/**
 * Look up and return the (ongoing/incomplete) result set that mmatches the DRXP frame timestamp
 * plus offset into the DRXP frame. Unwinds the frame timestap+offset relative to observation startTimeUTC
 * and the series of position switching cycles from startTimeUTC until the given DRXP frame timestap+offset.
 *
 * I.e. finds first k for which "T = sum { pos[n]:dwell pos[n]:transition ; n=0..k } >= t_frame + t_offset"
 * The dwell and transition times are those given in the MCConfiguration::timeSpec switching plan.
 *
 * \param[in] frame DRXP frame info with contained timestamp
 * \param[inout] offset_ms time offset in milliseconds (0..47) from start of DRXP frame for which to look up SpectralResultSet
 * \param[in] cfg an MCConfiguration with the position switching time specifications to use
 * \param[out] rs the SpectralResultSet(s) into which data of the frame should accumulate into; is returned with mutex lock held
 * \param[out] swpos the switching position; swpos = k modulo #positions in MCConfiguration::timeSpec
 * \param[out] residual_dwell_ms the residual dwell time availble in current frame starting from offset_ms during which to integrate DRXP data
 * \param[out] residual_transition_ms the residual time in current frame starting from offset_ms during which to ignore DRXP data
 *
 * \return True if lookup was successful
 */
bool SpectralResultSetManager::lookupSpectralResultSet(
    const DRXPInfoFrame* frame, int* offset_ms, const MCConfiguration* cfg,
    SpectralResultSet** pprs, int* swpos, int* residual_dwell_ms, int* residual_transition_ms)
{

    // Defaults
    *pprs = NULL;
    *swpos = -1;
    *residual_dwell_ms = 0;
    *residual_transition_ms = 48;

    assert(cfg->procConfig.Tswitchingcycle_msec == cfg->singleConfiguration.timeSpec.integrationDuration*1000);

    // How far DRXP data are from start of observation
    const int64_t frame_ms = getMillisecSince(&frame->swRxTime_UTC, &cfg->procConfig.requestedStartTime);
    if (frame_ms <= -48) {
        *offset_ms = 48; // skip frame
        return false;
    }
    if ((frame_ms + *offset_ms) <= 0) {
        // reading a frame just a bit before the requestedStartTime position in frame:
        // jump the offset ahead match to the 'requestedStartTime'
        *offset_ms = -frame_ms;
    }

    // How far the data-matching cycle is from start of observation
    int64_t data_ms = frame_ms + *offset_ms;
    int cycle_nr = data_ms / int64_t(cfg->procConfig.Tswitchingcycle_msec);
    int cycle_ms = data_ms % int64_t(cfg->procConfig.Tswitchingcycle_msec);
    int frame_avail_ms = DRXP_DT_MSEC - *offset_ms;
    //std::cout << "got frame_ms=" << frame_ms << "ms : at curr.cycle nr=" << cycle_nr << ", started at " << cycle_ms << "ms, single_cycle_ms=" << cfg->procConfig.Tswitchingcycle_msec << "ms\n";

    // Check whether 'requestedStopTime' is crossed by this switching cycle
    if (cfg->procConfig.requestedStopTime.tv_sec > 0) {
        int64_t obs_dur_ms = getMillisecSince(&cfg->procConfig.requestedStopTime, &cfg->procConfig.requestedStartTime);
        int64_t sw_end_ms = data_ms - int64_t(cycle_ms) + int64_t(cfg->procConfig.Tswitchingcycle_msec);
        if (sw_end_ms > obs_dur_ms) {
            //std::cout << "dbg: obs_dur_ms=" << obs_dur_ms << ", sw_end_ms=" << sw_end_ms << " at cycle " << cycle_nr << " and data_ms=" << data_ms << "\n";
            return false;
        }
    }

    // Determine which switching position of the cycle we're at
    int tmp_ms = 0;
    const XSD::timeSpec_t& tspec = cfg->singleConfiguration.timeSpec;
    for (size_t p=0; p<tspec.switching.size(); p++) {
        tmp_ms += tspec.switching[p].dwell_sec*1e3;
        if (cycle_ms < tmp_ms) {
            *swpos = p;
            *residual_dwell_ms = MIN(frame_avail_ms, tmp_ms - cycle_ms);
            *residual_transition_ms = tspec.switching[p].transition_sec*1e3;
            break;
        }
        tmp_ms += tspec.switching[p].transition_sec*1e3;
        if (cycle_ms < tmp_ms) {
            *swpos = p;
            *residual_dwell_ms = 0;
            *residual_transition_ms = MIN(frame_avail_ms, tmp_ms - cycle_ms);
            break;
        }
    }

    // Invalid switching position?
    if (*swpos == -1) {
        *offset_ms = 48; // skip frame
        L_(lwarning) << "lookupSpectralResultSet() failed due to no switching position found for cycle " << cycle_nr << " offset " << cycle_ms << "ms";
        return false;
    }

    // Debug info
    if (0) {
        int64_t cycle_start_ms = data_ms - cycle_ms;
        std::cout << "frame_start=" << frame_ms << "ms, read_start=" << data_ms << "ms, maps to sw cycle "
                  << cycle_nr << " at sw pos " << *swpos << " that start(ed) at " << cycle_start_ms << "ms, " << cycle_ms << "ms into cycle and still "
                  << "ddwell=" << *residual_dwell_ms << "ms dtransit=" << *residual_transition_ms << "ms remain in this 48ms frame"
                  << "\n";
    }

    // Look up SpectralResultSet or create a new one if necessary
    lockscope {
        boost::mutex::scoped_lock lock(storagelock);

        // Avoid already completed SpectralResultSets
        if (completedAPs.find(cycle_nr) != completedAPs.end()) {
            L_(ldebug) << "SpectralResultSetManager lookupSpectralResultSet() of DRXP frame associated with an already completed AP, skipping";
            return false;
        }

        // Look for existing SpectralResultSet
        std::map<int,SpectralResultSet*>::iterator obj = storage.find(cycle_nr);
        if (obj != storage.end()) {
            // Grab the existing result set
            obj->second->lock.lock();
            *pprs = obj->second;
        } else {
            // Create a new result set
            SpectralResultSet* rs = new SpectralResultSet();
            rs->id = cycle_nr;
            rs->timestamp = cfg->procConfig.requestedStartTime;
            rs->allocate(cfg->procConfig.nswpos, sizeof(float)*(cfg->procConfig.nchan + 0)*cfg->procConfig.nstokes, 0);
            rs->obs = NULL;
            rs->cfg = cfg;
            incTimespecMillisecs(&rs->timestamp, int64_t(cycle_nr)*int64_t(cfg->procConfig.Tswitchingcycle_msec));
            rs->lock.lock();
            // Return it
            *pprs = rs;

            // Push new result set into bookeeping storage
            L_(ldebug) << "SpectralResultSetManager pre-allocated SpectralResultSet of AP#" << cycle_nr;
            storage[cycle_nr] = rs;
        }

        // Keep track of most recent ID
        if ((*pprs)->id > head_id) {
            head_id = (*pprs)->id;
        }
    }

    return true;
}

/** \return True if given 48ms frame has data that contributes to actual spectral integration in any 'dwell' */
bool SpectralResultSetManager::isFrameDataIntegratable(const DRXPInfoFrame* frame, const MCConfiguration* cfg) const
{
    const size_t nsw = cfg->singleConfiguration.timeSpec.switching.size();

    // Simplest case: no switching
    if ((nsw == 1) && (cfg->singleConfiguration.timeSpec.switching[0].transition_sec == 0)) {
        return true;
    }

    // How far the data-matching cycle is from start of observation
    const int64_t frame_ms = getMillisecSince(&frame->swRxTime_UTC, &cfg->procConfig.requestedStartTime);
    const int ms_into_cycle = frame_ms % cfg->procConfig.Tswitchingcycle_msec;

    // Determine which switching position of the cycle we're at: 'dwell' or 'transition'
    int curr = 0;
    for (size_t p=0; p<nsw; p++) {
        curr += cfg->singleConfiguration.timeSpec.switching[p].dwell_sec*1e3;
        if (ms_into_cycle < curr) {
            return true;
        }
        curr += cfg->singleConfiguration.timeSpec.switching[p].transition_sec*1e3;
        if (ms_into_cycle < curr) {
            return false;
        }
    }
    return false;
}

/** \return Spectral result set object that has the given ID, or NULL if not found */
SpectralResultSet* SpectralResultSetManager::getSpectralResultSet(const int id) const
{
    boost::mutex::scoped_lock lock(storagelock);
    std::map<int,SpectralResultSet*>::const_iterator obj = storage.find(id);
    if (obj == storage.end()) {
        return NULL;
    }
    return obj->second;
}

/** Mark a result set as completed and remove it from internal bookkeeping, and delete the object */
bool SpectralResultSetManager::setCompleted(const int id)
{
    boost::mutex::scoped_lock lock(storagelock);
    std::map<int,SpectralResultSet*>::iterator obj = storage.find(id);
    if (obj == storage.end()) {
       L_(ldebug) << "SpectralResultSetManager error: attempt to mark as completed the non-existing AP# " << id;
       return false;
    }
    if (completedAPs.find(id) == completedAPs.end()) {
        completedAPs.insert(id);
        L_(ldebug) << "SpectralResultSetManager marked AP# " << id << " as complete";
        return true;
    }
    return false;
}

/** Remove the result set from internal storage and delete the object */
bool SpectralResultSetManager::releaseCompleted(const int id)
{
    boost::mutex::scoped_lock lock(storagelock);
    if (completedAPs.find(id) == completedAPs.end()) {
       L_(ldebug) << "SpectralResultSetManager error: attempt to remove incomplete AP# " << id;
       return false;
    }
    std::map<int,SpectralResultSet*>::iterator obj = storage.find(id);
    if (obj == storage.end()) {
       L_(ldebug) << "SpectralResultSetManager error: attempt to remove the non-existing AP# " << id;
       return false;
    }
    //std::cout << "releaseCompleted: tail_id=" << tail_id << "(old)/" << id << "(new) head_id=" << head_id << ", released_id=" << id << "\n";
    if (id > tail_id) {
        tail_id = id;
    }
    obj->second->lock.lock();
    storage.erase(obj);
    obj->second->lock.unlock();
    //std::cout << "Manager: release, delete AP# " << id << " (" << obj->second-id << ")" << std::endl;
    delete obj->second;
    L_(ldebug) << "SpectralResultSetManager removed AP# " << id;
    return true;
}

/** \return True if spectral result set with given id is already marked as completed */
bool SpectralResultSetManager::isCompleted(const int id) const
{
    boost::mutex::scoped_lock lock(storagelock);
    return (completedAPs.find(id) != completedAPs.end());
}

/**
 * Look for the next (oldest-first) completed result set that can e.g. be written out to a MIME file.
 * If a suitable result set is found, its mutex (SpectralResultSet::lock) is locked, and the result set is returned.
 * \return Pointer to the oldest SpectralResultSet that can/must now be processed externally, or NULL of no suitable found.
 */
SpectralResultSet* SpectralResultSetManager::getNextCompleted()
{
    boost::mutex::scoped_lock lock(storagelock);
    if (head_id < 0) {
        return NULL;
    }

    // If the tail id(s) are too late force them as completed
    if ((tail_id + MAX_UNWRITTEN_QUEUE) < head_id) {
        int id = tail_id + 1;

        // Locate first still existing result set
        std::map<int,SpectralResultSet*>::iterator obj;
        while ((id <= head_id) && ((obj = storage.find(id)) == storage.end())) {
            id++;
        }

        // If existing one found, force-mark it as completed, and return it
        if (obj != storage.end()) {
            // lock spectralresultset to prevent parallel edits
            obj->second->lock.lock();
            completedAPs.insert(id);
            L_(ldebug) << "SpectralResultSetManager returning partial AP#" << id << " that reached deadline";
            return obj->second;
        }
        return NULL;
    }

    // Return tail if it has completed; skip all non-existent ids
    int id = tail_id + 1;
    std::map<int,SpectralResultSet*>::iterator obj;
    while ((id <= head_id) && ((obj = storage.find(id)) == storage.end())) {
        id++;
    }
    if ((obj != storage.end()) && (completedAPs.find(id) != completedAPs.end())) {
        // lock spectralresultset to prevent parallel edits
        obj->second->lock.lock();
        L_(ldebug) << "SpectralResultSetManager returning ready AP#" << obj->second->id;
        return obj->second;
    }

    L_(ldebug) << "SpectralResultSetManager no new ready APs; storage has " << storage.size() << " APs, old completed were " << completedAPs.size() << " APs";

    return NULL;
}
