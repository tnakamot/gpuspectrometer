/**
 *
 * \class ResultRecipientPlain
 *
 * Writes results into a plain file. No headers.
 *
 */

#ifndef RESULTRECIPIENTPLAIN_HXX
#define RESULTRECIPIENTPLAIN_HXX

#include "datarecipients/ResultRecipient_iface.hxx"

#include <string>

#include <stdlib.h>
#include <sys/time.h>

class MCObservation;

class ResultRecipientPlain : public ResultRecipient
{

    public:
        /** C'stor with defaults */
        ResultRecipientPlain(std::string outputPath);

        /** D'stor, blocks until reasonably sure all data were written */
        ~ResultRecipientPlain();

    public:
        /** Open the BDF file */
        void open();

        /** Copy certain needed MIME/SIM infos from MCObservation; if used then call this before open() */
        void configureFrom(const MCObservation*);

        /** Block until all results were written */
        void sync();

        /** Safely close the BDF file */
        void close();

    public:

        /** Pretend to take a spectral dataset and store it in BDF format. Actually store the contents of a fixed test file instead */
        void takeResult(const void* resultset);

    private:
        void* pimpl;
};

#endif // RESULTRECIPIENTPLAIN_HXX
