#ifndef STATISTICS_H
#define STATISTICS_H

inline void computeStats(const float* data, const int N, float& mean, float& std, float& vmin, float& vmax)
{
    mean = 0.0f;
    std = 0.0f;
    vmin = data[0];
    vmax = data[0];
    for (int n=0; n<N; n++) {
        mean += data[n]/(float)N;
        if (data[n] < vmin) { vmin = data[n]; }
        if (data[n] > vmax) { vmax = data[n]; }
    }
    for (int n=0; n<N; n++) {
        std += (data[n]-mean)*(data[n]-mean)/(float)N;
    }
    std = sqrtf(std);
}

#endif // STATISTICS_H
