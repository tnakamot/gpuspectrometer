/////////////////////////////////////////////////////////////////////////////////////
//
// Assisting funcs to prepare CUDA FFT for data input to 4-baseband cross-correlation
// Must have a number of channels that is a multiple of 32. Must have 2-pol data.
//
// The FFT input stage performs pre-FFT time domain fringe stopping via phase
// rotation of the real-valued input samples.
//
// The 4-antenna X,Y signals are phase rotated prior to FFT, utilizing
// phase polynomial coefficients for each antenna signal and polarization.
//
// Coefficients 'c' as in phase[n]/pi = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3
//
// Member functions:
// transform()  Carries out fringe-stopping and c2c FFT of 8 signals.
// xstep()      Cross-multiplies FFT outputs, forms autos and visbilities incl. cross-pol
// copyVisibilities()  Copies visibilities from xstep() format into separate user arrays
//
// (C) 2019 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////
#ifndef ARITHMETIC_FXCORR_CUH
#define ARITHMETIC_FXCORR_CUH

#include <cufft.h>
#include <cstddef>

class FXCorr4 {

    public:

        enum CallbackType { NONE=0, SINCOS_LINEAR, SINCOS_CUBIC };

        /// Internal structure of arguments sent to custom CUFFT Callback
        typedef struct cu_fxcorr_FFT_cb_params_tt {
            // Continuous rotation related
            size_t sampleOffset;           // TODO: integer sample offset into input data
            float phasePoly[4];            // 0: phase of sample#0, 1: rate in phase/sample, 2: accel in phase/sample^2, 3: phase/sample^3; all in units suitable for sincospif()
        } cu_fxcorr_FFT_cb_params_t;

    public:

        /** Construct FX FFT object for fringe stopped 4-antenna 2-pol FFT */
        FXCorr4(size_t Nsamp_per_antenna, size_t Nchan)
         : Nant_(4), Nsignals_(4*2), Lbatch_(Nsamp_per_antenna/Nchan),
           Nsamp_per_antenna_(Nsamp_per_antenna), Nchan_(Nchan),
           Npolprod_(4)
        {
            init();
        }

        ~FXCorr4()
        {
            deinit();
        }

    public:

        /** Assign a specific CUDA Stream to all GPU tasks executed in this class (optional) */
        void setStream(cudaStream_t s);

        /**
         * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation
         * of real-valued input samples prior to complex-to-complex FFT.
         * Phase polynomial coefficients should provide phase 'pi*phase[n]' for
         * sample n=0..N-1, such that phase[n] = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3.
         *
         * \param d_X Device-side X-polarization signals of all 4 antennas, real-valued float32
         * \param phaseXCoeffs Phase polynomial coefficients X-pol for 4 signals
         * \param d_Y Device-side Y-polarization signals of all 4 antennas, real-valued float32
         * \param phaseYCoeffs Phase polynomial coefficients Y-pol for 4 signals
         */
        void transform(int phasePolyOrder,
            const float* d_X[4], float* phaseXCoeffs[4],
            const float* d_Y[4], float* phaseYCoeffs[4]);

        /**
         * Cross-multiply the results of transform() to produce visibility data
         * with cross-polarization products and autocorrelations. The output
         * visibility data layout is split-Re/im arrays of Nchan length for
         * all 10 products (6 baselines, 4 autos) concatenated.
         */
        void xstep();

        /**
         * Copy visibility data from FXCorr-specific representation into
         * user arrays. If the output of xstep() is to be used as input, it
         * has to be copied to host-side memory first (unless Unified mem).
         * The four output polarization pairs are 0:XX, 1:XY, 2:YX, 3:YY.
         *
         * \param h_gpuViz Visibilities in GPU format/layout, host side
         * \param viz      Output visibility arrays sized [6 baseline][4 polarization pair][Nchan]
         * \param autos    Output autocorrelation arrays sized [4 autos][4 polarization pair][Nchan]
         */
        void copyVisibilitiesFrom(const cufftComplex* h_gpuViz, cuComplex* viz[6][4], cuComplex* autos[4][4]) const;

        /**
         * Copy visibility data from FXCorr-specific representation into
         * user arrays. Will use data of most recent xstep() call.
         * The four output polarization pairs are 0:XX, 1:XY, 2:YX, 3:YY.
         *
         * \param viz      Output visibility arrays sized [6 baseline][4 polarization pair][Nchan]
         * \param autos    Output autocorrelation arrays sized [4 autos][4 polarization pair][Nchan]
         */
        void copyVisibilities(cuComplex* viz[6][4], cuComplex* autos[4][4]) const
        {
            return copyVisibilitiesFrom(h_viz_,viz, autos);
        }

        /**
         * \return Device pointer to the internal flat array used to store c2c FFT output of transform()
         */
        cufftComplex* getFFFTOutPtr() { return d_fftsOut_; }

        /**
         * \return Device pointer to internal flat array used to store visibilities and autos of xstep()
         */
        cufftComplex* getVisibilityOutPtr() { return d_viz_; }

    private:

        void init();

        void deinit();

        /**
         * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
         * Samples are converted from real to complex (with phase rotation) at the time of FFT,
         * utilizing continous linear poly with sincos() for every single sample.
         */
        void transformAntennaSinCos(int antenna, int polyOrder,
            const float* __restrict__ d_X, const float* phasePolyX,
            const float* __restrict__ d_Y, const float* phasePolyY,
            cufftComplex* __restrict__ d_fftsOut);

        /** Set CUFFT Callback to the specified type of internal callback function (none, various sin/cos phase poly orders) */
        void registerCallback(CallbackType ct);

    private:

         const size_t Nant_;
         const size_t Nsignals_;
         const size_t Nsamp_per_antenna_;
         const size_t Nchan_;
         const size_t Lbatch_;

         size_t MaxPhysThreads_;
         cudaStream_t stream_;

         cufftHandle c2cPlan;
         cufftComplex* d_fftsOut_;

         const size_t Npolprod_;
         size_t Nvisibilities_;
         size_t Nbyteviz_;
         cufftComplex* d_viz_;
         cufftComplex* h_viz_;

         cu_fxcorr_FFT_cb_params_t* rotatorParams_;

         CallbackType currentRegisteredCallback;

};


#endif // ARITHMETIC_FXCORR_CUH

