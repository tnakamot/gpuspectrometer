////////////////////////////////////////////////////////////////////////////////////////////////
//
// Assisting funcs to prepare CUDA FFT for data input to 4-baseband cross-correlation
// Must have a number of channels that is a multiple of 32. Must have 2-pol data.
//
// The FFT input stage performs pre-FFT time domain fringe stopping via phase
// rotation of the real-valued input samples. Two methods for phase rotation
// are supported:
// 1) supply 4-antenna X,Y signals and short vectors containing phasor values to
//    apply to stretches of >100 samples of each of signal (constant-phase stretches)
// 2) supply 4-antenna X,Y signals and phase polynomial coefficients for each antenna
//    signal, coefficients 'c' as in phase[n]/pi = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3
//
// The FFT output of 4-ant 2-pol spectra are interleaved in the way
// that arithmetic_fxcorr_kernels expect.
//
// (C) 2019 Jan Wagner
//
////////////////////////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <cuda.h>
#include <cufft.h>
#include <math.h>

#include "cuda_utils.cu"
#include "statistics.h"

#include "arithmetic_fxcorr.cuh" // wrapper class FXCorr4

////////////////////////////////////////////////////////////////////////////////////////////////
// FRINGE ROTATING CUFFT LoadC CALLBACKS
////////////////////////////////////////////////////////////////////////////////////////////////

/// Callback routine for CUFFT, no-op load of real to comples, without fringe rotation
static __device__ cufftComplex CB_loadReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    // No fringe rotation
    cufftReal sample = ((cufftReal*)dataIn)[index];
    return (cufftComplex) { sample, 0.0f };
}


/// Callback routine for CUFFT to produce fringe-rotated complex values from samples via continousphase rotation
template<int order>
static inline __device__ cufftComplex CB_loadRotateSinCosReal_T(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    // Evaluate phase polynomial
    FXCorr4::cu_fxcorr_FFT_cb_params_t* params = (FXCorr4::cu_fxcorr_FFT_cb_params_t*)cb_info;
    float theta = params->phasePoly[order];
    for (int n=order; n>=1; n--) {
        theta = params->phasePoly[n-1] + theta*(float)index;
    }

    // Phase to complex phasor
    // NOTE: sincos() in CUDA library
    //       could also use this one: https://devtalk.nvidia.com/default/topic/957538/sincospif-implementation-with-improved-performance-and-accuracy/
    cufftComplex phasor;
    sincospif(theta, &phasor.x, &phasor.y);

    // Get sample
    unsigned int phasewrapsamples = __float2uint_rd(theta) / 2;
    cufftReal sample = ((cufftReal*)dataIn)[index + phasewrapsamples];

    // Apply pre-FFT fringe rotation
    return (cufftComplex) { sample*phasor.x, sample*phasor.y };
}

/// Linear phase poly CUFFT callback
static __device__ cufftComplex CB_loadRotateSinCosLinearReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    return CB_loadRotateSinCosReal_T<1>(dataIn, index, cb_info, shmem);
}

/// Cubic phase poly CUFFT callback
static __device__ cufftComplex CB_loadRotateSinCosCubicReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    return CB_loadRotateSinCosReal_T<3>(dataIn, index, cb_info, shmem);
}

/// Device pointer to the callback routine. Needed by cufftXtSetCallback(), cannot be __managed__ (CUFFT limitation).
__device__ cufftCallbackLoadC d_CB_loadReal_ptr = CB_loadReal;
__device__ cufftCallbackLoadC d_CB_loadRotateSinCosLinearReal_ptr = CB_loadRotateSinCosLinearReal;
__device__ cufftCallbackLoadC d_CB_loadRotateSinCosCubicReal_ptr = CB_loadRotateSinCosCubicReal;


////////////////////////////////////////////////////////////////////////////////////////////////
// X-STEP FOR 4-ANTENNA 2-POL WITH AUTOCORRELATIONS, fastest of arithmetic_fxcorr_kernels.cu
////////////////////////////////////////////////////////////////////////////////////////////////

static inline __host__ __device__ float4 make_float4(float2 a, float2 b)
{
    return (float4) { a.x, a.y, b.x, b.y };
}

__device__ __host__ inline void operator+=(float2& lhs, const float2 rhs)
{
    lhs.x += rhs.x;
    lhs.y += rhs.y;
}

/// Complex conjugate cross-product
__device__ __host__ inline float2 complexConjMul(const float2 a, const float2 b)
{
    return (float2){ (a.x*b.x + a.y*b.y), (a.y*b.x - a.x*b.y) };
}

/// Single polarization 2-antenna complex conjugate cross product, one polarization product.
__device__ __host__ inline float2 xstep(const float2 ant1X, const float2 ant2X)
{
    return complexConjMul(ant1X,ant2X);
}

/// Full Stokes 2-antenna complex conjugate cross products, with in-place accumulation
__device__ __host__ inline void xstepFullStokes_I(const float4 ant1XY, const float4 ant2XY, float2* __restrict__ vizaccu)
{
    float2 ant1X = (float2){ant1XY.x,ant1XY.y};
    float2 ant1Y = (float2){ant1XY.z,ant1XY.w};
    float2 ant2X = (float2){ant2XY.x,ant2XY.y};
    float2 ant2Y = (float2){ant2XY.z,ant2XY.w};
    vizaccu[0] += xstep(ant1X,ant2X);
    vizaccu[1] += xstep(ant1X,ant2Y);
    vizaccu[2] += xstep(ant1Y,ant2X);
    vizaccu[3] += xstep(ant1Y,ant2Y);
}

/// Averaging across blocks. Channels-first layout with Re/Im separation e.g. [{ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,...,bl40} ] x {re, im}
template<const size_t NVIZ>
__device__ inline void cu_fxcorr_blockReduce(float2* __restrict__ out, const float2* __restrict__ localviz, const size_t localviz_ch, const size_t Nchan)
{
    for (size_t b=0; b<NVIZ; b++) {
         atomicAdd(&(((float*)out)[localviz_ch + b*Nchan]), localviz[b].x);
    }
    for (size_t b=0; b<NVIZ; b++) {
        atomicAdd(&(((float*)out)[localviz_ch + b*Nchan + NVIZ*Nchan]), localviz[b].y);
    }
}

/**
 * Cross-multiply and accumulate 4-antenna dual-polariation complex (post-FFT) input data, with autocorrelation output.
 * The input data are not interlaved, but rather are given as 8 separate vectors (in[0]=ant1X, in[1]=ant1Y, in[2]=ant2X, ...)
 */
__global__ void cu_fxcorr_4ant_2pol_withAuto(
    const float2* __restrict__ in1X, const float2* __restrict__ in1Y,
    const float2* __restrict__ in2X, const float2* __restrict__ in2Y,
    const float2* __restrict__ in3X, const float2* __restrict__ in3Y,
    const float2* __restrict__ in4X, const float2* __restrict__ in4Y,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im X,Y data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;           // 4 antennas in dual pol
    const size_t NPOLPROD = 4;       // {XX,YX,XY,YY}
    const size_t NVIZ = (6 + NANT)*NPOLPROD;  // = (NANT*(NANT-1)/2 baselines + Nant auto) * pol products = 40

    float2 vizaccu[NVIZ] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float4 dualpolsig[NANT];
        dualpolsig[0] = make_float4(in1X[idx], in1Y[idx]);
        dualpolsig[1] = make_float4(in2X[idx], in2Y[idx]);
        dualpolsig[2] = make_float4(in3X[idx], in3Y[idx]);
        dualpolsig[3] = make_float4(in4X[idx], in4Y[idx]);

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
            for (int ant2=ant1; ant2<NANT; ant2++) {
                xstepFullStokes_I(dualpolsig[ant1], dualpolsig[ant2], &vizaccu[bl]);
                bl += NPOLPROD;
            }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NVIZ>(out, vizaccu, my_ch, Nchan);
}


////////////////////////////////////////////////////////////////////////////////////////////////
// INIT and DEINIT
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Prepare for 4-antenna 2-pol FFTs (8 FFT) with fringe rotation.
 */
void FXCorr4::init()
{
    // Defaults
    int currDevice;
    cudaDeviceProp cudaDevProp;
    CUDA_CALL( cudaGetDevice(&currDevice) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, currDevice) );
    MaxPhysThreads_ = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    stream_ = 0;

    Nvisibilities_ = (Nant_*(Nant_-1))/2 + Nant_; // 6 baselines + 4 antennas
    Nvisibilities_ *= Npolprod_;                  // all visibility data in full Stokes (4 polzn prod.)
    Nbyteviz_ = Nvisibilities_*Nchan_*sizeof(float2);

    // CUFFT Plan c2c FFT
    int dimn[1] = {(int)Nchan_};   // DFT size
    int inembed[1] = {0};          // ignored for 1D xform
    int onembed[1] = {0};          // ignored for 1D xform
    int istride = 1;               // step between successive input elems
    int ostride = 1;               // step between successive output elems; do not interleave
    int idist = Nchan_;
    int odist = Nchan_;
    int batch = Lbatch_;

    CUFFT_CALL( cufftPlanMany(&c2cPlan,
        1, dimn,
        inembed, istride, idist,
        onembed, ostride, odist,
        CUFFT_C2C,
        batch)
    );
    CUFFT_CALL( cufftSetStream(c2cPlan, stream_) );

    // Storage for CUFFT callback aux parameters
    CUDA_CALL( cudaMallocManaged((void**)&rotatorParams_, sizeof(rotatorParams_)) );
    rotatorParams_->sampleOffset = 0;

    // Default callback handler
    currentRegisteredCallback = FXCorr4::NONE;

    // Storage for CUFFT results of 8 signals
    CUDA_CALL( cudaMalloc((void **)&d_fftsOut_, Nsamp_per_antenna_*sizeof(cufftComplex)*Nsignals_) );

    // Storage for 6 baseline 4 auto XX,XY,YX,YY results
    CUDA_CALL( cudaMalloc((void **)&d_viz_, Nbyteviz_) );
    CUDA_CALL( cudaMallocHost((void **)&h_viz_, Nbyteviz_) );

}

/** Set CUFFT Callback to the specified type of internal callback function (none, various sin/cos phase poly orders) */
void FXCorr4::registerCallback(CallbackType ct)
{
    if (currentRegisteredCallback != ct) {

        // Unregister callback first, required for changing it later!
        CUFFT_CALL( cufftXtClearCallback(c2cPlan, CUFFT_CB_LD_COMPLEX) );

        // Get device address of callback function and register it
        cufftCallbackLoadC h_cb_loadPtr;
        switch (ct) {
            case SINCOS_LINEAR:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadRotateSinCosLinearReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
            case SINCOS_CUBIC:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadRotateSinCosCubicReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
            default:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
        }

        // Register
        CUFFT_CALL( cufftXtSetCallback(c2cPlan, (void **)&h_cb_loadPtr, CUFFT_CB_LD_COMPLEX, (void**)&rotatorParams_) );
        currentRegisteredCallback = ct;
    }
}

/** Assign a specific CUDA Stream to all GPU tasks executed in this class (optional) */
void FXCorr4::setStream(cudaStream_t s)
{
    stream_ = s;
    CUFFT_CALL( cufftSetStream(c2cPlan, stream_) );
}

/**
 * Clean up all allocations used by FX FFT setup.
 */
void FXCorr4::deinit()
{
    CUDA_CALL( cudaFree(rotatorParams_) );
    CUDA_CALL( cudaFree(d_fftsOut_) );
    CUDA_CALL( cudaFree(d_viz_) );
    CUDA_CALL( cudaFreeHost(h_viz_) );
    CUFFT_CALL( cufftDestroy(c2cPlan) );
}


////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
 * Samples are converted from real to complex (with phase rotation) at the time of FFT,
 * utilizing continous linear poly with sincos() for every single sample.
 */
void FXCorr4::transformAntennaSinCos(int antenna, int polyOrder,
    const float* __restrict__ d_X, const float* phasePolyX,
    const float* __restrict__ d_Y, const float* phasePolyY,
    cufftComplex* __restrict__ d_fftsOut)
{
    assert(polyOrder==1 || polyOrder==3);
    size_t odist = Nchan_*Lbatch_;

    memcpy(rotatorParams_->phasePoly, phasePolyX, (polyOrder+1)*sizeof(float));
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_X, d_fftsOut + odist*(2*antenna+0), CUFFT_FORWARD) );

    memcpy(rotatorParams_->phasePoly, phasePolyY, (polyOrder+1)*sizeof(float));
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_Y, d_fftsOut + odist*(2*antenna+1), CUFFT_FORWARD) );
}


/**
 * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation
 * of real-valued input samples prior to complex-to-complex FFT.
 * Phase polynomial coefficients should provide phase 'pi*phase[n]' for
 * sample n=0..N-1, such that phase[n] = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3.
 *
 * \param d_X Device-side X-polarization signals of all 4 antennas, real-valued float32
 * \param phaseXCoeffs Phase polynomial coefficients X-pol for 4 signals
 * \param d_Y Device-side Y-polarization signals of all 4 antennas, real-valued float32
 * \param phaseYCoeffs Phase polynomial coefficients Y-pol for 4 signals
 */
void FXCorr4::transform(int phasePolyOrder,
    const float* d_X[4], float* phasePolysX[4],
    const float* d_Y[4], float* phasePolysY[4]
)
{
    assert(phasePolyOrder==1 || phasePolyOrder==3);
    if (phasePolyOrder==1) {
        registerCallback(FXCorr4::SINCOS_LINEAR);
    } else {
        registerCallback(FXCorr4::SINCOS_CUBIC);
    }

    for (size_t ant=0; ant<Nant_; ant++) {
        transformAntennaSinCos(ant, phasePolyOrder,
            d_X[ant], phasePolysX[ant],
            d_Y[ant], phasePolysY[ant],
            d_fftsOut_);
    }
}


/**
 * Cross-multiply the results of transform() to produce visibility data
 * with cross-polarization products and autocorrelations. The output
 * visibility data layout is split-Re/im arrays of Nchan length for
 * all 10 products (6 baselines, 4 autos) concatenated.
 */
void FXCorr4::xstep()
{
    // Clear before accumulate
    CUDA_CALL( cudaMemsetAsync(d_viz_, 0x00, Nbyteviz_, stream_) );

    // Per antenna/pol pointers according to Output format of FFT
    const float2* d_spectraldata[Nsignals_];
    for (size_t n=0; n<Nsignals_; n++) {
        d_spectraldata[n] = ((float2*)d_fftsOut_) + n * (Lbatch_*Nchan_);
    }

    // Cross-multiply accumulate
    size_t threadsPerBlock = 32;
    size_t numBlocks = MaxPhysThreads_ / threadsPerBlock;
    do {
        size_t stride = numBlocks*threadsPerBlock;
        if ((stride % Nchan_) == 0) { break; }
        threadsPerBlock--;
        numBlocks = MaxPhysThreads_ / threadsPerBlock;
    } while(threadsPerBlock > 1);

    cu_fxcorr_4ant_2pol_withAuto <<<numBlocks, threadsPerBlock, 0, stream_>>> (
        d_spectraldata[0], d_spectraldata[1], /* ant 1 X, Y complex spectral data */
        d_spectraldata[2], d_spectraldata[3], /* ... */
        d_spectraldata[4], d_spectraldata[5], /* ... */
        d_spectraldata[6], d_spectraldata[7], /* ant 4 X, Y complex spectral data */
        d_viz_,
        Lbatch_*Nchan_,
        Nchan_ );

    // Begin fetching the resulting visibility data
    CUDA_CALL( cudaMemcpyAsync(h_viz_, d_viz_, Nbyteviz_, cudaMemcpyDeviceToHost, stream_) );
}


/**
 * Copy visibility data from FXCorr-specific representation into
 * user arrays. If the output of xstep() is to be used as input, it
 * has to be copied to host-side memory first (unless Unified mem).
 * The four output polarization pairs are 0:XX, 1:XY, 2:YX, 3:YY.
 *
 * \param h_gpuViz Visibilities in GPU format/layout, host side
 * \param viz      Output visibility arrays sized [6 baseline][4 polarization pair][Nchan]
 * \param autos    Output autocorrelation arrays sized [4 autos][4 polarization pair][Nchan]
 */
void FXCorr4::copyVisibilitiesFrom(const cufftComplex* h_gpuViz, cuComplex* viz[6][4], cuComplex* autos[4][4]) const
{
    const float* in = (const float*)h_gpuViz;

    CUDA_CALL( cudaStreamSynchronize(stream_) );

    // Untangle from [{ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,...,bl40} ] x {re, im}
    size_t viznr = 0, baselinenr = 0;
    for (size_t ant1=0; ant1<Nant_; ant1++) {
        for (size_t ant2=ant1; ant2<Nant_; ant2++) {
            cuComplex** outpp;
            if (ant1 == ant2) {
                //printf(" viz=%zu ant1=%zu ant2=%zu pol=0..3 : auto %zu\n", viznr, ant1, ant2, ant1);
                outpp = autos[ant1];
            } else {
                //printf(" viz=%zu ant1=%zu ant2=%zu pol=0..3 : baseline %zu\n", viznr, ant1, ant2, baselinenr);
                outpp = viz[baselinenr];
                baselinenr++;
            }
            for (size_t pol=0; pol<Npolprod_; pol++) {
                cuComplex* out = outpp[pol];
                for (size_t n=0; n<Nchan_; n++) {
                    float re = in[n + viznr*Nchan_];
                    float im = in[n + viznr*Nchan_ + Nvisibilities_*Nchan_];
                    out[n] = make_cuComplex(re, im);
                }
                viznr++;
                assert(viznr <= Nvisibilities_);
            }
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH_FXFFT // standalone compile mode for testing

#define CUDA_DEVICE_NR 0

int main(int argc, char** argv)
{
    const size_t Nant = 4;
    const size_t Npol = 2;
    const size_t Nchan = 128;
    const size_t Nsamp_per_antenna = 4e-3 * 4e9; // 4 millisec at 4 GS/s

    cudaStream_t s = 0;
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    float dT_msec;

    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaDeviceReset() );
    CUDA_CALL( cudaEventCreate(&tstart) );
    CUDA_CALL( cudaEventCreate(&tstop) );

    float* d_signals[Npol*Nant];

    // Input and output arrays
    printf("Preparing arrays and data for 4-antenna 2-pol %zu-channel FX FFT processing with pre-FFT fringe stopping...\n", Nchan);
    for (size_t n=0; n<Npol*Nant; n++) {
        CUDA_CALL( cudaMalloc((void **)&d_signals[n], Nsamp_per_antenna*sizeof(float)) );
    }

    // Synthetic data
    if (1) {
        printf("Generating synthetic data...\n");
        float* h_signal;
        CUDA_CALL( cudaMallocHost((void**)&h_signal, Nsamp_per_antenna*sizeof(float)) );
        for (size_t n=0; n<Npol*Nant; n++) {
            for(size_t s=0; s<Nsamp_per_antenna; s++) {
                h_signal[s] = ((s + n) % 1237) * (n + 1);
            }
            CUDA_CALL( cudaMemcpy(d_signals[n], h_signal, Nsamp_per_antenna*sizeof(float), cudaMemcpyHostToDevice) );
        }
        CUDA_CALL( cudaFreeHost(h_signal) );
    }

    // Prepare FX FFT
    FXCorr4* fxcorr4 = new FXCorr4(Nsamp_per_antenna, Nchan);
    fxcorr4->setStream(s);

    // Invent some phasing info
    printf("Generating phase polynomial coeffs for pre-FFT continous per-sample sin/cos fringe stopping...\n");
    float* polyX[Nant];
    float* polyY[Nant];
    for (size_t n=0; n<Nant; n++) {
        polyX[n] = new float[4]();
        polyY[n] = new float[4]();
        polyX[n][0] = 0.010f + 0.002f*n;
        polyX[n][1] = -1.7e-3f + 0.002f*n;
        polyX[n][2] = -3.1e-5f + 0.003f*n;
        polyX[n][3] = -2.7e-7f + 0.005f*n;
        polyY[n][0] = 0.010f + 0.002f*n;
        // [1..3] = 0.0f
    }

    // Phase polynomial based fringe-stopping FFT: linear, 1st order poly
    printf("Executing sin/cos linear (order=1) phase poly fringe-stopping FFT transform set...\n");
    CUDA_CALL( cudaEventRecord(tstart, s) );
    fxcorr4->transform(1,
        (const float**)&d_signals[0], polyX,
        (const float**)&d_signals[4], polyY
    );
    CUDA_CALL( cudaEventRecord(tstop, s) );
    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Phase polynomial based fringe-stopping FFT: cubic, 3rd order poly
    printf("Executing sin/cos cubic (order=3) phase poly fringe-stopping FFT transform set...\n");
    CUDA_CALL( cudaEventRecord(tstart, s) );
    fxcorr4->transform(3,
        (const float**)&d_signals[0], polyX,
        (const float**)&d_signals[4], polyY
    );
    CUDA_CALL( cudaEventRecord(tstop, s) );
    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Complete FX: cubic phase poly FFT, x-step, albeit not counting CPU-side untangle
    printf("Executing sin/cos cubic (order=3) phase poly FFT _and_ then the X-Step...\n");
    CUDA_CALL( cudaEventRecord(tstart, s) );
    fxcorr4->transform(3,
        (const float**)&d_signals[0], polyX,
        (const float**)&d_signals[4], polyY
    );
    fxcorr4->xstep();
    CUDA_CALL( cudaEventRecord(tstop, s) );
    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Grab data
    cuComplex* viz[6][4];
    cuComplex* autos[4][4];
    for (int pp=0; pp<4; pp++) {
        for (int n=0; n<6; n++) {
//             viz[n][pp] = new cuComplex((float2){0.0f,0.0f})[Nchan];
             viz[n][pp] = new cuComplex[Nchan];
        }
        for (int n=0; n<4; n++) {
             autos[n][pp] = new cuComplex[Nchan];
        }
    }
    fxcorr4->copyVisibilities(viz, autos);

    // Cleanup ad device reset for 'cuda-memcheck'
    delete fxcorr4;
    CUDA_CALL( cudaDeviceReset() );

    return 0;
}

#endif
