////////////////////////////////////////////////////////////////////////////////////////////////
// Polyphase Filterbank CPU Reference : performs FIR filtering, skips the FFT step
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef PFB_FIR_C // In case this file is "include <pfb_fir.c>" from a CUDA program
#define PFB_FIR_C

/**
 * Convolution based FIR filtering, slow implementation on single CPU core.
 * The coefficients in flat "coeffs[ch*Ntaps + tap]" are assumed to be already time-reversed,
 * whereas coefficients in "coeffs_2d[ch][tap]" are not yet time-reversed.
 */
void cpu_pfb(const float* __restrict__ x_n, float* y_n, const float* __restrict__ coeffs, size_t Nsamp, float** __restrict__ coeffs_2D)
{
    for (size_t n = 0; n < Nsamp; n += PFB_HC_NCHAN) {

        memset(y_n, 0, PFB_HC_NCHAN*sizeof(float));

#if 1
        /* Direct : ~48 Ms/s for 32-ch 32-tap, ~24 Ms/s for 32-ch 64-tap, ~22 Ms/s for 64-ch 64-tap */
        for (int tap = 0; tap < PFB_HC_TAPS; tap++) {
            for (size_t ch = 0; ch < PFB_HC_NCHAN; ch++) {
                float b = coeffs[ch*PFB_HC_TAPS + tap]; // already time-reversed coeffs, in 1D array
                // float b = coeffs_2D[ch][(PFB_HC_TAPS-1) - tap]; // original coeffs, in 2D matrix
                y_n[ch] += b * x_n[ch + tap];
            }
        }
#else
        /* Transposed order : ~52 Ms/s for 32-ch 32-tap, ~22 Ms/s for 32-ch 64-tap, 23 Ms/s for 64-ch 64-tap */
        for (size_t ch = 0; ch < PFB_HC_NCHAN; ch++) {
            const float* x = &x_n[ch];
            const float* b = &coeffs[ch*PFB_HC_TAPS];
            float y = 0.0f;
            for (int tap = 0; tap < PFB_HC_TAPS; tap++) {
                y += x[tap] * b[tap];
            }
            y_n[ch] = y;
        }
#endif

        x_n += PFB_HC_NCHAN;
        y_n += PFB_HC_NCHAN;
    }

    // the last step is an IFFT/FFT to get time domain data, not done here!
}

#endif // PFB_FIR_C

