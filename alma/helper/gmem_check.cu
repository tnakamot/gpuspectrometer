// cuda program to check global memory error by cosmic ray.
// written by Jongsoo Kim
// last revision: 2018-02-15

// compile: nvcc -arch=sm_61 gmem_check.cu -o gmem_check
//          where sm_61 is for TITAN Xp
// run: nohup ./gmem_check 10 > gmem_check.data &
//          where 10 is duration of time for measurement in units of second
// check: tail -f gmem_check.data

#include <iostream>
#include <stdio.h>

__global__ void gmem_error (char *ch, const unsigned long long n, unsigned long long * d_count)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    for (unsigned long long i = index; i < n; i += stride) if (i<n) if (ch[i] != 'a') atomicAdd(d_count,1);
}

void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
    if (iRet != cudaSuccess) {
        (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n", pcFile, iLine, cudaGetErrorString(iRet));
        exit(EXIT_FAILURE);
    }
}
#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)

int main(int argc, char **argv)
{
    cudaEvent_t start, stop;
    CUDA_CALL( cudaEventCreate( &start ) );
    CUDA_CALL( cudaEventCreate( &stop ) );
    CUDA_CALL( cudaEventRecord( start,0 ) );
    float time = 0.0f;

    float etime  = 100.0f; // 100 sec

    if( argc > 1 ) {
      etime = atof(argv[1]);
    }

    cudaDeviceProp deviceProp;
    CUDA_CALL( cudaGetDeviceProperties(&deviceProp, 0) );
    std::cout << "Device" << deviceProp.name <<std::endl;
    std::cout << "Device has compute capability "<< deviceProp.major << "." << deviceProp.minor << std::endl;
    std::cout << "Total amount of global memory:" << (unsigned long long)deviceProp.totalGlobalMem << "bytes" << std::endl;

    // n is size of charater array
    // n should be less than the total global memory
    // due to probably padding, the allocable size is smaller than total global memory size
    size_t free, total;
    CUDA_CALL( cudaMemGetInfo(&free,&total) );
    free -= 16*1024*1024;
    unsigned long long n = free - (free % 65536); //12600000000;   // 12.6 GB
    std::cout << "n=" << n << std::endl;


    char *ch;
    CUDA_CALL( cudaMalloc((void**)&ch, n) );
// initialization of a 1d character array
    CUDA_CALL( cudaMemset(ch,'a',n) );
// insert artificial error
// the value of ch+100 has been changed from 'a' to 'b' 
// comment out the following line for real test
// cudaMemset(ch+100,'b',1);  // COMMENT OUT COMMENT OUT

    unsigned long long h_count;
    unsigned long long * d_count;
    cudaMalloc((void**)&d_count, sizeof(unsigned long long));

    while (time < etime) { 

//  initialize error count for device
    CUDA_CALL( cudaMemset(d_count,0,sizeof(unsigned long long)) );

    gmem_error<<<1024,1024>>>(ch, n, d_count);
    CUDA_CALL( cudaMemcpy (&h_count, d_count, sizeof(unsigned long long), cudaMemcpyDeviceToHost) );

    CUDA_CALL( cudaEventRecord(stop, 0) );
    CUDA_CALL( cudaEventSynchronize(stop) );
    CUDA_CALL( cudaEventElapsedTime( &time, start, stop ) );
    time = time/1000.0f;  // in units of second

    std::cout << "# of error=" << h_count <<", eplased time=" << time << " sec" <<", event rates=" << h_count*3600.0f/time << " /hour" << std::endl;
    } 

    cudaFree(ch);
    cudaFree(d_count);

    cudaDeviceReset();
    return 0;
}

