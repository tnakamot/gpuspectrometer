/**
 * \class ASMConfigfileReader
 *
 * \brief Helper class that reads the contents on a JSON-format ASM configuration file
 *
 */

#include <string>
#include <boost/property_tree/ptree.hpp>

class ASMConfigfileReader {

    public:
        ASMConfigfileReader() {};
        ~ASMConfigfileReader() {};

    public:
        bool readConfigFile(std::string jsonfilepath, boost::property_tree::ptree& config);
};
