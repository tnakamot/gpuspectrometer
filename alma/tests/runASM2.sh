rm -f asm_drxp1.log /tmp/drxp1/asm_drxp1.log

# numactl -H

# Single CPU core
# numactl --localalloc --physcpubind 0 ../ASM_MC_service -c asm_drxp1.json $@ 

# Multiple CPU cores, on one physical processor
# numactl --localalloc --cpunodebind 0 ../ASM_MC_service -c asm_drxp1.json $@ 
numactl --localalloc --cpunodebind 1 ../ASM_MC_service -c asm_drxp1.json $@ 


