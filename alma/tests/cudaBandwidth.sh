
## Location of test program
CUDA_DIR=/usr/local/cuda-9.0/
BWTEST_BIN=$CUDA_DIR/extras/demo_suite/bandwidthTest

## Size of transfer (emulate DRXP sizes)
# RX : 48ms data only                       = 144000000 byte; doesn't divide by 4096B pagesize
# RX : 48ms data only + pagesize pad        = 144003072 byte; equals 35157 x 4096B pagesize; total of 3072B padding
# RX : 48ms data + metaframe                = 144000000 + 48 = 144000048 byte; doesn't divide by 4096B pagesize
# RX : 48ms data + metaframe + pagesize pad = 144003072 byte; has total of 3024B of padding
# RX : 48ms data + metaframe + group pad    = 144000000 + 48 + 2800592 = 146800640 byte; equals 35840 x 4096B pages
SIZES=(144000000 144003072 144000048 144003072 146800640)
MEMTYPES=(pageable pinned)

## Required rate
# (144e6 / 48e-3) / 2^30 = 2.8 GB/sec  per GPU  so 12 GB/sec total over PCIe

## Test : pageable or pinned memory
for MTYPE in ${MEMTYPES[@]}; do
    for SZ in ${SIZES[@]}; do
        ARGS="--memory=$MTYPE --device=all --csv --htod --cputiming --mode=range --start=$SZ --end=$SZ --increment=4096"
        #echo "Running: $BWTEST_BIN $ARGS"
        $BWTEST_BIN $ARGS | grep NumDevsUsed
    done
    for SZ in ${SIZES[@]}; do
        ARGS="--memory=$MTYPE --device=all --csv --dtoh --cputiming --mode=range --start=$SZ --end=$SZ --increment=4096"
        #echo "Running: $BWTEST_BIN $ARGS"
        $BWTEST_BIN $ARGS | grep NumDevsUsed
    done
done

## Most descriptive test, for copy-paste:
#  /usr/local/cuda-9.0/extras/demo_suite/bandwidthTest --memory=pageable --device=all --csv --htod --cputiming --mode=range --start=144000000 --end=144000000 --increment=4096
#  /usr/local/cuda-9.0/extras/demo_suite/bandwidthTest --memory=pinned --device=all --csv --htod --cputiming --mode=range --start=144000000 --end=144000000 --increment=4096

return


## CUDA 9.0

## Results on 4 x TITAN Xp *without* GPU tuning (see './doc/GPU Tuning.txt')
# ~25 GB/s / 4 cards for pageable with 48ms data
# ~30 GB/s / 4 cards for pageable with 48ms data + 3072B padding
# ~30 GB/s / 4 cards for pageable with 48ms data + metaframe + 3028B padding
# ~29 GB/s / 4 cards for pageable with 48ms data + metaframe + group padding
# ~46 GB/s / 4 cards for pinned   with 48ms data + metaframe
# ~46 GB/s / 4 cards for pinned   with 48ms data + metaframe + 3028B padding
# ~46 GB/s / 4 cards for pinned   with 48ms data + metaframe + group padding

## Results on 4 x TITAN Xp with GPU tuning (applied './doc/GPU Tuning.txt')
# ~29 GB/s / 4 cards for pageable with 48ms data
# ~30 GB/s / 4 cards for pageable with 48ms data + 3072B padding
# ~29 GB/s / 4 cards for pageable with 48ms data + metaframe
# ~29 GB/s / 4 cards for pageable with 48ms data + metaframe + 3028B padding
# ~30 GB/s / 4 cards for pageable with 48ms data + metaframe + group padding
# pinned : crash

### CUDA 8.0 and without tunings
bandwidthTest-H2D-Paged, Bandwidth = 30575.3 MB/s, Time = 0.00449 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 31750.2 MB/s, Time = 0.00433 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 30913.2 MB/s, Time = 0.00444 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 29330.0 MB/s, Time = 0.00468 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 29461.8 MB/s, Time = 0.00475 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Paged, Bandwidth = 29867.1 MB/s, Time = 0.00460 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 32957.1 MB/s, Time = 0.00417 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 33431.7 MB/s, Time = 0.00411 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 30171.9 MB/s, Time = 0.00455 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 29330.2 MB/s, Time = 0.00477 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-H2D-Pinned, Bandwidth = 46603.7 MB/s, Time = 0.00295 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46180.0 MB/s, Time = 0.00297 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46147.5 MB/s, Time = 0.00298 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46561.7 MB/s, Time = 0.00295 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46695.9 MB/s, Time = 0.00300 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Pinned, Bandwidth = 51465.8 MB/s, Time = 0.00267 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51478.4 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51488.9 MB/s, Time = 0.00267 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51482.8 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51478.1 MB/s, Time = 0.00272 s, Size = 146800640 bytes, NumDevsUsed = 4

### CUDA 9.0 and without tunings
bandwidthTest-H2D-Paged, Bandwidth = 31009.3 MB/s, Time = 0.00443 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 30693.8 MB/s, Time = 0.00447 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 30975.1 MB/s, Time = 0.00443 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 28997.5 MB/s, Time = 0.00474 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 28982.7 MB/s, Time = 0.00483 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Paged, Bandwidth = 34179.7 MB/s, Time = 0.00402 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 28331.2 MB/s, Time = 0.00485 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 29516.4 MB/s, Time = 0.00465 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 33421.8 MB/s, Time = 0.00411 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 31212.3 MB/s, Time = 0.00449 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-H2D-Pinned, Bandwidth = 46170.1 MB/s, Time = 0.00297 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 45869.5 MB/s, Time = 0.00299 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 45863.0 MB/s, Time = 0.00299 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46138.8 MB/s, Time = 0.00298 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46172.9 MB/s, Time = 0.00303 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Pinned, Bandwidth = 51481.9 MB/s, Time = 0.00267 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51464.4 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51460.5 MB/s, Time = 0.00267 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51471.2 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51485.5 MB/s, Time = 0.00272 s, Size = 146800640 bytes, NumDevsUsed = 4

### CUDA 9.0 and with default tunings (5705 MHz memory, 1404 MHz compute)
bandwidthTest-H2D-Paged, Bandwidth = 29044.5 MB/s, Time = 0.00473 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 28038.9 MB/s, Time = 0.00490 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 29184.9 MB/s, Time = 0.00471 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 29851.0 MB/s, Time = 0.00460 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Paged, Bandwidth = 27435.3 MB/s, Time = 0.00510 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Paged, Bandwidth = 27883.3 MB/s, Time = 0.00493 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 33530.7 MB/s, Time = 0.00410 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 33790.9 MB/s, Time = 0.00406 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 29601.7 MB/s, Time = 0.00464 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Paged, Bandwidth = 35394.6 MB/s, Time = 0.00396 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-H2D-Pinned, Bandwidth = 46669.8 MB/s, Time = 0.00294 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46132.1 MB/s, Time = 0.00298 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46147.6 MB/s, Time = 0.00298 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46620.1 MB/s, Time = 0.00295 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-H2D-Pinned, Bandwidth = 46615.5 MB/s, Time = 0.00300 s, Size = 146800640 bytes, NumDevsUsed = 4
#--
bandwidthTest-D2H-Pinned, Bandwidth = 51481.1 MB/s, Time = 0.00267 s, Size = 144000000 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51456.7 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51455.8 MB/s, Time = 0.00267 s, Size = 144000048 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51482.3 MB/s, Time = 0.00267 s, Size = 144003072 bytes, NumDevsUsed = 4
bandwidthTest-D2H-Pinned, Bandwidth = 51480.4 MB/s, Time = 0.00272 s, Size = 146800640 bytes, NumDevsUsed = 4

