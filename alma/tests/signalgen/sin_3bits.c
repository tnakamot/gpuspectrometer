/* 2017.10.25. Hyunwoo Kang (KASI)					*/
/* It makes sine wave as input values <frequency> and <amplitude>.	*/
/* Output format is DTX. 						*/
/* Time is unit of nano-second, frequency is GHz unit.			*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define _PI_		3.1415926
#define	iteration 	500000		// To make 8ms data by 64bytes

int main(int argc, char *argv[])
{
	int i, j, rpt;
	double freq, ampl;
	double sin_real[64];
	int sin_norm[64];
	int dt = 0;

	char sin_3bits[3][64];
	char dtx[48];

	FILE *bfp;

	bfp=fopen("dtx_sin.bin","wb");

	if(argc!=3) {
		printf("usage:sin_3bits <freq[GHz]> <ampl>\n");
		return -1;
	}

	freq=atof(argv[1]);
	ampl=atof(argv[2]);

	if(freq<=0 || ampl <=0) {
		printf("usage: frequency[GHz] or amplitude must be higher than zero\n");
		return -1;
	}
	rpt=0;
	while(rpt<iteration) {
//
// Routine for sinewave
// 
	for(i=0;i<64;i++){
		dt+=16; 	//nano-second unit
		sin_real[i]=ampl*sin(freq*dt*(_PI_/180.0));
				//Normalization by amplitude with 3 bits
		sin_norm[i]=(int)((sin_real[i]+ampl)/(2*ampl/8));
	}
//
// Routine for 3 bits calculation
// 
	for(i=0;i<64;i++){
		sin_3bits[0][i]=('0')+(sin_norm[i] & 0X001);
		sin_3bits[1][i]=('0')+(sin_norm[i] & 0X002);
		sin_3bits[2][i]=(int)('0')+(int)((sin_norm[i]) & (0X004)); 
	}
//
// Routine of conversion from 3 bits to DTX format
	for(i=0;i<8;i++){
		j = (i/2)*2+i;		// It returns 0, 1, 4, 5, 8, 9, 12, 13
					// of X bytes position.
		dtx[j]=0X00;
		dtx[j+2]=0X00;
		dtx[j+16]=0X00;
		dtx[j+16+2]=0X00;
		dtx[j+32]=0X00;
		dtx[j+32+2]=0X00;
// X(L)byte
		dtx[j]|=(sin_3bits[0][j*8]     >0X00?  0X01 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j]|=(sin_3bits[0][j*8 + 7] >0X00?  0X80 : 0X00);
// X(M) byte
		dtx[j+16]|=(sin_3bits[1][j*8]     >0X00?  0X01 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j+16]|=(sin_3bits[1][j*8 + 7] >0X00?  0X80 : 0X00);
// X(H) byte
		dtx[j+16*2]|=(sin_3bits[2][j*8]     >0X00?  0X01 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j+16*2]|=(sin_3bits[2][j*8 + 7] >0X00?  0X80 : 0X00);
// Y(L) byte
		dtx[j+2]|=(sin_3bits[0][j*8]     >0X00?  0X01 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j+2]|=(sin_3bits[0][j*8 + 7] >0X00?  0X80 : 0X00);
// Y(M) byte
		dtx[j+16+2]|=(sin_3bits[1][j*8]     >0X00?  0X01 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j+16+2]|=(sin_3bits[1][j*8 + 7] >0X00?  0X80 : 0X00);
// Y(H) byte
		dtx[j+16*2+2]|=(sin_3bits[2][j*8]     >0X00?  0X01 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 1] >0X00?  0X02 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 2] >0X00?  0X04 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 3] >0X00?  0X08 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 4] >0X00?  0X10 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 5] >0X00?  0X20 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 6] >0X00?  0X40 : 0X00);
		dtx[j+16*2+2]|=(sin_3bits[2][j*8 + 7] >0X00?  0X80 : 0X00);
	}

	for (i=0;i<48;i++) fprintf(bfp,"%c",dtx[i]);
	rpt++;
	}
	fclose(bfp);

	return 0;





	
}
