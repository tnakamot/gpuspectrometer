%
% Matlab script to generate time series from a synthetic spectrum
%
% Writes 3-bit dual-pol output data file in presumably DTS-like sample order.
% Uses the Matlab fwrite(...,'ubit<n>') arbitrary bit length file write
% function which is not available in GNU OCtave.
%
% The output file contains 48ms worth of samples at 4 Gs/s in dual polarization.
% Sample data of the polarizations is identical.
%

%% Where to store dual-pol 3-bit ALMA data
outfile = 'C:\MatlabData\spectrometer\alma\synthetic.3bit';
do_write = 0;  % 1=overwrite outfile, 0=just plot without writing
do_graycode = 1; % 1=write 3-bit data in Gray code, as used by ALMA

%% Template spectrum
% Length
N = 32768;
% Noise
sref = 1e-1*(randn(N,1));
% Baseline
sref = sref + 1.5;
% Spectral components
sref = sref + circshift(0.5*gausswin(N,10), +round(0.1*N));
sref = sref + circshift(2.8*gausswin(N,50), -round(0.15*N));
sref = sref + circshift(3*gausswin(N,500), -round(0.25*N));
sref = sref + circshift(15*gausswin(N,500), -round(0.255*N));
% Discard negative power spectral components
sref(sref < 0) = std(sref);
% Randomize the phase
sref = sref .* exp(-1i*random('unif',-pi,pi,N,1));

%% Generate simple time domain signal : compare its spectrum to the reference
R = real(ifft(sref,2*N));
dR = std(R);
if 1,
    figure(1),clf,
    subplot(3,1,1), plot(abs(sref)), title('Reference');
    subplot(3,1,2), plot(R), title('Time domain');
    subplot(3,1,3), plot(abs(fft(R))), title('Resynthesized');
end

%% Generate signal : for initial test
wf = hanning(2*N);
M = 20*(2*N);
sig = R(1:N);
while numel(sig)<M,
    % Randomize phase again, derive time domain signal
    R = sref .* exp(-1i*random('unif',-pi,pi,N,1));
    R = real(ifft(R,2*N));
    sig = [sig; R];
end
if 1,
    K = 8192;
    L = floor(numel(sig)/K);
    s = reshape(sig(1:K*L), [K, L]);
    S = fft(s,[],1);
    S = sum(abs(S),2);
    figure(2),clf,
    subplot(2,1,1), plot(abs(sref)), title('Reference');
    subplot(2,1,2), plot(S), title('Synthesized, average of several spectra');
end

%% Generate output file : 3-bit quantized and dual-pol with identical data per polarization
% TODO rearrange the 3-bit data so that the data layout matches the DTX-Simulator format
m = 0;
M = 48e-3 * 2*2e9; % 48ms, 2 GHz @ 4 Gs/s
nlev = 8;
if do_write==1, fdo = fopen(outfile,'w'); end
while m<M,

    mleft = M-m;
    if (mleft <= 0), break, end;
    mleft = min(mleft, 2*N);

    % Randomize phase again, derive time domain signal
    R = sref .* exp(-1i*random('unif',-pi,pi,N,1));
    R = real(ifft(R,2*N));

    % Quantize
    dR = 3*std(R);
    dQ = dR/nlev;
    Q = 0.5*(1/dQ) * quant(R,dQ); % quantize
    Q = Q + (nlev-1)/2; % center 3-bit value (0,1,2,3,4,5,6,7)
    Q = round(Q);
    Q(Q<0) = 0; % clip lower
    Q(Q>(nlev-1)) = nlev-1; % clip upper
    hist(Q,nlev);

    % Duplicate X-pol -> X,Y pol
    Q2 = reshape([Q Q]', [1 2*2*N]);
    if mleft < 2*N,
        Q2 = Q2(1:(2*mleft));
    end
    
    % Write data
    if do_write==1,
        if do_graycode==0,
            fwrite(fdo, Q2, 'ubit3');
        else
            % in:  0 1 2 3 4 5 6 7
            % out: 0 1 3 2 6 7 5 4
            gray_mapping = [0 1 3 2 6 7 5 4];
            Q2 = gray_mapping(Q2+1);
            fwrite(fdo, Q2, 'ubit3');
        end
    end
    
    m = m + numel(R);
end
ylabel('Occurrences');
xlabel('Sample value (3-bit unsigned)');

if do_write==1, fclose(fdo); end
