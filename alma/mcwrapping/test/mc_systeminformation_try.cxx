#include "mcwrapping/mc_systeminformation.hxx"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <iostream>

int main(int argc, char** argv)
{
    MCSystemInformation info;

    std::cout << "Selftest: " << (info.selfTestOK() ? "succeeded" : "failed") << "\n";

    std::cout << "XML doc : ";
    write_xml(std::cout, info.get());
    std::cout << "\n";

    std::cerr << "The std::cerr is still working.\n";

    return 0;
}

