/**
 * \class MCSpectrometerState
 *
 * \brief Container for various states of the spectrometer
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_SPECTROMETER_STATE_HXX
#define MC_SPECTROMETER_STATE_HXX

#include "helper/logger.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/noncopyable.hpp>

#include <string>
#include <vector>
#include <map>
#include <exception>
#include <iostream>

class MCSpectrometerState : private boost::noncopyable
{
    public:
        MCSpectrometerState();
        void reset();

    public:

        enum Error_t {
            None=0, Unexpected, IllegalSetErrorCode, IllegalObsState, IllegalIntState, IllegalOpState
        };

        /** \return Human-readable string representation of the given Error_t */
        std::string getError(MCSpectrometerState::Error_t t);

        /** \return Current error code as a number and string, function returns True if error!=None */
        bool getError(MCSpectrometerState::Error_t& e, std::string& s);

        /** \return Current error code as a number and string, function returns True if error!=None */
        bool getError(int& e, std::string& s);

        /** Sets error state (e.g., None, and others) to the given error.
         * \return The previously set error or IllegalSetErrorCode if function failed
         */
        MCSpectrometerState::Error_t setError(MCSpectrometerState::Error_t error_id);

    public:

        enum InternalState_t {
            Ready, Running, Preparing, Initialized, Failure
        };

        /** \return Human-readable string representation of the given InternalState_t */
        std::string InternalState_str(MCSpectrometerState::InternalState_t t) const;

        /** \return Currently active internal state */
        MCSpectrometerState::InternalState_t getInternalState() const;

        /** \return Currently active internal state, as as string */
        std::string getInternalStateStr() const;

        /** Change internal state to the given new state and return the previously active state.
         * The state transition is assumed to be valid, relevant checks must be done by caller.
         */
        MCSpectrometerState::InternalState_t switchInternalState(MCSpectrometerState::InternalState_t newstate);

    public:

        enum OperatingMode_t {
            NormalDataMode, TestDataMode, SelfTestMode_Simple, SelfTestMode_Thorough, SelfTestMode_Endurance
        };

        /** \return Human-readable string representation of the given OperatingMode_t */
        std::string OperatingMode_str(MCSpectrometerState::OperatingMode_t t) const;

        /** \return Currently set operating mode */
        MCSpectrometerState::OperatingMode_t getOperatingMode();

        /** Change operating mode to the given new mode and return the previously active mode.
         * The mode transition is assumed to be valid, relevant checks must be done by caller.
         */
        MCSpectrometerState::OperatingMode_t switchOperatingMode(MCSpectrometerState::OperatingMode_t newmode);

    public:

        /** Store the internal state and mode and errors into a Boost PropertyTree */
        bool getStatusAll(boost::property_tree::ptree& p);

    private:
        mutable boost::mutex m_mutex;

    protected:
        std::map<Error_t,const char*> m_errorMap;
        InternalState_t m_internalState;
        OperatingMode_t m_operatingMode;
        Error_t m_latestError_Id;

};

#endif // MC_SPECTROMETER_STATE_HXX
