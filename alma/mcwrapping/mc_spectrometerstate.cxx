/**
 * \class MCSpectrometerState
 *
 * \brief Container for various states of the spectrometer
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_spectrometerstate.hxx"
#include "helper/logger.h"

#include <boost/assign/list_of.hpp>
#include <iostream>
#include <map>

MCSpectrometerState::MCSpectrometerState()
{
    m_errorMap = boost::assign::map_list_of
        (None, "No error")
        (Unexpected, "Unexpected internal error")
        (IllegalSetErrorCode, "Illegal error given to setError()")
        (IllegalObsState, "Illegal observation state")
        (IllegalIntState, "Illegal internal state")
        (IllegalOpState, "Illegal operating state")
    ;
    reset();
}

void MCSpectrometerState::reset()
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    m_latestError_Id = None;
    m_internalState = Initialized;
    m_operatingMode = NormalDataMode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** \return Current error code as a number and string, function returns True if error!=None */
bool MCSpectrometerState::getError(MCSpectrometerState::Error_t& e, std::string& s)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    e = m_latestError_Id;
    s = m_errorMap.at(m_latestError_Id);
    return !(m_latestError_Id = MCSpectrometerState::None);
}

/** \return Current error code as a number and string, function returns True if error!=None */
bool MCSpectrometerState::getError(int& e, std::string& s)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    e = m_latestError_Id;
    s = m_errorMap.at(m_latestError_Id);
    return !(m_latestError_Id = MCSpectrometerState::None);
}

/** Sets error state (e.g., None, and others) to the given error.
 * \return The previously set error or IllegalSetErrorCode if function failed
 */
MCSpectrometerState::Error_t MCSpectrometerState::setError(MCSpectrometerState::Error_t error_id)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    Error_t prevErr = m_latestError_Id;
    try {
        m_latestError_Id = error_id;
        std::string tmp = m_errorMap.at(error_id);
    } catch (std::exception &e) {
        L_(lerror) << "MCSpectrometerState::setError() called with unknown error code " << error_id << ".\n";
        m_latestError_Id = IllegalSetErrorCode;
    }
    return prevErr;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** \return Human-readable string representation of the given OperatingMode_t */
std::string MCSpectrometerState::OperatingMode_str(MCSpectrometerState::OperatingMode_t t) const
{
    switch (t) {
        case NormalDataMode: return std::string("normaldatamode");
        case TestDataMode: return std::string("testdatamode");
        case SelfTestMode_Simple: return std::string("selftestmodesimple");
        case SelfTestMode_Thorough: return std::string("selftestmodethorough");
        case SelfTestMode_Endurance: return std::string("selftestmodeendurance");
    }
    L_(lwarning) << "Got unexpected OperatingMode_t value " << t << " in MCSpectrometerState::OperatingMode_str\n";
    //perhaps: setError(IllegalOpState);
    return std::string("illegal_state");
}

/** \return Currently set operating mode */
MCSpectrometerState::OperatingMode_t MCSpectrometerState::getOperatingMode()
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    return m_operatingMode;
}

/** Change operating mode to the given new mode and return the previously active mode.
 * The mode transition is assumed to be valid, relevant checks must be done by caller.
 */
MCSpectrometerState::OperatingMode_t MCSpectrometerState::switchOperatingMode(MCSpectrometerState::OperatingMode_t newmode)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    OperatingMode_t old = m_operatingMode;
    m_operatingMode = newmode;
    L_(linfo) << "Spectrometer operating mode change " << OperatingMode_str(old) << "-->" << OperatingMode_str(newmode);
    return old;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** \return Human-readable string representation of the given InternalState_t */
std::string MCSpectrometerState::InternalState_str(MCSpectrometerState::InternalState_t t) const
{
    switch (t) {
        case Running: return std::string("running");
        case Ready: return std::string("ready");
        case Preparing: return std::string("preparing");
        case Initialized: return std::string("initialized");
        case Failure: return std::string("failure");
        default: break;
    }
    L_(lwarning) << "Got unexpected InternalState_t value " << t << " in MCSpectrometerState::InternalState_str\n";
    //perhaps: setError(IllegalIntState);
    return std::string("illegal_state");
}

/** \return Currently active internal state */
MCSpectrometerState::InternalState_t MCSpectrometerState::getInternalState() const
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    return m_internalState;
}

/** \return Currently active internal state, as as string */
std::string MCSpectrometerState::getInternalStateStr() const
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    return InternalState_str(m_internalState);
}

/** Change internal state to the given new state and return the previously active state.
 * The state transition is assumed to be valid, relevant checks must be done by caller.
 */
MCSpectrometerState::InternalState_t MCSpectrometerState::switchInternalState(MCSpectrometerState::InternalState_t newstate)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    InternalState_t old = m_internalState;
    m_internalState = newstate;
    L_(linfo) << "Spectrometer state change " << InternalState_str(old) << "-->" << InternalState_str(newstate);
    return old;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Store the internal state and mode and errors into a Boost PropertyTree */
bool MCSpectrometerState::getStatusAll(boost::property_tree::ptree& p)
{
    p.put("status", InternalState_str(getInternalState()));
    return true;
}

