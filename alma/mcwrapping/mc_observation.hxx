/**
 * \class MCObservation
 *
 * \brief Groups together a spectral configuration and observation settings (time, ID).
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_OBSERVATION_HXX
#define MC_OBSERVATION_HXX

#include "mcwrapping/mc_configuration.hxx" // class MCConfiguration
class ResultRecipient;

#include <ostream>
#include <string>
#include <boost/thread/recursive_mutex.hpp>

class MCObservation {

    public:

        /** C'stor */
        MCObservation();

        /** D'stor */
        ~MCObservation();

        /** Assignment op */
        MCObservation& operator=(const MCObservation&);

        /** Copy c'stor */
        MCObservation(const MCObservation&);

        /** Update some internal derived parameters */
        void updateDerivedParams();

    public:

        enum ObservationState_t {
            ObsUninitialized=0, Pending, Ongoing, Faulted, Stopped, Missed
        };


        /** Return string representation of ObservationState_t enum */
        static std::string ObservationState_str(const ObservationState_t&);

    public:
        mutable boost::recursive_mutex mutex;

        MCConfiguration cfg; /// copy of config at time of startObservation() since original might be modified in background
        ResultRecipient* outputWriter; // pointer to externally-set new ResultRecipient-derived object that should store data of this observation

        std::string observationId;      /// unique ID assigned by ASC
        std::string antennaId;          /// array-unique antenna identifier (part of BDF/MIME results), TODO: where should this come from?
        std::string baseBandName;       /// antenna-unique baseband identifier (part of BDF/MIME results), TODO: where should this come from?
        ObservationState_t state;       /// current state of the observation

        std::string startTimeUTC;       /// time the observation should start, string
        std::string stopTimeUTC;        /// time the observation was stopped or is to be stopped, string
        struct timespec ts_startTimeUTC;/// time the observation should start; string 'startTimeUTC' converted to timespec
        struct timespec ts_stopTimeUTC; /// time the observation was stopped or is to be stopped; string 'stopTimeUTC' converted to timespec

    public:

        bool operator<(const MCObservation& rhs) const;
        bool operator<=(const MCObservation& rhs) const;
        bool operator>(const MCObservation& rhs) const;
        bool operator>=(const MCObservation& rhs) const;
        bool operator==(const MCObservation& rhs) const;
        bool operator==(const std::string& rhs) const;

    private:
        friend std::ostream& operator<<(std::ostream&, const MCObservation&);
};

extern std::ostream& operator<<(std::ostream&, const MCObservation&);

#endif // MC_OBSERVATION_HXX
