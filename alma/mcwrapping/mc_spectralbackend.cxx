/**
 * \class MCSpectralBackend
 *
 * \brief Wraps an implementation of a CPU or GPU spectral processor.
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_spectralbackend.hxx"
#include "spectralprocessors/SpectralProcessorCPU.hxx"
#ifdef HAVE_GPU
    #include "spectralprocessors/SpectralProcessorGPU.hxx"
#endif
class ResultRecipient;

#include <stdlib.h>
#include <string>
#include <sstream>

MCSpectralBackend::MCSpectralBackend(bool useGPU)
{
    m_useGPU = useGPU;
    m_isObserving = true; // TODO: control from outside?
    m_ndevices = 0;
#ifdef HAVE_GPU
    if (m_useGPU) {
        spu = new SpectralProcessorGPU();
    } else {
        spu = new SpectralProcessorCPU();
    }
#else
    // Always use CPU when compiling without CUDA
    spu = new SpectralProcessorCPU();
#endif
}

MCSpectralBackend::~MCSpectralBackend()
{
    if (spu != NULL) {
#ifdef HAVE_GPU
        if (m_useGPU) {
            delete (SpectralProcessorGPU*)spu;
        } else {
            delete (SpectralProcessorCPU*)spu;
        }
#else
        delete (SpectralProcessorCPU*)spu;
#endif
        spu = NULL;
    }
}

/** Interface inherited by SpectralProcessor from DataRecipient */
bool MCSpectralBackend::takeData(RawDRXPDataGroup data)
{
    bool keepReserved = false;
    if (spu != NULL) {
        if (m_isObserving) {
            keepReserved = spu->takeData(data);
        }
    }
    return keepReserved;
}

/** Set the recipient to be invoked by SpectralProcessor to store spectral results */
void MCSpectralBackend::setSpectralRecipient(ResultRecipient* r)
{
    if (spu != NULL) {
        spu->setSpectralRecipient(r);
    }
}

/** Pass a device selection to current/future processors */
void MCSpectralBackend::setAffinity(int ndevices, const int* deviceIds)
{
    m_ndevices = ndevices;
    for (int n = 0; n < ndevices; n++) {
        m_deviceIds[n] = deviceIds[n];
    }
    if ((m_ndevices > 0) && (spu != NULL)) {
        spu->setAffinity(m_ndevices, m_deviceIds);
    }
}


/** Pass a device selection to current/future processors */
void MCSpectralBackend::setAffinity(const std::string& devList_commaseparated)
{
    std::stringstream ss(devList_commaseparated);
    m_ndevices = 0;
    while (ss.good()) {
        std::string item;
        getline(ss, item, ',');
        if (item.length() > 0) {
            m_deviceIds[m_ndevices] = atoi(item.c_str());
            m_ndevices++;
        }
    }
    if ((m_ndevices > 0) && (spu != NULL)) {
        spu->setAffinity(m_ndevices, m_deviceIds);
    }
}
