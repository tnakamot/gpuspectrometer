/**
 * \class MCSubarrays
 *
 * \brief Manages a list of subArray_t items and their spectrometerInput_t's.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_SUBARRAYS_HXX
#define MC_SUBARRAYS_HXX

#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/thread/mutex.hpp>

#include <map>
#include <ostream>
#include <string>
#include <vector>

class MCSubarrays
{
    public:
        MCSubarrays();
        ~MCSubarrays();

    public:
        bool hasSubarray(const XSD::subArray_t& array) const;

        bool addSubarray(const XSD::subArray_t& array);

        bool delSubarray(const XSD::subArray_t& array);

        bool addSubarrayInputs(const XSD::subArray_t& array, std::vector<XSD::spectrometerInput_t>& newinputs);

        bool getSubarrayInputs(const XSD::subArray_t& array, std::vector<XSD::spectrometerInput_t>& inputs);

    private:
        mutable boost::mutex mutex;

        typedef std::vector<XSD::spectrometerInput_t> SpecinputSet;
        typedef std::map<XSD::subArray_t, SpecinputSet> SubarraySet;
        SubarraySet subarrays;

    friend std::ostream& operator<<(std::ostream&, const MCSubarrays&);
};

extern std::ostream& operator<<(std::ostream&, const MCSubarrays&);

#endif

