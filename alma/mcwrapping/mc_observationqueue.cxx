/**
 * \class MCObservationQueue
 *
 * \brief Manages a list of MCObservation items.
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_observationqueue.hxx"
#include "mcwrapping/mc_spectrometerstate.hxx"
#include "datarecipients/ResultRecipientMIME.hxx"
#include "datarecipients/ResultRecipientSim.hxx"
#include "spectralprocessors/SpectralProcessor_iface.hxx"
#include "time/TimeUTC.hxx"
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/version.hpp>

#include <boost/assign/list_of.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#if (BOOST_VERSION>=105300)
    #include <boost/chrono.hpp>
#endif

#include <algorithm>
#include <string>
#include <vector>
#include <limits>
#include <iomanip>

#define lockscope  // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes

MCObservationQueue::MCObservationQueue()
{
    reset();
}

MCObservationQueue::~MCObservationQueue()
{
}

/* Clear the observation queue, and stop monitoring and processing the queue */
void MCObservationQueue::reset()
{
    lockscope {
        boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
        m_observationQueue.clear();
        //m_observationQueue.reserve(OBS_QUEUE_MAX_LENGTH);
    }
}

/** \return True if at least one observation was found. Also fills out Boost PropertyTree with infos of observing queue entries */
bool MCObservationQueue::getCurrentObservations(boost::property_tree::ptree& p, const int nmax)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
    int nadded = 0;

    // Add only observations that are not in a Stopped state
    std::deque<MCObservation>::const_iterator obs = m_observationQueue.begin();
    while (obs != m_observationQueue.end()) {
        boost::property_tree::ptree observation;
        obs->mutex.lock();
        if (obs->state == MCObservation::Stopped) {
            obs->mutex.unlock();
            obs++;
            continue;
        }
        observation.put("configId", obs->cfg.getId());
        // observation.put("spectrometerInput", obs->cfg.spectrometerInput.input); // removed in XSD 19aug2019
        observation.put("observationId", obs->observationId);
        observation.put("time", obs->startTimeUTC);
        observation.put("state", MCObservation::ObservationState_str(obs->state));
        obs->mutex.unlock();
        p.add_child("observation", observation);
        obs++;
        nadded++;
        if ((nmax > 0) && (nadded >= nmax)) { break; }
    }

    return (nadded >= 1);
}

/** \return True if the given observation ID does not yet exist (i.e. if it is still available) */
bool MCObservationQueue::isObservationIdUnique(const std::string& id)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
    std::deque<MCObservation>::const_iterator it;
    it = std::find(m_observationQueue.begin(), m_observationQueue.end(), id);
    return (it == m_observationQueue.end());
}

/** \return True if the (optional) observation stop time field was successfully set */
bool MCObservationQueue::setObservationStopTime(const std::string& id, const std::string& utc)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
    std::deque<MCObservation>::iterator obs;
    obs = std::find(m_observationQueue.begin(), m_observationQueue.end(), id);
    if (obs == m_observationQueue.end()) {
        L_(lwarning) << "MCObservationQueue::setObservationStopTime() could not find obs. " << id;
        return false;
    }
    obs->mutex.lock();
    obs->stopTimeUTC = utc;
    obs->mutex.unlock();
    obs->updateDerivedParams();
    return true;
}

/** \return True if observation was successfully added to the queue, False if not (e.g. duplicate ID) */
bool MCObservationQueue::enqueueObservation(MCObservation& obs)
{
    // Verify that the ID is not a duplicate
    boost::unique_lock<boost::recursive_mutex> obs_lock(obs.mutex);
    std::string id = obs.observationId;
    if (!isObservationIdUnique(id)) {
        L_(lwarning) << "Discarded request for enqueueing an observation with already existing ID '" << id << "'";
        return false;
    }

    // Discard invalid or invalidated Configurations
    if (!obs.cfg.valid()) {
        L_(lwarning) << "Discarded request for enqueueing an observation with an invalid configuration object";
        return false;
    }

    // Insert new observation and keep an increasing start time order
    lockscope {
        if (m_observationQueue.size() >= (OBS_QUEUE_MAX_LENGTH-1)) {
            L_(lerror) << "Discarded request for enqueueing an observation, because limit of " << OBS_QUEUE_MAX_LENGTH << " queued observations reached!";
            return false;
        }
        obs.state = MCObservation::Pending;
        obs_lock.unlock();
        obs.updateDerivedParams();
        boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
        m_observationQueue.push_back(obs);
        //std::sort(m_observationQueue.begin(), m_observationQueue.end()); // avoid, as this can invalidate pointers to std::vector[] elements somewhere else
    }

    return true;
}

/** Stop an observation. Has no effect on an already stopped observation.
 *  Removes the observation if its stop time was reached, or if it had no assigned stop time.
 *  \return True if observation stopped at the return from this function.
 */
bool MCObservationQueue::stopObservation(const std::string& id)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
    std::deque<MCObservation>::iterator obs;
    obs = std::find(m_observationQueue.begin(), m_observationQueue.end(), id);
    if (obs == m_observationQueue.end()) {
        L_(lwarning) << "MCObservationQueue::stopObservation() could not find obs. " << id;
        return false;
    }

    // Do nothing if already Stopped
    boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
    if (obs->state == MCObservation::Stopped) {
        L_(linfo) << "MCObservationQueue::stopObservation() obs. " << obs->observationId << " was already in stopped state!";
        return true;
    }

    // Immediate stop
    if ((obs->stopTimeUTC == "") || (TimeUTC::getSecondsSince(obs->stopTimeUTC) >= 0)) {
        L_(linfo) << "MCObservationQueue::stopObservation() stop of obs. " << obs->observationId << " is immediate";
        obs->state = MCObservation::Stopped;
        return true;
    }

    // Pending stop
    // could either 1) set obs->stopTimeUTC to "" to force an untimed stop
    // or 2) let worker thread handle the auto-stopping once obs.stopTimeUTC is reached
    L_(linfo) << "MCObservationQueue::stopObservation() obs. " << obs->observationId << " to stop at future time '" << obs->stopTimeUTC;

    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////

/** Check for a soon-upcoming observation in the queue.
 * Chooses the next PENDING observation that has a future start time close to current computer UTC time.
 * Optionally also considers any PENDING observation whose start time (but not stop time) is already in the past.
 * \return True if a suitable observation is found.
 */
bool MCObservationQueue::getUpcomingObservation(
    int* index, MCObservation** pobs,
    double* dT_to_next,
    bool allow_past,
    const double min_leadtime_sec)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);

#if 0
    // Print out the queue
    std::stringstream ss;
    ss << "Observations queue:\n";
    for (size_t i=0; i<m_observationQueue.size(); i++) {
        boost::unique_lock<boost::recursive_mutex> obs_lock(m_observationQueue[i].mutex);
        double dT = TimeUTC::getSecondsSince(m_observationQueue[i].startTimeUTC);
        ss << " obs " << i << " " << m_observationQueue[i] << " : in " << dT << "s\n";
    }
    L_(linfo) << ss.str();
#endif

    *dT_to_next = -std::numeric_limits<double>::infinity();

    for (size_t n=0; n<m_observationQueue.size(); n++) {

        boost::unique_lock<boost::recursive_mutex> obs_lock(m_observationQueue[n].mutex);

        // Consider only PENDING observation(s)
        if (m_observationQueue[n].state != MCObservation::Pending) { continue; }

        // Check the start time versus current time
        bool found = false;
        double dT = TimeUTC::getSecondsSince(m_observationQueue[n].startTimeUTC); // negative when start time in the future
        if ((dT < 0) && (dT >= -min_leadtime_sec)) {
            found = true;
            L_(ldebug) << "MCObservationQueue::getUpcomingObservation() found pending future obs. "
                       << m_observationQueue[n].observationId << " with " << std::fixed << std::setprecision(3) << (-dT) << " seconds until start";
        }
        if (!found && (dT >= 0) && allow_past) {
            found = true;
            L_(ldebug) << "MCObservationQueue::getUpcomingObservation() found past unstarted obs. "
                       << m_observationQueue[n].observationId << " requested to start " << std::fixed << std::setprecision(3) << dT << " seconds ago";
        }
        if (!found && (dT >= 0)) {
            m_observationQueue[n].state = MCObservation::Missed;
            L_(lwarning) << "MCObservationQueue::getUpcomingObservation() found a missed obs. "
                         << m_observationQueue[n].observationId << " to start " << std::fixed << std::setprecision(3) << dT << " seconds ago, marking it as Missed";
            continue;
        }

        // Time to closest future observation (largest of negative dT's)
        if (dT < 0) {
            *dT_to_next = (dT > *dT_to_next) ? dT : *dT_to_next;
        } else {
            *dT_to_next = 0.0;
        }

        // Done?
        if (found) {
            *pobs = &m_observationQueue[n];
            *index = n;
            return true;
        }
    }

    return false;
}

/** Remove old observations from queue */
int MCObservationQueue::removeExpiredObservations()
{
    boost::unique_lock<boost::mutex> scoped_lock(m_obsQueue_mutex);
    int nremoved = 0;

    std::deque<MCObservation>::iterator it;
    for (it = m_observationQueue.begin(); it != m_observationQueue.end(); /*no increment*/ ) {

        bool expire = false;

        // Observations without stop time: remove if start time in the past, and not pending/running
        if (it->stopTimeUTC == "") {
            double dT_start = TimeUTC::getSecondsSince(it->startTimeUTC);
            if (dT_start > OBS_QUEUE_PURGE_AGE_SEC && it->state != MCObservation::Pending && it->state != MCObservation::Ongoing) {
                expire = true;
            }
        // Observations with stop time: remove if stop time in the past
        } else {
            double dT_stopped = TimeUTC::getSecondsSince(it->stopTimeUTC);
            if (dT_stopped > OBS_QUEUE_PURGE_AGE_SEC) {
                expire = true;
            }
        }

        if (expire) {
            it = m_observationQueue.erase(it);
            ++nremoved;
        } else {
            ++it;
        }

    }

    return nremoved;
}
