/**
 * \class MCDRXPStatus
 *
 * \brief The status information of one DRXP card as per M&C configuration
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_DRXP_STATUS_HXX
#define MC_DRXP_STATUS_HXX

#include "drxp/DRXPIface.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

class DRXP;
class DRXPDummy;

#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>
#include <map>

#include <stdint.h>

class MCDRXPStatus
{
    friend class DRXPIface;
    friend class DRXP;
    friend class DRXPDummy;

    public:
        MCDRXPStatus() : is_valid(false) { clear(); }

    public:
        /** Read DRXP monitoring points and store in current object */
        bool deviceReadout(DRXPIface* drxp);
        /** \return True if status informations are valid */
        bool valid() const { return is_valid; }

        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getChecksumCounters(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getSyncErrorCounters(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getAlarmStatus(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getPowerAlarms(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDfrStatus(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDfrVoltages(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getSyncStatus(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDrxpTestmode(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDrxpStatus(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDrxpSerialNumber(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDrxpVersion(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getDrxpMetaframeDelay(boost::property_tree::ptree& pt) const;
        /** Read out the specific DRXP monitoring point and store into boost PropertyTree */
        bool getOpticalPower(boost::property_tree::ptree& pt, bool jsonCompatible=false) const;
        /** Read out DRXP temperatures and store into boost PropertyTree */
        bool getDrxpTemperatures(boost::property_tree::ptree& pt, bool jsonCompatible=false) const;

    public:
        /** Start periodic monitoring of DRXP. This call simply wraps the call to the
            initiation of periodic monitoring via DRXPIface::startPeriodicMonitoring().
            When periodic monitoring is started this MCDRXPStatus object will auto-refresh
            its contents (alarms, counters, etc) at regular intervals.
        */
        bool startPeriodicMonitoring(DRXPIface& drxp, float interval_secs=5.0f, const char* multicastgroup = NULL, int multicastport = 0);

        /** Stop periodic monitoring of DRXP. This call simply wraps DRXPIface::stopPeriodicMonitoring().
            Once stopped, the this MCDRXPStatus object no longer auto-refreshes its contents.
        */
        void stopPeriodicMonitoring(DRXPIface& drxp) const;

        /** Return a summary. Uses VT-100 Terminal escape codes to color the alarms or warnings (green/yellow/red). */
        std::string toString() const;

    private:
        bool is_valid; /// object contents are valid
        bool invariant() {
            is_valid = true;
            return is_valid;
        }
        void clear();

    public:
        XSD::drxpAlarmStatus_t alarmStatus;       /// the Alarm Status values at time of last deviceReadout() or similar call
        XSD::drxpPowerAlarm_t powerAlarm;         /// the Power Alarm values at time of last deviceReadout() or similar call
        XSD::drxpDfrStatus_t dfrStatus;           /// the DFR Status values at time of last deviceReadout() or similar call
        XSD::drxpDfrVoltageStatus_t dfrVoltageStatus;   /// the Voltage Status values at time of last deviceReadout() or similar call
        XSD::drxpDfrSyncLockStatus_t syncLockStatus;    /// the Sync Status values at time of last deviceReadout() or similar call
        XSD::drxpTestMode_t drxpTestMode;               /// the Test Mode setting at time of last deviceReadout() or similar call
        XSD::drxpSignalAveragePower_t drxpOpticalPower; /// the Optical Power (received power) values at time of last deviceReadout() or similar call
        XSD::drxpTemperatures_t drxpTemperatures; /// the temperatures of components on the DRXP board
        XSD::fpgaSEUInfo_t fpgaSEUInfo;           /// the FPGA single event upset values of last deviceReadout() or similar call

        XSD::drxpDfrChecksumCounter_t checksumCounter;      /// the count of Checksum Errors at time of last deviceReadout() or similar call
        XSD::drxpDfrSyncErrorCounter_t syncErrorCounter;    /// the count of Errors at time of last deviceReadout() or similar call
        XSD::drxpMetaframeDelay_t metaframeDelay;           /// the values of Metaframe Delay at time of last deviceReadout() or similar call

        int drxpStarted;                     /// the ST_DRXPD_RCVSTS.State, either 0 (stopped) or 1 (started)
        int drxpSending;                     /// the ST_DRXPD_SNDSTS.State, either 0 (stopped) or 1 (started)

        std::string serialNumber;            /// the SFP+ module serial numbers
        bool product;                        /// true=production release version of DRXP card, false=prototyping card
        unsigned int driver_version;         /// driver version number, encoded into integer
        unsigned int fpgaPcieLogic_version;  /// FPGA PCIe logic version number, encoded into integer
        unsigned int fpgaMainLogic_version;  /// FPGA User Logic version number, encoded into integer

        std::string deviceName;
        unsigned int naccepts;
        unsigned int ndiscards;
        unsigned int noverruns;
};

#endif // MC_DRXP_STATUS_HXX

