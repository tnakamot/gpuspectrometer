#ifndef MC_XMLSERVER_HXX
#define MC_XMLSERVER_HXX

#include "network/XMLIO_iface.hxx"       // class XMLIOIface :: str xmlHandover(str)

#include <boost/asio.hpp>
#include <string>

class XMLServerSession;

struct XMLServer
{
    public:
        /// Start listening for clients on given port. Traffic gets send to XMLIOIface::xmlHandover().
        XMLServer(XMLIOIface& callbackObj, int port);

        /// Wait for clients
        void acceptClients();

    private:
        /// Client acceptor; accepts and handles new client, while spawning another listener for accepting future extra clients
        void handle_accept(XMLServerSession* session, const boost::system::error_code& error);

    private:
        XMLIOIface& xmlcallback_;

        const int port_;
        bool error_;

    private:
        boost::asio::io_service service_;
        boost::asio::ip::tcp::endpoint endpoint_;
        boost::asio::ip::tcp::acceptor acceptor_;
        
};

class XMLServerSession
{
    public:
        XMLServerSession(boost::asio::io_service& service, XMLIOIface& callback);
        boost::asio::ip::tcp::socket& getSocket();
        void start();

    private:
        void handle_read_header(const boost::system::error_code& error, size_t nbytes);
        void handle_read_body(const boost::system::error_code& error, size_t nbytes);
        void handle_write_response(const boost::system::error_code& error);

        /// Remove newlines, tabs, trailing whitespace
        void trim_xml(std::string& xml) const;

    private:
        boost::asio::ip::tcp::socket clientsocket_;
        XMLIOIface& xmlcallback_;

        int32_t nbyte_in_;
        char data_in_[32*1024]; // note: boost async_read() with a fixed buffer, could try other streambuf type
        int32_t data_out_[1 + 32*1024/sizeof(int32_t)];
};

#endif // MC_XMLSERVER_HXX
