#include "helper/logger.h"
#include "network/XMLIO_iface.hxx"
#include "network/XMLServer.hxx"
#include "global_defs.hxx" // for DEFAULT_MC_TCP_PORT

#include <boost/thread/mutex.hpp>

class XmlAcceptor : public XMLIOIface
{
    public:
        std::string xmlHandover(std::string xmlIn);
    private:
        boost::mutex mutex_;
};

std::string XmlAcceptor::xmlHandover(std::string xmlIn)
{
    boost::mutex::scoped_lock serializer(mutex_);
    L_(linfo) << "Dummy ASM api XmlAcceptor received command '" << xmlIn << "', echoing it back";
    return xmlIn;
}

int main(void)
{
    XmlAcceptor xmlAcceptor;

    XMLServer srv(xmlAcceptor, DEFAULT_MC_TCP_PORT);
    srv.acceptClients();  // blocking; calls xmlAcceptor.xmlHandover() for each received command

    return 0;
}
