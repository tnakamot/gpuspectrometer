/**
 * \class ASMMime
 *
 * \brief Creates MIME data in ASM MIME format (tiny subset of MIME) as specified in TP Spectrometer ICD.
 *
 */
#ifndef ASMMIME_HXX
#define ASMMIME_HXX

#include <iostream>
#include <string>

#define MIME_MAX_LEVELS 64

class ASMMime
{
    public:
        /** Create ASM writer and direct its output to the given ostream */
        ASMMime(std::ostream& o);

    public:

        /** Start a new ASM MIME 'multipart/related' grouping starting with default type 'text/xml' */
        void beginMultipart();

        /** Start a new ASM MIME 'multipart/related' grouping starting with default type 'text/xml' */
        void beginMultipart(const char* contentdescr, bool related=true);

        /** Add a MIME part that is in XML format */
        void addXML(const char* xml, const std::string& loc);

        /** Add a MIME part that is in XML format */
        void addBinary(const void* octets, size_t len, const std::string& loc);

        /** End a ASM MIME 'multipart/related' grouping opened earlier with beginMultipart(). */
        void endMultipart();

    private:
        std::ostream& m_out;
        int m_level;
        int m_level_ids[MIME_MAX_LEVELS];
        int m_multipart_count;
        const std::string lf;
};

#endif // ASMMIME_HXX

