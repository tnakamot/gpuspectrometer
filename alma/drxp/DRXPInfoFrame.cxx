/**
 * \class DRXPInfoFrame
 *
 * Container for DRXP Information Frame data (48 bytes; timing information).
 *
 */

#include "drxp/DRXPInfoFrame.hxx"

#include <string.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <stdio.h>

/** Take raw 48-byte long ALMA Information Frame and extract the time information from it */
void DRXPInfoFrame::fromByteArray(const unsigned char* d)
{
    frameStatus = ((int)d[8]) | ((int)d[9])<<8 | ((int)d[10])<<16 | ((int)d[11])<<24;
    hwTickCount_48ms =
          ((size_t)d[0])     | ((size_t)d[1])<<8  | ((size_t)d[2])<<16 | ((size_t)d[3])<<24
        | ((size_t)d[4])<<32 | ((size_t)d[5])<<40 | ((size_t)d[6])<<48 | ((size_t)d[7])<<56;
    memcpy(&swRxTime_UTC, d+32, 16);
}

/** Adjust swRxTime_UTC (PC time) to the nearest 8 millisecond multiple. Useful with 1 PPS TE. */
void DRXPInfoFrame::roundTime8ms()
{
    // Make sure that the nanoseconds fraction is not negative
    while (swRxTime_UTC.tv_nsec < 0) {
        swRxTime_UTC.tv_nsec += 1000000000UL;
        swRxTime_UTC.tv_sec--;
    }

    // With 1PPS ticks use 8ms granularity; N*48ms always ends up at some k*8ms (k=0..124; 1000ms/8ms=125)
    // Rounding method is round-to-nearest, see, e.g., https://stackoverflow.com/questions/29557459
    const unsigned long T_GRANULARITY_NS = 8000000;
    swRxTime_UTC.tv_nsec = ((swRxTime_UTC.tv_nsec + T_GRANULARITY_NS/2) / T_GRANULARITY_NS) * T_GRANULARITY_NS;

    // Adjust the time in case we rounded across a 1 second boundary (shouldn't happen)
    while (swRxTime_UTC.tv_nsec >= 1000000000L) {
        swRxTime_UTC.tv_nsec -= 1000000000UL;
        swRxTime_UTC.tv_sec++;
    }
}

/** Adjust swRxTime_UTC (PC time) to the nearest 48 millisecond multiple. Useful with 48ms TE. */
void DRXPInfoFrame::roundTime48ms()
{
    // Make sure that the nanoseconds fraction is not negative
    while (swRxTime_UTC.tv_nsec < 0) {
        swRxTime_UTC.tv_nsec += 1000000000UL;
        swRxTime_UTC.tv_sec--;
    }

    // With 48ms TE use 48ms granularity
    // Rounding method is round-to-nearest, see, e.g., https://stackoverflow.com/questions/29557459
    const unsigned long T_GRANULARITY_NS = 48000000;
    swRxTime_UTC.tv_nsec = ((swRxTime_UTC.tv_nsec + T_GRANULARITY_NS/2) / T_GRANULARITY_NS) * T_GRANULARITY_NS;

    // Adjust the time in case we rounded across a 1 second boundary (shouldn't happen)
    while (swRxTime_UTC.tv_nsec >= 1000000000L) {
        swRxTime_UTC.tv_nsec -= 1000000000UL;
        swRxTime_UTC.tv_sec++;
    }
}

/** Adust swRxTime_UTC (PC time) to nearest multiple of time granularity (e.g., 8000000ns(8ms) or 48000000ns(48ms) or other) */
void DRXPInfoFrame::roundTime(const unsigned long timegranularity_ns)
{
    if (timegranularity_ns == 8000000) {
        roundTime8ms();
    } else if (timegranularity_ns == 48000000) {
        roundTime48ms();
    } else {
        while (swRxTime_UTC.tv_nsec < 0) {
            swRxTime_UTC.tv_nsec += 1000000000UL;
            swRxTime_UTC.tv_sec--;
        }
        swRxTime_UTC.tv_nsec = ((swRxTime_UTC.tv_nsec + timegranularity_ns/2) / timegranularity_ns) * timegranularity_ns;
        while (swRxTime_UTC.tv_nsec >= 1000000000L) {
            swRxTime_UTC.tv_nsec -= 1000000000UL;
            swRxTime_UTC.tv_sec++;
        }
    }
}

/** Adjust swRxTime_UTC (PC time) to fall on a fixed 48ms grid
 * \arg refUTC The reference UTC time to use for setting swRxTime_UTC to refUTC + N*48ms
 * \arg refUTCtick The 48ms tick count at the reference UTC time
 * \return True on success, False if a re-sync appears needed due to too large drift (happens on DRXP Reset commands)
 */
bool DRXPInfoFrame::adjustTime(const struct timespec& refUTC, const unsigned long refUTCtick)
{
    struct timespec new_time = swRxTime_UTC;
    unsigned long new_tick = hwTickCount_48ms;

    // Add integer seconds; lcm(48,1000)=6000 = 125 x 48ms
    const signed long ticks_per_6sec = 125;
    signed long N = new_tick - refUTCtick;
    new_time = refUTC;
    new_time.tv_sec += 6 * (signed long)(N / ticks_per_6sec);

    // Add residual 48ms steps
    N = N % ticks_per_6sec;
    new_time.tv_nsec += N * 48000000UL;
    new_time.tv_sec += (new_time.tv_nsec /1000000000L);
    new_time.tv_nsec = new_time.tv_nsec % 1000000000L;
    while (new_time.tv_nsec >= 1000000000L) {
        new_time.tv_nsec -= 1000000000L;
        new_time.tv_sec++;
    }
    while (new_time.tv_nsec < 0) {
        new_time.tv_nsec += 1000000000L;
        new_time.tv_sec--;
    }

    // Check if the difference became too large
    double T_ref = refUTC.tv_sec + refUTC.tv_nsec*1e-9;
    double T_initial = swRxTime_UTC.tv_sec + swRxTime_UTC.tv_nsec*1e-9;
    double T_adjusted = new_time.tv_sec + new_time.tv_nsec*1e-9;
    //std::cerr << "adjustTime dT=" << 1e3*(T_initial - T_adjusted) << "ms\n";
    if (fabs(T_initial - T_adjusted) > 2*48e-3) {
        std::cerr << "Warning: DRXP data timestamp " << std::fixed << std::setprecision(6)
                  << T_initial << " tick " << hwTickCount_48ms << " vs. "
                  << " adjusted/re-gridded (ref: " << T_ref << " tick:" << refUTCtick << ")"
                  << " timestamp " << T_adjusted
                  << " have drifted apart by " << 1e3*(T_initial - T_adjusted) << "ms" << std::endl;
        return false;
    }

    // Adopt the adjusted time
    swRxTime_UTC = new_time;
    hwTickCount_48ms = new_tick;
    return true;
}

