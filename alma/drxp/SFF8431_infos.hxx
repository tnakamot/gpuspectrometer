#ifndef SFF8431_INFOS_HXX
#define SFF8431_INFOS_HXX

#include <string>

// SFF-8431 SFP+ 10G transceiver I2C registers
// See ftp://ftp.seagate.com/sff/SFF-8431.PDF
//     https://github.com/feuerrot/sfp-i2c
typedef struct SFF8431_Xceiver_infos_tt {
    std::string manufacturer;   /// manufacturer string
    std::string revision;       /// product revision string
    std::string partNumber;     /// vendor specific part numbre string
    std::string serialNumber;   /// module serial number
    int wavelength_nm;          /// module wavelength in nanometers
    int reach_m;                /// fiber optic max path length ("reach") in meters
    int reach_km;               /// fiber optic max path length ("reach") in kilometers
    std::string xceiverFormfactor; /// transceiver form factor e.g. SFP+
    std::string xceiverType;       /// transceiver type e.g. LR
    std::string summary;        /// summary of some of the SFP+ information fields
    bool infoValid;             /// 'true' if object was filled successfully by deviceGetSFPInfos()
    ~SFF8431_Xceiver_infos_tt() {}
} SFF8431_Xceiver_infos_t;

#endif // SFF8431_INFOS_HXX

