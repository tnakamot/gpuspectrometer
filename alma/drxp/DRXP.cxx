/**
 * \class DRXP
 *
 * \brief Low-level access to the DRXP via kernel driver and /dev/drxpd[n] character device ioctl's
 *
 * \author $Author: janw $
 *
 */

#include "drxpd.h"                // DRXP kernel module user-space API; header is part of Elecs' source code package
#undef MAX_SIZE

#include "drxp/DRXP.hxx"
#include "drxp/drxp_const.hxx"    // common DRXP constants
#include "drxp/drxp_bitfields.hxx"// helpful bitfields, structs, and GCC_BITFLD_ORDER defining the bit order
#include "drxp/SFF8431_I2C.h"     // structures for decoding SFF-8431 I2C data fields
#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/SFF8431_infos.hxx"

#include "helper/logger.h"
#include "mcwrapping/mc_drxp_status.hxx"
#include "datarecipients/DataRecipient_iface.hxx"

#define lockscope // helper for tidynes

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <exception>
#include <cstring>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <sys/ioctl.h>
#include <sys/eventfd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////

// Helper macros, somewhat ugly

#ifdef DRXP_REG_DEBUGPRINT
    static void print_mreg(const ST_DRXPD_MONDAT& mon);
    static void print_creg(const ST_DRXPD_CONTDAT& ctl);
    #define READ_MON_REG_DbgPrint(x) print_mreg(x)
    #define WRITE_CTL_REG_DbgPrint(x) print_creg(x)
#else
    #define READ_MON_REG_DbgPrint(x)
    #define WRITE_CTL_REG_DbgPrint(x)
#endif

#include "drxp/drxp_ioctl.hxx"

static bool readMonReg(int fd, unsigned long basereg, dfr_mon_reg_t& mreg)
{
    mreg.mon.ItemID = basereg;
    if (ioctl(fd, DRXPD_GET_MONSTS, &mreg.mon) == -1) {
        DIO_LOG_ERROR << __func__ << " ioctl error, errno=" << errno
           << ", on register " << std::hex << basereg << std::dec << DIO_endl;
        return false;        
    }
    READ_MON_REG_DbgPrint(mreg.mon);
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Convert integer to string */
static std::string int_to_stdstr(int64_t x)
{
    return dynamic_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str();
}

/** Trim a std::string */
static std::string trim(const std::string& str)
{
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    if (first == std::string::npos) return "";
    return str.substr(first, (last-first+1));
}

/** Convert a byte array into 32-bit unsigned int, MSB first at index 0 of the array. */
static uint64_t byteArr4_to_uint32(const unsigned char* msb_first_raw)
{
    uint32_t out = msb_first_raw[0];
    for (int i=1; i<4; i++) {
        out = 256UL*out + msb_first_raw[i];
    }
    return out;
}

/** Convert a byte array into 32-bit unsigned int, MSB first at index 0 of the array. */
static uint64_t byteArr8_to_uint64(const unsigned char* msb_first_raw)
{
    uint64_t out = msb_first_raw[0];
    for (int i=1; i<8; i++) {
        out = 256UL*out + msb_first_raw[i];
    }
    return out;
}

#ifdef DRXP_REG_DEBUGPRINT

/** Print the contents of ST_DRXPD_MONDAT */
static void print_mreg(const ST_DRXPD_MONDAT& mon)
{
    printf("mreg: id=%04X, bytes[0..7]=[%02X %02X %02X %02X | %02X %02X %02X %02X]\n",
        mon.ItemID,
        mon.RdValue[0], mon.RdValue[1], mon.RdValue[2], mon.RdValue[3],
        mon.RdValue[4], mon.RdValue[5], mon.RdValue[6], mon.RdValue[7]
    );
}

/** Print the contents of ST_DRXPD_CONTDAT */
static void print_creg(const ST_DRXPD_CONTDAT& ctl)
{
    printf("creg: id=%04X, bytes[0..7]=[%02X %02X %02X %02X | %02X %02X %02X %02X]\n",
        ctl.ItemID,
        ctl.WrValue[0], ctl.WrValue[1], ctl.WrValue[2], ctl.WrValue[3],
        ctl.WrValue[4], ctl.WrValue[5], ctl.WrValue[6], ctl.WrValue[7]
    );
}

#endif

static int timespec2str(char *buf, int len, struct timespec *ts)
{
    // https://stackoverflow.com/questions/8304259/formatting-struct-timespec
    int ret;
    struct tm t;
    if (gmtime_r(&(ts->tv_sec), &t) == NULL) { return 1; }
    ret = strftime(buf, len, "%F %T", &t);
    if (ret == 0) { return 2; }
    len -= ret - 1;
    ret = snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len) { return 3; }
    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Private func, open the device */
void DRXP::deviceOpen(bool rx)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    // TODO: what if device already opened?
    m_usermode = DRXPIface::PUSH;

    // Open the DRXP char device
    if (rx) {
        m_fd = open(m_dev.c_str(), O_RDONLY);
    } else {
        m_fd = open(m_dev.c_str(), O_RDWR);
    }
    if (m_fd < 0) {
        L_(lerror) << "Failed to open " << m_dev << " for DRXP readout";
        // TODO: make fatal?
        throw std::logic_error("Failed to open char dev");
    }

    // Defaults
    m_rx_mode = rx;
    m_rxtx_started = false;
    m_shutdown_rx_thread = false;
    m_shutdown_mon_thread = false;
    m_naccepts = 0;
    m_ndiscards = 0;
    m_noverruns = 0;
    m_ngroups_avail = 0;
    m_reference_tickcount = 0;
    m_reference_set = false;
    memset(&m_group_busy, 0, sizeof(m_group_busy));
    memset(&m_groups_avail, 0, sizeof(m_groups_avail));

    // Set up groups
    DRXP_IOCTL(m_fd, DRXPD_RCVGROUPS, DRXP_NUM_GROUPS_DEFAULT);

    // Prepare memory mapping in advance of any use
    if (rx) {
        m_maddr_size = DRXP_RX_GROUP_SIZE * DRXP_NUM_GROUPS_DEFAULT;
        m_maddr = mmap(NULL, m_maddr_size, PROT_READ, MAP_SHARED, m_fd, 0);
    } else {
        m_maddr_size = DRXP_TX_SIZE;
        m_maddr = mmap(NULL, m_maddr_size, PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, 0);
    }
    if (m_maddr == (void*)-1) {
        L_(lerror) << __func__ << " mmap error on " << m_dev << ", errno=" << errno << ", " << strerror(errno) << std::dec;
        throw std::logic_error("Failed to mmap() char dev");
    }

    // Determine card version; 'prototype' uses UTC time (originally TAI), 'production' uses UTC time
    dfr_mon_reg_t mreg;
    mreg.mon.ItemID = GET_DFR_SN_ID;
    ioctl(m_fd, DRXPD_GET_MONSTS, &mreg.mon);
    if ((mreg.mon.RdValue[3] & 1) == 0) {
        L_(linfo) << "Prototype DRXP detected, assuming DRXP returns UTC timestamps";
    } else {
        L_(linfo) << "Production DRXP detected, assuming DRXP returns UTC timestamps";
    }

#ifdef HAVE_DRXP_SEU_DETECTION
    // Enable SEU monitoring
    ST_DRXPD_CONTDAT st_con;
    ST_SEU_STATUS *pst_sts = (ST_SEU_STATUS*)st_con.WrValue;
    st_con.ItemID = SET_SEU_STATUS_ID;
    pst_sts->uiStatus = SEU_STATUS_OBSERVATION;
    ioctl(m_fd, DRXPD_SET_CONTROL, &st_con);
#endif

    L_(linfo) << "Opened " << m_dev << " in mode rx=" << rx;
}

/* Private func, close the device */
void DRXP::deviceClose()
{
    boost::mutex::scoped_lock iolock(m_iomutex);

    stopPeriodicMonitoring();
    stopRx();
    stopTx();

    if (m_maddr != NULL) {
        munmap(m_maddr, m_maddr_size);
        m_maddr = NULL;
    }
    if (m_evtfd >= 0) {
        close(m_evtfd);
        m_evtfd = -1;
    }
    if (m_fd >= 0) {
        close(m_fd);
        m_fd = -1;
        L_(linfo) << "Closed " << m_dev;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Prepare DRXP object, without open any device yet (use selectDevice() for that) */
DRXP::DRXP()
{
    // Make sure that compiler produces the expected bitfield order
    dfr_mon_reg_t tmp;
    tmp.mon.RdValue[0] = 0x01;
    if (tmp.volSts.byte0.GCC_BITFLD_ORDER.p1_35VDdrForDdr != true) {
        std::string msg("GCC_BITFLD_ORDER in DRXP C++ module does not match actual bitfield order, please check DRXP_aux.hxx and recompile!");
        L_(lerror) << msg;
        std::cerr << __func__ << " : " << msg << std::endl;
        throw std::logic_error(msg.c_str());
    }

    // Defaults
    m_fd = -1;
    m_maddr = NULL;
    m_maddr_size = 0;
    m_monitoringThread = NULL;
    m_shutdown_rx_thread = false;
    m_shutdown_mon_thread = false;
    m_rx_recipient = NULL;
    m_rxtx_started = false;
    m_simpattern = memalign(4096, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);
    m_timestamp_granularity_ns = 48000000;
}

/** Close the DRXP device, without resetting any previously set alarms or flags on the card */
DRXP::~DRXP()
{
    deviceClose();
    free(m_simpattern);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Get pointers and length of all 'groups' in raw DRXP data area. Intended to help (un)pinning in CUDA. */
bool DRXP::getBufferPtrs(size_t* Ngroups, void** groupbuf, size_t* len)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_maddr != NULL) {
        *Ngroups = DRXP_NUM_GROUPS_DEFAULT;
        for (size_t n = 0; n < DRXP_NUM_GROUPS_DEFAULT; n++) {
            groupbuf[n] = (void*)((char*)m_maddr + DRXP_RX_GROUP_SIZE * n);
        }
        *len = DRXP_RX_GROUP_SIZE;
        return true;
    } else {
        *Ngroups = 0;
        *len = 0;
        groupbuf[0] = 0;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Change to another DRXP board */
bool DRXP::selectDevice(int iface_nr, bool open_rdonly)
{
    // Derive a char dev name
    m_dev = std::string("/dev/drxpd") + int_to_stdstr(iface_nr);
    return selectDevice(m_dev, open_rdonly);
}

/** Change to another DRXP board */
bool DRXP::selectDevice(std::string dev_name, bool open_rdonly)
{
    // Config
    m_dev = dev_name;
    m_monitoringThread = NULL;
    m_rxDatapumpThread = NULL;
    m_rx_recipient = NULL;
    m_rx_mode = open_rdonly;
    m_rxtx_started = false;

    // Open the device
    deviceOpen(m_rx_mode);

    // Read out SFP+ details once (manufacturer, part number, etc); monitoring them is not necessary
    deviceGetSFPInfos(0, m_sfpInfos[0]);
    deviceGetSFPInfos(1, m_sfpInfos[1]);
    deviceGetSFPInfos(2, m_sfpInfos[2]);

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Reset all flags and alarms */
bool DRXP::resetDevice()
{
    ST_DRXPD_CONTDAT ctl;

    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Master reset
    memset(&ctl, 0x00, sizeof(ctl));
    ctl.WrValue[0] = 1<<7;  // DFR_RST : bit7: Master Reset, bit0: clear Status and Voltage Status registers
    WRITE_CTL_REG(DFR_RST_ID_ALL, ctl);
    L_(linfo) << "Requested a full reset of " << m_dev;

    // Elecs recommends a ~1 second wait after master reset. Sleep 1.5 seconds.
    if (1) {
        struct timespec req={1,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Reset individual: checksums, sync statuses, status and voltage warnings/alarms
    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_CHK_ID_ALL, ctl);
    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_SYNC_ID_ALL, ctl);
    memset(&ctl, 0x00, sizeof(ctl));
    ctl.WrValue[0] = 0x01; // DFR_RST : bit7: Master Reset, bit0: clear Status and Voltage Status registers
    WRITE_CTL_REG(DFR_RST_ID_ALL, ctl);

    // Do an additional but shorter wait after clearing status registers
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Reset internal states
    m_naccepts = 0;
    m_ndiscards = 0;
    m_noverruns = 0;
    m_ngroups_avail = 0;
    m_reference_set = false;

    return true;
}

/** Reset all alarms */
bool DRXP::resetAlarms()
{
    ST_DRXPD_CONTDAT ctl;

    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Clear the Status, Voltage Status, Sync registers
    memset(&ctl, 0x00, sizeof(ctl));
    ctl.WrValue[0] = 1;  // DFR_RST : bit7: Master Reset, bit0: clear Status and Voltage Status registers
    WRITE_CTL_REG(DFR_RST_ID_ALL, ctl);

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Reset internal synchronization engine on DRXP, forcing a hardware re-sync. */
bool DRXP::resetSync()
{
    ST_DRXPD_CONTDAT ctl;

    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_SYNC_ID_ALL, ctl);

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Reset all checksums */
bool DRXP::resetChecksums()
{
    ST_DRXPD_CONTDAT ctl;

    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_CHK_ID_ALL, ctl);

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Read out all device information, flags, alarms, and statuses */
bool DRXP::deviceReadout(MCDRXPStatus& mc)
{
    bool ok = true;
    ok &= versionReadout(mc);
    ok &= dcmstatusReadout(mc);
    ok &= voltageReadout(mc);
    ok &= opticalReadout(mc);
    ok &= counterReadout(mc);
    ok &= syncReadout(mc);
    ok &= receiveStatusReadout(mc);
    ok &= sendStatusReadout(mc);
    ok &= temperaturesReadout(mc);
    ok &= fpgaSEUReadout(mc);

    mc.naccepts = m_naccepts;
    mc.ndiscards = m_ndiscards;
    mc.noverruns = m_noverruns;

    //READ_MON_REG(GET_DFR_TST_DAT_ID_D, mreg_D.mon);
    //READ_MON_REG(GET_DFR_TST_DAT_ID_C, mreg_C.mon);
    //READ_MON_REG(GET_DFR_TST_DAT_ID_B, mreg_B.mon);
    // manual: "refer to DFR_TEST_DATA", byte 0 is used only, bytes 1-7 are reserved

    return ok;
}

/** Read out board version information */
bool DRXP::versionReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg;
    bool i2c_ok;

    if (m_fd < 0) {
        return false;
    }

    mc.deviceName = m_dev;

    i2c_ok = readMonReg(m_fd, GET_DFR_SN_ID, mreg);
    mc.serialNumber = int_to_stdstr(byteArr4_to_uint32(mreg.mon.RdValue) >> 8);
    mc.product = (mreg.mon.RdValue[3] & 1) != 0;

    i2c_ok = readMonReg(m_fd, GET_DFR_VER_ID, mreg);
    mc.driver_version = byteArr4_to_uint32(mreg.mon.RdValue) >> 8; // maj, min, micro, <reserved 0x00>
    mc.fpgaMainLogic_version = byteArr4_to_uint32(mreg.mon.RdValue + 4);
    mc.fpgaPcieLogic_version = mc.fpgaMainLogic_version; // TODO, not found in driver specs!

    mc.invariant();
    return i2c_ok;
}

/** Read out all counters */
bool DRXP::counterReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg_D, mreg_C, mreg_B;
    bool i2c_ok_D, i2c_ok_C, i2c_ok_B;

    if (m_fd < 0) {
        return false;
    }

    i2c_ok_D = readMonReg(m_fd, GET_DFR_CHK_CNT_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_DFR_CHK_CNT_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_DFR_CHK_CNT_ID_B, mreg_B);
    mc.checksumCounter.ch1BitD = byteArr8_to_uint64(mreg_D.mon.RdValue);
    mc.checksumCounter.ch2BitC = byteArr8_to_uint64(mreg_C.mon.RdValue);
    mc.checksumCounter.ch3BitB = byteArr8_to_uint64(mreg_B.mon.RdValue);

    i2c_ok_D = readMonReg(m_fd, GET_DFR_SYNC_ERR_CNT_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_DFR_SYNC_ERR_CNT_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_DFR_SYNC_ERR_CNT_ID_B, mreg_B);
    mc.syncErrorCounter.ch1BitD = byteArr4_to_uint32(mreg_D.mon.RdValue);
    mc.syncErrorCounter.ch2BitC = byteArr4_to_uint32(mreg_C.mon.RdValue);
    mc.syncErrorCounter.ch3BitB = byteArr4_to_uint32(mreg_B.mon.RdValue);

    mc.invariant();
    return (i2c_ok_D && i2c_ok_C && i2c_ok_B);
}

/** Read out DFR Status */
bool DRXP::dcmstatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg;
    bool i2c_ok;

    if (m_fd < 0) {
        return false;
    }

    i2c_ok = readMonReg(m_fd, GET_DFR_STS_ID, mreg);

    // Note: Elecs driver spec DFR_STATUS in 6.1.4 says "PLL Locked" but actually the fields mean "PLL Unlocked Alarm"
    mc.dfrStatus.referenceClock125MhzPllUnlock  = mreg.dfrSts.byte0.GCC_BITFLD_ORDER.referenceClock125MhzPllLock;
    mc.dfrStatus.ddrClock200MhzPll2Unlock       = mreg.dfrSts.byte0.GCC_BITFLD_ORDER.ddrClock200MhzPll2Lock;
    mc.dfrStatus.ddrClock200MhzPll1Unlock       = mreg.dfrSts.byte0.GCC_BITFLD_ORDER.ddrClock200MhzPll1Lock;
    mc.dfrStatus.dataInputClockPllUnlockCh1BitD = mreg.dfrSts.byte1.GCC_BITFLD_ORDER.dataInputClockPllLockCh1BitD;
    mc.dfrStatus.dataInputClockPllUnlockCh2BitC = mreg.dfrSts.byte1.GCC_BITFLD_ORDER.dataInputClockPllLockCh2BitC;
    mc.dfrStatus.dataInputClockPllUnlockCh3BitB = mreg.dfrSts.byte1.GCC_BITFLD_ORDER.dataInputClockPllLockCh3BitB;
    mc.dfrStatus.syncErrorDetectedCh1BitD       = mreg.dfrSts.byte2.GCC_BITFLD_ORDER.syncErrorDetectedCh1BitD;
    mc.dfrStatus.syncErrorDetectedCh2BitC       = mreg.dfrSts.byte2.GCC_BITFLD_ORDER.syncErrorDetectedCh2BitC;
    mc.dfrStatus.syncErrorDetectedCh3BitB       = mreg.dfrSts.byte2.GCC_BITFLD_ORDER.syncErrorDetectedCh3BitB;

    mc.invariant();
    return i2c_ok;
}

/** Read out FPGA related voltage alarms (FPGA, on-FPGA MGT, Vddr) and SFP+ 3.3V voltage warnings/alarms */
bool DRXP::voltageReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg, mreg_D, mreg_C, mreg_B;
    bool i2c_ok, i2c_ok_D, i2c_ok_C, i2c_ok_B;

    if (m_fd < 0) {
        return false;
    }

    i2c_ok = readMonReg(m_fd, GET_DFR_VOL_STS_ID, mreg);
    mc.dfrVoltageStatus.p0_95VForFpgaCore     = mreg.volSts.byte0.GCC_BITFLD_ORDER.p0_95VForFpgaCore;
    mc.dfrVoltageStatus.p1_8VForFpga          = mreg.volSts.byte0.GCC_BITFLD_ORDER.p1_8VForFpga;
    mc.dfrVoltageStatus.p1_0VmgtavccForFpga   = mreg.volSts.byte0.GCC_BITFLD_ORDER.p1_0VmgtavccForFpga;
    mc.dfrVoltageStatus.p1_2VmgtavttForFpga   = mreg.volSts.byte0.GCC_BITFLD_ORDER.p1_2VmgtavttForFpga;
    mc.dfrVoltageStatus.p1_8VmgtvccauxForFpga = mreg.volSts.byte0.GCC_BITFLD_ORDER.p1_8VmgtvccauxForFpga;
    mc.dfrVoltageStatus.p3_3VForFpgaIo        = mreg.volSts.byte0.GCC_BITFLD_ORDER.p3_3VForFpgaIo;
    mc.dfrVoltageStatus.p1_35VDdrForDdr       = mreg.volSts.byte0.GCC_BITFLD_ORDER.p1_35VDdrForDdr;

    i2c_ok_D = readMonReg(m_fd, GET_POW_ALM_REG_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_POW_ALM_REG_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_POW_ALM_REG_ID_B, mreg_B);
    mc.powerAlarm.ch1BitD.p3_3VAlarm   = mreg_D.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VAlarm;
    mc.powerAlarm.ch1BitD.p3_3VWarning = mreg_D.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VWarning;
    mc.powerAlarm.ch1BitD.i2cAccessError = !i2c_ok_D;
    mc.powerAlarm.ch2BitC.p3_3VAlarm   = mreg_C.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VAlarm;
    mc.powerAlarm.ch2BitC.p3_3VWarning = mreg_C.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VWarning;
    mc.powerAlarm.ch2BitC.i2cAccessError = !i2c_ok_C;
    mc.powerAlarm.ch3BitB.p3_3VAlarm   = mreg_B.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VAlarm;
    mc.powerAlarm.ch3BitB.p3_3VWarning = mreg_B.pwrAlm.byte0.GCC_BITFLD_ORDER.p3_3VWarning;
    mc.powerAlarm.ch3BitB.i2cAccessError = !i2c_ok_B;

    mc.invariant();
    return i2c_ok;
}

/** Read out optical power measuremets and warnings/alarms */
bool DRXP::opticalReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg_D, mreg_C, mreg_B;
    bool i2c_ok_D, i2c_ok_C, i2c_ok_B;

    if (m_fd < 0) {
        return false;
    }

    // Read SIG_AVG: signal average optical power in nW units, 24-bit
    i2c_ok_D = readMonReg(m_fd, GET_SIG_AVG_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_SIG_AVG_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_SIG_AVG_ID_B, mreg_B);

    // TODO: XML Schema does not yet have a command to check the optical power readings (?)
    mc.drxpOpticalPower.ch1BitD_power_nW = byteArr4_to_uint32(mreg_D.mon.RdValue) >> 8;
    mc.drxpOpticalPower.ch2BitC_power_nW = byteArr4_to_uint32(mreg_C.mon.RdValue) >> 8;
    mc.drxpOpticalPower.ch3BitB_power_nW = byteArr4_to_uint32(mreg_B.mon.RdValue) >> 8;
    mc.drxpOpticalPower.ch1BitD_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch1BitD_power_nW*1e-9 / 0.001);
    mc.drxpOpticalPower.ch2BitC_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch2BitC_power_nW*1e-9 / 0.001);
    mc.drxpOpticalPower.ch3BitB_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch3BitB_power_nW*1e-9 / 0.001);

    // Read alarms/warnings of all optical link triplet registers
    i2c_ok_D = readMonReg(m_fd, GET_ALM_STS_REG_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_ALM_STS_REG_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_ALM_STS_REG_ID_B, mreg_B);
    mc.alarmStatus.ch1BitD.temperatureWarning       = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.temperatureWarning;
    mc.alarmStatus.ch1BitD.outputBiasCurrentWarning = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentWarning;
    mc.alarmStatus.ch1BitD.outputPowerWarning       = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.outputPowerWarning;
    mc.alarmStatus.ch1BitD.rxInputPowerWarning      = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerWarning;
    mc.alarmStatus.ch1BitD.temperatureAlarm         = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.temperatureAlarm;
    mc.alarmStatus.ch1BitD.outputBiasCurrentAlarm   = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentAlarm;
    mc.alarmStatus.ch1BitD.outputPowerAlarm         = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.outputPowerAlarm;
    mc.alarmStatus.ch1BitD.rxInputPowerAlarm        = mreg_D.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerAlarm;
    mc.alarmStatus.ch1BitD.i2cAccessError           = !i2c_ok_D;
    mc.alarmStatus.ch2BitC.temperatureWarning       = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.temperatureWarning;
    mc.alarmStatus.ch2BitC.outputBiasCurrentWarning = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentWarning;
    mc.alarmStatus.ch2BitC.outputPowerWarning       = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.outputPowerWarning;
    mc.alarmStatus.ch2BitC.rxInputPowerWarning      = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerWarning;
    mc.alarmStatus.ch2BitC.temperatureAlarm         = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.temperatureAlarm;
    mc.alarmStatus.ch2BitC.outputBiasCurrentAlarm   = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentAlarm;
    mc.alarmStatus.ch2BitC.outputPowerAlarm         = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.outputPowerAlarm;
    mc.alarmStatus.ch2BitC.rxInputPowerAlarm        = mreg_C.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerAlarm;
    mc.alarmStatus.ch2BitC.i2cAccessError           = !i2c_ok_C;
    mc.alarmStatus.ch3BitB.temperatureWarning       = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.temperatureWarning;
    mc.alarmStatus.ch3BitB.outputBiasCurrentWarning = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentWarning;
    mc.alarmStatus.ch3BitB.outputPowerWarning       = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.outputPowerWarning;
    mc.alarmStatus.ch3BitB.rxInputPowerWarning      = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerWarning;
    mc.alarmStatus.ch3BitB.temperatureAlarm         = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.temperatureAlarm;
    mc.alarmStatus.ch3BitB.outputBiasCurrentAlarm   = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.outputBiasCurrentAlarm;
    mc.alarmStatus.ch3BitB.outputPowerAlarm         = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.outputPowerAlarm;
    mc.alarmStatus.ch3BitB.rxInputPowerAlarm        = mreg_B.almSts.byte0.GCC_BITFLD_ORDER.rxInputPowerAlarm;
    mc.alarmStatus.ch3BitB.i2cAccessError           = !i2c_ok_B;

    mc.invariant();
    return (i2c_ok_D && i2c_ok_C && i2c_ok_B);
}

/** Read out ALMA frame decoder sync status and metaframe delay */
bool DRXP::syncReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg_D, mreg_C, mreg_B;
    bool i2c_ok_D, i2c_ok_C, i2c_ok_B;

    if (m_fd < 0) {
        return false;
    }

    i2c_ok_D = readMonReg(m_fd, GET_DFR_SYNC_STS_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_DFR_SYNC_STS_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_DFR_SYNC_STS_ID_B, mreg_B);
    mc.syncLockStatus.ch1BitD.syncUnlock     = (mreg_D.mon.RdValue[0] & 0x80) == 0; // bit7: 1=sync'ed, 0=unlocked
    mc.syncLockStatus.ch1BitD.syncLockOffset = mreg_D.mon.RdValue[1] + 256L*(mreg_D.mon.RdValue[0] & 1); // byte0 bit0 = MSB of 9-bit offset
    mc.syncLockStatus.ch2BitC.syncUnlock     = (mreg_C.mon.RdValue[0] & 0x80) == 0;
    mc.syncLockStatus.ch2BitC.syncLockOffset = mreg_C.mon.RdValue[1] + 256L*(mreg_C.mon.RdValue[0] & 1);
    mc.syncLockStatus.ch3BitB.syncUnlock     = (mreg_B.mon.RdValue[0] & 0x80) == 0;
    mc.syncLockStatus.ch3BitB.syncLockOffset = mreg_B.mon.RdValue[1] + 256L*(mreg_B.mon.RdValue[0] & 1);

    i2c_ok_D = readMonReg(m_fd, GET_DFR_MTFRM_DLY_ID_D, mreg_D);
    i2c_ok_C = readMonReg(m_fd, GET_DFR_MTFRM_DLY_ID_C, mreg_C);
    i2c_ok_B = readMonReg(m_fd, GET_DFR_MTFRM_DLY_ID_B, mreg_B);
    mc.metaframeDelay.ch1BitD = byteArr4_to_uint32(mreg_D.mon.RdValue); // Note: DRXP driver v1.0.7 says count 0 to 0xFFFFFFFF,
    mc.metaframeDelay.ch2BitC = byteArr4_to_uint32(mreg_C.mon.RdValue); //       but ICD / XML Schema says no input signal if count is 0x80000000 !?
    mc.metaframeDelay.ch3BitB = byteArr4_to_uint32(mreg_B.mon.RdValue);

    mc.invariant();
    return (i2c_ok_D && i2c_ok_C && i2c_ok_B);
}

/** Read out DRXP Receive Status */
bool DRXP::receiveStatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    ST_DRXPD_RCVSTS rxstat;

    if (m_fd < 0) {
        return false;
    }

    rxstat.State = 0;
    if (m_rx_mode) {
        DRXP_IOCTL_PTR(m_fd, DRXPD_GET_RCVSTS, &rxstat);
    }
    mc.drxpStarted = rxstat.State;

    mc.invariant();
    return true;
}

/** Read out DRXP Send Status */
bool DRXP::sendStatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    ST_DRXPD_SNDSTS txstat;

    if (m_fd < 0) {
        return false;
    }

    txstat.State = 0;
    if (!m_rx_mode) {
        DRXP_IOCTL_PTR(m_fd, DRXPD_GET_SNDSTS, &txstat);
    }
    mc.drxpSending = txstat.State;

    mc.invariant();
    return true;
}

/** Read out DRXP Temperatures */
bool DRXP::temperaturesReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    dfr_mon_reg_t mreg;
    bool i2c_ok;

    if (m_fd < 0) {
        return false;
    }

    i2c_ok = readMonReg(m_fd, GET_FPGA_TEMP_ID, mreg);
    mc.drxpTemperatures.fpga = mreg.dfrTemp.tempC;

    // TODO
    mc.drxpTemperatures.xfp1 = 0.0f;
    mc.drxpTemperatures.xfp2 = 0.0f;
    mc.drxpTemperatures.xfp3 = 0.0f;

    mc.invariant();
    return i2c_ok;
}

/** Read out DRXP FPGA Single Event Upset error counts and status */
bool DRXP::fpgaSEUReadout(MCDRXPStatus& mc)
{
#ifdef HAVE_DRXP_SEU_DETECTION
    boost::mutex::scoped_lock iolock(m_iomutex);
    ST_DRXPD_MONDAT st_mon;

    // SEU Status
    ST_SEU_STATUS *pst_sts = (ST_SEU_STATUS*)st_mon.RdValue;
    st_mon.ItemID = GET_SEU_STATUS_ID;
    memset(st_mon.RdValue, 0, 8);
    ioctl(m_fd, DRXPD_GET_MONSTS, &st_mon);
    mc.fpgaSEUInfo.status = pst_sts->uiStatus;
    mc.fpgaSEUInfo.error = pst_sts->uiError;

    // SEU Counts
    ST_SEU_ERRCNT *pst_cnt = (ST_SEU_ERRCNT*)st_mon.RdValue;
    st_mon.ItemID = GET_SEU_ERRCNT_ID;
    memset(st_mon.RdValue, 0, 8);
    ioctl(m_fd, DRXPD_GET_MONSTS, &st_mon);
    mc.fpgaSEUInfo.corErrCnt = pst_cnt->uiCorErrCnt;
    mc.fpgaSEUInfo.uncorErrCnt = pst_cnt->uiUnCorErrCnt;

    // Monitoring duration
    ST_SEU_ELPSD *pst_elps = (ST_SEU_ELPSD*)st_mon.RdValue;
    st_mon.ItemID = GET_SEU_ELAPSED_ID;
    memset(st_mon.RdValue, 0, 8);
    ioctl(m_fd, DRXPD_GET_MONSTS, &st_mon);
    mc.fpgaSEUInfo.elapsedObs = pst_elps->uiObs;
    mc.fpgaSEUInfo.elapsedTot = pst_elps->uiTot;

    if (m_debugFileEnabled) {
        m_debugFile << "fpga_seu status " << mc.fpgaSEUInfo.status << ", error " << mc.fpgaSEUInfo.error << ", corErrCnt " << mc.fpgaSEUInfo.corErrCnt << ", unCorErrCnt " << mc.fpgaSEUInfo.uncorErrCnt
                    << ", elapsed " << mc.fpgaSEUInfo.elapsedObs << ", totaltime " << mc.fpgaSEUInfo.elapsedTot << "\n";
    }
    std::stringstream s;
    s << "SEU data " << m_dev << " fpga_seu status " << mc.fpgaSEUInfo.status << ", error " << mc.fpgaSEUInfo.error << ", corErrCnt " << mc.fpgaSEUInfo.corErrCnt << ", unCorErrCnt " << mc.fpgaSEUInfo.uncorErrCnt
      << ", elapsed " << mc.fpgaSEUInfo.elapsedObs << ", totaltime " << mc.fpgaSEUInfo.elapsedTot;
    L_(linfo) << s.str();
#else
    mc.fpgaSEUInfo.status = 8; // SEU_STATUS_IDLE
    mc.fpgaSEUInfo.error = 0;
    mc.fpgaSEUInfo.corErrCnt = 0;
    mc.fpgaSEUInfo.uncorErrCnt = 0;
    mc.fpgaSEUInfo.elapsedObs = 0;
    mc.fpgaSEUInfo.elapsedTot = 0;
    L_(linfo) << "DRXP FPGA firmware does not support SEU detection";
#endif
    mc.invariant();
    return true;
}

/** Read out internal SFP+ transceiver information via I2C */
bool DRXP::deviceGetSFPInfos(const int slot, SFF8431_Xceiver_infos_tt& xi)
{
    xi.infoValid = false;
    if (m_fd < 0) {
        return false;
    }

    // For details on I2C addresses and data fields, see https://www.finisar.com/sites/default/files/resources/AN_2030_DDMI_for_SFP_Rev_E2.pdf
    // The SFP+ I2C-addressable memory behind each bus address (0xA0, 0xA2) is 256 bytes long.
    // Note however that the DRXP I2C buffer is just 96 bytes long.

    ST_DRXPD_I2CRD i2c_template;
    memset(&i2c_template, 0x00, sizeof(i2c_template));
    i2c_template.sfp_no = slot % 3;

    sff8431_A0h_fields_t i2c_fields_A0h;
    unsigned char* raw_i2c_0xA0 = (unsigned char*)&i2c_fields_A0h;

    // Read the entire 0xA0 address buffer
    ST_DRXPD_I2CRD i2c = i2c_template;
    i2c.i2c_address = 0xA0;
    for (size_t i=0; i<sizeof(i2c_fields_A0h); ) {
        size_t nremain = sizeof(i2c_fields_A0h) - i;
        size_t ngetnow = (sizeof(i2c.RdValue) <= nremain) ? sizeof(i2c.RdValue) : nremain;
        i2c.data_address = i;
        i2c.length = ngetnow;
        lockscope {
            boost::mutex::scoped_lock iolock(m_iomutex);
            DRXP_I2C(m_fd, &i2c);
        }
        memcpy(raw_i2c_0xA0+i, i2c.RdValue, ngetnow);
        i += ngetnow;
    }

    xi.manufacturer = trim(std::string(i2c_fields_A0h.vendor_name, i2c_fields_A0h.vendor_name+sizeof(i2c_fields_A0h.vendor_name)));
    xi.partNumber = trim(std::string(i2c_fields_A0h.partnumber, i2c_fields_A0h.partnumber+sizeof(i2c_fields_A0h.partnumber)));
    xi.serialNumber = trim(std::string(i2c_fields_A0h.serialnumber, i2c_fields_A0h.serialnumber+sizeof(i2c_fields_A0h.serialnumber)));
    xi.revision = trim(std::string(i2c_fields_A0h.revision, i2c_fields_A0h.revision+sizeof(i2c_fields_A0h.revision)));
    xi.wavelength_nm = ((int)i2c_fields_A0h.wavelength[0])*256 + i2c_fields_A0h.wavelength[1];
    xi.xceiverFormfactor = trim(std::string(g_SFF8431_identifier_names[i2c_fields_A0h.identifier]));
    if (i2c_fields_A0h.max_9um_len_km > 0) {
        xi.reach_m = 1000 * i2c_fields_A0h.max_9um_len_km;
    } else if (i2c_fields_A0h.max_9um_len_m > 0) {
        xi.reach_m = i2c_fields_A0h.max_9um_len_m;
    } else if (i2c_fields_A0h.max_OM2_len_10m > 0) {
        xi.reach_m = 10 * i2c_fields_A0h.max_OM2_len_10m;
    } else if (i2c_fields_A0h.max_OM1_len_10m > 0) {
        xi.reach_m = 10 * i2c_fields_A0h.max_OM1_len_10m;
    } else if (i2c_fields_A0h.max_dac_len_m > 0) {
        xi.reach_m = i2c_fields_A0h.max_dac_len_m;
    } else if (i2c_fields_A0h.max_50um_len_10m > 0) {
        xi.reach_m = 10 * i2c_fields_A0h.max_50um_len_10m;
    }
    xi.reach_km = xi.reach_m / 1000;

    std::stringstream ss;
    ss << m_dev << " SFP+ transceiver " << 1+(slot%3) << " : "
       << xi.manufacturer
       << ", part# " << xi.partNumber << " rev " << xi.revision
       << ", serial# " << xi.serialNumber
       << ", " << xi.wavelength_nm << " nm " << xi.xceiverFormfactor
       << " for " << xi.reach_km << " km reach";

    xi.summary = ss.str();
    xi.infoValid = true;

    L_(linfo) << xi.summary;

/*
    // Read the entire 0xA2 address buffer
    unsigned char raw_i2c_0xA2[256];
    i2c = i2c_template;
    i2c.i2c_address = 0xA2;
    for (size_t i=0; i<sizeof(raw_i2c_0xA2); ) {
        size_t nremain = sizeof(raw_i2c_0xA2) - i;
        size_t ngetnow = (sizeof(i2c.RdValue) <= nremain) ? sizeof(i2c.RdValue) : nremain;
        i2c.data_address = i;
        i2c.length = ngetnow;
        DRXP_I2C(m_fd, &i2c);
        memcpy(raw_i2c_0xA0+i, i2c.RdValue, ngetnow);
        i += ngetnow;
    }
    // TODO: check again if there is some useful information in 0xA2 after all...
*/

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Attach a data processing recipient that gets the received data once/while RX is running.
 * The recipient object must provide a function 'takeData(data_t*)' into which DRXP RX data will be pushed.
 */
bool DRXP::attachRxRecipient(DataRecipient* dr)
{   
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_rx_recipient = dr;
    return true;
}

/** Start DRXP RX data pump (thread).
 * The data pump rxDatapumpThread() waits for a new group of 48ms of raw data.
 * The new group is held "reserved" and is passed to the recipient via takeData().
 * The recipient should transfer the data (DMA onto GPU) and block until the transfer completes.
 * Once takeData() returns, the data pump "releases" the group, allowing DRXP DMA to fill it with new data later.
 */
bool DRXP::startRx()
{
    // Already running?
    if (m_rxDatapumpThread != NULL) { // equiv. to 'm_rxtx_started=true'
        L_(lwarning) << "Attempted to start RX, but RX data pump background thread already active!";
        return false;
    }

    // Make sure the device is open in the correct mode
    if (!m_rx_mode) {
       deviceClose();
       deviceOpen(/*rx=*/true);
    }

    // Lock m_iomutex and prepare RX
    lockscope {
        boost::mutex::scoped_lock iolock(m_iomutex);
        int ioctl_result = 0;

        // Modify the memory map protection
        if (mprotect(m_maddr, m_maddr_size, PROT_READ) < 0) {
             L_(lerror) << __func__ << " mprotect error, errno=" << errno << std::dec;
             //return false;
        }

        // Prepare EventFD
        m_evtfd = eventfd(0, 0);
        DRXP_IOCTL(m_fd, DRXPD_SET_RCVEVT, m_evtfd);

        // Set up reception
        DRXP_IOCTL_IPTR(m_fd, DRXPD_INIT, &ioctl_result);
        DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVEVTSTART, &ioctl_result);
        uint64_t evt;
        if (read(m_evtfd, &evt, sizeof(uint64_t)) < 0) {
            L_(lerror) << __func__ << "() failed to read from event-fd (errno=" << errno << "), cannot start data reception";
            return false;
        }
        DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVSTART, &ioctl_result);

        // Launch datapump thread
        if (m_usermode == DRXPIface::PUSH) {
            m_shutdown_rx_thread = false;
            m_rxDatapumpThread = new boost::thread(&DRXP::rxDatapumpThread, this, &m_rx_recipient);
        } else { 
            m_shutdown_rx_thread = false;
            m_rxDatapumpThread = NULL;
        }
    }

    return true;
}

/** Signal the RX data pump to stop and wait until it finishes */
bool DRXP::stopRx()
{
    if ((m_fd < 0) || !m_rxtx_started || !m_rx_mode) {
        return false;
    }

    if (m_rxtx_started) {
        boost::mutex::scoped_lock iolock(m_iomutex);
        int ioctl_result = 0;
        DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVSTOP, &ioctl_result);
        DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVEVTSTOP, &ioctl_result);
        // note: the v1.0.7 driver documentation provides this order (stop first, then evtstop last)
        L_(ldebug) << "Stopped DRXP board I/O, ioctl() result = " << ioctl_result;
    }

    if (m_rxDatapumpThread != NULL) {
        m_shutdown_rx_thread = true;
        m_rxDatapumpThread->interrupt();
        m_rxDatapumpThread->join();
        delete m_rxDatapumpThread;
        m_rxDatapumpThread = NULL;
        m_shutdown_rx_thread = false;
    }
    m_rxtx_started = false;
    m_reference_tickcount = 0;
    m_reference_set = false;
    memset(&m_group_busy, 0, sizeof(m_group_busy));

    return true;
}

/** Change the granularity of software timestamp re-alignment */
void DRXP::setTimestampGranularity(int granularity_msec)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_timestamp_granularity_ns = 1000000 * (unsigned long)granularity_msec;
}

/**Show debug printout of a 'ST_DRXPD_RCVSTS' struct */
void DRXP::showRCVSTS(void* p)
{
    ST_DRXPD_RCVSTS* rxst = (ST_DRXPD_RCVSTS*)p;
    int nidle = 0, nwriting = 0, nwritten = 0, noverrun = 0, nprocessing = 0;
    for (int n = 0; n < rxst->GroupNum; n++) {
        if (rxst->Group[n] == DRXPD_IDLE) { nidle++; }
        else if (rxst->Group[n] == DRXPD_IDLE_WR) { nwriting++; }
        else if (rxst->Group[n] == DRXPD_COMPLETE) { nwritten++; }
        else if (rxst->Group[n] == DRXPD_OVERRUN) { noverrun++; }
        else if (rxst->Group[n] == DRXPD_OVERRUN_WR) { noverrun++; nwriting++; }
        if (m_group_busy[n] != 0) { nprocessing++; }
    }
    L_(ldebug1) << "Status of groups : " << nprocessing << " in user-processing : "
        << nidle << " idle, " << nwriting << " filling, "
        << nwritten << " full, " << noverrun << " overrun";
    if (m_debugFileEnabled) {
        m_debugFile << "rcvsts_summary " << nidle << " " << nwriting << " " << nwritten << " " << noverrun << "\n";
    }
}

/** Get a list of all available data groups */
bool DRXP::getGroups(int* groups, int* ngroups)
{
    if ((groups == NULL) || (ngroups == NULL)) { return false; }

    ST_DRXPD_RCVSTS rxstat;
    *ngroups = 0;

    while (*ngroups == 0) {

        // Wait for one or more groups to be available
        uint64_t evt;
        if (read(m_evtfd, &evt, sizeof(uint64_t)) < 0) {
            L_(linfo) << __func__ << " event-fd read failed";
            return false;
        }

        // With io_mutex locked get the DMA status of all 48ms-long data groups
        lockscope {
            boost::mutex::scoped_lock iolock(m_iomutex);
            DRXP_IOCTL_PTR(m_fd, DRXPD_GET_RCVSTS, &rxstat);
        }

        // List the available data
        if (rxstat.State == DRXPD_STOP) {
            L_(lwarning) << __func__ << " found DRXP unexpectedly in state DRXPD_STOP!";
            continue;
        }
        for (int grp = 0; grp < rxstat.GroupNum; grp++) {
            if ((m_group_busy[grp] == 0) && ((rxstat.Group[grp] == DRXPD_COMPLETE) || (rxstat.Group[grp] == DRXPD_OVERRUN))) {
                if (rxstat.Group[grp] == DRXPD_OVERRUN) {
                    m_noverruns++;
                }
                groups[*ngroups] = grp;
                *ngroups += 1;
            }
        }

        // Debug print
        if (1) {
           showRCVSTS(&rxstat);
        }
    }

    return true;
}

/** Get the next available 48ms data group */
bool DRXP::getGroup(int* group, void** data, DRXPInfoFrame* meta)
{
    if (group == NULL) { return false; }

    // Wait until some group(s) are available
    while (m_ngroups_avail <= 0) {
        bool ok = getGroups(m_groups_avail, &m_ngroups_avail);
        if (!ok) { return false; }
        continue;
    }

    // Get group
    lockscope {
       boost::mutex::scoped_lock iolock(m_iomutex);

       // Reserve the first available group
       int grp = m_groups_avail[0];
       m_group_busy[grp] = 1;
       *group = grp;

       // Remove from available groups
       for (int n = 1; n < m_ngroups_avail; n++) {
           m_groups_avail[n-1] = m_groups_avail[n];
       }
       m_ngroups_avail--;
    }

    // Data area
    size_t data_off = (*group)*DRXP_RX_GROUP_SIZE;
    if (data != NULL) {
        *data = (void*)((char*)m_maddr + data_off);
    }

    // Metaframe info
    size_t meta_off = (*group)*DRXP_RX_GROUP_SIZE + DRXP_RX_DATA_SIZE;
    if ((meta_off + DRXP_RX_IFRAME_SIZE) >= m_maddr_size) {
        L_(lerror) << __func__ << " oops, group " << (*group) << " info frame offset " << meta_off << " plus 48B falls outsize size of " << m_maddr_size;
    } else {
        meta->fromByteArray((unsigned char*)m_maddr + meta_off);
    }

    // Log the original timestamps
    if (m_debugFileEnabled) {
        m_debugFile << "timing_original " << *group << " " << meta->swRxTime_UTC.tv_sec << " " << meta->swRxTime_UTC.tv_nsec << " " << meta->hwTickCount_48ms << "\n";
    }

    // Synchronization and re-timing
resync:
    if (!m_reference_set) {
        m_reference_set = true;
        meta->roundTime(m_timestamp_granularity_ns);
        m_reference_time = meta->swRxTime_UTC;
        m_reference_tickcount = meta->hwTickCount_48ms;
        L_(linfo) << "Tied DRXP timestamps to software UTC "
                  << std::fixed << std::setprecision(6) << (m_reference_time.tv_sec + 1e-9*m_reference_time.tv_nsec)
                  << " + 48ms * (hardware tick N - " << m_reference_tickcount << ")";
    } else {
        bool ok = meta->adjustTime(m_reference_time, m_reference_tickcount);
        if (!ok) {
            m_reference_set = false;
            goto resync;
        }
    }
    if (m_debugFileEnabled) {
        m_debugFile << "timing_retimed " << *group << " " << meta->swRxTime_UTC.tv_sec << " " << meta->swRxTime_UTC.tv_nsec << " " << meta->hwTickCount_48ms << "\n";
    }

    // Display some status information
    if (0) {
        L_(ldebug1) << "Found group " << std::setw(2) << (*group) << " (of " << (m_ngroups_avail+1) << " avail.) with data; "
                << "48ms tick " << meta->hwTickCount_48ms << ", " << std::hex << meta->frameStatus << std::dec
                << ", tstamp " << std::fixed << std::setprecision(6) << (meta->swRxTime_UTC.tv_sec + meta->swRxTime_UTC.tv_nsec*1e-9)
                << " : naccepts=" << m_naccepts << " ndiscards=" << m_ndiscards << " noverruns=" << m_noverruns;
    } else if (1) {
        const uint TIME_FMT = sizeof("2012-12-31 12:59:59.123456789") + 1;
        char timestr[TIME_FMT];
        timespec2str(timestr, sizeof(timestr), &meta->swRxTime_UTC);
        L_(ldebug1) << "Found group " << std::setw(2) << (*group) << " (of " << (m_ngroups_avail+1) << " avail.) with data; "
                << "48ms tick " << meta->hwTickCount_48ms << ", " << std::hex << meta->frameStatus << std::dec
                << ", tstamp " << std::fixed << std::setprecision(6) << (meta->swRxTime_UTC.tv_sec + meta->swRxTime_UTC.tv_nsec*1e-9)
                << " : " << timestr
                << " : naccepts=" << m_naccepts << " ndiscards=" << m_ndiscards << " noverruns=" << m_noverruns;
    }

    return true;
}

/** Release a 48ms data group back to the DRXP */
bool DRXP::releaseGroup(int grp)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    DRXP_IOCTL(m_fd, DRXPD_CLR_RCV, grp);
    m_group_busy[grp] = 0;
    return true;
}

/** Return the cumulative nr of overruns since start */
int DRXP::getOverruns()
{
    return m_noverruns;
}

/** The RX data pump thread, waits for new data from DRXP, shuffles it into a DataRecipient object. */
void DRXP::rxDatapumpThread(DataRecipient* volatile* ppdr)
{
     if (m_rxtx_started) {
        L_(lwarning) << "Internal warning: rxDatapumpThread() called while DRXP fd=" << m_fd << ", rx=" << m_rx_mode << ", started=" << m_rxtx_started;
        return;
     }
    if ((m_fd < 0) || !m_rx_mode) {
        L_(lerror) << "Internal error: rxDatapumpThread() called while DRXP fd=" << m_fd << ", rx=" << m_rx_mode << ", started=" << m_rxtx_started;
        m_rxtx_started = false;
        return;
    }

    m_rxtx_started = true;

    try { while (!m_shutdown_rx_thread) {

        DRXPInfoFrame info;
        void* payload;
        int grp;

        // Get next group
        bool success = getGroup(&grp, &payload, &info);
        if (!success) {
            m_rxtx_started = false;
            m_shutdown_rx_thread = true;
            break;
        }

        // Assemble a data info packet for ::takeData()
        RawDRXPDataGroup data;
        data.frameinfo = &info; // TODO: maybe copy is better?
        data.buf = payload;
        data.bufSize = DRXP_RX_DATA_SIZE;
        data.weight = 1.0;
        data.originator = this;
        data.group = grp;

        // Callback to external handler via ::takeData()
        bool releaseLater = false;
        if ((ppdr != NULL) && (*ppdr != NULL)) {
            boost::this_thread::disable_interruption diTmp;
            releaseLater = (*ppdr)->takeData(data);
            m_naccepts++;
        } else {
            L_(ldebug1) << "Dropping new rx'd data since no valid data recipient registered yet";
            m_ndiscards++;
        }

        // Release the data unless a recipient took it (and will free the group on its own later)
        if (!releaseLater) {
            releaseGroup(grp);
        }

    } /*while*/ } catch (boost::thread_interrupted& e) {
        L_(ldebug) << __func__ << " was interrupted and is exiting";
        m_shutdown_rx_thread = true;
    }

    m_rxtx_started = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Loads contents of file (48ms data + Info Frame) and serves that as fake rx/tx data.
 *  Call after startTx().
 */
bool DRXP::loadPattern(const char* filename)
{
    std::ifstream f(filename, std::ios::binary);
    if (!f) {
        L_(lwarning) << __func__ << " could not open pattern file " << filename;
        return false;
    }
    f.read((char*)m_simpattern, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);
    f.close();

    if ((m_maddr != NULL) && m_rxtx_started && !m_rx_mode) {
        memcpy(m_maddr, m_simpattern, DRXP_RX_DATA_SIZE);
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Start debug output into a file */
bool DRXP::startDebugLogging(const char* filename)
{
    m_debugFile.close();
    m_debugFile.open(filename, std::ofstream::out | std::ofstream::app);
    m_debugFileEnabled = m_debugFile.is_open();
    return m_debugFileEnabled;
}

/** Stop debug output */
bool DRXP::stopDebugLogging()
{
    m_debugFile.close();
    m_debugFileEnabled = m_debugFile.is_open();
    return m_debugFileEnabled;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

bool DRXP::startTx()
{
    // Make sure the device is open in the correct mode
    if (m_rx_mode) {
       deviceClose();
       deviceOpen(/*rx=*/false);
    }

    // Lock m_iomutex and prepare TX
    if (1) {
        int ioctl_result = 0;
        boost::mutex::scoped_lock iolock(m_iomutex);

        // Modify the memory map protection
        if (mprotect(m_maddr, m_maddr_size, PROT_READ|PROT_WRITE) < 0) {
             L_(lerror) << __func__ << " mprotect error, errno=" << errno << std::dec;
             //return false;
        }

        // Data to transmit
        if (m_simpattern != NULL) {
            memcpy(m_maddr, m_simpattern, DRXP_RX_DATA_SIZE);
        }

        // Set up transmit
        DRXP_IOCTL(m_fd, DRXPD_SET_SNDSIZ, DRXP_TX_DATA_SIZE);
        DRXP_IOCTL_IPTR(m_fd, DRXPD_INIT, &ioctl_result);
        DRXP_IOCTL_IPTR(m_fd, DRXPD_SNDSTART, &ioctl_result);
        m_rxtx_started = true;
    }

    return m_rxtx_started;
}

bool DRXP::stopTx()
{
    if ((m_fd < 0) || !m_rxtx_started || m_rx_mode) {
        return false;
    }

    if (m_rxtx_started) {
        int ioctl_result = 0;
        DRXP_IOCTL_IPTR(m_fd, DRXPD_SNDSTOP, &ioctl_result);
        L_(ldebug) << "Stopped DRXP board I/O, ioctl() result = " << ioctl_result;
    }
    m_rxtx_started = false;

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void DRXP::monitoringThreadFunc(const char* mgrp, int mport, MCDRXPStatus* mc, float interval_secs)
{
    using namespace boost::asio;
    ip::address destaddr = boost::asio::ip::address::from_string(mgrp);
    ip::udp::endpoint source(ip::address_v4::any(), mport);
    ip::udp::endpoint destination(destaddr, mport);

    if (mgrp == NULL || mc == NULL) {
        std::cerr << "Multicast transmission " << __func__ << "() startup had NULL parameters, exiting thread.\n";
        return;
    }

    if (interval_secs < 1) {
        interval_secs = 5;
    }

    while (!m_shutdown_mon_thread) {

        if (1) {
            // Disable thread interruption in this scope
            boost::this_thread::disable_interruption diTmp;

            // Perform readout and update our target MCDRXPStatus
            deviceReadout(*mc);

            // Also send a summary out by multicast if possible
            std::string summary = mc->toString();
            try {
                io_service service;
                ip::udp::socket socket(service);
                socket.open(ip::udp::v4());
                socket.set_option(ip::udp::socket::reuse_address(true));
                socket.set_option(ip::multicast::enable_loopback(true));
                socket.send_to(buffer(summary), destination);
            } catch (...) {
                std::cerr << "Multicast transmission in " << __func__ << "() failed\n";
            }
        }

        // Pause and potential thread exit point
        try {
            boost::this_thread::sleep(boost::posix_time::seconds(interval_secs));
        } catch (boost::thread_interrupted& e) {
            return;
        }
    }
}

/** Start periodic monitoring. Periodically updates the referenced MCDRXPStatus and multicasts an ASCII summary string */
bool DRXP::startPeriodicMonitoring(const char* mgrp, int mport, MCDRXPStatus& mc, float interval_secs)
{
    if (m_monitoringThread != NULL) {
        return false;
    }
    if ((interval_secs < 0.192f) || (interval_secs > 30.0f)) {
        interval_secs = 5.0f;
    }
    m_shutdown_mon_thread = false;
    m_monitoringThread = new boost::thread(&DRXP::monitoringThreadFunc, this, mgrp, mport, &mc, interval_secs);
    return true;
}

/** Stop periodic monitoring */
void DRXP::stopPeriodicMonitoring()
{
    if (m_monitoringThread != NULL) {
        m_shutdown_mon_thread = true;
        m_monitoringThread->interrupt();
        m_monitoringThread->join();
        delete m_monitoringThread;
        m_monitoringThread = NULL;
        m_shutdown_mon_thread = false;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

// undefine macros in case this file was added via <#include "DRXP.cxx">
#undef READ_MON_REG
#undef WRITE_CTL_REG

//////////////////////////////////////////////////////////////////////////////////////////////////////
