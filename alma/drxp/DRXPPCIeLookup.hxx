/**
 * \class DRXPPCIeLookup
 *
 * \brief Translates an XSD drxpInterfaceId_t (PCI root and interface <1|2>) into char device
 *
 * \author $Author: janw $
 *
 */
#ifndef DRXPPCIELOOKUP_HXX
#define DRXPPCIELOOKUP_HXX

#include "xsd_datatypes/drxpInterfaceId_t.hxx"

#include <string>
#include <vector>
#include <set>

struct drxpInterfaceExt_t
{
    XSD::drxpInterfaceId_t interface;
    std::string chardev;
};

class DRXPPCIeLookup
{
    public:
        /// C'stor
        DRXPPCIeLookup();

        /// Return true if device and interface are present and have a char device (i.e. driver loaded)
        bool isPresent(const XSD::drxpInterfaceId_t& dev) const;

        /// Return char device path and name
        std::string getCharDevice(const XSD::drxpInterfaceId_t& dev) const;

        /// Return installed interfaces
        const std::vector<XSD::drxpInterfaceId_t>& getInstalledInterfaces() const;

        /// Return installed devices
        const std::set<std::string>& getInstalledDevices() const;

        /// Count installed interfaces
        int getInstalledInterfacesCount() const;

        /// Count installed devices
        int getInstalledDevicesCount() const;

        /// Count interfaces of a device
        int getDeviceInterfaceCount(std::string device) const;

    private:
        void gatherInstalledDevices();

    private:
        std::vector<XSD::drxpInterfaceId_t> interfaces;
        std::set<std::string> devices;
        std::vector<drxpInterfaceExt_t> interfaces_ext;
};

#endif
