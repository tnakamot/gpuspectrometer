/**
 * This file definies helpful "bitfields" for interpreting the data
 * in the low-level monitoring and control commands to the DRXP kernel driver.
 */
#ifndef DRXP_BITFIELDS_H
#define DRXP_BITFIELDS_H

#define GCC_BITFLD_ORDER ltoh         // need to choose 'ltoh' or 'htol' depending on compiler and platform

#include <string>
#include <cstdio>
#include <cstdlib>

#ifdef HAVE_DRXP
    // DRXP kernel module user-space API, definition of struct ST_DRXPD_MONDAT
    #include "drxpd.h"
    #undef MAX_SIZE // fixes name clash, MAX_SIZE unused anyway!
#endif

#ifndef _DRXPD_H_ // if(!HAVE_DRXP)
    // Bare essentials from drxpd.h
    /** Copy of definition of drxpd_mon_dat as in  drxpd.h */
    typedef struct drxpd_mon_dat{
        int ItemID;
        unsigned char RdValue[8];
    } ST_DRXPD_MONDAT;
    #define DRXPD_IDLE              0
    #define DRXPD_IDLE_WR           256
    #define DRXPD_COMPLETE          1
    #define DRXPD_OVERRUN           2
    #define DRXPD_OVERRUN_WR        258
#endif

////////////////////////////////////////////////////////////////////////////////////////////

/** Bit field that helps decoding of raw Voltage Status register data */
typedef struct DFRVoltageStatus_regs_tt {
    union byte0_elem {
        unsigned char raw;
        struct {
            bool p1_35VDdrForDdr : 1;
            bool p3_3VForFpgaIo : 1;
            bool p1_8VmgtvccauxForFpga : 1;
            bool p1_2VmgtavttForFpga : 1;
            bool p1_0VmgtavccForFpga : 1;
            bool p1_8VForFpga : 1;
            bool p0_95VForFpgaCore : 1;
            bool unused1 : 1;
        } ltoh;
        struct {
            bool unused1 : 1;
            bool p0_95VForFpgaCore : 1;
            bool p1_8VForFpga : 1;
            bool p1_0VmgtavccForFpga : 1;
            bool p1_2VmgtavttForFpga : 1;
            bool p1_8VmgtvccauxForFpga : 1;
            bool p3_3VForFpgaIo : 1;
            bool p1_35VDdrForDdr : 1;
        } htol;
    } byte0;
    union { unsigned char raw; } byte1;
    union { unsigned char raw; } byte2;
    union { unsigned char raw; } byte3;
    union { unsigned char raw; } byte4;
    union { unsigned char raw; } byte5;
    union { unsigned char raw; } byte6;
    union { unsigned char raw; } byte7;
} DFRVoltageStatus_regs_t;

////////////////////////////////////////////////////////////////////////////////////////////

/** Bit field that helps decoding of raw Alarm Status register data */
typedef struct DFRAlarmStatus_regs_tt {
    union byte0_elem {
        unsigned char raw;
        struct {
            bool rxInputPowerAlarm : 1;
            bool outputPowerAlarm : 1;
            bool outputBiasCurrentAlarm : 1;
            bool temperatureAlarm : 1;
            bool rxInputPowerWarning : 1;
            bool outputPowerWarning : 1;
            bool outputBiasCurrentWarning : 1;
            bool temperatureWarning : 1;
        } ltoh;
        struct {
            bool temperatureWarning : 1;
            bool outputBiasCurrentWarning : 1;
            bool outputPowerWarning : 1;
            bool rxInputPowerWarning : 1;
            bool temperatureAlarm : 1;
            bool outputBiasCurrentAlarm : 1;
            bool outputPowerAlarm : 1;
            bool rxInputPowerAlarm : 1;
        } htol;
    } byte0;
    union byte1_elem {
        unsigned char raw;
        struct {
            bool i2c_access_error : 1;
            unsigned char unused2 : 7;
        } ltoh;
        struct {
            unsigned char unused1 : 7;
            bool i2c_access_error : 1;
        } htol;
    } byte1;
    union { unsigned char raw; } byte2;
    union { unsigned char raw; } byte3;
    union { unsigned char raw; } byte4;
    union { unsigned char raw; } byte5;
    union { unsigned char raw; } byte6;
    union { unsigned char raw; } byte7;
} DFRAlarmStatus_regs_t;

////////////////////////////////////////////////////////////////////////////////////////////

/** Bit field that helps decoding of raw Power Status register data */
typedef struct DFRPowerAlarm_regs_tt {
    union byte0_elem {
        unsigned char raw;
        struct {
            bool p3_3VAlarm : 1;
            bool p3_3VWarning : 1;
            unsigned char unused1 : 6;
        } ltoh;
        struct {
            unsigned char unused1 : 6;
            bool p3_3VWarning : 1;
            bool p3_3VAlarm : 1;
        } htol;
    } byte0;
    union byte1_elem {
        unsigned char raw;
        struct {
            bool i2c_access_error : 1;
            unsigned char unused2 : 7;
        } ltoh;
        struct {
            unsigned char unused1 : 7;
            bool i2c_access_error : 1;
        } htol;
    } byte1;
    union { unsigned char raw; } byte2;
    union { unsigned char raw; } byte3;
    union { unsigned char raw; } byte4;
    union { unsigned char raw; } byte5;
    union { unsigned char raw; } byte6;
    union { unsigned char raw; } byte7;
} DFRPowerAlarm_regs_t;

////////////////////////////////////////////////////////////////////////////////////////////

/** Bit field that helps decoding of raw DRXP Status register data */
typedef struct DFRStatus_regs_tt {
    union byte0_elem {
        unsigned char raw;
        struct {
            bool referenceClock125MhzPllLock : 1;
            bool ddrClock200MhzPll2Lock : 1;
            bool ddrClock200MhzPll1Lock : 1;
            unsigned char unused1 : 5;
        } ltoh;
        struct {
            unsigned char unused1 : 5;
            bool ddrClock200MhzPll1Lock : 1;
            bool ddrClock200MhzPll2Lock : 1;
            bool referenceClock125MhzPllLock : 1;
        } htol;
    } byte0;
    union byte1_elem {
        unsigned char raw;
        struct {
            bool dataInputClockPllLockCh1BitD : 1;
            bool dataInputClockPllLockCh2BitC : 1;
            bool dataInputClockPllLockCh3BitB : 1;
            unsigned char unused2 : 5;
        } ltoh;
        struct {
            unsigned char unused2 : 5;
            bool dataInputClockPllLockCh3BitB : 1;
            bool dataInputClockPllLockCh2BitC : 1;
            bool dataInputClockPllLockCh1BitD : 1;
        } htol;
    } byte1;
    union byte2_elem {
        unsigned char raw;
        struct {
            bool syncErrorDetectedCh1BitD : 1;
            bool syncErrorDetectedCh2BitC : 1;
            bool syncErrorDetectedCh3BitB : 1;
            unsigned char unused3 : 5;
        } ltoh;
        struct {
            unsigned char unused3 : 5;
            bool syncErrorDetectedCh3BitB : 1;
            bool syncErrorDetectedCh2BitC : 1;
            bool syncErrorDetectedCh1BitD : 1;
        } htol;
    } byte2;
    union { unsigned char raw; } byte3;
    union { unsigned char raw; } byte4;
    union { unsigned char raw; } byte5;
    union { unsigned char raw; } byte6;
    union { unsigned char raw; } byte7;
} DFRStatus_regs_t;

////////////////////////////////////////////////////////////////////////////////////////////

typedef struct DFRTemp_regs_tt {
    float tempC;
    float undefined;
} DFRTemp_regs_t;

////////////////////////////////////////////////////////////////////////////////////////////

/** Struct that helps decoding of raw Monitoring Data register data as a Voltage, Alarm, Power, or DRXP Status structs */
typedef union {
    ST_DRXPD_MONDAT mon;
    struct {
        int ItemID_tmp;
        union {
            DFRVoltageStatus_regs_t volSts;
            DFRAlarmStatus_regs_t almSts;
            DFRPowerAlarm_regs_t pwrAlm;
            DFRStatus_regs_t dfrSts;
            DFRTemp_regs_t dfrTemp;
        };
    };
} dfr_mon_reg_t;

////////////////////////////////////////////////////////////////////////////////////////////

/** \return Descriptive string for given RX State */
inline const std::string rxGroupState2Str(int rxState)
{
    switch (rxState) {
        case DRXPD_IDLE:     return "free";
        case DRXPD_IDLE_WR:  return "busy";
        case DRXPD_COMPLETE: return "data";
        case DRXPD_OVERRUN:  return "data(overflow)";
        case DRXPD_OVERRUN_WR: return "busy(overflow)";
        default: return "--unk--";
    }
}

////////////////////////////////////////////////////////////////////////////////////////////

#endif // DRXP_BITFIELDS_H

