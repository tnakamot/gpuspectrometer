/**
 * \class DRXPIface
 *
 * \brief Low-level access to the DRXP via kernel driver and /dev/drxp[n] character device ioctl's
 *
 * \author $Author: janw $
 *
 */
#ifndef DRXP_IFACE_HXX
#define DRXP_IFACE_HXX

#define DRXP_MONITOR_MULTICAST_GROUP "224.0.0.1" /// Target IP for multicast data; also see http://www.iana.org/assignments/multicast-addresses/multicast-addresses.xhtml
#define DRXP_MONITOR_MULTICAST_PORT  30001       /// Target port for multicast data

#include "drxp/SFF8431_infos.hxx"

#include <string>

class MCDRXPStatus;
class DataRecipient;
class DRXPInfoFrame;

class DRXPIface
{
    friend class MCDRXPStatus;

    public:
        enum UserMode { PUSH=0, POLLING }; /// PUSH: callback from DRXP worker to a registered listener, POLLING: user has to query DRXP interface

    public:
        /** Prepare DRXP object, without open any device yet (use selectDevice() for that) */
        DRXPIface() { }

        /** Close the DRXP driver interface, without resetting anything */
        virtual ~DRXPIface() { };

    public:
        /** Change to another DRXP board */
        virtual bool selectDevice(int iface_nr, bool open_rdonly=true) = 0;

        /** Change to another DRXP board */
        virtual bool selectDevice(std::string dev_name, bool open_rdonly=true) = 0;

        /** Reset all flags and alarms */
        virtual bool resetDevice() = 0;

        /** Reset all flags and alarms */
        virtual bool resetAlarms() = 0;

        /** Reset internal synchronization engine on DRXP, forcing a hardware re-sync. */
        virtual bool resetSync() = 0;

        /** Reset all checksums */
        virtual bool resetChecksums() = 0;

        /** Read out all device information, flags, alarms, and statuses */
        virtual bool deviceReadout(MCDRXPStatus& mc) = 0;

        /** Read out board version information */
        virtual bool versionReadout(MCDRXPStatus& mc) = 0;

        /** Read out DFR Status */
        virtual bool dcmstatusReadout(MCDRXPStatus& mc) = 0;

        /** Read out FPGA related voltage alarms (FPGA, on-FPGA MGT, Vddr) and SFP+ 3.3V voltage warnings/alarms */
        virtual bool voltageReadout(MCDRXPStatus& mc) = 0;

        /** Read out all counters */
        virtual bool counterReadout(MCDRXPStatus& mc) = 0;

        /** Read out optical power measuremets and warnings/alarms */
        virtual bool opticalReadout(MCDRXPStatus& mc) = 0;

        /** Read out ALMA frame decoder sync status and metaframe delay */
        virtual bool syncReadout(MCDRXPStatus& mc) = 0;

        /** Read out DRXP Receive Status */
        virtual bool receiveStatusReadout(MCDRXPStatus& mc) = 0;

        /** Read out DRXP Send Status */
        virtual bool sendStatusReadout(MCDRXPStatus& mc) = 0;

        /** Read out DRXP Temperatures */
        virtual bool temperaturesReadout(MCDRXPStatus& mc) = 0;

        /** Read out DRXP FPGA Single Event Upset error counts and status */
        virtual bool fpgaSEUReadout(MCDRXPStatus& mc) = 0;

        /** Start periodic monitoring. Periodically updates the referenced MCDRXPStatus and multicasts an ASCII summary string */
        virtual bool startPeriodicMonitoring(const char* mgrp, int mport, MCDRXPStatus& mc, float interval_secs=5.0f) = 0;

        /** Stop periodic monitoring. */
        virtual void stopPeriodicMonitoring() = 0;

    public:
        /** Start transmission of test pattern (TODO properly) */
        virtual bool startTx() = 0;

        /** Stop transmission of test pattern */
        virtual bool stopTx() = 0;

    public:
        /** Attach a data processing recipient that gets the received data once/while RX is running.
         * The recipient object must provide a function 'takeData(data_t*)' into which DRXP RX data will be pushed.
         */
        virtual bool attachRxRecipient(DataRecipient* dr) = 0;

        /** Detach a data processing recipient */
        virtual bool detachRxRecipient() = 0;

        /** In "polling" mode allow user to check for an available data group */
        virtual bool getGroup(int* group, void** data, DRXPInfoFrame* meta) = 0;

        /** In "polling" mode allow user to free a data group back to the DRXP */
        virtual bool releaseGroup(int group) = 0;

        /** Return the cumulative nr of overruns since start */
        virtual int getOverruns() = 0;

        /** Start DRXP RX data pump (thread).
         * The data pump loopRxThread() waits for a new group of 48ms of raw data.
         * The new group is held "reserved" and is passed to the recipient via takeData().
         * The recipient should transfer the data (DMA onto GPU) and block until the transfer completes.
         * Once takeData() returns, the data pump "releases" the group, allowing DRXP DMA to fill it with new data later.
         */
        virtual bool startRx() = 0;

        /** Stop data reception */
        virtual bool stopRx() = 0;

        /** Change the granularity of software timestamp re-alignment */
        virtual void setTimestampGranularity(int granularity_msec) = 0;

    public:
        /** Get pointers and length of all 'groups' in raw DRXP data area. Intended to help (un)pinning in CUDA. */
        virtual bool getBufferPtrs(size_t* Ngroups, void** groupbuf, size_t* len) = 0;

    public:
        /** Loads contents of file (48ms data + Info Frame) and serves that as fake rx/tx data.
         *  Call after startRx() or startTx().
         */
        virtual bool loadPattern(const char* filename) = 0;

        /** Start debug output into a file */
        virtual bool startDebugLogging(const char* filename) = 0;

        /** Stop debug output */
        virtual bool stopDebugLogging() = 0;

    public:
        /** Read out internal SFP+ transceiver information of given slot (0, 1, or 2) via the I2C bus. */
        virtual bool deviceGetSFPInfos(const int slot, SFF8431_Xceiver_infos_tt& xi) = 0;

        /** Get a copy of SFP+ transceiver information (slot 0) that was read out once in c'stor. */
        virtual const SFF8431_Xceiver_infos_t& sfpInfosSlot0() const = 0;

        /** Get a copy of SFP+ transceiver information (slot 2) that was read out once in c'stor. */
        virtual const SFF8431_Xceiver_infos_t& sfpInfosSlot1() const = 0;

        /** Get a copy of SFP+ transceiver information (slot 3) that was read out once in c'stor. */
        virtual const SFF8431_Xceiver_infos_t& sfpInfosSlot2() const = 0;

};

#endif // DRXP_IFACE_HXX

