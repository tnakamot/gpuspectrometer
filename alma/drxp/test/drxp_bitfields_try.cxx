#include "drxp/drxp_bitfields.hxx"

#include <stdio.h>

/** Test program to check data alignment and bit order direction on a given hardware platform */

int main(int argc, char** argv)
{
    DFRVoltageStatus_regs_t r;
    dfr_mon_reg_t mr;

    r.byte0.raw = 0x01;
    if (argc == 2) {
        r.byte0.raw = atoi(argv[1]);
    }
    printf("Setting monreg.mon.RdValue[0] to %u. Can specify another with '%s <value>'\n", r.byte0.raw, argv[0]);
    mr.mon.ItemID = 0xFFFFFFFF;
    mr.mon.RdValue[0] = r.byte0.raw;

    printf("For DFRVoltageStatus_regs_t::raw.byte[0] = 0x%02X, bitfield bits are:\n", r.byte0.raw);
    printf("unused1               = %d\n", r.byte0.GCC_BITFLD_ORDER.unused1              );
    printf("p0_95VForFpgaCore     = %d\n", r.byte0.GCC_BITFLD_ORDER.p0_95VForFpgaCore    );
    printf("p1_8VForFpga          = %d\n", r.byte0.GCC_BITFLD_ORDER.p1_8VForFpga         );
    printf("p1_0VmgtavccForFpga   = %d\n", r.byte0.GCC_BITFLD_ORDER.p1_0VmgtavccForFpga  );
    printf("p1_2VmgtavttForFpga   = %d\n", r.byte0.GCC_BITFLD_ORDER.p1_2VmgtavttForFpga  );
    printf("p1_8VmgtvccauxForFpga = %d\n", r.byte0.GCC_BITFLD_ORDER.p1_8VmgtvccauxForFpga);
    printf("p3_3VForFpgaIo        = %d\n", r.byte0.GCC_BITFLD_ORDER.p3_3VForFpgaIo       );
    printf("p1_35VDdrForDdr       = %d\n", r.byte0.GCC_BITFLD_ORDER.p1_35VDdrForDdr      );

    printf("For dfr_mon_reg_t::RdValue[0] overload DFRVoltageStatus_regs_t, bitfield bits are:\n");
    printf("unused1               = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.unused1              );
    printf("p0_95VForFpgaCore     = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p0_95VForFpgaCore    );
    printf("p1_8VForFpga          = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p1_8VForFpga         );
    printf("p1_0VmgtavccForFpga   = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p1_0VmgtavccForFpga  );
    printf("p1_2VmgtavttForFpga   = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p1_2VmgtavttForFpga  );
    printf("p1_8VmgtvccauxForFpga = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p1_8VmgtvccauxForFpga);
    printf("p3_3VForFpgaIo        = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p3_3VForFpgaIo       );
    printf("p1_35VDdrForDdr       = %d\n", mr.volSts.byte0.GCC_BITFLD_ORDER.p1_35VDdrForDdr      );

    return 0;
}
