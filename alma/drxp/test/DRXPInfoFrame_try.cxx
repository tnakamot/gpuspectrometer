#include "drxp/DRXPInfoFrame.hxx"

#include <stdio.h>

int main()
{

    // Testing to reproduce (now fixed) an earlier error
    // "Warning: DRXP data timestamp 1512606503.710302 tick 312 vs. adjusted/re-gridded (ref: 1512606504.000000 tick:319) giving timestamp 19959350577.373550 have drifted apart by -18446744073663.250000ms"
    if (1) {
        DRXPInfoFrame f;
        struct timespec refUTC;
        unsigned long refTick;

        //   timestamp 1512606503.710302 tick 312
        f.swRxTime_UTC.tv_sec = 1512606503;
        f.swRxTime_UTC.tv_nsec = 0.710302 * 1e9;
        f.hwTickCount_48ms = 312;
        //   adjusted/re-gridded (ref: 1512606504.000000 tick:319)
        refUTC.tv_sec = 1512606504;
        refUTC.tv_nsec = 0;
        refTick = 319;
        //   giving timestamp 19959350577.373550 have drifted apart by -18446744073663.250000ms
        f.adjustTime(refUTC, refTick);
        // fixed --> no more error message about drift
    }

    // Test time rounding
    if (1) {
        DRXPInfoFrame f;
        f.swRxTime_UTC.tv_sec = 1512606503;
        f.swRxTime_UTC.tv_nsec = 0.710302 * 1e9;
        f.hwTickCount_48ms = 312;
        printf("Time rounding\n");
        printf("original time   %ld%.4f\n", f.swRxTime_UTC.tv_sec, 1e-9*f.swRxTime_UTC.tv_nsec);
        f.roundTime8ms(); // or f.roundTime(8000000);
        printf("rounded to 8ms  %ld%.4f\n", f.swRxTime_UTC.tv_sec, 1e-9*f.swRxTime_UTC.tv_nsec);
        f.swRxTime_UTC.tv_sec = 1512606503;
        f.swRxTime_UTC.tv_nsec = 0.710302 * 1e9;
        f.roundTime48ms(); // or f.roundTime(48000000);
        printf("rounded to 48ms %ld%.4f\n", f.swRxTime_UTC.tv_sec, 1e-9*f.swRxTime_UTC.tv_nsec);
        f.swRxTime_UTC.tv_sec = 1512606503;
        f.swRxTime_UTC.tv_nsec = 0.710302 * 1e9;
        f.roundTime(10000000);
        printf("rounded to 10ms %ld%.4f\n", f.swRxTime_UTC.tv_sec, 1e-9*f.swRxTime_UTC.tv_nsec);
    }

}
