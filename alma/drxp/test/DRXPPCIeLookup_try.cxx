#include "DRXPPCIeLookup.hxx"

#include <iostream>

int main(void)
{
    DRXPPCIeLookup lookup;

    // Show all installed devices
    const std::vector<XSD::drxpInterfaceId_t>& ifaces = lookup.getInstalledInterfaces();
    const std::set<std::string>& devices = lookup.getInstalledDevices();

    std::cout << "Lookup found " << lookup.getInstalledDevicesCount() << " devices:\n";
    std::set<std::string>::const_iterator dev;
    for (dev = devices.begin(); dev != devices.end(); dev++) {
        std::cout << "    " << *dev << " with " << lookup.getDeviceInterfaceCount(*dev) << " interfaces\n";
    }

    std::cout << "Devices provide " << lookup.getInstalledInterfacesCount() << " interfaces:\n";
    std::vector<XSD::drxpInterfaceId_t>::const_iterator iface;
    for (iface = ifaces.begin(); iface != ifaces.end(); iface++) {
        std::cout << "    " << iface->deviceId << " iface# " << iface->InterfaceNumber
            << " found on " << lookup.getCharDevice(*iface) << "\n";

    }

    // Look up a specific device
    XSD::drxpInterfaceId_t tofind;
    tofind.deviceId = "b1:00";
    tofind.InterfaceNumber = 1;

    std::cout << "Looking for " << tofind.deviceId << " iface " << tofind.InterfaceNumber << "\n";
    std::cout << "isPresent()     : " << lookup.isPresent(tofind) << "\n";
    std::cout << "getCharDevice() : " << lookup.getCharDevice(tofind) << "\n";
    return 0;
}
