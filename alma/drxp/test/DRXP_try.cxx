#include "drxp/DRXP.hxx"
#include "helper/logger.h"
#include "mcwrapping/mc_drxp_status.hxx"
#include "datarecipients/DataRecipient_iface.hxx"
#include "time/TimeUTC.hxx"

#ifdef HAVE_GPU
#include "spectralprocessors/SpectralProcessorGPU.hxx"
#endif
#include "spectralprocessors/SpectralProcessorCPU.hxx"
#include "datarecipients/ResultRecipientPlain.hxx"
#include "mcwrapping/mc_observation.hxx"

#include <iomanip>
#include <cmath>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>


int main(int argc, char** argv)
{
    DRXP drxp;
    int iface = 0;
    bool rx = true;
    int affinities[8] = {0,1,2,3,4,5,6,7};

    // Command line args
    for (int i=1; i<argc; i++) {
        std::string arg(argv[i]);
        if (arg == "rx") {
            rx = true;
        } else if (arg == "tx") {
            rx = false;
        } else if ((arg == "0") || (arg == "1") || (arg == "2")  || (arg == "3")) {
            iface = (arg.c_str())[0] - '0';
        } else {
            std::cout << "Usage: " << argv[0] << " [0|1|2|3] [rx|tx]\n"
                      << "  0|1|2|3 : choose /dev/drxpd0 (default) or /dev/drxpd1 or other\n"
                      << "  rx|tx   : receive mode (default) or transmit mode\n";
            exit(EXIT_FAILURE);
        }
    }

    // Dummy observation to process
    MCObservation obs;
    struct timeval tvobs;
    gettimeofday(&tvobs, NULL);
    tvobs.tv_sec += 10;
    TimeUTC::timevalToString(tvobs, obs.startTimeUTC);
    tvobs.tv_sec += 10;
    TimeUTC::timevalToString(tvobs, obs.stopTimeUTC);
    obs.observationId = "xyz987654321";
    obs.updateDerivedParams();
    obs.state = MCObservation::Ongoing;
    obs.cfg.createTestconfig1();
    std::cout << obs.cfg << "\n";

    // Spectra processor to push raw data into
#ifdef HAVE_GPU
    SpectralProcessorGPU spu;
    affinities[0] = iface;
    spu.setAffinity(/*nGPU*/1, affinities); // use single GPU; comment out to use all
    spu.initialize(&obs);
#else
    SpectralProcessorCPU spu;
    spu.initialize(&obs);
#endif

    // Output writer for storing results
    ResultRecipientPlain out(std::string("/tmp/"));
    out.configureFrom(&obs);
    out.open();
    spu.setSpectralRecipient(&out);

    // Reset the DRXP
    drxp.startDebugLogging("/tmp/drxp_N.trace");
    drxp.selectDevice(iface, rx);
    drxp.resetDevice();

    // Start receive or transmit
    if (rx) {
        drxp.startRx();
        drxp.attachRxRecipient(&spu);
    } else {
        drxp.startTx();
    }

    // Optimize the buffer access (for CUDA)
    size_t drxp_nbuf, drxp_buflen;
    void* drxp_buf[128];
    if (drxp.getBufferPtrs(&drxp_nbuf, drxp_buf, &drxp_buflen)) {
        spu.pinUserBuffers(drxp_nbuf, drxp_buf, drxp_buflen);
    }

    // Read all status registers
    MCDRXPStatus s;
    s.deviceReadout(&drxp);

    // Translate some status registers into JSON/PropertyTree
    boost::property_tree::ptree pt;
    s.getDrxpSerialNumber(pt);
    s.getDrxpVersion(pt);
    s.getAlarmStatus(pt);
    s.getPowerAlarms(pt);
    s.getDfrStatus(pt);
    s.getDfrVoltages(pt);
    s.getSyncStatus(pt);
    s.getDrxpTestmode(pt);
    s.getDrxpMetaframeDelay(pt);
    s.getChecksumCounters(pt);
    s.getSyncErrorCounters(pt);
    write_json("drxp_readout.json", pt);

    // Regularly monitor the board and processor
    drxp.startPeriodicMonitoring(DRXP_MONITOR_MULTICAST_GROUP, DRXP_MONITOR_MULTICAST_PORT, s);
    spu.startPeriodicMonitoring(DRXP_MONITOR_MULTICAST_GROUP, DRXP_MONITOR_MULTICAST_PORT+1);
    std::cout << "Reading out counters repeatedly in monitoring thread.\n"
              << "Please use UDP multicast client to see monitoring data.\n"
              << "Press Ctrl-C to stop.\n";

    // Attach a rx'd data processor (even if we do not rx)
    drxp.attachRxRecipient(&spu);

    while (1) {
        if (1) {
            // testing of attach/detach of a recipient:
            L_(linfo) << "Attaching a data recipient to the DRXP rx pump";
            drxp.attachRxRecipient(&spu);
            sleep(10);
            L_(linfo) << "Detaching a data recipient from the DRXP rx pump";
            drxp.detachRxRecipient();
        }
        sleep(10);
    }

    if (drxp.getBufferPtrs(&drxp_nbuf, drxp_buf, &drxp_buflen)) {
        spu.unpinUserBuffers(drxp_nbuf, drxp_buf, drxp_buflen);
    }

    return 0;
}
