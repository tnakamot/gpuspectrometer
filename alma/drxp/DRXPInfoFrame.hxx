/**
 * \class DRXPInfoFrame
 *
 * Container for DRXP Information Frame data (48 bytes; timing information).
 *
 */
#ifndef DRXP_INFO_FRAME_HXX
#define DRXP_INFO_FRAME_HXX

#include <time.h> // struct timespec

class DRXPInfoFrame
{
    public:
        unsigned long hwTickCount_48ms; /// count of 48ms DTS metaframe "ticks" in DRXP RX
        struct timespec swRxTime_UTC;   /// kernel timestamp after data reception (was TAI in earlier driver, now UTC)
        unsigned int frameStatus;       /// frame status bits

    public:
        /** Take raw 48-byte long ALMA Information Frame and extract the time information from it */
        void fromByteArray(const unsigned char*);

        /** Adjust swRxTime_UTC (PC time) to the nearest 8 millisecond multiple. Useful with 1 PPS TE. */
        void roundTime8ms();

        /** Adjust swRxTime_UTC (PC time) to the nearest 48 millisecond multiple. Useful with 48ms TE. */
        void roundTime48ms();

        /** Adust swRxTime_UTC (PC time) to nearest multiple of time granularity (e.g., 8000000ns(8ms) or 48000000ns(48ms) or other) */
        void roundTime(const unsigned long timegranularity_ns);

        /** Adjust swRxTime_UTC (PC time) to fall on a fixed 48ms grid
         * \arg refUTC The reference UTC time to use for setting swRxTime_UTC to refUTC + N*48ms
         * \arg refUTCtick The 48ms tick count at the reference UTC time
         * \return True on success, False if a re-sync appears needed due to too large drift (happens on DRXP Reset commands)
         */
        bool adjustTime(const struct timespec& refUTC, const unsigned long refUTCtick);
};

#endif

