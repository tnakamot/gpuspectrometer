/**
 * \class drxpSignalAveragePower_t
 *
 * Container for XSD DrxpSignalAveragePowerType, with additions (::convertTodBm()).
 * Wraps also the simple types DrxpBitAveragedOpticalSignalPowerType and DrxpWoUnitBitAveragedOpticalSignalPowerType.
 */
#ifndef DRXPSIGNALAVERAGEPOWER_T_HXX
#define DRXPSIGNALAVERAGEPOWER_T_HXX

#include "xsd_ptree_iface.hxx"

#include <cmath>

namespace XSD {

class drxpSignalAveragePower_t : public XSDPtreeInterface
{
    public:
        unsigned int ch1BitD_power_nW;
        unsigned int ch2BitC_power_nW;
        unsigned int ch3BitB_power_nW;
        bool i2caccessError_ch1;
        bool i2caccessError_ch2;
        bool i2caccessError_ch3;
        float ch1BitD_power_dBm;
        float ch2BitC_power_dBm;
        float ch3BitB_power_dBm;

    public:
        drxpSignalAveragePower_t() : ch1BitD_power_nW(0),ch2BitC_power_nW(0), ch3BitB_power_nW(0) {
            convertTodBm();
        }

    public:
        void convertTodBm()
        {
            ch1BitD_power_dBm = 10.0*log10(ch1BitD_power_nW*1e-9 / 0.001);
            ch2BitC_power_dBm = 10.0*log10(ch2BitC_power_nW*1e-9 / 0.001);
            ch3BitB_power_dBm = 10.0*log10(ch3BitB_power_nW*1e-9 / 0.001);
        }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
