/**
 * \class delayList_t
 *
 * Container for XSD DelayListType
 */
#ifndef DELAYLIST_T_HXX
#define DELAYLIST_T_HXX

#include "delay_t.hxx"
#include "spectrometerInput_t.hxx"
#include "global_defs.hxx"
#include "xsd_ptree_iface.hxx"

#include <deque>

namespace XSD
{

class delayList_t : public XSDPtreeInterface
{
    public:
        spectrometerInput_t spectrometerInput;
        std::deque<delay_t> delayList; /// should hold 0..100 (MAX_DELAYLIST_LENGTH) delay_t at most

    public:
        delayList_t() {
            spectrometerInput = 1;
            delayList.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= spectrometerInput.validate();
            valid &= (delayList.size() <= MAX_DELAYLIST_LENGTH);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

        void push_back(delay_t& delays);

};

}

#endif
