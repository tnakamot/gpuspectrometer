/**
 * \class refTemp_t
 *
 * Container for XSD RefTempType
 */
#ifndef REFTEMP_T_HXX
#define REFTEMP_T_HXX

#include "inputRefTemp_t.hxx"
#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD
{

class refTemp_t : public XSDPtreeInterface
{
    public:
        unsigned int index;
        std::vector<inputRefTemp_t> inputRefTemp;

    public:
        refTemp_t() : index(0) {
            inputRefTemp.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (inputRefTemp.size() >= 1);
            for (std::vector<inputRefTemp_t>::const_iterator it = inputRefTemp.begin(); it != inputRefTemp.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
