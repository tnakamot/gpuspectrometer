/**
 * \class drxpDfrStatus_t
 *
 * Container for XSD DrxpDfrStatusType
 */
#ifndef DRXPDFRSTATUS_T_HXX
#define DRXPDFRSTATUS_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpDfrStatus_t : public XSDPtreeInterface
{
    public:
        bool ddrClock200MhzPll1Unlock;
        bool ddrClock200MhzPll2Unlock;
        bool referenceClock125MhzPllUnlock;
        bool dataInputClockPllUnlockCh3BitB;
        bool dataInputClockPllUnlockCh2BitC;
        bool dataInputClockPllUnlockCh1BitD;
        bool syncPatternNotDetectedCh3BitB;
        bool syncPatternNotDetectedCh2BitC;
        bool syncPatternNotDetectedCh1BitD;
        bool syncErrorDetectedCh3BitB;
        bool syncErrorDetectedCh2BitC;
        bool syncErrorDetectedCh1BitD;

    public:
        drxpDfrStatus_t() : ddrClock200MhzPll1Unlock(false), ddrClock200MhzPll2Unlock(false),
            referenceClock125MhzPllUnlock(false), dataInputClockPllUnlockCh3BitB(false),
            dataInputClockPllUnlockCh2BitC(false), dataInputClockPllUnlockCh1BitD(false),
            syncPatternNotDetectedCh3BitB(false), syncPatternNotDetectedCh2BitC(false),
            syncPatternNotDetectedCh1BitD(false), syncErrorDetectedCh3BitB(false),
            syncErrorDetectedCh2BitC(false), syncErrorDetectedCh1BitD(false)
        {
        }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif

