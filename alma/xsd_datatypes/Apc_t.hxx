/**
 * \class Apc_t
 *
 * Container for XSD ApcType
 */
#ifndef APC_T_HXX
#define APC_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD
{

class Apc_t : public XSDPtreeInterface
{
    public:
        bool expectWvr;
        double wvrIntegrationDuration; /// seconds, positive double >0

    public:
        Apc_t() : expectWvr(false), wvrIntegrationDuration(48e-3) { }

    public:
        bool validate() const {
            return wvrIntegrationDuration > 0;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
