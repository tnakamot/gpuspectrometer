/**
 * \class chanCoeff_t
 *
 * Container for XSD ChanCoeffType
 */
#ifndef CHANCOEFF_T_HXX
#define CHANCOEFF_T_HXX

#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD
{

class chanCoeff_t : public XSDPtreeInterface
{
    public:
        unsigned int index;
        std::vector<double> polyCoeff;

    public:
        chanCoeff_t() : index(0) {
            polyCoeff.clear();
        }

    public:
        bool validate() const {
            return (polyCoeff.size() >= 1);
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
