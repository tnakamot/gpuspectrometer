/**
 * \class delay_t
 *
 * Container for XSD DelayType
 */
#ifndef DELAY_T_HXX
#define DELAY_T_HXX

#include "xsd_ptree_iface.hxx"

#include <string>

namespace XSD {

class delay_t : public XSDPtreeInterface
{
    // delay(t) = c4*t^4 + c3*t^3 + c2*t^2 + c1*t + c0 {c4,c3,c2,c1,c0}: cx is a double.
    public:
        std::string startTime; // UtcDateTimeMs
        std::string stopTime; // UtcDateTimeMs
        double delayCoefficients[5];
        double instrumentalDelayX;
        double instrumentalDelayY;

    public:
        delay_t() {
            startTime = "<invalid>";
            stopTime = "<invalid>";
            instrumentalDelayX = 0;
            instrumentalDelayY = 0;
            for (size_t n=0; n<sizeof(delayCoefficients)/sizeof(delayCoefficients[0]); n++) {
                delayCoefficients[n] = 0;
            }
        }

    public:
        bool validate() const {
            bool valid = true;
            for (size_t n=0; n<sizeof(delayCoefficients)/sizeof(delayCoefficients[0]); n++) {
                valid &= delayCoefficients[n] >= 0 && delayCoefficients[n] <= 2.0;
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

    private:
        template<typename T> void parse_coefficients(std::string raw, T* out, const int Nmax, const char separator=' ');
        template<typename T> std::string generate_coefficients(const T* in, const int Nmax, const char* separator=" ") const;
        
};

}

#endif
