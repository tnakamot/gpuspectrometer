/**
 * \class delayListList_t
 *
 * Container for a list of XSD DelayListType's, see RspGetDelayListType
 */
#ifndef DELAYLISTLIST_T_HXX
#define DELAYLISTLIST_T_HXX

#include "delayList_t.hxx"
#include "global_defs.hxx"
#include "xsd_ptree_iface.hxx"

#include <deque>

namespace XSD
{

class delayListList_t : public XSDPtreeInterface
{
    public:
        std::deque<delayList_t> delayListList; /// should hold 0..32 (MAX_DELAYLIST_LIST_LENGTH) delayList_t at most

    public:
        delayListList_t() {
            delayListList.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (delayListList.size() <= MAX_DELAYLIST_LENGTH);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

        void push_back(delayList_t& delayList);

};

}

#endif
