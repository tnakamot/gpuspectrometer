/**
 * \class drxpBitPowerAlarm_t
 *
 * Container for XSD DrxpBitPowerAlarmType
 */
#ifndef DRXPBITPOWERALARM_T_HXX
#define DRXPBITPOWERALARM_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpBitPowerAlarm_t : public XSDPtreeInterface
{
    public:
        bool p3_3VWarning;
        bool p3_3VAlarm;
        bool i2cAccessError;

    public:
        drxpBitPowerAlarm_t() : p3_3VWarning(false), p3_3VAlarm(false), i2cAccessError(false) { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
