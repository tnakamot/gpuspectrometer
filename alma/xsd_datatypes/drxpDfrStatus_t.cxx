#include "drxpDfrStatus_t.hxx"

namespace XSD {

bool drxpDfrStatus_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpDfrStatus_t::put(boost::property_tree::ptree& out) const
{
    out.put("ddrClock200MhzPll1Unlock", ddrClock200MhzPll1Unlock);
    out.put("ddrClock200MhzPll2Unlock", ddrClock200MhzPll2Unlock);
    out.put("referenceClock125MhzPllUnlock", referenceClock125MhzPllUnlock);
    out.put("dataInputClockPllUnlockCh3BitB", dataInputClockPllUnlockCh3BitB);
    out.put("dataInputClockPllUnlockCh2BitC", dataInputClockPllUnlockCh2BitC);
    out.put("dataInputClockPllUnlockCh1BitD", dataInputClockPllUnlockCh1BitD);
    out.put("syncErrorDetectedCh3BitB", syncErrorDetectedCh3BitB);
    out.put("syncErrorDetectedCh2BitC", syncErrorDetectedCh2BitC);
    out.put("syncErrorDetectedCh1BitD", syncErrorDetectedCh1BitD);

    return true;
}

}
