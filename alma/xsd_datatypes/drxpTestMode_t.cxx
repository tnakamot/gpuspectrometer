#include "drxpTestMode_t.hxx"

namespace XSD {

bool drxpTestMode_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpTestMode_t::put(boost::property_tree::ptree& out) const
{
    out.put("ch1BitD", getTestModeStr(ch1BitD));
    out.put("ch2BitC", getTestModeStr(ch2BitC));
    out.put("ch3BitB", getTestModeStr(ch3BitB));

    return true;
}

std::string drxpTestMode_t::getTestModeStr(drxpTestMode_Type_t t) const
{
    switch (t) {
        case DRXP_TestMode_No: return "No";
        case DRXP_TestMode_Random: return "Random";
        case DRXP_TestMode_Serial: return "Serial";
        case DRXP_TestMode_Periodic: return "Periodic";
        case DRXP_TestMode_Fixed_0x3C: return "0x3C";
        case DRXP_TestMode_Fixed_0xC3: return "0xC3";
        case DRXP_TestMode_Fixed_0x3: return "0x3";
        case DRXP_TestMode_Fixed_0xC: return "0xC";
        case DRXP_TestMode_Fixed_0x5: return "0x5";
        case DRXP_TestMode_Fixed_0x5555AAAA: return "0x5555AAAA";
        case DRXP_TestMode_Fixed_0x0: return "0x0";
        case DRXP_TestMode_Fixed_0x1: return "0x1";
        case DRXP_TestMode_Fixed_0xA: return "0xA";
        case DRXP_TestMode_Fixed_0xAAAA5555: return "0xAAAA5555";
        case DRXP_TestMode_Fixed_0xFEDCBA98: return "0xFEDCBA98";
        case DRXP_TestMode_Fixed_0x01234567: return "0x01234567";
    }
    return "--illegal--";
}


}
