#include "inputRefTemp_t.hxx"

namespace XSD {

bool inputRefTemp_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool inputRefTemp_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
