/**
 * \class drxpTestMode_t
 *
 * Container for DRXP Test Mode as in DRXP User Manual
 */
#ifndef DRXPTESTMODE_T_HXX
#define DRXPTESTMODE_T_HXX

#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include <string>

namespace XSD {

class drxpTestMode_t : public XSDPtreeInterface
{
    public:
        drxpTestMode_Type_t ch1BitD;
        drxpTestMode_Type_t ch2BitC;
        drxpTestMode_Type_t ch3BitB;

    public:
        drxpTestMode_t() : ch1BitD(DRXP_TestMode_No),
            ch2BitC(DRXP_TestMode_No),
            ch3BitB(DRXP_TestMode_No) { 
        }

        std::string getTestModeStr(drxpTestMode_Type_t t) const;

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
