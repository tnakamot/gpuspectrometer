/**
 * \class channelAverageRegion_t
 *
 * Container for XSD ChannelAverageRegionType
 */
#ifndef CHANNELAVERAGEREGION_T_HXX
#define CHANNELAVERAGEREGION_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class channelAverageRegion_t : public XSDPtreeInterface
{
    public:
        unsigned int startChannel;
        unsigned int numberChannels;

    public:
        channelAverageRegion_t() : startChannel(0), numberChannels(0) { }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (numberChannels > 0);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
