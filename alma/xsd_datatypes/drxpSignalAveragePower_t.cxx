#include "drxpSignalAveragePower_t.hxx"

namespace XSD {

bool drxpSignalAveragePower_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool drxpSignalAveragePower_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
