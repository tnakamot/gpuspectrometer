/**
 * \class drxpMetaframeDelay_t
 *
 * Container for DRXP Metaframe Delay measurements as in DRXP User Manual
 */
#ifndef DRXPMETAFRAMEDELAY_T_HXX
#define DRXPMETAFRAMEDELAY_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpMetaframeDelay_t : public XSDPtreeInterface
{
    public:
        unsigned int ch1BitD;
        unsigned int ch2BitC;
        unsigned int ch3BitB;

    public:
        drxpMetaframeDelay_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
