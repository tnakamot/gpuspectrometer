#include "Apc_t.hxx"

namespace XSD {

bool Apc_t::get(const boost::property_tree::ptree& in)
{
    expectWvr = in.get("expectWvr", false);
    wvrIntegrationDuration = in.get("wvrIntegrationDuration", 48e-3);
    return validate();
}

bool Apc_t::put(boost::property_tree::ptree& out) const
{
    out.put("expectWvr", expectWvr ? "true" : "false");
    out.put("wvrIntegrationDuration", wvrIntegrationDuration);
    return validate();
}

}
