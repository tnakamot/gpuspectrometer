/**
 * \class drxpPowerAlarm_t
 *
 * Container for XSD DrxpPowerAlarmType
 */
#ifndef DRXPPOWERALARM_T_HXX
#define DRXPPOWERALARM_T_HXX

#include "drxpBitPowerAlarm_t.hxx"
#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpPowerAlarm_t : public XSDPtreeInterface
{
    public:
        drxpBitPowerAlarm_t ch1BitD;
        drxpBitPowerAlarm_t ch2BitC;
        drxpBitPowerAlarm_t ch3BitB;

    public:
        drxpPowerAlarm_t() : ch1BitD(), ch2BitC(), ch3BitB() { }

    public:
        bool validate() const {
            return ch1BitD.validate() && ch2BitC.validate() && ch3BitB.validate();
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
