#include "drxpDfrVoltageStatus_t.hxx"

namespace XSD {

bool drxpDfrVoltageStatus_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpDfrVoltageStatus_t::put(boost::property_tree::ptree& out) const
{
    using boost::property_tree::ptree;

    // Note: drxpStatus with elements like <p0.95VForFpgaCore>false</p0.95VForFpgaCore> have to use an
    // alternate delimiter call e.g. pt.put(boost::property_tree::ptree::path_type("p0.95VForFpgaCore", '_'), false);

    out.put(ptree::path_type("p0.95VForFpgaCore", '|'), p0_95VForFpgaCore);
    out.put(ptree::path_type("p1.8VForFpga", '|'), p1_8VForFpga);
    out.put(ptree::path_type("p1.0VmgtavccForFpga", '|'), p1_0VmgtavccForFpga);
    out.put(ptree::path_type("p1.2VmgtavttForFpga", '|'), p1_2VmgtavttForFpga);
    out.put(ptree::path_type("p1.8VmgtvccauxForFpga", '|'), p1_8VmgtvccauxForFpga);
    out.put(ptree::path_type("p3.3VForFpgaIo", '|'), p3_3VForFpgaIo);
    out.put(ptree::path_type("p1.35VDdrForDdr", '|'), p1_35VDdrForDdr);
   
    return true;
}

}
