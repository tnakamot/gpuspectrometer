/**
 * \class
 *
 * Container for XSD InputRefTempType
 */
#ifndef INPUTREFTEMP_T_HXX
#define INPUTREFTEMP_T_HXX

#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD
{

class inputRefTemp_t : public XSDPtreeInterface
{
    public:
        unsigned int index;
        std::vector<double> chanRefTemp;

    public:
        inputRefTemp_t() : index(0) {
            chanRefTemp.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (chanRefTemp.size() >= 1);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
