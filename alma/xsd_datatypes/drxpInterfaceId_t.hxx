/**
 * \class drxpInterfaceId_t
 *
 * Container for XSD DrxpInterfaceIdType
 */
#ifndef DRXPINTERFACEID_T_HXX
#define DRXPINTERFACEID_T_HXX

#include "xsd_ptree_iface.hxx"

#include <string>

namespace XSD
{

class drxpInterfaceId_t : XSDPtreeInterface
{
    public:
        std::string deviceId;   /// PCI root e.g. 3d:00 ($ lspci | grep Xilinx: "3d:00.0 Memory controller: Xilinx Corporation Device 903f")
        int InterfaceNumber;    /// 1 or 2

    public:
        drxpInterfaceId_t() : deviceId("00:00"), InterfaceNumber(1) { }

        bool operator==(const drxpInterfaceId_t& rhs) const {
            return (deviceId == rhs.deviceId) && (InterfaceNumber == rhs.InterfaceNumber);
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (InterfaceNumber==1 || InterfaceNumber==2);
            // hard to check if valid in the true sense of the DRXP interface actually being physically present
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
