/**
 * \class drxpTemperatures_t
 *
 * Container for DRXP Temperatures
 *
 */
#ifndef DRXPTEMPERATURES_H
#define DRXPTEMPERATURES_H

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpTemperatures_t : public XSDPtreeInterface
{
    public:
        float xfp1;
        float xfp2;
        float xfp3;
        float fpga;

    public:
        drxpTemperatures_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
