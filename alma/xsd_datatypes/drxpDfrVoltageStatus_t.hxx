/**
 * \class drxpDfrVoltageStatus_t
 *
 * Container for XSD DrxpDfrVoltageStatusType
 */
#ifndef DRXPDFRVOLTAGESTATUS_T_HXX
#define DRXPDFRVOLTAGESTATUS_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpDfrVoltageStatus_t : public XSDPtreeInterface
{
    public:
        bool p0_95VForFpgaCore;
        bool p1_8VForFpga;
        bool p1_0VmgtavccForFpga;
        bool p1_2VmgtavttForFpga;
        bool p1_8VmgtvccauxForFpga;
        bool p3_3VForFpgaIo;
        bool p1_35VDdrForDdr;

    public:
        drxpDfrVoltageStatus_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;
};

}

#endif

