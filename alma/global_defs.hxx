#ifndef GLOBAL_DEFS_HXX
#define GLOBAL_DEFS_HXX

///////////////////////////////////////////////////////////////////////////////////////
//// Defaults overrideable by JSON user config
///////////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_LOG_FILE           "ASM_MC_service.log"
#define DEFAULT_LOG_LEVEL                       "error"

#define DEFAULT_IF_NUMBER                            1
#define DEFAULT_SCHEMA_FILE_PATH "/opt/alma/conf/ASC_MC.xsd"
#define DEFAULT_CONFIG_FILE_PATH "/opt/alma/conf/ASM_MC_config.json"
#define DEFAULT_MC_TCP_PORT                       1337
#define DEFAULT_ASC_TCP_PORT                      1234
#define DEFAULT_ASC_UDP_PORT                      1234
#define DEFAULT_STARTOBS_MINIMUM_LEADTIME_SECS       2 /// Seconds that the time in a M&C startObservation (if specified) must lie in the future

#define DEFAULT_DRXP_DEVICE               "/dev/drxpd0"
#define DEFAULT_DRXP_MCAST_GROUP            "224.0.0.1"
#define DEFAULT_DRXP_MCAST_PORT                  30001
#define DEFAULT_DRXP_TIMEALIGN_GRANULARITY_MS       48

//#define DEBUG_MIME_TO_STDOUT      yes     /// Debug: define in order to output MIME/BDF data to console; comment out to enable file output
#define DEFAULT_MIME_OUTPUT_PATH "/tmp/"    /// The root path where to place MIME output files

#define DEFAULT_SIMULATION_MIME_INPUT "/opt/alma/share/uid__X0_X1_X4.mime"
#define DEFAULT_SIMULATION_DTS_INPUT ""
#define DEFAULT_INTERNAL_DFT_LENGTH 1048576 /// FFT length 262.144us
#define DEFAULT_INTERNAL_DFT_INPUTSTEP 1000000 /// FFT stepping of 250us (overlapped FFTs to fit into 1 ms)

///////////////////////////////////////////////////////////////////////////////////////
//// DRXP
///////////////////////////////////////////////////////////////////////////////////////

#define USE_DRXP_PRODUCTION_SAMPLELAYOUT /// define to use F2 layout; un-define to use prototype F1 layout
#define DRXP_DT_MSEC 48
#define DRXP_BYTE_PER_MILLISEC 3000000 /// 2 pol x 4e9 samples/sec * 3 bit(raw)/sample * 1ms = 3000000 byte; also 144000000 Groupsize / 48 = 3000000 byte
#define DRXP_SAMP_PER_MILLISEC 4000000 /// 4 Msamples per millisecond in 1 polarization
#define DRXP_SAMP_PER_SEC   4000000000

///////////////////////////////////////////////////////////////////////////////////////
//// Spectra
///////////////////////////////////////////////////////////////////////////////////////

#define SPEC_INVALID_FLAG      -1.0

///////////////////////////////////////////////////////////////////////////////////////
//// M&C Observation queue timing
///////////////////////////////////////////////////////////////////////////////////////

#define OBS_QUEUE_SLEEP_MSEC           250 /// Millisecond interval at which the observation queue should be re-checked
#define OBS_QUEUE_REPORT_MSEC         5000 /// Millisecond interval at which the state of the obs. queue should be logged
#define OBS_QUEUE_MAX_LENGTH         65535 /// Maximum nr of entries in observing queue
#define OBS_QUEUE_PURGE_AGE_SEC         60 /// Maximum time past stop time of observation for which to keep observation in queue storage

///////////////////////////////////////////////////////////////////////////////////////
//// Correlation
///////////////////////////////////////////////////////////////////////////////////////

#define MAX_DELAYLIST_LENGTH        100 /// Maximum number of delay_t items in delayList_t
#define MAX_DELAYLIST_LIST_LENGTH   32  /// Maximum number of delayList_t to store

///////////////////////////////////////////////////////////////////////////////////////
//// Antenna IF range
///////////////////////////////////////////////////////////////////////////////////////
// Used to determine actual placement of spectral channels requested by MC Configuration

#define ANTENNA_IF_LOWEDGE_MHZ      2000.0
#define ANTENNA_IF_HIGHEDGE_MHZ     4000.0
#define ANTENNA_IF_SIDEBAND_USB     true

#define MIN_SPECTROMETER_INPUTS         1
#define MAX_SPECTROMETER_INPUTS         4   /// The maximum of 'SpectrometerInputType'

///////////////////////////////////////////////////////////////////////////////////////
//// XML
///////////////////////////////////////////////////////////////////////////////////////

#ifndef HAVE_XERCES
    #define HAVE_XERCES 0
#endif

#define XML_INVALID_CMD_NAME "no_command"   // TODO: any better choice?
#define XML_INVALID_CMD_ID   0              // TODO: any better choice?

///////////////////////////////////////////////////////////////////////////////////////
//// NTP time checks
///////////////////////////////////////////////////////////////////////////////////////

#define NTP_PORT            123
#define NTP_CLIENT_HOST     "127.0.0.1"
#define NTP_MAX_REATTEMPTS  5
#define NTP_TIMEOUT_MSEC    300

#define NTP_REQUIRED_PEER   "ntp.osf.alma.cl"   /// Hostname (not IP) of the ALMA NTP server that the ASM ntpd is required to be associated with
#define NTP_TESTENV_PEER    "pi3-gps1"          /// Hostname (not IP) of an NTP server in a test environment
#define NTP_MINIMUM_STRATUM                2    /// Minimum stratum required for ASM observing (specs: 2, test server with no stratum 1 ref: 4)
#define NTP_MAXIMUM_SYNC_DISTANCE_MSEC  21.0    /// Maximum allowed synchronization distance for ASM observing (root delay/2 + root dispersion; cf. section 6.5 of document "ACA Spectrometer Module Timestamping Accuracy")
#define NTP_NUM_ASSOCIATIONS_REQUIRED      1    /// Number of peer associations of NTP server; only 1 because use of 2+ NTP servers caused large time steps in the ACA Correlator computers

///////////////////////////////////////////////////////////////////////////////////////
//// CUDA
///////////////////////////////////////////////////////////////////////////////////////

#define CUDA_CFG_POOL_MAXWORKERS 8*16


#endif // GLOBAL_DEFS_HXX
