
#include "xml2api/xml2api_factory.hxx"
#include "version.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getVersion);

int MCMsgCommandResponse_getVersion::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());
    out.put("response.getVersion.completionCode", "success");
    out.put("response.getVersion.version", ASM_VERSION_STR);

    return 0;
}
