#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_systeminformation.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getSystemDetails);

int MCMsgCommandResponse_getSystemDetails::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());
    out.put("response.getSystemDetails.completionCode", "success");

    MCSystemInformation info;
    //out.put_child("response.getSystemDetails", info.get()); // problem: deletes 'completionCode'
    //out.add_child("response.getSystemDetails", info.get()); // problem: adds new <getSystemDetails> after first <getSystemDetails>
    info.fill(out.get_child("response.getSystemDetails")); // workaround: let SystemInfo fill out tree directly 

    return 0;
}
