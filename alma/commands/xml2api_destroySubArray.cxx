/**
 * \class MCMsgCommandResponse_destroySubArray
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(destroySubArray);

int MCMsgCommandResponse_destroySubArray::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    ptree p = in.get_child("subArray");
    XSD::subArray_t subarray;
    subarray.get(p);

    if (!asm_.getSubarrays().hasSubarray(subarray)) {
        out.put("response.commandId", this->getId());
        out.put("response.setSubArray.completionCode", "non-existing-subArrayId");
        return 0;
    }

    asm_.getSubarrays().delSubarray(subarray);

    out.put("response.commandId", this->getId());
    out.put("response.destroySubArray.completionCode", "success");

    return 0;
}
