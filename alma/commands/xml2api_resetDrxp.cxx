
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_drxp_status.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(resetDrxp);

int MCMsgCommandResponse_resetDrxp::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    MCDRXPStatus ds;
    boost::property_tree::ptree dsp;

    asm_.getDRXP()->resetDevice();
    asm_.getDRXP()->deviceReadout(ds);
    ds.getDrxpStatus(dsp);

    out.put("response.commandId", this->getId());
    out.put("response.resetDrxp.completionCode", "success");
    out.add_child("response.resetDrxp.status", dsp); // ICD says "response.resetDrxp.drxpStatus", schema says "response.resetDrxp.status"...

    return 0;
}
