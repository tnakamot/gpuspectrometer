/**
 * \class MCMsgCommandResponse_loadConfiguration
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getAllConfigurationIds);

int MCMsgCommandResponse_getAllConfigurationIds::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());

    // Schema requires 'completionCode', ICD is missing it. Add code...?
    out.put("response.getAllConfigurationIds.completionCode", "success");

    // Append config IDs
    MCConfigurationsMap_t::iterator it = asm_.getConfigsMap().begin();
    while (it != asm_.getConfigsMap().end()) {
#if 0
        // Series of <data><sequence>...</sequence> <sequence>...</sequence> <sequence>...</sequence></data> as in ICD
        // http://stackoverflow.com/questions/16136605/adding-nodes-with-the-same-key-to-a-property-tree
        boost::property_tree::ptree t;
        t.put("configId", it->second.configId);
        t.put("spectrometerConfiguration", "");
        out.add_child("response.getAllConfigurationIds.data.sequence", t);
#else
        // Series of <configId>...</configId> <configId>...</configId> <configId>...</configId> as in Schema
        out.add("response.getAllConfigurationIds.configId", it->second.configId);
#endif
        it++;
    }

    return 0;
}
