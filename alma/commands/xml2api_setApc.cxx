/**
 * \class MCMsgCommandResponse_setApc
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setApc);

int MCMsgCommandResponse_setApc::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    XSD::Apc_t apc;
    apc.get(in);

    // TODO: store somewhere useful
    asm_.getCorrelationSettings().apc = apc;

    out.put("response.commandId", this->getId());
    out.put("response.setApc.completionCode", "success");

    return 0;
}
