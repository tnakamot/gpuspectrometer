
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_spectrometerstate.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(stopSelfTest);

int MCMsgCommandResponse_stopSelfTest::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    boost::property_tree::ptree dsp;

    out.put("response.commandId", this->getId());

    MCSpectrometerState::OperatingMode_t currmode = asm_.getStateContainer().getOperatingMode();
    if ((currmode == MCSpectrometerState::NormalDataMode) || (currmode == MCSpectrometerState::TestDataMode)) {
        out.put("response.stopSelfTest.completionCode", "no-test-running");
    }

// TODO: wait for completion of test and return the actual result
//    g.gMC---.joinSelfTest();

    out.put("response.stopSelfTest.completionCode", "success");  // what if self-test could not be stopped?
    out.put("response.stopSelfTest.status", "ready"); // should return "module status" i.e. ::InternalState_str()
                                                      // would make more sense to returnthe self-test result

    return 0;
}
