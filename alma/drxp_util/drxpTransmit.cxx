#include "drxp/DRXP.hxx"
#include "mcwrapping/mc_drxp_status.hxx"

void usage(void)
{
    std::cout << "Usage: drxpTransmit <0|1|2|3> <txfile>\n"
              << "  0|1|2|3 : choose /dev/drxp0 (default) or /dev/drxp<n>\n"
              << "  txfile  : file that should be repeatedly sent (144MB)\n";
    exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
    int iface = 0;
    char *pattern;
    DRXP drxp;

    if (argc != 3) {
        usage();
    }

    iface = argv[1][0] - '0';
    if ((iface < 0) || (iface > 3)) {
        usage();
    }

    pattern = argv[2];
    if (!drxp.loadPattern(pattern)) {
        usage();
    }

    drxp.selectDevice(iface, false);
    drxp.resetDevice();
    drxp.startTx();

    MCDRXPStatus ds;
    drxp.startPeriodicMonitoring(DRXP_MONITOR_MULTICAST_GROUP, DRXP_MONITOR_MULTICAST_PORT, ds);
    std::cout << "Reading out counters repeatedly in monitoring thread.\n"
              << "Please use UDP multicast client to see monitoring data.\n"
              << "Press Ctrl-C to stop.\n";

    while(1) { };

    return 0;
}
