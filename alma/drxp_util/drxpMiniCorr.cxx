/**
 * Small correlator implementation DRXP -> GPU/CPU without any M&C
 */

#include "drxp/DRXP.hxx"
#include "drxp/DRXPDummy.hxx"
#include "drxp/drxp_const.hxx"    // common DRXP constants
#include "drxp/drxp_bitfields.hxx"// helpful bitfields, structs, and GCC_BITFLD_ORDER defining the bit order
#include "drxp/SFF8431_I2C.h"     // structures for decoding SFF-8431 I2C data fields

#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/SFF8431_infos.hxx"

#include "helper/logger.h"
#include "mcwrapping/mc_drxp_status.hxx"
#include "mcwrapping/mc_observation.hxx"

#include "spectralprocessors/SpectralProcessorGPU.hxx"
#include "spectralprocessors/SpectralProcessorCPU.hxx"
#include "datarecipients/ResultRecipientPlain.hxx"

#include <iomanip>
#include <iostream>
#include <cmath>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#define DEFAULT_PATTERN_FILE "/opt/alma/share/drxp_rxtx_pattern.bin"

void usage()
{
    std::cout << "\n"
        << "Usage: drxpMiniCorr <drxpId 0..3|-1=dummy> <gpuId 0..N>\n\n"
        << "Default input in dummy mode comes from " << DEFAULT_PATTERN_FILE << ",\n"
        << "assumed to be in DRXP Data Format.\n"
        << std::endl;
}

int main(int argc, char** argv)
{
    int iface = 0;
    int affinity[1] = {0};
    bool rx = true;

    // Command line args
    if (argc != 3) {
        usage();
        exit(EXIT_FAILURE);
    }
    iface = atoi(argv[1]);
    affinity[0] = atoi(argv[2]);

    // Create objects
    ResultRecipientPlain out("/tmp/");
    SpectralProcessorGPU spu;
    MCObservation obs;
    MCDRXPStatus s;

    DRXPIface* drxp;
    if (iface <= -1) {
       drxp = new DRXPDummy();
    } else {
       drxp = new DRXP();
    }

    // Dummy observation to process
    obs.observationId = "xyz987654321";
    obs.cfg.createTestconfig1();

    // Prepare the Spectral Processor
    spu.setAffinity(/*nGPU*/1, affinity);
    spu.initialize(&obs);
    spu.startPeriodicMonitoring(DRXP_MONITOR_MULTICAST_GROUP, DRXP_MONITOR_MULTICAST_PORT+1);

    // Output writer for storing results
    out.configureFrom(&obs);
    out.open();
    spu.setSpectralRecipient(&out);

    // Prepare the DRXP
    drxp->selectDevice(iface, rx);
    drxp->resetDevice();
    drxp->loadPattern(DEFAULT_PATTERN_FILE);
    drxp->startPeriodicMonitoring(DRXP_MONITOR_MULTICAST_GROUP, DRXP_MONITOR_MULTICAST_PORT, s);

    // Start data reception and direct it from DRXP into Spectral Processor
    drxp->startRx();
    drxp->attachRxRecipient(&spu);

    while (1) {
        sleep(5);
    }

    return 0;
}
