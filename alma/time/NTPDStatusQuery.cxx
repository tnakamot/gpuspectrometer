/**
 * \class NTPDStatusQuery
 *
 * \brief Functionality to check NTP Daemon (local) for synchronization and quality
 *
 * Contains a comparison of NTP statistics against the requirements stated under
 * Section 6.5 of the document "ACA Spectrometer Module Timestamping Accuracy" are met.
 *
 * The local NTP daemon on ASM is queried over NTP Mode 6 commands (UDP/IP).
 * Reference documentation cf. https://docs.ntpsec.org/latest/mode6.html
 *
 * Requirements for proper NTP sync based on Section 6.5 are:
 *   #1 Leap field in the system status is not 3 (leap_alarm).
 *   #2 Source field in the system status is 6 (sync_ntp).
 *   #3 Stratum of the system variable is 2.
 *   #4 refid of the system variable is the IP address of the NTP server: ntp.osf.alma.cl with IP via gethostbyname()
 *   #5 Select field in the peer status is 6 (sel_sys.peer); https://manpages.debian.org/ntp/ntpq.1.en.html#peer
 *   #6 Synchronization distance is <= 21 milliseconds
 *   #7 Only one association (peer) is allowed; 2+ NTP servers caused large time steps in the ACA Correlator computers
 *
 * \author $Author: janw $
 *
 */

#include "time/NTPDStatusQuery.hxx"
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include <iostream>
#include <limits>
#include <cmath>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////

static const char* C_request_ntp_system_vars = "offset,refid,stratum,rootdelay,rootdisp,peer";
static const char* C_request_ntp_peer_vars = "jitter";

//////////////////////////////////////////////////////////////////////////////////////////////////////////

NTPDStatusQuery::NTPDStatusQuery(bool isProductionEnv) : m_ntpd_hostname(NTP_CLIENT_HOST),
    m_ntpd_port(NTP_PORT), m_productionEnvironment(isProductionEnv)
{
    /* Defaults */
    m_synchronizationdistance = -1;
    m_synced = false;
    m_error = false;
    m_stratum = 15;
    m_rootdelay = -1;
    m_rootdispersion = -1;
    m_refId = "0.0.0.0";
    m_peer_jitter = -1;
    if (m_productionEnvironment) {
        m_requiredPeer = NTP_REQUIRED_PEER;
    } else {
        m_requiredPeer = NTP_TESTENV_PEER;
    }

    /* Read data from NTP server */
    readoutNTP();
}

bool NTPDStatusQuery::requirementsMet()
{
    /* Defaults */
    m_synchronizationdistance = -1;
    m_synced = false;
    m_error = false;
    m_stratum = 15;

    /* Get all relevant NTP data and check them */
    if (!readoutNTP()) {
        return false;
    }

    if (!validateConditions()) {
        return false;
    }

    return m_synced;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool NTPDStatusQuery::queryMode6(std::string request_vars, int opCode, int associationId, NTPMode6Packet_t& response) const
{
    // See https://docs.ntpsec.org/latest/mode6.html
    // mode 6, op CTL_OP_READVAR (2) : "read system or peer variables"

    /* Connect to NTP server (local ntpd) */
    struct sockaddr_in sock;
    memset(&sock, 0, sizeof(sock));
    sock.sin_family = AF_INET;
    sock.sin_addr.s_addr = inet_addr(m_ntpd_hostname.c_str());
    sock.sin_port = htons(m_ntpd_port);
    int sd = socket(PF_INET, SOCK_DGRAM, 0);
    connect(sd, (struct sockaddr *)&sock, sizeof(sock));

    /* Query the server with a few reattempts until UDP/IP has a reply */
    int attempt = 0;
    memset(&response, 0, sizeof(response));
    while (attempt < NTP_MAX_REATTEMPTS) {

        // Send a Mode 6 request to ask for values of 'request_vars'
        NTPMode6Packet_t req;
        memset(&req, 0, sizeof(req));
        strcpy((char*)req.udata.data, request_vars.c_str());
        req.li_vn_mode = (4 << 3) | 6;  // version 4 | mode 6
        req.r_m_e_op = opCode;          // 8-bit, CTL_OP_READVAR(2) or CTL_OP_READSTAT(1)
        req.sequence = htons(1);
        req.offset   = htons(0);
        req.count    = htons(strlen((char *)req.udata.data));
        req.associd = htons((uint16_t)associationId);
        send(sd, &req, sizeof(req), 0);

        L_(ldebug) << "Requested '" << request_vars << "' from " << m_ntpd_hostname << ":" << m_ntpd_port
            << " for assoc=" << associationId << " via opcode=" << opCode;

        // Wait for response
        fd_set fds;
        struct timeval tv;
        FD_ZERO(&fds);
        FD_SET(sd, &fds);
        tv.tv_sec = 0;
        tv.tv_usec = NTP_TIMEOUT_MSEC*1000;
        int n = select(sd+1, &fds, (fd_set *)0, (fd_set *)0, &tv);
        if (n == 0) {
            L_(ldebug)
                << "NTP query attempt " << (attempt+1) << " to NTP server "
                << m_ntpd_hostname << ":" << m_ntpd_port << " timed out";
            ++attempt;
            continue;
        } else if (n == -1) {
            L_(ldebug)
                << "NTP query attempt " << (attempt+1) << " to NTP server "
                << m_ntpd_hostname << ":" << m_ntpd_port << " had select() error";
            ++attempt;
            continue;
        }

        // Get response
        n = recv(sd, &response, sizeof(response), 0);
        break;
    }

    if (attempt >= NTP_MAX_REATTEMPTS) {
        L_(lerror) << "No reply from NTP server " << m_ntpd_hostname << ":" << m_ntpd_port;
        return false;
    }

    /* Convert 16-bit/32-bit fields */
    response.sequence = ntohs(response.sequence);
    response.status = ntohs(response.status);
    response.associd = ntohs(response.associd);
    response.offset = ntohs(response.offset);
    response.count = ntohs(response.count);

    /* Check validity of response */
    const int version = (response.li_vn_mode >> 3) & 0x07;
    const int mode = response.li_vn_mode & 0x07;
    if (opCode == CTL_OP_READVAR && strlen((const char*)response.udata.data) < 1) {
        if (associationId == 0) {
            L_(lerror) << "No proper textual reply from NTP server " << m_ntpd_hostname << ":" << m_ntpd_port
                << " : " << ((const char*)response.udata.data);
        } else {
            L_(lerror) << "No proper textual reply from NTP server " << m_ntpd_hostname << ":" << m_ntpd_port
                << " for peer " << associationId
                << " : " << ((const char*)response.udata.data);
        }
        return false;
    }
    if (version != 4 || mode != 6) {
        L_(lerror) << "Unexpected NTP server reply: NTP version " << version << " mode " << mode << " instead of v4 mode 6";
        return false;
    }
    if (response.r_m_e_op & FLAG_ERROR) {
        L_(lerror) << "Unexpected NTP server reply: response had the Error flag set";
        return false;
    }
    if (response.r_m_e_op & FLAG_MORE) {
        L_(lwarning) << "Unsupported NTP server reply: the More flag set, but NTPDStatusQuery does not support multi-UDP-packet responses";
        //return false;
    }

    return true;
}


std::map<std::string,std::string> NTPDStatusQuery::tokenizeNtpResponse(std::string response) const
{
    typedef boost::tokenizer<boost::char_separator <char> > wstokenizer;

    std::map<std::string,std::string> keyValMap;

    boost::trim(response); // or trim_all()
    response.erase(std::remove(response.begin(), response.end(), '\r'), response.end());
    response.erase(std::remove(response.begin(), response.end(), '\n'), response.end());
    response.erase(std::remove(response.begin(), response.end(), '\t'), response.end());

    L_(ldebug) << "NTP server " << m_ntpd_hostname << ":" << m_ntpd_port << " replied with " << response;

    boost::char_separator<char> sep(",");
    wstokenizer tok(response, sep);
    for (wstokenizer::iterator b=tok.begin(); b!=tok.end(); b++) {

        std::string key_val(*b);
        boost::trim(key_val); // or trim_all()
        //key_val.erase(std::remove(key_val.begin(), key_val.end(), ' '), key_val.end());

        int sepPos = key_val.find('=');
        if (key_val.length() > 4 && sepPos > 0) {
            std::string key = key_val.substr(0, sepPos);
            std::string val = key_val.substr(sepPos+1);
            keyValMap.insert(std::pair<std::string,std::string>(key, val));
            //std::cout <<"     tok : >" << key << " : " << val << "<\n";
        }
    }

    return keyValMap;
}


bool NTPDStatusQuery::compareIPv4Addr(std::string ip, std::string hostname) const
{
    struct hostent* host = gethostbyname(hostname.c_str());
    if (host != NULL) {
        for (int i = 0; host->h_addr_list[i]; ++i) {
            struct sockaddr_in sock_addr;
            char hostip[16] = {'\0'};
            sock_addr.sin_addr = *((struct in_addr*) host->h_addr_list[i]);
            inet_ntop(AF_INET, &sock_addr.sin_addr, hostip, sizeof(hostip));
            if (std::string(hostip) == ip) {
                return true;
            }
        }
    } else {
        L_(lerror) << "Could not resolve IP of " << hostname;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool NTPDStatusQuery::readoutNTP()
{
    /* Read out the local NTP server System Status */
    NTPMode6Packet_t sysstats;
    if (!queryMode6("associations", CTL_OP_READSTAT, ASSOCIATION_ID_HOST, sysstats)) {
        m_error = true;
        return false;
    }
    // peers are listed in binary in sysstats.data.u32 as pairs of 16-bit assocId + 16-bit flag word
    m_numpeers = sysstats.count / sizeof(uint32_t);

    /* Read out the local NTP server System Variables */
    NTPMode6Packet_t sysvars;
    if (!queryMode6(C_request_ntp_system_vars, CTL_OP_READVAR, ASSOCIATION_ID_HOST, sysvars)) {
        m_error = true;
        return false;
    }

    std::string sysvarsStr((const char*)sysvars.udata.data);
    std::map<std::string,std::string> sysItems = tokenizeNtpResponse(sysvarsStr);

    m_rootdelay = atof(sysItems["rootdelay"].c_str());
    m_rootdispersion = atof(sysItems["rootdisp"].c_str());
    m_refId = sysItems["refid"];
    m_stratum = atoi(sysItems["stratum"].c_str());
    m_peer = atoi(sysItems["peer"].c_str());
    m_leapindicator = sysvars.li_vn_mode >> 6;
    m_syncsource = (sysvars.status >> 8) & 0x3F;

    int leapindicator2 = (sysvars.status >> 14);
    if (m_leapindicator != leapindicator2) {
        L_(lwarning) << "Unexpected NTP server reply inconsistency, header has leap indicator of " << m_leapindicator << " vs. status field leap indicator of " << leapindicator2;
    }


    /* Query the first association to get Peer Status word */
    NTPMode6Packet_t peerstat;
    if (!queryMode6(C_request_ntp_peer_vars, CTL_OP_READVAR, m_peer, peerstat)) {
        m_error = true;
        return false;
    }

    std::string peerStatusStr((const char*)peerstat.udata.data);
    std::map<std::string,std::string> peerItems = tokenizeNtpResponse(peerStatusStr);

    m_peer_jitter = atof(peerItems["jitter"].c_str());
    m_peerselectflag = (peerstat.status >> 8) & 0x07;

    return true;
}


bool NTPDStatusQuery::validateConditions()
{
    /* Check validity of sync */
    m_synced = true;
    // req#1 "Leap field in the system status is not 3 (leap_alarm)."
    m_synced &= (m_leapindicator != LEAP_IND_NO_SYNC);
    // req#2 "Source field in the system status is 6 (sync_ntp)."
    m_synced &= (m_syncsource == SOURCE_ID_NTP);
    // req#3 "Stratum of the system variable is 2."
    m_synced &= (m_stratum <= NTP_MINIMUM_STRATUM);
    // req#4 "refid of the system variable is the IP address of the NTP server."
    //       should equal NTP server: ntp.osf.alma.cl with IP via gethostbyname()
    // TODO: cannot reliably check, without querying all IPs of all local network interfaces
    m_refidIsCorrect = compareIPv4Addr(m_refId, m_requiredPeer);
    m_synced &= m_refidIsCorrect;
    // req#5 "Select field in the peer status is 6 (sel_sys.peer)."
    m_synced &= (m_peerselectflag == CTL_PST_SEL_SYSPEER);
    // req#6 Sychronization distance does not exceed 21 ms
    m_synchronizationdistance = m_rootdelay/2 + m_rootdispersion;
    m_synchronizationdistance += m_peer_jitter; // Section 6.4: root dispersion from NTP incorrectly lacks peer jitter, add!
    m_synced &= (m_synchronizationdistance < NTP_MAXIMUM_SYNC_DISTANCE_MSEC);
    // #7 Only one association (peer) is allowed; 2+ NTP servers caused large time steps in the ACA Correlator computers
    m_synced &= (m_numpeers == NTP_NUM_ASSOCIATIONS_REQUIRED);

    /* More detailed reporting for the log file */
    if (!m_synced) {
        L_(lerror) << "Time sync error! NTP server " << m_ntpd_hostname << ":" << m_ntpd_port
            << " badly synchronized! Details follow in the next log entries.";
    }

    if (!m_refidIsCorrect) {
        L_(lerror) << "NTP server mismatch: ASM requires " << m_requiredPeer
            << " (and its IPv4 addresse(s)) but NTP server has different peer with IP of " << m_refId;
    }

    if (m_stratum > NTP_MINIMUM_STRATUM) {
        L_(lerror) << "NTP stratum is " << m_stratum << " but ASM timings require stratum "
            << NTP_MINIMUM_STRATUM << " or better.";
    }

    if (m_syncsource != SOURCE_ID_NTP) {
        L_(lerror) << "NTP sync source is " << m_syncsource << " but required is " << SOURCE_ID_NTP << " (sync_ntp)!";
    }

    if (m_peerselectflag != CTL_PST_SEL_SYSPEER) {
        L_(lerror) << "NTP peer " << m_refId << " has selection status " << m_peerselectflag << " but required is "
            << CTL_PST_SEL_SYSPEER << " (sel_sys.peer; '*' in ntpq)!";
    }

    if (m_numpeers != NTP_NUM_ASSOCIATIONS_REQUIRED) {
        L_(lerror) << "NTP server reports " << m_numpeers << " peers, ASM requires exactly " << NTP_NUM_ASSOCIATIONS_REQUIRED;
    }

    if (m_synchronizationdistance > NTP_MAXIMUM_SYNC_DISTANCE_MSEC) {
        L_(lerror)
            << "NTP reports root delay " << m_rootdelay << " ms, root "
            << "dispersion " << m_rootdispersion << " ms, "
            << "peer jitter " << m_peer_jitter << " ms. "
            << "The resulting synchronization distance of " << m_synchronizationdistance << " "
            << "exceeds " << NTP_MAXIMUM_SYNC_DISTANCE_MSEC << " ms";
    } else {
        // Report good sync distance values, for bookkeeping
        L_(linfo) << "NTP server " << m_ntpd_hostname << ":" << m_ntpd_port << " ok, "
            << "statistics yield a synchronization distance of " << m_synchronizationdistance << " ms, "
            << "within ASM requirement of <=" << NTP_MAXIMUM_SYNC_DISTANCE_MSEC << " ms";
    }

    return m_synced;
}
