#include "helper/logger.h"
#include "time/TimeUTC.hxx"

#include <cmath>
#include <iostream>
#include <iomanip>

#include <unistd.h>

int main(int argc, char** argv)
{
    double utc_s;
    struct timeval tv;
    struct timespec ts;
    std::string s;

    //initLogger("TimeUTC.log", ldebug4);
    std::cout << std::fixed << std::setprecision(3);

    gettimeofday(&tv, NULL);
    TimeUTC::timevalToString(tv, s);
    std::cout << "Current time from gettimeofday() encoded by TimeUTC::timevalToString() = " << s << "\n\n";

    std::string utc1("2016-10-17T13:35:35.000");
    std::cout << "Hardcoded utc_str = " << utc1 << "\n";
    std::cout << "---------------------------------------\n";
    TimeUTC::stringToSeconds(utc1, &utc_s);
    TimeUTC::stringToTimeval(utc1, &tv);
    TimeUTC::stringToTimespec(utc1, &ts);
    std::cout << "UTC " << utc1 << " = " << utc_s << " sec from " << C_UTC_refepstr << "\n";
    std::cout << "UTC " << utc1 << " = " << (utc_s + C_UTC_refsec) << " sec from 1900-01-1\n";
    std::cout << "UTC " << utc1 << " = timeval  { sec:" << tv.tv_sec <<", usec:" << tv.tv_usec << " }\n";
    std::cout << "UTC " << utc1 << " = timespec { sec:" << ts.tv_sec <<", nsec:" << ts.tv_nsec << " }\n";
    std::cout << "---------------------------------------\n\n";

    std::string utc2("2017-01-01T23:59:59.999");
    if (argc == 2) {
        utc2 = std::string(argv[1]);
        std::cout << "User-provided UTC_str = " << utc2 << "\n";
        std::cout << "---------------------------------------\n";
        TimeUTC::stringToSeconds(utc2, &utc_s);
        TimeUTC::stringToTimeval(utc2, &tv);
        TimeUTC::stringToTimespec(utc2, &ts);
        std::cout << "UTC " << utc2 << " = " << utc_s << " sec from " << C_UTC_refepstr << "\n";
        std::cout << "UTC " << utc2 << " = " << (utc_s + C_UTC_refsec) << " sec from 1900-01-1\n";
        std::cout << "UTC " << utc2 << " = timeval  { sec:" << tv.tv_sec <<", usec:" << tv.tv_usec << " }\n";
        std::cout << "UTC " << utc2 << " = timespec { sec:" << ts.tv_sec <<", nsec:" << ts.tv_nsec << " }\n";
        std::cout << "---------------------------------------\n\n";
    } else {
        std::cout << "Hardcoded UTC_str = " << utc2 << "\n";
    }

    for (int n=0; n<4; n++) {
        double dT = TimeUTC::getSecondsSince(utc2);
        std::cout << "Now " << std::fabs(dT) << " seconds "
                  << ((dT > 0) ? "since " : "until ") << utc2 << "\n";
        sleep(1);
    }
    return 0;
}
