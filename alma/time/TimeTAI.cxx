/**
 * \class TimeTAI
 *
 * \brief Conversion from system clock (UTC) to TAI time.
 *
 * References:
 * http://cr.yp.to/libtai.html
 * https://www.ietf.org/timezones/data/leap-seconds.list
 *
 * \author $Author: janw $
 *
 */
#include "helper/logger.h"
#include "time/TimeTAI.hxx"

#include <boost/date_time/local_time/local_time.hpp>

#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cmath>
#include <cstdlib>

#include <sys/time.h>
#include <stdint.h>

namespace pt = boost::posix_time;

///////////////////////////////////////////////////////////////////////////////////////

static const pt::ptime C_TAI_refep(pt::time_from_string(C_TAI_refepstr));

int TimeTAI::m_leapIndex = 0;

///////////////////////////////////////////////////////////////////////////////////////

// Hardcoded table that can be refreshed from a file
TimeTAI::leapSecEntry_t TimeTAI::m_leapSecs[MAX_LEAPSEC_ENTRIES+1] = {
    { 2272060800, 0, 10, " 1 Jan 1972" },
    { 2287785600, 15724800, 11, " 1 Jul 1972" },
    { 2303683200, 31622400, 12, " 1 Jan 1973" },
    { 2335219200, 63158400, 13, " 1 Jan 1974" },
    { 2366755200, 94694400, 14, " 1 Jan 1975" },
    { 2398291200, 126230400, 15, " 1 Jan 1976" },
    { 2429913600, 157852800, 16, " 1 Jan 1977" },
    { 2461449600, 189388800, 17, " 1 Jan 1978" },
    { 2492985600, 220924800, 18, " 1 Jan 1979" },
    { 2524521600, 252460800, 19, " 1 Jan 1980" },
    { 2571782400, 299721600, 20, " 1 Jul 1981" },
    { 2603318400, 331257600, 21, " 1 Jul 1982" },
    { 2634854400, 362793600, 22, " 1 Jul 1983" },
    { 2698012800, 425952000, 23, " 1 Jul 1985" },
    { 2776982400, 504921600, 24, " 1 Jan 1988" },
    { 2840140800, 568080000, 25, " 1 Jan 1990" },
    { 2871676800, 599616000, 26, " 1 Jan 1991" },
    { 2918937600, 646876800, 27, " 1 Jul 1992" },
    { 2950473600, 678412800, 28, " 1 Jul 1993" },
    { 2982009600, 709948800, 29, " 1 Jul 1994" },
    { 3029443200, 757382400, 30, " 1 Jan 1996" },
    { 3076704000, 804643200, 31, " 1 Jul 1997" },
    { 3124137600, 852076800, 32, " 1 Jan 1999" },
    { 3345062400, 1073001600, 33, " 1 Jan 2006" },
    { 3439756800, 1167696000, 34, " 1 Jan 2009" },
    { 3550089600, 1278028800, 35, " 1 Jul 2012" },
    { 3644697600, 1372636800, 36, " 1 Jul 2015" },
    { 3692217600, 1420156800, 37, " 1 Jan 2017" },
    { 0, 0, 0, "" },
};

///////////////////////////////////////////////////////////////////////////////////////

/**
 * \return Seconds since reference epoch 1972-01-01 00:00:00 for time in the given TAI string. True on success.
 */
bool TimeTAI::taiStringToSec(const std::string& tai, double* tai_sec)
{
    const std::string::size_type nexpect = std::string("2017-01-01T23:59:59.999").size();
    if (tai.size() != nexpect) {
        L_(lerror) << "TimeTAI::taiStringToSec() time '" << tai << "' does not match expected format of '2017-01-01T23:59:59.999'";
        return false;
    }

    try {
        std::string s(tai);
        std::replace(s.begin(), s.end(), 'T', ' ');
        *tai_sec = (pt::ptime(pt::time_from_string(s)) - C_TAI_refep).total_milliseconds() * 1e-3;
    } catch (...) {
        L_(lerror) << "TimeTAI::taiStringToSec() failed to parse '" << tai << "'! Use a format like '2017-01-01T23:59:59.999'!";
        return false;
    }
    return true;
}

/** \return Current time in TAI as seconds since reference epoch 1972-01-01 00:00:00 */
void TimeTAI::getCurrentTAI(double* tai_sec)
{
    pt::ptime now_utc = pt::microsec_clock::universal_time();
    double utc_sec = (now_utc - C_TAI_refep).total_milliseconds() * 1e-3;
    //L_(ldebug1) << " leap secs=" << m_leapSecs[findTableIndex(utc_sec)].leapsecs << " for utc_sec=" << utc_sec;
    *tai_sec = utc_sec + (double)m_leapSecs[findTableIndex(utc_sec)].leapsecs;
}

/** \return Current time in TAI as seconds since reference epoch 1972-01-01 00:00:00 */
std::string TimeTAI::strUTC2TAI(std::string utc)
{
    std::replace(utc.begin(), utc.end(), 'T', ' ');
    pt::ptime pt_utc(pt::time_from_string(utc));
    double utc_sec = (pt_utc - C_TAI_refep).total_milliseconds() * 1e-3;
    utc_sec += (double)(m_leapSecs[findTableIndex(utc_sec)].leapsecs);
    return pt::to_iso_extended_string(pt_utc);
}

/** \return Difference in seconds from current time and given time, or NaN on error. */
double TimeTAI::getSecondsSince(const std::string& tai)
{
    double dT;
    double tgiven, tnow;
    getCurrentTAI(&tnow);
    if (!taiStringToSec(tai, &tgiven)) {
        return NAN;
    }
    dT = tnow - tgiven;
    //std::cout << "getSecsSince: curr=" << tnow << ", other=" << tgiven << " (str=" << tai << "), dT=" << dT << "\n";
    return dT;
}

///////////////////////////////////////////////////////////////////////////////////////

int TimeTAI::findTableIndex(const double utc_sec_1972_fp64)
{
    const int64_t utc_sec_1972 = utc_sec_1972_fp64;
    m_leapIndex = 0;
    while (m_leapSecs[m_leapIndex + 1].at_secs_since1972 < utc_sec_1972) {
        if (m_leapSecs[m_leapIndex + 1].at_secs_since1972 == 0) {
            // Reached end of table, keep at last entry
            //L_(ldebug1) << "TimeTAI::findTableIndex() lookup for " << utc_sec_1972 << " secs since 1972 ended at last table entry";
            break;
        } else {
            m_leapIndex++;
        }
    }
    return m_leapIndex;
}

bool TimeTAI::loadLeapseconds(const std::string& filename)
{
    std::string line, tmp;
    m_leapIndex = 0;

    int i = 0;
    std::ifstream f(filename.c_str());
    while (std::getline(f, line) && (i < MAX_LEAPSEC_ENTRIES)) {
        if ((line.size() < 4) || (line[0] == '#')) continue;
        std::stringstream ss(line);
        ss >> m_leapSecs[i].at_secs_since1900;
        ss >> m_leapSecs[i].leapsecs;
        ss >> tmp;
        std::getline(ss, m_leapSecs[i].comment);
        m_leapSecs[i].at_secs_since1972 = m_leapSecs[i].at_secs_since1900 - C_TAI_refsec;
        //std::cout << "Entry " << i << " : " << m_leapSecs[i].at_secs_since1900
        //          << " " << m_leapSecs[i].leapsecs << " -- " << m_leapSecs[i].comment
        //          << " | " << m_leapSecs[i].at_secs_since1972 << " since 1972\n";
        i++;
    }
    if (i >= MAX_LEAPSEC_ENTRIES) {
        L_(lerror) << "TimeTAI::loadLeapseconds file " << filename << " has too many entries (>=" << MAX_LEAPSEC_ENTRIES << ")\n";
        return false;
    }
    if (i == 0) {
        L_(lerror) << "TimeTAI::loadLeapseconds file " << filename << " had no entires\n";
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////

void TimeTAI::tableToCcode()
{
    std::cout << "TimeTAI::leapSecEntry_t TimeTAI::m_leapSecs[MAX_LEAPSEC_ENTRIES+1] = {\n";
    for (int i=0; i<MAX_LEAPSEC_ENTRIES; i++) {
        std::cout << "    { "
                  << m_leapSecs[i].at_secs_since1900 << ", "
                  << m_leapSecs[i].at_secs_since1972 << ", "
                  << m_leapSecs[i].leapsecs << ", "
                  << "\"" << m_leapSecs[i].comment << "\""
                  << " },\n";
        if (m_leapSecs[i].at_secs_since1900 == 0) break;
    }
    std::cout << "};\n\n";
}

///////////////////////////////////////////////////////////////////////////////////////
