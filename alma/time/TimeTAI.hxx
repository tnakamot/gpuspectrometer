/**
 * \class TimeTAI
 *
 * \brief Conversion from system clock (UTC) to TAI time.
 *
 * References:
 * http://cr.yp.to/libtai.html
 * https://www.ietf.org/timezones/data/leap-seconds.list
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_TIME_TAI_HXX
#define MC_TIME_TAI_HXX

#include <string>
#include <cmath>
#include <stdint.h>

#define MAX_LEAPSEC_ENTRIES 1000

#define C_TAI_refepstr  "1972-01-01 00:00:00.0"
#define C_TAI_refsec    2272060800ULL // 1972-01-01 as seconds since 1900-01-01 (used by the IETF leap sec table)

class TimeTAI
{
    public:
        TimeTAI() { }

    public:

        /** \return Current time as TAI time string. */
        std::string getCurrentTAI();

        /** \return Current time as TAI time in seconds since reference epoch 1972. */
        static void getCurrentTAI(double* tai_sec_1972);

        /** \return Seconds since reference epoch 1972 for the time in given TAI string */
        static bool taiStringToSec(const std::string& tai, double* tai_sec);

        /** \return Given UTC time string converted into TAI time string */
        static std::string strUTC2TAI(std::string utc);

        /** \return Time difference in seconds between the current TAI/wallclock time and a specified time. */
        static double getSecondsSince(const std::string& tai);

        /** Load a text file containing updated leap seconds.
            You may use "wget -q https://www.ietf.org/timezones/data/leap-seconds.list -O - | grep -v "^\#" > leap-seconds.list"
            to generate such a leap seconds file from IETF data.
         */
        static bool loadLeapseconds(const std::string& filename);

        typedef struct leapSecEntry_tt {
            int64_t at_secs_since1900;
            int64_t at_secs_since1972;
            int64_t leapsecs;
            std::string comment;
        } leapSecEntry_t;
        static leapSecEntry_t m_leapSecs[MAX_LEAPSEC_ENTRIES+1];
        static int m_leapIndex;

        /** Update global m_leapIndex to point to m_leapSecs[] table index for given UTC.
            \return New m_leapIndex.
         */
        static int findTableIndex(const double utc_sec);

        /** Print the loaded leap seconds table in C format */
        static void tableToCcode();

};

#endif // MC_TIME_TAI_HXX
