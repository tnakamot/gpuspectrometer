/**
 * \class SpectralPostprocessor
 *
 * Helper class that applies post-processing to full-resolution spectra
 * from the spectrometer, combined with histograms, and produces pre-calibrated
 * spectra of all configured spectral windows x switching positions.
 */

#include "datarecipients/SpectralResultSet.hxx"
#include "spectralprocessors/SpectralPostprocessor.hxx"
#include "spectralprocessors/SpectralAverager.hxx"
#include "calibrations/CalibNonlinearity.hxx"
#include "mcwrapping/mc_configuration.hxx"
#include "helper/logger.h"

#include "kernels/histogram_kernels.h" // definition of GPU kernel hist_t (currently 'unsigned int')

#define ORDER_SPW_BIN // define to produce data in SPW BIN order, undefine to use BIN SPW order

/** C'stor */
SpectralPostprocessor::SpectralPostprocessor()
{
    m_interleaveworkbuf_size = 8192;
    m_interleaveworkbuf = new float[m_interleaveworkbuf_size];
}

/** D'stor */
SpectralPostprocessor::~SpectralPostprocessor()
{
    delete[] m_interleaveworkbuf;
}

/** Take raw spectra and histograms in a results set, transform them
 * into final output spectra, and store them into a flat array in the
 * same result set container
 */
bool SpectralPostprocessor::process(SpectralResultSet* rs, const MCConfiguration* cfg)
{
    if (rs == NULL) {
        return false;
    }

    // Factors in non-linearity correction
    const int nlev = 8;

    // Helpers
    SpectralAverager avg;
    CalibratorNonLinearity<hist_t,float> nonLinCorr_X(nlev);
    CalibratorNonLinearity<hist_t,float> nonLinCorr_Y(nlev);
    nonLinCorr_X.setHistogram(rs->histogram_X);
    nonLinCorr_Y.setHistogram(rs->histogram_Y);

    const XSD::basebandConfig_t& bb = cfg->singleConfiguration.basebandConfig;

    // Determine final size expected for the post-processed output data set
    const int Nsw = bb.spectralWindow.size();
    size_t points_in_output = 0;
    for (int sw=0; sw<Nsw; sw++) {
        size_t nch = bb.spectralWindow[sw].effectiveNumberOfChannels;
        nch /= bb.spectralWindow[sw].spectralAveragingFactor;
        switch (bb.spectralWindow[sw].polnProducts) {
            case (XSD::POLN_XX_YY_XY):
                nch *= 4;
                break;
            case (XSD::POLN_XX_YY):
                nch *= 2;
                break;
            default:
                break;
        }
        points_in_output += nch;
    }
    points_in_output *= cfg->procConfig.nswpos;

    // Ensure there is space to store data
    if (rs->bytesinfinalspectra != (points_in_output * sizeof(float))) {
        L_(ldebug) << "SpectralPostprocessor (re)allocating to post-processed output size of " << points_in_output << " float32 total";
        if (rs->finalspectra != NULL) {
            delete[] rs->finalspectra;
        }
        rs->finalspectra = new float[points_in_output];
        rs->bytesinfinalspectra = points_in_output * sizeof(float);
    }

    // Switching positions, spectral points in wide spectra
    const int Npos = cfg->procConfig.nswpos;
    const int Nch_in = (rs->bytesperspectrum/sizeof(float))/4; // {autoX,crossXY,autoY}
    if (Nch_in != cfg->procConfig.nchan) {
        L_(lerror) << "SpectralPostprocessor: got data with " << Nch_in << " channels, expected " << cfg->procConfig.nchan << " channels";
        return false;
    }
    assert(Nch_in == cfg->procConfig.nchan);

    // Copy and calibrate the desired spectral windows
    size_t out_offset = 0;
#ifdef ORDER_SPW_BIN
    for (int sw=0; sw<Nsw; sw++) {
        for (int pos=0; pos<Npos; pos++) {
#else
    for (int pos=0; pos<Npos; pos++) {
        for (int sw=0; sw<Nsw; sw++) {
#endif
             // Channel range
             size_t ch0;
             const size_t nfinechan = double(Nch_in) * bb.spectralWindow[sw].effectiveBandwidth / cfg->procConfig.implicit_IF_width_MHz;
             const size_t ncoarsechan = bb.spectralWindow[sw].effectiveNumberOfChannels / bb.spectralWindow[sw].spectralAveragingFactor;
             if (cfg->procConfig.implicit_IF_sideband == XSD::SIDEBAND_LSB) {
                 double fhighedge = bb.spectralWindow[sw].centerFrequency + bb.spectralWindow[sw].effectiveBandwidth/2;
                 ch0 = size_t( (double(Nch_in) * (cfg->procConfig.implicit_IF_highedge_MHz - fhighedge) / cfg->procConfig.implicit_IF_width_MHz) + 0.5);
             } else {
                 double flowedge = bb.spectralWindow[sw].centerFrequency - bb.spectralWindow[sw].effectiveBandwidth/2;
                 ch0 = size_t( (double(Nch_in) * (flowedge - cfg->procConfig.implicit_IF_lowedge_MHz) / cfg->procConfig.implicit_IF_width_MHz) + 0.5);
             }

             // Effective averaging factor (fine ch spacing -> window ch spacing -> window/spectralAveragingFactor ch spacing)
             const size_t avg_1st = nfinechan / bb.spectralWindow[sw].effectiveNumberOfChannels;
             const size_t avg_2nd = bb.spectralWindow[sw].spectralAveragingFactor;
             //const size_t R = avg_1st * avg_2nd;
             const size_t R = nfinechan / ncoarsechan;

             // Input spectrum
             const float* xx = rs->spectra[pos];
             const float* yy = rs->spectra[pos] + Nch_in;
             const float* xy = rs->spectra[pos] + 2*Nch_in;
             const size_t start_pt = out_offset;

             int npolprod = 0;
             if ((bb.spectralWindow[sw].polnProducts == XSD::POLN_XX)
               || (bb.spectralWindow[sw].polnProducts == XSD::POLN_XX_YY)
               || (bb.spectralWindow[sw].polnProducts == XSD::POLN_XX_YY_XY)) {
                 // Extract window from XX
                 avg.spectralAvgAuto(xx + ch0, rs->finalspectra + out_offset, nfinechan, R);
                 if(bb.spectralWindow[sw].quantizationCorrection) {
                     nonLinCorr_X.calibrateAutocorr(rs->finalspectra + out_offset, ncoarsechan);
                 }
                 out_offset += ncoarsechan;
                 npolprod++;
                 // TODO: do something with channelAverageRegion's, even though output goes nowhere?
             }
             if (!(bb.spectralWindow[sw].polnProducts == XSD::POLN_XX)) {
                 // Extract window from YY
                 avg.spectralAvgAuto(yy + ch0, rs->finalspectra + out_offset, nfinechan, R);
                 if(bb.spectralWindow[sw].quantizationCorrection) {
                     nonLinCorr_Y.calibrateAutocorr(rs->finalspectra + out_offset, ncoarsechan);
                 }
                 out_offset += ncoarsechan;
                 npolprod++;
                 // TODO: do something with channelAverageRegion's, even though output goes nowhere?
             }
             if (bb.spectralWindow[sw].polnProducts == XSD::POLN_XX_YY_XY) {
                 // Extract window from XY
                 avg.spectralAvgCross(xy + 2*ch0, rs->finalspectra + out_offset, nfinechan, R);
                 if(bb.spectralWindow[sw].quantizationCorrection) {
                     // no calibration for cross-pol intended, for now
                 }
                 out_offset += 2 * ncoarsechan;
                 npolprod += 2;
                 // TODO: do something with channelAverageRegion's, even though output goes nowhere?
             }

             // The ICD defines data order inconveniently (for GPU, and postprocessing)
             // Reshape here (Nch x XX, Nch x YY) --> Nch x (XX,YY)
             reshapeInterleave(rs->finalspectra + start_pt, ncoarsechan, npolprod);

             if (0) {
                 std::cerr << "SpectralPostprocessor: nfinewide=" << Nch_in << ", nfinech[win" << sw <<"]=" << nfinechan << ", ncoarsech[win" << sw << "]=" << ncoarsechan
                           << ", R=" << R << ", avg_1st=" << avg_1st << " * avg_2nd=" << avg_2nd << ", npolprod=" << npolprod << std::endl;
             }
        }
    }
    L_(ldebug) << "SpectralPostprocessor produced " << out_offset << " float32 output values";
    assert(out_offset == points_in_output);

    return true;
}

/** Rearrange flat array from an order of "POL-CH" into "CH-POL" order.
 * In a 2-polproduct case : (Nch x XX, Nch x YY) converts into Nch x (XX,YY)
 * In a 4-polproduct case : (Nch x XX, Nch x YY, Nch x ReIm XY) converts into Nch x (XX,YY,ReIm XY)
 */
void SpectralPostprocessor::reshapeInterleave(float* inout, int nchan, int npolprod)
{
    if ((npolprod <= 1) || (nchan <= 1) || (inout == NULL)) {
        L_(lerror) << "SpectralPostprocessor::reshapeInterleave() : illegal args!";
        return;
    }

    if (m_interleaveworkbuf_size != (nchan * npolprod)) {
        m_interleaveworkbuf_size = nchan * npolprod;
        delete[] m_interleaveworkbuf;
        m_interleaveworkbuf = new float[m_interleaveworkbuf_size];
    }

    if (0) {
        // generic code
        int n=0;
        for (int ch=0; ch<nchan; ch++) {
            for (int p=0; p<npolprod; p++) {
                m_interleaveworkbuf[n] = inout[p*nchan + ch];
                n++;
            }
        }
        memcpy(inout, m_interleaveworkbuf, m_interleaveworkbuf_size*sizeof(float));
        return;
    }

    if (npolprod == 2) {
        float* xx = inout;
        float* yy = inout + nchan;
        const int N_unroll = 8;
        int n;
        for (n = 0; n<(nchan-N_unroll); n+=N_unroll) {
            for (int k=0; k<N_unroll; k++) {
                m_interleaveworkbuf[2*(n+k)+0] = xx[n+k];
                m_interleaveworkbuf[2*(n+k)+1] = yy[n+k];
            }
        }
        for (; n<nchan; n++) {
            m_interleaveworkbuf[2*n+0] = xx[n];
            m_interleaveworkbuf[2*n+1] = yy[n];
        }
    } else if (npolprod == 3) {
        float* pp = inout;
        float* re = inout + nchan;
        float* im = inout + 2*nchan;
        const int N_unroll = 4;
        int n;
        for (n = 0; n<(nchan-N_unroll); n+=N_unroll) {
            for (int k=0; k<N_unroll; k++) {
                m_interleaveworkbuf[3*(n+k)+0] = pp[n+k];
                m_interleaveworkbuf[3*(n+k)+1] = re[n+k];
                m_interleaveworkbuf[3*(n+k)+2] = im[n+k];
            }
        }
        for (; n<nchan; n++) {
            m_interleaveworkbuf[3*n+0] = pp[n];
            m_interleaveworkbuf[3*n+1] = re[n];
            m_interleaveworkbuf[3*n+2] = im[n];
        }
    } else if (npolprod == 4) {
        float* xx = inout;
        float* yy = inout + nchan;
        float* re = inout + 2*nchan;
        float* im = inout + 3*nchan;
        const int N_unroll = 4;
        int n;
        for (n = 0; n<(nchan-N_unroll); n+=N_unroll) {
            for (int k=0; k<N_unroll; k++) {
                m_interleaveworkbuf[4*(n+k)+0] = xx[n+k];
                m_interleaveworkbuf[4*(n+k)+1] = yy[n+k];
                m_interleaveworkbuf[4*(n+k)+2] = re[n+k];
                m_interleaveworkbuf[4*(n+k)+3] = im[n+k];
            }
        }
        for (; n<nchan; n++) {
            m_interleaveworkbuf[4*n+0] = xx[n];
            m_interleaveworkbuf[4*n+1] = yy[n];
            m_interleaveworkbuf[4*n+2] = re[n];
            m_interleaveworkbuf[4*n+3] = im[n];
        }
    } else {
        L_(lerror) << "SpectralPostprocessor::reshapeInterleave() does not support " << npolprod << " pol products!";
        return;
    }
    memcpy(inout, m_interleaveworkbuf, m_interleaveworkbuf_size*sizeof(float));
}

//////////////////////////////////////////////////////////////////////////////////////////
