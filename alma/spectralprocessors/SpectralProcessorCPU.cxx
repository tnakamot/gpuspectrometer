
#include "global_defs.hxx"
#include "spectralprocessors/SpectralProcessorCPU.hxx"
#include "spectralprocessors/SpectralPostprocessor.hxx"
#include "mcwrapping/mc_configuration.hxx"     // class MCConfiguration
#include "mcwrapping/mc_observation.hxx"       // class MCObservation
#include "drxp/DRXPIface.hxx"                  // class DRXPIface
#include "drxp/DRXPInfoFrame.hxx"              // class DRXPInfoFrame
#include "datarecipients/ResultRecipient_iface.hxx"  // class ResultRecipient
#include "datarecipients/SpectralResultSet.hxx"      // class SpectralResultSet
#include "datarecipients/SpectralResultSetManager.hxx"   // class SpectralResultSetManager
#include "helper/logger.h"
#include "time/TimeUTC.hxx"

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <iomanip>

#include <boost/noncopyable.hpp>

#ifndef MIN
    #define MIN(a,b) (((a)<(b))?(a):(b))
    #define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#define lockscope

struct SpectralProcessorCPU_impl_t : private boost::noncopyable
{
    void* d_data;
    void* h_data;

    MCConfiguration cfg;
    MCObservation* obs;
    SpectralResultSetManager resultsManager;
    SpectralPostprocessor postProcess;
    ResultRecipient* resultsHandler;
    DRXPIface* drxp;

    SpectralProcessorCPU_impl_t() {
        d_data = NULL;
        h_data = NULL;
        obs = NULL;
        resultsHandler = NULL;
        drxp = NULL;
    }
};

static int64_t getMillisecSince(const struct timespec* arg, const struct timespec* ref)
{
    int64_t dt_msec = 1000*((int64_t)arg->tv_sec - ref->tv_sec);
    dt_msec += ((int64_t)arg->tv_nsec - ref->tv_nsec + 1000000/2)/1000000; // rounded to closest millisec
    return dt_msec;
}

SpectralProcessorCPU::SpectralProcessorCPU()
{
    SpectralProcessorCPU_impl_t* p = new SpectralProcessorCPU_impl_t;
    pimpl = (void*)p;
}

bool SpectralProcessorCPU::initialize(MCObservation* obs)
{
    SpectralProcessorCPU_impl_t* p = (SpectralProcessorCPU_impl_t*)pimpl;

    // Keep reference to observation
    p->obs = obs;

    // Make copy configuration
    p->cfg = p->obs->cfg;

    // Update config
    p->obs->cfg.updateDerivedParamsFrom(p->obs);
    p->cfg.updateDerivedParamsFrom(p->obs);

    // Clear any old stored spectral results
    p->resultsManager.clear();

    return true;
}

bool SpectralProcessorCPU::deinitialize()
{
    SpectralProcessorCPU_impl_t* p = (SpectralProcessorCPU_impl_t*)pimpl;

    SpectralResultSet* r;
    while ((r = p->resultsManager.first()) != NULL) {
        L_(lwarning) << "At CPU deinit had incomplete spectrum of ID#" << r->id << " with " << r->total_msec << "ms of data";
        p->resultsManager.setCompleted(r->id);
        r->weight = float(r->total_msec) / float(p->cfg.procConfig.Tswitchingcycle_msec);
        if (1 && (p->obs != NULL) && (p->resultsHandler != NULL)) {
            L_(linfo) << "Writing out incomplete spectrum with ID#" << r->id << ", weight " << std::fixed << std::setprecision(4) << r->weight;
            p->postProcess.process(r, &p->cfg);
            r->setPostprocessed();
            r->deallocateRaw();
            p->resultsHandler->takeResult(r);
        }
        r->lock.unlock();
        p->resultsManager.releaseCompleted(r->id);
    }
    if (p->resultsHandler != NULL) {
        p->resultsHandler->sync();
    }

    if (p->obs != NULL) {

        p->cfg.mutex.lock();
        p->obs->mutex.lock();
        p->obs->cfg.procConfig.requestedStopTime = p->obs->cfg.procConfig.requestedStartTime;
        p->cfg.procConfig.requestedStopTime = p->cfg.procConfig.requestedStartTime;
        if (p->obs->state == MCObservation::Pending) {
            p->obs->state = MCObservation::Missed;
        } else if (p->obs->state == MCObservation::Ongoing) {
            p->obs->state = MCObservation::Stopped;
        }
        p->obs->mutex.unlock();
        p->cfg.mutex.unlock();

        p->obs = NULL;
    }
    return true;
}

void SpectralProcessorCPU::process48ms(const void* src, int n, double w, const DRXPInfoFrame* frameinfo)
{
    SpectralProcessorCPU_impl_t* p = (SpectralProcessorCPU_impl_t*)pimpl;
    SpectralResultSet* prs;
    int swpos, residual_dwell_ms, residual_transition_ms;

    // Fake histogram; eval ' round(exp(-[-7,-5,-3,-1,1,3,5,7].^2/(2*(7/3)^2)) * 1000) '
    hist_t dummyHist[8] = { 11, 101, 438, 912, 912, 438, 101, 11 };

    //const int64_t frame_ms = getMillisecSince(data.frameinfo->swRxTime_UTC, &p->cfg.procConfig.requestedStartTime);

    // Get all integrations possible during this frame
    int offset_ms = 0;
    while (offset_ms < 48) {

        // Look up (and auto-create if necessary) the SpectralResultSet responsible for next piece of data in this 48ms
        bool ok = p->resultsManager.lookupSpectralResultSet(frameinfo, &offset_ms, &(p->cfg), &prs, &swpos, &residual_dwell_ms, &residual_transition_ms);
        if (!ok) {
            L_(linfo) << "CPU::process48ms() got data that SpectralResultSetManager could not map into observing time range";
            break;
        }
        assert(prs != NULL);

        // How much data can be used
        int frame_avail_ms = (48 - offset_ms);
        int integ_use_ms = MIN(frame_avail_ms, residual_dwell_ms);
        int transit_use_ms = MIN(frame_avail_ms, residual_transition_ms);

        // Nothing to do if in a 'transition' stage of switching cycle
        if (integ_use_ms <= 0) {
            // Next
            // L_(ldebug) << "  ** transition during " << (frame_ms + offset_ms) << "..." << (frame_ms + offset_ms + transit_use_ms) << "ms from obs start";
            //boost::unique_lock<boost::recursive_mutex> resultSetLock(prs->lock);
            boost::unique_lock<boost::mutex> resultSetLock(prs->lock);
            prs->total_msec += transit_use_ms;
            offset_ms += transit_use_ms;
            prs->lock.unlock();
            continue;
        }

        // L_(ldebug) << "  ** dwell+accu during " << (frame_ms + offset_ms) << "..." << (frame_ms + offset_ms + integ_use_ms) << "ms from obs start";

        // Compute with data starting from offset_ms (0..<48ms) to offset_ms+integ_use_ms
        // ... compute histogram
        // .. compute integrated spectrum
        for (size_t n = 0; n < prs->bytesperspectrum/sizeof(float); n++) {
             prs->spectra[swpos][n] = (float)(n % 123);
        }
        if (!prs->hasHistogram()) {
            prs->storeHistogram(dummyHist, dummyHist);
        }
        prs->total_msec += integ_use_ms;
        prs->integrated_msec += integ_use_ms;

        // Completed an averaging period?
        L_(ldebug) << "CPU::process48ms() looked up AP #" << prs->id << "/pos#" << swpos
                   << " has now " << prs->integrated_msec << "ms int. and " << prs->total_msec << "ms total ; "
                   << " resid.dwell=" << residual_dwell_ms << "ms use.dwell=" << integ_use_ms
                   << "ms resid.trans=" << residual_transition_ms << "ms use.trans=" << transit_use_ms;
        if (!prs->allocated()) {
            // Other thread writing to this same ID# got here first,
            // already wrote out the spectral data; nothing to do then
         } else {
            // Mark as completed when averaging time reached
            if (prs->total_msec >= p->cfg.procConfig.Tswitchingcycle_msec) {
                p->resultsManager.setCompleted(prs->id);
            }
        }

        prs->lock.unlock();

        // Next
        offset_ms += integ_use_ms;
    }
}

/** Data PUSH method; accept new data via callback from DRXP.
 * \return True if data will be used and group free later, False if group can be freed immediately
 */
bool SpectralProcessorCPU::takeData(RawDRXPDataGroup data)
{
    SpectralProcessorCPU_impl_t* p = (SpectralProcessorCPU_impl_t*)pimpl;

    // Show an info just once
    static bool first_notice = true;
    if (first_notice) {
        L_(linfo) << "SpectralProcessorCPU got data, but actual spectral processing with CPU not implemented";
        first_notice = false;
    }

    // Lock the 'observation' and 'configuration' while we're inspecting it (start time, stop time, current data in-range)
    boost::unique_lock<boost::recursive_mutex> cfg_lock(p->cfg.mutex);
    boost::unique_lock<boost::recursive_mutex> obs_lock(p->obs->mutex);

    // Make sure we catch background changes to stopTime
    p->cfg.procConfig.requestedStopTime = p->obs->ts_stopTimeUTC;

    // Make sure data are in timerange of observation
    int64_t msec_from_start = getMillisecSince(&data.frameinfo->swRxTime_UTC, &p->cfg.procConfig.requestedStartTime);
    if (p->cfg.procConfig.requestedStartTime.tv_sec != 0) {
        if (msec_from_start <= -DRXP_DT_MSEC) {
            L_(ldebug) << "CPU::takeData() : data timestamp not yet in range of observation (still " << (-msec_from_start) << " msec)";
            return false;
        }
    }
    if (p->cfg.procConfig.requestedStopTime.tv_sec != 0) {
        int64_t msec_from_stop = getMillisecSince(&data.frameinfo->swRxTime_UTC, &p->cfg.procConfig.requestedStopTime);
        if (msec_from_stop >= 0) {
            L_(ldebug) << "CPU::takeData() : data timestamp past the range of observation (since " << msec_from_stop << " msec)";
            p->obs->state = MCObservation::Stopped;
            return false;
        }
    } else {
        L_(ldebug) << "CPU::takeData() : no stop time is set, maybe will be set later?";
    }

    // Are the data needed?
    if (!p->resultsManager.isFrameDataIntegratable(data.frameinfo, &p->cfg)) {
        // Nothing to do with any part of the data, release it back to DRXP
        L_(ldebug1) << "CPU::takeData() drop frame due not isFrameDataIntegratable()";
        return false;
    }

     // Unlock the 'observation' and 'configuration' again so M&C can modify start/stop time
    cfg_lock.unlock();
    obs_lock.unlock();

    // Release the DRXP group
    data.originator->releaseGroup(data.group);

    // Data are in range so process them
    this->process48ms(data.buf, data.bufSize, data.weight, data.frameinfo);
    usleep(10e3);

    // Check if any averaging periods can/must be written out now
    SpectralResultSet* r;
    while ((r = p->resultsManager.getNextCompleted()) != NULL) {
        r->weight = float(r->total_msec) / float(p->cfg.procConfig.Tswitchingcycle_msec);
        if ((p->obs != NULL) && (p->resultsHandler != NULL)) {
            L_(linfo) << "Writing out spectrum with ID#" << r->id << ", weight " << std::fixed << std::setprecision(4) << r->weight;
            p->postProcess.process(r, &p->cfg);
            r->setPostprocessed();
            r->deallocateRaw();
            p->resultsHandler->takeResult(r);
        }
        r->lock.unlock();
        p->resultsManager.releaseCompleted(r->id);
    }

    // return false; // 'False' to indicate to caller that DRXP group to be freed now, not needed later
    return true; // 'True' to indicate to caller that we free'd (or will free) the group, caller does not need to free it
}

void SpectralProcessorCPU::pinUserBuffers(int Nbuffers, void** buffers, size_t buflen)
{
}

void SpectralProcessorCPU::unpinUserBuffers(int Nbuffers, void** buffers, size_t buflen)
{
}

void SpectralProcessorCPU::setSpectralRecipient(ResultRecipient* r)
{
    SpectralProcessorCPU_impl_t* p = (SpectralProcessorCPU_impl_t*)pimpl;
    p->resultsHandler = r;
}

/** Assign to a subset of devices (CPU) */
void SpectralProcessorCPU::setAffinity(int ndevices, const int* deviceIds)
{
}
