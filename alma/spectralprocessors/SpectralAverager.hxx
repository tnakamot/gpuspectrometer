/**
 * \class SpectralAverager
 *
 * Helper class that does some simple processing on already time averaged spectra.
 *
 */
#ifndef SPECTRALAVERAGER_HXX
#define SPECTRALAVERAGER_HXX

class SpectralAverager {

    public:
        /** C'stor, currently no special args */
        SpectralAverager() {}

    public:
        /** Average 'nchan'-long auto-power data across every 'step' channels and write the result into 'out'. */
        void spectralAvgAuto(const float* in, float* out, int nchan, int step);

        /** Average 'nchan'-long cross-power data across every 'step' channels (Re and Im are averaged independently) and write the result into 'out' */
        void spectralAvgCross(const float* in, float* out, int nchan, int step);

        /** Multiply 'nchan'-long auto-power data (use 2*nchan for cross-power data) inplace by a constant factor */
        void spectralRescale(float* d, int nchan, const float factor);
};

#endif // SPECTRALAVERAGER_HXX
