/**
 * \class cuda_config_pool_t
 *
 * \brief Low-level details of a CUDA GPU worker pool with up to CUDA_CFG_POOL_MAXWORKERS workers.
 *
 * \author $Author: janw $
 *
 */
#ifndef SPECTRALPROCESSOR_CUDA_CONFIG_POOL_T_HXX
#define SPECTRALPROCESSOR_CUDA_CONFIG_POOL_T_HXX

#include "global_defs.hxx"
#include "spectralprocessors/SpectralProcessorGPU_cuda_worker_t.hxx"

struct cuda_config_pool_t : private boost::noncopyable
{
    /* Mutexing */
    //boost::mutex   poolmutex; /// not needed, covered by SpectralProcessorGPU::m_implMutex

    /* Observation and configuration */
    MCObservation* obsRef; /// Pointer to the MCObservation that the GPU should be running
    MCConfiguration scfg;  /// Private copy of 'obsRef->cfg' i.e. the MCConfiguration assigned to the MCObservation

    /* Individual workers and their settings */
    int nworkers; /// number of actual workers
    cuda_worker_t pool[CUDA_CFG_POOL_MAXWORKERS]; /// pre-reserved worker pool, not all entries actually used

    int nGPUsInstalled; /// The number of GPUs actually installed
    int nGPUs; /// The number of GPUs installed or assigned via ::setAffinity()
    int gpuIds[CUDA_CFG_POOL_MAXWORKERS]; /// Mapping from 0...nGPUs onto a set of GPU IDs either default or assigned via ::setAffinity()

    boost::condition_variable idlegpucond;
    bool drxpMemoryIsPinned; /// True when DRXP DMA ring buffer memory was successfully pinned into CUDA, in this case host->GPU memcpy must be in 4MB sized pieces

    /* Performance info */
    int peak_busy_workers;
    int min_idle_workers;

    /* Manager for integration periods and switching cycles */
    SpectralResultSetManager resultsManager;

    /* Postprocessor, takes full-resolution spectra, outputs spectrally averaged spectral windows */
    SpectralPostprocessor postProcess;

    /* Default MIME/ASCII/BIN output object (shared) for workers to push spectral data into */
    ResultRecipient* resultsHandler;
    boost::mutex resultshandlermutex; /// lock for changing 'resultsHandler' pointer

    /* C'stor. Initialize to defaults. */
    cuda_config_pool_t()
    {
        obsRef = NULL;
        drxpMemoryIsPinned = false;
        nworkers = 0;
        resultsHandler = NULL;
        nGPUsInstalled = 1;
        nGPUs = CUDA_CFG_POOL_MAXWORKERS;
        peak_busy_workers = 0;
        min_idle_workers = CUDA_CFG_POOL_MAXWORKERS;
        for (int i = 0; i < CUDA_CFG_POOL_MAXWORKERS; i++) {
            pool[i].state = cuda_worker_t::Exited;
            pool[i].idlegpucond = &idlegpucond;
            gpuIds[i] = i;
        }
    }
};

#endif // SPECTRALPROCESSOR_CUDA_CONFIG_POOL_T_HXX
