#include "asm/ASM.hxx"
#include "drxp/DRXP.hxx"
#include "drxp/DRXPDummy.hxx"
#include "datarecipients/ResultRecipientSim.hxx"
#include "datarecipients/ResultRecipientMIME.hxx"
#include "datarecipients/ResultRecipientPlain.hxx"
#include "datarecipients/ResultRecipientNoOp.hxx"
#include "helper/ASMConfigfileReader.hxx"
#include "helper/logger.h"
#include "network/XMLServer.hxx"
#include "xml2api/XMLMessage.hxx"
#include "spectralprocessors/SpectralProcessor_iface.hxx"
#include "time/TimeUTC.hxx"
#include "time/NTPDStatusQuery.hxx"
#include "xml2api/xml2apirouter.hxx"
#include "global_defs.hxx"

#include <string>
#include <iostream>
#include <string>
#include <sstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define lockscope  // for convenience

ASM::ASM(std::string configfilename)
{
    // Defaults
    xml2apirouter = NULL;
    drxp = NULL;
    moncontrol_thread = NULL;
    drxpstatus = NULL;
    resultwriter = NULL;
    observationQueueWorker_thread = NULL;

    // Load JSON config file
    ASMConfigfileReader cfgReader;
    cfgReader.readConfigFile(configfilename, userconfig);

    // Verify some settings
    if (userconfig.get<std::string>("operation.mode") == "simulateMIME") {
        std::ifstream f(userconfig.get<std::string>("simulation.inputFileMIME").c_str(), std::ios::binary);
        if (!f) {
            L_(lerror) << "Could not access MIME dummy template file "
                << userconfig.get<std::string>("simulation.inputFileMIME")
                << " given in " << configfilename;
            std::cout << "Error: Could not access MIME dummy template file "
                << userconfig.get<std::string>("simulation.inputFileMIME")
                << " given in " << configfilename << std::endl;
        } else {
            L_(linfo) << "ASM configured to provide dummy MIME output from file "
                << userconfig.get<std::string>("simulation.inputFileMIME");
            std::cout << "ASM configured to provide dummy MIME output from file "
                << userconfig.get<std::string>("simulation.inputFileMIME") << std::endl;
        }
    }

    // Init backend
    backend.setAffinity(
        userconfig.get<std::string>("hardware.assignedGPUIds","")
    );

    // Init M&C pass-through class with XML Validation
    xml2apirouter = new XML2ApiRouter(
        userconfig.get<std::string>("mc_xml.schema",DEFAULT_SCHEMA_FILE_PATH)
    );

    // Initialize DRXP handler
    try {
        prepareDRXP();
    } catch (std::exception &e) {
        std::cout << "DRXP preparation error: " << e.what() << "\n";
        dstor();
    }
}

void ASM::dstor()
{
    stopObservingQueue();
    if (drxp != NULL) {
        drxp->detachRxRecipient();
#ifndef HAVE_DRXP
        bool dummy_drxp = true;
#else
        bool dummy_drxp = false;
#endif
        if (dummy_drxp || (userconfig.get<std::string>("operation.mode") == "simulateDTS")) {
            delete (DRXPDummy*)drxp;
        } else {
            delete (DRXP*)drxp;
        }
        drxp = NULL;
    }
    if (xml2apirouter != NULL) {
        delete xml2apirouter;
        xml2apirouter = NULL;
    }
}

void ASM::startLogging(int verbosity, bool logfile_enabled)
{
    if (logfile_enabled) {
        const char* logfile = userconfig.get<std::string>("operation.logfile").c_str();
        std::string level = userconfig.get<std::string>("operation.loglevel");
        if (level == "debug2") {
           initLogger(logfile, ldebug2);
        } else if (level == "debug1") {
           initLogger(logfile, ldebug1);
        } else if (level == "debug") {
           initLogger(logfile, ldebug);
        } else if (level == "info") {
           initLogger(logfile, linfo);
        } else if (level == "warning") {
           initLogger(logfile, lwarning);
        } else {
           initLogger(logfile, lerror);
        }
    } else {
        if (verbosity >= 4) {
           FILELog::ReportingLevel() = ldebug3;
        } else if (verbosity == 3) {
           FILELog::ReportingLevel() = ldebug2;
        } else if (verbosity == 2) {
           FILELog::ReportingLevel() = ldebug;
        } else {
           FILELog::ReportingLevel() = linfo;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Control of M&C TCP/XML Worker
///////////////////////////////////////////////////////////////////////////////////////////////////////////

bool ASM::startMC()
{
    const int tcp_port = userconfig.get<int>("network.tcpPortMC",DEFAULT_MC_TCP_PORT);

    if (moncontrol_thread != NULL) {
        stopMC();
    }

    L_(linfo) << xml2apirouter->dbg_ListAllCommands();

    moncontrol_thread_keepalive = true;
    moncontrol_thread = new boost::thread(&ASM::moncontrolAcceptorWorker, this, tcp_port);

    return true;
}

bool ASM::stopMC()
{
#if 0
    if (moncontrol_thread != NULL) {
        moncontrol_thread_keepalive = false;
        moncontrol_thread->interrupt();
        // TODO: pull ASM::moncontrolAcceptorWorker out of loop; currently not possible since asio::accept() is not interrupt'ible!
        moncontrol_thread->join();
        delete moncontrol_thread;
        moncontrol_thread = NULL;
    }
#else
    // TODO: make XMLServer class use asynchronous asio so it becomes interruptible,
    // otherwise it is stuck on the acceptor ::accept() unaffected by ::interrupt()

    // Current "workaround": detach thread to at least allow clean ASM exit
    if (moncontrol_thread != NULL) {
        moncontrol_thread->detach();
        moncontrol_thread = NULL;
    }
#endif
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Control of DRXP(s)
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASM::prepareDRXP()
{
    const std::string drxp_device = userconfig.get<std::string>("hardware.assignedDRXPDevice");
    const std::string mcastgroup = userconfig.get<std::string>("network.drxpMonMulticastGroup");
    const int mcastport = userconfig.get<int>("network.drxpMonMulticastPort");
    const std::string patternfile = userconfig.get<std::string>("simulation.inputFileDTS");
    const int timealignment_msec = userconfig.get<int>("hardware.timeAlignGranularityMsec",DEFAULT_DRXP_TIMEALIGN_GRANULARITY_MS);
    const bool simulateDTS = (userconfig.get<std::string>("operation.mode") == "simulateDTS");

    bool dummy_drxp = false;
#ifndef HAVE_DRXP
    dummy_drxp = true;
#endif

    if (drxpstatus != NULL) {
        delete drxpstatus;
    }
    drxpstatus = new MCDRXPStatus();

    // Create DRXP; actual or simulated
    if (dummy_drxp || (userconfig.get<std::string>("operation.mode") == "simulateDTS")) {
        drxp = new DRXPDummy();
        L_(linfo) << "Server configured to provide dummy DTS 3-bit input from file " << patternfile;
        std::cout << "Server configured to provide dummy DTS 3-bit input from file " << patternfile << std::endl;
    } else {
        drxp = new DRXP();
    }
    drxp->setTimestampGranularity(timealignment_msec);

    //if (drxp_debuglog) {
    //    char logfile[80];
    //    sprintf(logfile, "/tmp/drxp_IF%1d.trace", userconfig.get<int>("hardware.assignedIF",DEFAULT_IF_NUMBER));
    //    drxp->startDebugLogging(logfile);
    //    L_(linfo) << "DRXP low level details are being logged into " << logfile;
    //    std::cout << "DRXP low level details are being logged into " << logfile << std::endl;
    //}

    drxp->selectDevice(drxp_device);
    if (simulateDTS) {
        drxp->loadPattern(patternfile.c_str());
    }

    // Multicast monitor, results stored also into drxpstatus
    drxp->startPeriodicMonitoring(mcastgroup.c_str(), mcastport, *drxpstatus);
}

bool ASM::startDRXPs()
{
    if (!drxp->startRx()) {
        L_(lerror) << "ASM startDRXPs failed";
        return false;
    }
    return true;
}

bool ASM::stopDRXPs()
{
    if (!drxp->stopRx()) {
        L_(lerror) << "ASM stopDRXPs failed";
        return false;
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Control of Observing Queue Worker
///////////////////////////////////////////////////////////////////////////////////////////////////////////

bool ASM::startObservingQueue()
{
    if (observationQueueWorker_thread != NULL) {
        return false;
    }

    if (resultwriter == NULL) {
        std::string outmode = userconfig.get<std::string>("operation.mode");
        std::string outfilepath = userconfig.get<std::string>("operation.mimeOutputPath");
        if (outmode == "simulateMIME") {
            ResultRecipientSim* simu = new ResultRecipientSim(outfilepath);
            simu->setSourceMIME(userconfig.get<std::string>("simulation.inputFileBDF",DEFAULT_SIMULATION_MIME_INPUT));
            resultwriter = simu;
        } else {
            resultwriter = new ResultRecipientMIME(outfilepath);
        }
    }

    observationqueue.reset();
    observationQueueWorker_keepalive = true;
    observationQueueWorker_thread = new boost::thread(&ASM::observationQueueWorker, this);

    return true;
}

bool ASM::stopObservingQueue()
{
    observationQueueWorker_keepalive = false;
    if (observationQueueWorker_thread != NULL) {
        observationQueueWorker_thread->interrupt();
        observationQueueWorker_thread->join();
        delete observationQueueWorker_thread;
        observationQueueWorker_thread = NULL;
    }
    if (resultwriter != NULL) {
        delete resultwriter;
        resultwriter = NULL;
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//// XML I/O Interface (XMLIOIface)
//// Called back by XML Server hosted in ASM::moncontrolAcceptorWorker() upon rx of new M&C XML
///////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Act on an XML-containing string and return an XML response
std::string ASM::xmlHandover(std::string xmlStringIn)
{
    XMLMessage xmlmsg(
        userconfig.get<std::string>("mc_xml.schema",DEFAULT_SCHEMA_FILE_PATH)
    );

    boost::property_tree::ptree ptreeOut;
    std::string xmlStringOut("");

    // Invoke M&C XML/API handler factory
    try {

        // Load XML input command and convert to BOOST PropertyTree
        xmlmsg.load(xmlStringIn);
        boost::property_tree::ptree ptreeIn = xmlmsg.getPTree();

        // Use XML-->API handler factory to execute the command on ourself
        int rc;
        L_(linfo) << xmlmsg.getCommandName() << " : handling command";
        lockscope {
            boost::mutex::scoped_lock serializer(xml2apirouter_mutex);
            rc = xml2apirouter->handle(xmlmsg.getPTree(), ptreeOut, *this);
        }
        L_(linfo) <<  xmlmsg.getCommandName() << " : handler return code : " << rc;

        // Convert the returned PropertyTree into XML
        std::ostringstream os;
        write_xml(/*dest*/os, /*src*/ptreeOut);
        xmlStringOut = os.str();

    } catch (std::exception &e) {

        // Invalid XML error
        xmlStringOut = xml2apirouter->generateBasicResponse(
            ptreeOut,
            xmlmsg.getCommandId(), xmlmsg.getCommandName().c_str(),
            "invalid-xml", ""
        );
        L_(lerror) << xmlmsg.getCommandName() << " : handling error : " << e.what() << ", XML = " << xmlStringOut;

    }

    // Validate the XML-formatted M&C Response found in 'xmlStringOut'
    bool valid = xmlmsg.validateXML(xmlStringOut);
    if (valid) {
        L_(linfo) <<  xmlmsg.getCommandName() << " : response : " << xmlStringOut;
    } else {
        L_(lerror) <<  xmlmsg.getCommandName() << " : own XML response not valid under schema : response : " << xmlStringOut;
    }

    // Return the XML response
    return xmlStringOut;
}

void ASM::moncontrolAcceptorWorker(int port)
{
    XMLServer srv(*this, port);
    srv.acceptClients(); // blocking; will call back *this::xmlHandover() upon client data
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Observation Queue Worker Thread
//// Get next observation -> prepare ASM internals -> observe -> clean up -> wait for next observation
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASM::observationQueueWorker()
{
    L_(ldebug) << "ASM::observationQueueWorker() worker thread is ready to monitor the observing queue";

    spectrometerstate.switchInternalState(MCSpectrometerState::Ready);

    // Work through observations
    // User M&C commands may be adding further observations in the background   
    while (observationQueueWorker_keepalive) {

        // Clean up old observations
        int nexpired = observationqueue.removeExpiredObservations();
        if (nexpired > 0) {
            L_(linfo) << "Removed " << nexpired << " old observations from queue";
        }

        // Get a soon-upcoming observation
        MCObservation* obs = NULL;
        std::string obsId;
        try {
            obs = observationGetNext();
        } catch (boost::thread_interrupted&) {
            L_(ldebug) << "ASM::() was interrupted while waiting for the start of an observation, observationQueueWorker_keepalive=" << observationQueueWorker_keepalive;
            continue;
        }
        assert(obs != NULL);

        // Get Observation ID
        lockscope {
            boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
            obsId = obs->observationId;
        }

        // Prepare for observation
        bool ok = false;
        L_(linfo) << "ASM::observationQueueWorker() making preparations for obs. " << obsId
                  << " with configId " << obs->cfg.configId
                  << " currently in state '" << MCObservation::ObservationState_str(obs->state) << "'";
        try {
            ok = observationPrepare(obs);
            if (ok) {
                L_(ldebug) << "ASM::observationQueueWorker() finished preparing backend to handle upcoming obs. " << obsId;
            }
        }  catch (boost::thread_interrupted&) {
            L_(linfo) << "ASM::observationQueueWorker() was interrupted while preparing backend for obs. " << obsId;
            ok = false;
        }
        if (!ok) {
            observationFinish(obs, true);
            continue;
        }

        // Run the observation; monitor until observation state changes
        try {
            ok = observationRun(obs);
        }  catch (boost::thread_interrupted&) {
            L_(linfo) << "ASM::observationQueueWorker() interrupted during observation";
            ok = false;
        }
        if (!ok) {
            observationFinish(obs, true);
            continue;
        }

        // Completed cleanly
        L_(linfo) << "ASM::observationQueueWorker() about to stop obs. " << obsId;
        observationFinish(obs);   

        // Done. Report and continue to next obs.
        L_(linfo) << "ASM::observationQueueWorker() finished obs. " << obsId
                  << " in state " << MCObservation::ObservationState_str(obs->state);

    }

    // Got told to quit; observationQueueWorker_keepalive != true
    L_(linfo) << "ASM::observationQueueWorker() is terminating, observationQueueWorker_keepalive=" << observationQueueWorker_keepalive;

    drxp->detachRxRecipient();
    resultwriter->close();
    if (backend.getCurrentProcessor() != NULL) {
        backend.getCurrentProcessor()->deinitialize();
    }

    spectrometerstate.switchInternalState(MCSpectrometerState::Ready); // TODO: there is no 'Stopped' state?
}


MCObservation* ASM::observationGetNext(void)
{
    const double minleadsecs = userconfig.get<double>("operation.startObsMinLeadtime", DEFAULT_STARTOBS_MINIMUM_LEADTIME_SECS);
    MCObservation* obs = NULL;

    bool found_pending = false;
    int wait_iter_count = 0;
    int obs_idx = 0;
    double dT = 0;

    while (observationQueueWorker_keepalive && !found_pending) {

        obs = NULL; 
        found_pending = observationqueue.getUpcomingObservation(&obs_idx, &obs, &dT, /*allow_past=*/false, minleadsecs);

        if (!found_pending) {
            // Sleep a bit before checking the queue again

            #if (BOOST_VERSION>=105300) // boost v1.53 uses sleep_for()
            boost::this_thread::sleep_for(boost::chrono::milliseconds(OBS_QUEUE_SLEEP_MSEC));
            #else
            boost::this_thread::sleep(boost::posix_time::milliseconds(OBS_QUEUE_SLEEP_MSEC));
            #endif

            wait_iter_count++;
            if (wait_iter_count >= (OBS_QUEUE_REPORT_MSEC/OBS_QUEUE_SLEEP_MSEC)) {
                L_(ldebug) << "ASM::observationGetNext() no upcoming observation yet,"
                    << " queue depth = " << observationqueue.getQueueLength()
                    << ", next in " << std::fixed << std::setprecision(3) << (-dT) << " seconds.";
                wait_iter_count = 0;
            }
        }
    }

    return obs;
}

bool ASM::observationPrepare(MCObservation* obs)
{
    std::string obsId;

    spectrometerstate.switchInternalState(MCSpectrometerState::Preparing);

    // Make sure NTP time sync is still proper
    NTPDStatusQuery ntpStatus;
    if (!ntpStatus.requirementsMet()) {
        L_(lerror) << "Cannot start observation as NTP time is currently of too poor quality.";
        return false;
    }

    // TODO: after NTP looks fine, could resynchronize DRXP here for the case that NTP Daemon was restarted in background?

    // Initialize MIME data outputter
    lockscope {
        boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
        obsId = obs->observationId;

        resultwriter->configureFrom(obs);
        resultwriter->open();
        obs->outputWriter = resultwriter;
        obs->state = MCObservation::Ongoing;

        backend.setSpectralRecipient(resultwriter);
    }

    // Initialize backend processor to the settings of this observation
    SpectralProcessorIFace* spu_ = backend.getCurrentProcessor();
    if (spu_ == NULL) {
        L_(lerror) << "ASM::observationQueueWorker() got NULL as current spectral processor object! No output for obs. " << obsId;
        return false;
    }
    spu_->initialize(obs);

    // Optimize the DRXP<->GPU buffer access (for CUDA)
    void* drxp_buf[128];
    size_t drxp_nbuf, drxp_buflen = 0;
    if (drxp->getBufferPtrs(&drxp_nbuf, drxp_buf, &drxp_buflen)) {
        spu_->pinUserBuffers(drxp_nbuf, drxp_buf, drxp_buflen);
    }

    // Get DRXP data flowing into backend
    drxp->attachRxRecipient(&backend);

    return true;
}

bool ASM::observationRun(MCObservation* obs)
{
    spectrometerstate.switchInternalState(MCSpectrometerState::Running);
    const std::string obsId = obs->observationId;

    L_(ldebug) << "ASM::observationQueueWorker() now waiting for obs. " << obsId << " to end.";

    int wait_iter_count = 0;
    while (observationQueueWorker_keepalive) {
 
        boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);

        if (obs->state == MCObservation::Stopped) {
            break;
        }
        if (obs->state == MCObservation::Faulted) {
            break;
        }

        // Make sure any changes in stop time injected via M&C get propagated to backend
        if (!obs->stopTimeUTC.empty()) {
            obs->cfg.mutex.lock();
            obs->cfg.procConfig.requestedStopTime = obs->ts_stopTimeUTC;
            obs->cfg.mutex.unlock();
        }

        // Catch when time exceeded, without backend having automatically set obs state MCObservation::Stopped
        const float max_backlog_T = 15.0f;
        double dT = std::numeric_limits<double>::infinity();
        if (!obs->stopTimeUTC.empty()) {
            dT = TimeUTC::getSecondsSince(obs->stopTimeUTC);
            if (dT >= max_backlog_T) {
                // TODO: allow this 15sec(or other) backlog before forced quit, but check it does not interfere with next obs?
                L_(lwarning) << "Forcing stop of obs. " << obsId << " that was still "
                             << MCObservation::ObservationState_str(obs->state) << " for " << (int)max_backlog_T << "s past the requested stop time";
                obs->state = MCObservation::Stopped;
                return true;
            }
        }

        // Regular reporting
        if (wait_iter_count >= (OBS_QUEUE_REPORT_MSEC/OBS_QUEUE_SLEEP_MSEC)) {
            L_(ldebug) << "ASM::observationQueueWorker() observation " << obsId
                       << " is " << MCObservation::ObservationState_str(obs->state) << ", "
                       << std::fixed << std::setprecision(3) << (-dT) << " seconds remain.";
            wait_iter_count = 0;
        }
        wait_iter_count++;

        // Sleep a bit before checking observation progress again
        obs_lock.unlock();
        #if (BOOST_VERSION>=105300) // boost v1.53 uses sleep_for()
        boost::this_thread::sleep_for(boost::chrono::milliseconds(OBS_QUEUE_SLEEP_MSEC));
        #else
        boost::this_thread::sleep(boost::posix_time::milliseconds(OBS_QUEUE_SLEEP_MSEC));
        #endif
    }

    return true;
}

bool ASM::observationFinish(MCObservation* obs, bool faulted)
{
    const std::string obsId = obs->observationId;

    // Stop DRXP data flow into backend(s)
    L_(linfo) << "ASM::observationFinish() stopping obs. " << obsId << ", detaching DRXP...";
    drxp->detachRxRecipient();

    // Get backend processor if any, for shutting it down
    SpectralProcessorIFace* spu_ = backend.getCurrentProcessor();
    if (spu_ != NULL) {
        L_(linfo) << "ASM::observationFinish() stopping obs. " << obsId << ", de-initializing the processor...";
        spu_->deinitialize();
    }

    // Close the MIME writer
    L_(linfo) << "ASM::observationFinish() stopping obs. " << obsId << ", closing MIME output...";
    lockscope {
        boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
        if (obs->outputWriter != NULL) {
            obs->outputWriter->close();
        }
    }
    backend.setSpectralRecipient(NULL);

    // Un-register DRXP<->GPU buffers (from CUDA)
    void* drxp_buf[128];
    size_t drxp_nbuf, drxp_buflen = 0;
    if (drxp->getBufferPtrs(&drxp_nbuf, drxp_buf, &drxp_buflen)) {
        if (spu_ != NULL) {
            spu_->unpinUserBuffers(drxp_nbuf, drxp_buf, drxp_buflen);
        }
    }

    // Set final state of Observation
    if (faulted) {
        boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
        obs->state = MCObservation::Faulted;
    } else {
        boost::unique_lock<boost::recursive_mutex> obs_lock(obs->mutex);
        if (obs->state == MCObservation::Ongoing) { // preserve Faulted state
            obs->state = MCObservation::Stopped;
        }
    }

    // Set state of ASM
    if (faulted) {
        spectrometerstate.switchInternalState(MCSpectrometerState::Failure);
    } else {
        spectrometerstate.switchInternalState(MCSpectrometerState::Ready);
    }

    return true;
}
