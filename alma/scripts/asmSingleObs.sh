#!/bin/sh
SCRIPTDIR=$(cd $(dirname $0) && pwd)

#
# This script executes ASM_MC_service and run one observation
# with one integration and one spectral window.
#

######################################################
# Settings
#  (change the settings for your test accordingly)
######################################################
ASM_INPUT=/data1/DR_test_20180817/twolines_DR_10000_df_500_kHz.drxp
ASM_DIR=$(realpath ${SCRIPTDIR}/../..)

# Integration time in seconds.
INTEGRATION_TIME=1.0

######################################################
# Spectral window setting
#  (only spectral window is supported by this script)
######################################################

# Center freqeuncy in MHz
SW1_CENTER_FREQUENCY=3333.3333333

# Band Width in MHz (minimum 31.25)
SW1_BAND_WIDTH=31.25

# Number of channels
SW1_CHANNELS=8192

# Use quantization correction (true or false)
SW1_QUANTIZ_CORR=true


######################################################
# Static settings
#  (you normally do not need to change)
######################################################
ASM_MC_SERVICE=${ASM_DIR}/alma/ASM_MC_service
ASM_TX_CMD=${ASM_DIR}/alma/scripts/asmTxCmd.py
ASM_MIME_PLOT=${ASM_DIR}/alma/scripts/asmPlotMime.py
ASM_MC_SCHEMA=${ASM_DIR}/alma/ASC_MC.xsd

OUTPUT_DIR=${HOME}/ASM/test_$(date +%Y%m%d_%H%M%S)

ASM_CONFIG=${OUTPUT_DIR}/asm.json
ASM_LOG=${OUTPUT_DIR}/asm_gpu0.log
MIME_OUTPUT_DIR=${OUTPUT_DIR}
OBS_CONFIG=${OUTPUT_DIR}/obs_config.xml
START_OBS_CMD_FILE=${OUTPUT_DIR}/startObservation.xml
STOP_OBS_CMD_FILE=${OUTPUT_DIR}/stopObservation.xml

ASM_MC_PORT=1337

OBS_LEAD_TIME=1.0
OBS_LEAD_TIME_MARGIN=1.0

OBS_CONFIG_ID=1

######################################################
# Function to exit with an error message.
######################################################
function exit_failure() {
    # Kill ASM_MC_service if 
    if [ -n "${ASM_MC_PID}" ]; then
        kill ${ASM_MC_PID}
    fi
    echo $1 >&2
    exit 1
}

######################################################
# Prepare and check the environment.
######################################################
[ -x ${ASM_MC_SERVICE} ] || exit_failure "${ASM_MC_SERVICE} does not exist, or is not an executable."

mkdir -p ${OUTPUT_DIR} || exit_failure
mkdir -p ${MIME_OUTPUT_DIR} || exit_failure

# Output the git tag to the log file.
GIT_TAG=$(cd ${ASM_DIR} && git describe --all --long)
echo ${GIT_TAG} >> ${ASM_LOG}

######################################################
# Output ASM configuration.
######################################################
cat <<EOF > ${ASM_CONFIG} || exit_failure
{
  "network":{
    "tcpPortMC":"${ASM_MC_PORT}",
    "tcpPortASC":"5001",
    "udpPortASC":"5001",
    "drxpMonMulticastGroup":"224.0.0.1",
    "drxpMonMulticastPort":"30001",
    "hostNameASC":"localhost"
  },
  "mc_xml":{
    "schema":"${ASM_MC_SCHEMA}",
    "encapsulation":"binxml"
  },
  "hardware":{
    "assignedIF":"1",
    "assignedGPUIds":"0"
  },
  "operation":{
    "mode":"simulateDTS",
    "logfile":"${ASM_LOG}",
    "loglevel":"debug",
    "startObsMinLeadtime":"${OBS_LEAD_TIME}",
    "mimeOutputPath":"${MIME_OUTPUT_DIR}/"
  },
  "simulation":{
    "inputFileDTS":"${ASM_INPUT}"
  }
}
EOF

######################################################
# Output observation configuration file.
######################################################
cat <<EOF > ${OBS_CONFIG} || exit_failure
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<command>
  <commandId>1</commandId>
  <loadConfiguration>
    <configId>${OBS_CONFIG_ID}</configId>
    <singleConfiguration>
      <spectrometerInput>0</spectrometerInput>
      <timeSpec>
        <switching>
          <position>0</position>
          <dwell unit="s">${INTEGRATION_TIME}</dwell>
          <transition unit="s">0.0</transition>
        </switching>
        <integrationDuration unit="s">${INTEGRATION_TIME}</integrationDuration>
        <channelAverageDuration unit="s">${INTEGRATION_TIME}</channelAverageDuration>
      </timeSpec>
      <basebandConfig>
        <loOffset unit="MHz">0.0</loOffset>
        <spectralWindow polnProducts="XX,YY">
          <centerFrequency unit="MHz">${SW1_CENTER_FREQUENCY}</centerFrequency>
          <spectralAveragingFactor>1</spectralAveragingFactor>
          <name>SW-1</name>
          <effectiveBandwidth unit="MHz">${SW1_BAND_WIDTH}</effectiveBandwidth>
          <effectiveNumberOfChannels>${SW1_CHANNELS}</effectiveNumberOfChannels>
          <quantizationCorrection>${SW1_QUANTIZ_CORR}</quantizationCorrection>
          <channelAverageRegion>
            <startChannel>0</startChannel>
            <numberChannels>256</numberChannels>
          </channelAverageRegion>
          <windowFunction overlap="0.0">UNIFORM</windowFunction>
        </spectralWindow>
      </basebandConfig>
    </singleConfiguration>
  </loadConfiguration>
</command>
EOF

######################################################
# Launch ASM
######################################################

echo "Launching ${ASM_MC_SERVICE}..."

nohup ${ASM_MC_SERVICE} -d -c ${ASM_CONFIG} > /dev/null 2>&1 &
ASM_MC_PID=$!

# Wait until ASM_MC_service starts to listen on the MC port.
ASM_MC_TIMEOUT=10
for i in $(seq ${ASM_MC_TIMEOUT}); do
    # Exit the loop if the MC port is open.
    nc localhost ${ASM_MC_PORT} < /dev/null > /dev/null 2>&1 && break
    sleep 1
done

if ! nc localhost ${ASM_MC_PORT} < /dev/null > /dev/null 2>&1; then
    exit_failure "ASM_MC_service failed to start listening on port ${ASM_MC_PORT}."
fi

########################################################
# Send the observation configuration to ASM_MC_service.
########################################################
echo "Configure the observation."
${ASM_TX_CMD} ${OBS_CONFIG} > /dev/null || exit_failure "Failed to send the observation configuration"

########################################################
# Start the observation.
########################################################
OBS_START_TIME_SINCE_EPOCH=$(echo "$(date +%s.%N) + ${OBS_LEAD_TIME} + ${OBS_LEAD_TIME_MARGIN}" | bc)
OBS_START_TIME=$(date --date="@${OBS_START_TIME_SINCE_EPOCH}" +%Y-%m-%dT%H:%M:%S.%3N)
OBS_END_TIME_SINCE_EPOCH=$(echo ${OBS_START_TIME_SINCE_EPOCH} + ${INTEGRATION_TIME} | bc)
OBS_END_TIME=$(date --date="@${OBS_END_TIME_SINCE_EPOCH}" +%Y-%m-%dT%H:%M:%S.%3N)
UID_BASE="$(date --date="@${OBS_START_TIME_SINCE_EPOCH}" +%m%dT%H%M%S)"
OBS_UID="uid::${UID_BASE:0:5}/${UID_BASE:5:3}/${UID_BASE:8:3}"
MIME_FILE=${MIME_OUTPUT_DIR}/$(echo "${OBS_UID}" | tr ':' '_' | tr '/' '_')_1_BB_1.mime

cat <<EOF > ${START_OBS_CMD_FILE} || exit_failure
<?xml version="1.0"?>
<command>
<commandId>2</commandId>
<startObservation>
    <configId>${OBS_CONFIG_ID}</configId>
    <observationId>${OBS_UID}</observationId>
    <time>${OBS_START_TIME}</time>
</startObservation>
</command>
EOF

cat <<EOF > ${STOP_OBS_CMD_FILE} || exit_failure
<?xml version="1.0"?>
<command>
<commandId>3</commandId>
<stopObservation>
    <observationId>${OBS_UID}</observationId>
    <time>${OBS_END_TIME}</time>
</stopObservation>
</command>
EOF

${ASM_TX_CMD} ${START_OBS_CMD_FILE} > /dev/null || exit_failure "Failed to send startObservation command."
${ASM_TX_CMD} ${STOP_OBS_CMD_FILE} > /dev/null || exit_failure "Failed to send stopObservation command."

echo "Start observation."

########################################################
# Quit ASM_MC_service
########################################################

# Wait until the observation completes.
sleep $(echo ${OBS_END_TIME_SINCE_EPOCH} - $(date +%s) | bc)

# Wait until the expected MIME file is generated.
for i in $(seq 10); do
    [ -f ${MIME_FILE} ] && break
    sleep 0.5
done

[ -f ${MIME_FILE} ] || exit_failure "Couldn't find ${MIME_FILE}."

echo "Completed the observation."

kill ${ASM_MC_PID}
wait ${ASM_MC_PID} 2> /dev/null

########################################################
# Show the spectrum.
########################################################

echo
echo "Observation summary"
echo "  Input file  : ${ASM_INPUT}"
echo "  Obs. config : ${OBS_CONFIG}"
echo "  MIME oputput: ${MIME_FILE}"
echo "  ASM Git Ver.: ${GIT_TAG}"
echo

${ASM_MIME_PLOT} ${OBS_CONFIG} ${MIME_FILE} > /dev/null
