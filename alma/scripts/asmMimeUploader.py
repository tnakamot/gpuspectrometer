#!/usr/bin/python
"""
Usage: asmMimeUploader.py [-r|--realtime] [-p|--progress] [-c <ASM_MC_config.json>]

Monitors the MIME output directory given in the configuration file
and transfers each newly finished (closed) *.mime or *.mime file
over a new TCP connection to a host given in the configuration file.
"""

import glob, os, socket, sys
import json
import inotify, inotify.adapters  # needs https://pypi.python.org/pypi/inotify and not the obsolete 'pynotify' package

extensions = ['MIME', 'BDF']
inotify_events = ['IN_MOVED_TO', 'IN_CLOSE_WRITE']
inotify_events_realtime = ['IN_CREATE'] # ['IN_CREATE', 'IN_OPEN'] unfortunately self-triggers a re-send on open('r')

class TransfersizeReporter:
	def __init__(self,max_size=-1):
		self.max_size = max_size
		self.units = ['kB','MB','GB','TB','PB','']
		self.divs = [2**10, 2**20, 2**30, 2**40, 2**50, 2**60, 0]
		self.curr_unit = 0
		self.max_label = ''
		if max_size >= 0:
			self.max_label = ' / ' + self.value2Str(max_size)
			self.curr_unit = 0
	def value2Str(self,val):
		if (val > 2*self.divs[self.curr_unit+1]):
			self.curr_unit += 1
		return '%d %s' % (int(val/self.divs[self.curr_unit]),self.units[self.curr_unit])
	def update(self,newsize):
		s = self.value2Str(newsize)
		s += self.max_label
		print ('\033[s%s   \033[u\b' % (s)),
		sys.stdout.flush()

"""Make TCP connection to remote host"""
def connect(cfg):
	port = int(cfg['network']['tcpPortASC'])
	host = str(cfg['network']['hostNameASC'])
	s = socket.socket()
	s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
	try:
		s.connect((host,port))
	except Exception as e:
		print ('Could not connect %s:%d : %s' % (host,port,str(e)))
		return None
	return s

"""Send a file over a new TCP connection. Rename the file once done."""
def doUpload(fn,cfg):

	# Type of upload: a still growing file, or a completed file
	try:
		if cfg['realtime']:
			upOk = doUploadLive(fn,cfg)
		else:
			upOk = doUploadOffline(fn,cfg)
	except socket.error as e:
		print('Network error during upload: %s' % (str(e)))
		upOk = False

	# Rename the original file, or delete it
	if upOk:
		fndone = fn[:fn.rfind('.')] + '.bak'
		os.rename(fn, fndone)
		print ('Renamed %s to %s' % (fn,fndone))

"""Send a file over a new TCP connection."""
def doUploadOffline(fn,cfg):

	port = int(cfg['network']['tcpPortASC'])
	host = str(cfg['network']['hostNameASC'])
	print ('About to do offline transfer of %s to %s:%d' % (fn,host,port))

	# Connect (TCP)
	s = connect(cfg)
	if s == None:
		print ('Failed to upload %s' % (fn))
		return False

	# Progress bar setup
	tr = TransfersizeReporter(max_size = os.path.getsize(fn))
	ncopied = 0

	# Transfer the file
	f = open(fn, 'rb')
	while True:
		d = f.read(16384)
		if len(d)<1:
			break
		s.send(d)
		ncopied += len(d)
		if cfg['progressbar']:
			tr.update(ncopied)

	f.close()
	s.shutdown(socket.SHUT_WR)
	s.close()

	# Summary
	print ('Sent file %s to %s:%d' % (fn,host,port))
	return True

"""Send a still growing file over a new TCP connection. Stop when file is closed externally."""
def doUploadLive(fn,cfg):

	port = int(cfg['network']['tcpPortASC'])
	host = str(cfg['network']['hostNameASC'])
	print ('About to do realtime transfer of %s to %s:%d' % (fn,host,port))

	# Connect (TCP)
	s = connect(cfg)
	if s == None:
		print ('Failed to upload %s' % (fn))
		return False

	# Progress bar setup
	tr = TransfersizeReporter()
	ncopied = 0

	# Watch the file, send every new written piece over the network
	i = inotify.adapters.Inotify()
	i.add_watch(fn)
	f = open(fn, 'rb')
	try:
		for event in i.event_gen():
			if event is not None:
				(header, type_names, watch_path, fn2) = event
				# print fn2, type_names
				if 'IN_MODIFY' in type_names:
					d = f.read(16384)
					while len(d)>0:
						s.send(d)
						ncopied += len(d)
						if cfg['progressbar']:
							tr.update(ncopied)
						d = f.read(16384)
				if 'IN_CLOSE_WRITE' in type_names:
					break
	except Exception as e:
		print ('Stopped realtime transfer of %s due to %s' % (fn,str(e)))
	finally:
		f.close()
		i.remove_watch(fn)
		s.shutdown(socket.SHUT_WR)
		s.close()

	# Summary
	print ('Sent file %s to %s:%d' % (fn,host,port))
	return True

"""Monitor a directory for new files"""
def watch(cfg):

	path = cfg['operation']['mimeOutputPath']
	i = inotify.adapters.Inotify()
	i.add_watch(path)
	
	action_evts = []
	if cfg['realtime']:
		action_evts = inotify_events_realtime
		print('Watching %s for new written-to MIME files, for realtime transfer' % (path))
	else:
		action_evts = inotify_events
		print('Watching %s for newly completed MIME files' % (path))

	try:
		for event in i.event_gen():
			if event is not None:
				(header, type_names, watch_path, fn) = event
				fp = watch_path + fn
				ext = fn[fn.rfind('.')+1:].upper()
				evt_hit = any([type_name in action_evts for type_name in type_names])
				if (evt_hit) and (ext in extensions):
					print ('Processing file %s, after event %s' % (fn,str(event)))
					doUpload(fp,cfg)
				elif (evt_hit):
					print ('Ignoring file %s with wrong extension %s, event %s' % (fn,ext,str(event)))
				else:
					# print ('Ignoring event %s' % (str(event)))
					pass
	except Exception as e:
		print ('Stopped watching %s due to %s' % (path,str(e)))
	finally:
		i.remove_watch(path)

"""Scan a directory for existing files to transfer first, before monitoring the directory"""
def init(cfg):
	path = cfg['operation']['mimeOutputPath']
	print ('Checking %s for existing files to upload' % (path))
	for fp in glob.glob(path + '/*'):
		ext = fp[fp.rfind('.')+1:].upper()
		if (ext in extensions):
			rt = cfg['realtime']
			cfg['realtime'] = False # realtime upload doesn't cope with existing closed non-growing files...
			doUpload(fp,cfg)
			cfg['realtime'] = rt

"""Merge two settings"""
def merge(cfgMain,cfgSecondary):
	cfg = cfgMain
	for key in cfgSecondary.keys():
		if not(key in cfg):
			cfg[key] = cfgSecondary[key]
	return cfg

"""Default settings"""
def defaultConfig():
	cfg = {}
	cfg['operation'] = {}
	cfg['operation']['mimeOutputPath'] = '/tmp/'
	cfg['network'] = {}
	cfg['network']['tcpPortASC'] = '12345'
	cfg['network']['hostNameASC'] = 'localhost'
	cfg['realtime'] = False
	cfg['progressbar'] = False
	return cfg

if __name__ == '__main__':
	cfg = defaultConfig()
	args = sys.argv[1:]
	while len(args)>0:
		if (args[0]=='-c') and (len(args)>1):
			with open(args[1],'r') as json_data:
				cfg_json = json.load(json_data)
				cfg = merge(cfg_json, cfg)
				print ('Loaded config file %s' % (args[1]))
			args = args[2:]
		elif args[0] == '-r' or args[0] == '--realtime':
			cfg['realtime'] = True
			args = args[1:]
		elif args[0] == '-p' or args[0] == '--progressbar':
			cfg['progressbar'] = True
			args = args[1:]
		elif args[0][0] == '-':
			print __doc__
			sys.exit(0)
		elif args[0][0] != '-':
			break
	init(cfg)
	watch(cfg)

