#!/usr/bin/python
"""
Usage: asmPlotMime.py <singleConfiguration.xml> <asm_file.mime>

Displays spectra in a MIME file (mixed text and binary)
to roughly verify that spectra match some expectation.

The MIME file holds flat arrays with multiple spectra.
Unlike ALMA BDF files the MIME files from ASM do not contain
any detailed data description. This script derives the data
dimensions from external file singleConfiguration.xml that
somewhere in the XML tree must contain a 'singleConfiguration'
element. It can be e.g. the loadConfiguration command sent to ASM.
"""
import email, sys
import numpy as np
import xml.etree.ElementTree as ET

# Check args
args = sys.argv[1:]
while len(args)>0:
        if args[0] == '-h' or args[0] == '--help':
                print __doc__
                sys.exit(0)
        else:
		break
if len(args) != 2:
	print __doc__
	sys.exit(0)

# Import GUI/plotting late, to speed up cmd line args checking
import matplotlib.pyplot as plt 

####

def getLayout(nfigs):
	N = int(np.sqrt(nfigs))
	M = int(0.5 + float(nfigs)/N)
	return (N,M)

def plotEntry(raw,d):
	if len(d)<1:
		print ('Error: Got spectral data but no XML description for it yet!')
		return

	len_expected = 0
	for dims in d['dataDimensions']:
		len_expected += dims['Nch'] * dims['Npolprod']
	if len(raw) != len_expected:
		print ('Warning: Got %d float32 values but expected %d based on singleConfiguration.xml!' % (len(raw),len_expected))

	Nspecwin = len(d['dataDimensions'])
	(N,M) = getLayout(Nspecwin)

	print ('Plotting %d figures on %d x %d panel' % (Nspecwin,N,M))

	fig, axs = plt.subplots(N,M, sharex=False, sharey=False, squeeze=False)
	axs = axs.flatten()
	kwargs = dict(family='sans-serif', weight='light')

	ndone = 0
	for nr in range(Nspecwin):
		Nprod = d['dataDimensions'][nr]['Npolprod']
		Nch = d['dataDimensions'][nr]['Nch']
		bw = d['dataDimensions'][nr]['BW']
		Lspec = Nch * Nprod
		spec = np.reshape(raw[ndone:(ndone+Lspec)], (Nprod,Nch), order='F')
		xpos = np.reshape(np.arange(Lspec), (Nprod,Nch))
		axs[nr].semilogy(xpos[0],spec[0],'r')
		if Nprod >= 2:
			axs[nr].semilogy(xpos[1],spec[1],'g')
		if Nprod >= 4:
			axs[nr].semilogy(xpos[2],spec[2],'b')
			axs[nr].semilogy(xpos[3],spec[3],'b')
		ndone += Lspec
		axs[nr].set_xlim(left=0, right=Lspec-1)
		title = '%s, %.2f MHz' % (d['dataTitles'][nr],bw)
		axs[nr].set_title(title, **kwargs)
	#plt.xticks([nch-1])
	plt.show()

def defaultDesc():
	desc = {}
	desc['basebands'] = []
	desc['pols'] = []
	desc['axes_order'] = []
	desc['nchan'] = 0
	desc['nantennas'] = 0
	desc['specs'] = []
	desc['dataDimensions'] = []
	desc['dataTitles'] = []
	return desc

def singleconfigurationToDesc(xml):
	desc = defaultDesc()

	# Look for 'singleConfiguration' element at root, or one, two nodes down the tree
	singleCfg = xml.find('singleConfiguration')
	if singleCfg == None:
		singleCfg = xml.find('./singleConfiguration')
	if singleCfg == None:
		singleCfg = xml.find('.//singleConfiguration')

	# Decode the spectral windows in the 'singleConfiguration'
	bb = singleCfg.find('basebandConfig')
	for sw in bb.findall('spectralWindow'):
		swname = sw.find('name').text
		pols = sw.attrib['polnProducts']
		Npolprod = pols.count(',')+1 # 1, 2, or 4
		Nch = float(sw.find('effectiveNumberOfChannels').text)
		Nch = int(Nch / float(sw.find('spectralAveragingFactor').text))
		bw = float(sw.find('effectiveBandwidth').text)
		dataDim = {'Nch':Nch, 'Npolprod':Npolprod, 'BW':bw}
		desc['dataDimensions'].append(dataDim)
		desc['dataTitles'].append('%s %dch %s' % (swname,Nch,pols))
		print desc['dataTitles'][-1]
	return desc

def processMIME(fn,asmCfg):

	# Defaults
	desc = defaultDesc()

	# Parse file
	nent = 0
	f = open(fn,'r')
	msg = email.message_from_file(f)
	for part in msg.walk():

		T = part.get_content_maintype()
		ST = part.get_content_subtype()
		D = part.get_payload(decode=True)
		LOC = part['Content-Location']
		if T == 'multipart':
		        continue

		if "sdmDataHeader.xml" in LOC:
			# Complete header from ASC; no longer supported
			xml = ET.fromstring(D)
			print ('Error: got sdmDataHeader.xml (ASC, ACA) but it is no longer supported by asmPlotMime.py')
			sys.exit(1)
		elif "dataHeader.xml" in LOC:
			desc = singleconfigurationToDesc(asmCfg)
		elif "autoData.bin" in LOC:
			f32 = np.fromstring(D, dtype=np.float32)
			plotEntry(f32, desc)

asmCfg = ET.parse(args[0])
mimeFile = args[1]
processMIME(mimeFile, asmCfg)
