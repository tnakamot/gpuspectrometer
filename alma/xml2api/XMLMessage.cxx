
#include "helper/logger.h"
#include "xml2api/XMLMessage.hxx"

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

#include <string>
#include <cstring>
#include <set>
#include <map>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>

#include <limits.h>
#include <stdlib.h>  // realpath()

#ifndef XML_MESSAGE_DBG
#define XML_MESSAGE_DBG 0
#endif

#if HAVE_XERCES
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#endif

#if !HAVE_XERCES

/// C'stor in case Xerces is not available
XMLMessage::XMLMessage(const std::string &schemafilename)
{
    m_valid = false;
    m_commandId = XML_INVALID_CMD_ID;
    m_commandName = XML_INVALID_CMD_NAME;
    m_pt.clear();
}

/// D'stor in case Xerces is not available
XMLMessage::~XMLMessage() { }

/// Dummy XML validator in case Xerces is not available
bool XMLMessage::validate(const std::string &xml) const
{
    L_(lwarning) << "Xerces-C not enabled, will not validated XML.\n";
    return true;
}
#endif

std::string XMLMessage::generateBasicResponse(boost::property_tree::ptree& pt_out, unsigned int Id, const char* cmd, const char* code, const char* status)
{
    std::ostringstream os;
    std::string _cmd(cmd);
    pt_out.put("response.commandId", Id);
    pt_out.put("response." + _cmd + ".completionCode", code);
    if (strlen(status) > 0) {
        pt_out.put("response." + _cmd + ".status", status);
    }
    write_xml(/*dest*/os, /*src*/pt_out);
    return os.str();
}

/** \brief Loads an XML document from a file.
 *
 * Loads XML from an file. It is then subsequently parsed by Boost ParserTree.
 * If Xerces-C C++ is available the XML document is validated against the schema.
 */
bool XMLMessage::load(const char* xmlfilename)
{
    // Open file then load it
    std::ifstream ifs(xmlfilename, std::ifstream::in);
    if (!ifs) {
        m_commandId = XML_INVALID_CMD_ID;
        m_commandName = XML_INVALID_CMD_NAME;
        m_pt.clear();
        throw std::invalid_argument("could not access XML file");
    }
    return load(ifs);
}

/** \brief Loads an XML document from an inputstream.
 *
 * Loads XML from an istream. It is then subsequently parsed by Boost ParserTree.
 * If Xerces-C C++ is available the XML document is validated against the schema.
 */
bool XMLMessage::load(std::istream& xmlistream)
{
    // Read complete 'istream' then load it
    std::stringstream ss;
    xmlistream >> ss.rdbuf();
    std::string xml = ss.str();
    return load(xml);
}

/** \brief Processes an XML string.
 *
 * Processes an XML string, parsing it by Boost ParserTree.
 * If Xerces-C C++ is available the XML document is validated against the schema.
 */
bool XMLMessage::load(std::string& xml)
{
    m_xmlStr = xml;
    m_valid = validate(xml);
    parse(xml);
    return m_valid;
}

/** \brief Parses XML from a string into a Boost PropertyTree,
 *         while doing some basic validity checks, and determing
 *         if the XML contains a command or a response.
 */
void XMLMessage::parse(std::string& xml)
{
    using boost::property_tree::ptree;
    using boost::optional;

    m_commandId = XML_INVALID_CMD_ID;
    m_commandName = XML_INVALID_CMD_NAME;
    m_pt.clear();

    std::istringstream is(xml);
    boost::property_tree::xml_parser::read_xml(is, m_pt); // works only with "(char*)filename" or with "(istream)f"

    optional<ptree&> cmd = m_pt.get_child_optional("command");
    optional<ptree&> rsp = m_pt.get_child_optional("response");
    if ((!cmd && !rsp) || (cmd && rsp)) {
        throw std::invalid_argument("invalid XML (no single command or response found)");
    }

    if (cmd) {
        m_isCommand = true;
        m_commandId = m_pt.get("command.commandId", 0);
    } else {
        m_isCommand = false;
        m_commandId = m_pt.get("response.commandId", 0);
    }

    m_commandName.clear();
    bool cmdFound = false;
    BOOST_FOREACH(ptree::value_type &v, m_pt.get_child(m_isCommand ? "command" : "response")) {
        std::string elem(v.first.data());
        boost::trim(elem);
        if (XML_MESSAGE_DBG) std::cout << (m_isCommand ? "command" : "response") << " section, next key = " << elem << "\n";
        if (elem != "commandId") {
            if (cmdFound) {
                throw std::invalid_argument("invalid XML (contains multiple commands or responses)");
            } else {
                cmdFound = true;
                m_commandName = elem;
            }
        }
    }
}

#if HAVE_XERCES

class ParserErrorHandler : public xercesc::ErrorHandler
{
    private:
        void reportParseException(const xercesc::SAXParseException& ex) {
            char *str = xercesc::XMLString::transcode(ex.getMessage());
            L_(lerror) << "Xerces-C ParseException at line " << ex.getColumnNumber()
                      << " column " << ex.getLineNumber()
                      << ": " << str << "\n";
            xercesc::XMLString::release(&str);
        }
    public:
        void warning(const xercesc::SAXParseException& ex) { reportParseException(ex); }
        void error(const xercesc::SAXParseException& ex) { reportParseException(ex); }
        void fatalError(const xercesc::SAXParseException& ex) { reportParseException(ex); }
        void resetErrors() { }
};

XMLMessage::XMLMessage(const std::string &schemafilename)
{
    m_valid = false;
    m_commandId = XML_INVALID_CMD_ID;
    m_commandName = XML_INVALID_CMD_NAME;
    m_pt.clear();

    parserErrorHandler = new ParserErrorHandler;
    domParser = new xercesc::XercesDOMParser();
    char *xsd_filepath = realpath(schemafilename.c_str(), NULL);
    if (!xsd_filepath) {
        throw std::invalid_argument("could not access schema file");
    }
    if (domParser->loadGrammar(xsd_filepath, xercesc::Grammar::SchemaGrammarType) == NULL) {
        free(xsd_filepath);
        throw std::invalid_argument("could not load schema");
    }
    free(xsd_filepath);

    domParser->setErrorHandler(parserErrorHandler);
    domParser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
    domParser->setDoNamespaces(true);
    domParser->setDoSchema(true);
    domParser->setValidationSchemaFullChecking(true);
    domParser->setExternalNoNamespaceSchemaLocation(schemafilename.c_str());
}

XMLMessage::~XMLMessage()
{
    if (domParser == NULL) {
        return;
    }
    domParser->resetEntities();
    domParser->resetCachedGrammarPool();
    domParser->resetDocumentPool();
    domParser->reset();
    delete domParser;
    delete parserErrorHandler;
}

/** \brief Uses the Xerces-C C++ library to validate XML against schema.
 *
 * Uses the Xerces-C C++ library to validate an XML file against a schema file,
 * but does not actually parse the XML into any spectrometer structure. For
 * such tasks Xerces-C wrapper code ('xsdcxx' generator) is much too bloated.
 *
 * Based on
 * http://stackoverflow.com/questions/22015661/xerces-xml-validation-with-xsd/
 * https://sivachandranp.wordpress.com/2010/10/10/xml-schema-validation-using-xerces-c/
 */
bool XMLMessage::validate(const std::string &xml) const
{
    bool valid = true;
    if (domParser == NULL) {
        throw std::invalid_argument("XMLMessage::Validate() no domParser exists!");
    }
    xercesc::MemBufInputSource xmlbuf((const XMLByte*)xml.c_str(), xml.size(), "xml in-memory copy");
    domParser->parse(xmlbuf);
    if(domParser->getErrorCount() != 0) {
        //throw std::invalid_argument("invalid XML for given schema");
        valid = false;
    }
    return valid;
}

#endif

