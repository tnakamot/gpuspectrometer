/**
 * \class MCMsgCommandResponseFactory
 *
 * \brief A factory pattern for registering new M&C handler classes and
 *        creating new objects based on the name of an M&C command.
 *
 * \author $Author: janw $
 *
 */
#ifndef XML2API_FACTORY_HXX
#define XML2API_FACTORY_HXX

#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <map>
#include <set>
#include <string>

#include "asm/ASMInterface.hxx"

struct MCMsgCommandResponse { // base class

    public:
        MCMsgCommandResponse() : m_is_registered(false) {}
        MCMsgCommandResponse(bool isRegistered) : m_is_registered(isRegistered) {}
        virtual ~MCMsgCommandResponse() {}

    public:
        bool isRegistered() const { return this->m_is_registered; }
        void setId(unsigned int id) { m_Id=id; }
        unsigned int getId() const { return m_Id; }

    public:
        virtual int handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_) = 0;
        virtual std::string getName() = 0;

    private:
        const bool m_is_registered;
        unsigned int m_Id;
};

typedef MCMsgCommandResponse* (*factoryMethod)();

class MCMsgCommandResponseFactory {
    public:
        static bool registerClass(std::string name, factoryMethod createMethod);
        static MCMsgCommandResponse* createObject(std::string name);
    protected:
        static std::map<std::string, factoryMethod>& m_registeredNames();
};

template<typename T>
class MCMsgCommandResponseImpl : public MCMsgCommandResponse {
    public:
        static MCMsgCommandResponse* createObject() { return new T(); }
        std::string getName() { return T::name; }
    protected:
        MCMsgCommandResponseImpl() : MCMsgCommandResponse(m_is_registered) { }
        static const bool m_is_registered;
};

template<typename T>
const bool MCMsgCommandResponseImpl<T>::m_is_registered =
    MCMsgCommandResponseFactory::registerClass(T::name, &MCMsgCommandResponseImpl<T>::createObject);

#define XML2API_CREATE_AND_REGISTER_CMD(x) \
    struct MCMsgCommandResponse_##x : public MCMsgCommandResponseImpl<MCMsgCommandResponse_##x> \
    { \
        MCMsgCommandResponse_##x() : MCMsgCommandResponseImpl<MCMsgCommandResponse_##x>() {} \
        static const std::string name; \
        int handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_); \
     }; \
     const std::string MCMsgCommandResponse_##x::name = #x;

#endif // XML2API_FACTORY_HXX
