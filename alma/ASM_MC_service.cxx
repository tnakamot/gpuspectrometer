/**
 * Main ASM entry point
 */

#include "asm/ASM.hxx"
#include "helper/logger.h"
#include "time/NTPDStatusQuery.hxx"
#include "global_defs.hxx"
#include "version.hxx"
#include "version_gittag.hxx"

#include <iostream>
#include <exception>
#include <string>

#include <signal.h>

#if HAVE_XERCES
#include <xercesc/parsers/XercesDOMParser.hpp>
#endif

static volatile bool control_C = false;

static void sigint_handler(int s);
static void registerSigintHandler();

int main(int argc, char **argv)
{

#if HAVE_XERCES
    xercesc::XMLPlatformUtils::Initialize();
#endif

    // Parse user args
    std::string configfilename(DEFAULT_CONFIG_FILE_PATH);
    bool log_to_file = true;
    int opt;
    bool drxp_debuglog = false;
    int verbosity = 0;
    opterr = 0;
    while ((opt = getopt(argc, argv, "c:dtvh")) != -1) {
        switch (opt) {
            case 'c':
                configfilename = optarg;
                break;
            case 'd':
                drxp_debuglog = true;
                break;
            case 'v':
                log_to_file = false;
                verbosity++;
                break;
            case 'h':
            case '?':
            default:
                std::cout << "\nUsage: " << argv[0] << " [-c configfile.json] [-d] [-v]\n"
                          << "    -c specifies a config file to use (default: " << configfilename << ")\n"
                          << "    -d enables low-level DRXP log messages into /tmp/drxp_IF<n>.trace\n"
                          << "    -v logs to console rather than file, use more -v for higher verbosity\n"
                          << "\n";
                return -1;
        }
    }
    if (argc > optind) {
        std::cout << "\nError: unexpected extra argument: '" << argv[optind] << "'\n";
        return -1;
    }

    // Report version and also print on screen if file-logging
    L_(linfo) << "Started version " << ASM_VERSION_STR << " built from Git tag " << GIT_TAG << " of " << GIT_DATE;
    L_(linfo) << "Using ASM configuration from " << configfilename;
    if (log_to_file) {
        std::cout << "Started version " << ASM_VERSION_STR << " built from Git tag " << GIT_TAG << " of " << GIT_DATE << "\n";
        std::cout << "Using ASM configuration from " << configfilename << "\n";
    }

#ifndef HAVE_GPU
    L_(linfo) << "Note: compiled without GPU support!";
    std::cout << "Server compiled without GPU support, using (incomplete) emulation on CPU instead." << std::endl;
#endif

#ifndef HAVE_DRXP
    L_(linfo) << "Note: compiled without DRXP support, using dummy!";
    std::cout << "Server compiled without DRXP support, simulating a dummy DRXP instead." << std::endl;
#endif

    // Check that NTP is good, at least right now (who knows about later...)
    NTPDStatusQuery ntpStatus;
    if (!ntpStatus.requirementsMet()) {
        L_(lerror) << "Cannot start observation ASM as NTP time is of too poor quality.";
        std::cerr << "Exiting due to NTP issues. Need stratum 2 and a synchronization distance of at most 21 ms.\n";
        std::cerr << "Please fix local NTP settings! Then try to start ASM again.\n";
        return -1;
    }

    // Initialize ASM object and start accepting M&C
    ASM asm_(configfilename);
    asm_.startLogging(verbosity, log_to_file);
    asm_.startObservingQueue();
    asm_.startDRXPs();
    asm_.startMC();
    // registerSigintHandler();
    while (!control_C) {
        sleep(5);
        std::cout << "<still running, state " << asm_.getStateContainer().getInternalStateStr() << ">\n";
    }

    // Clean-up
    std::cout << "Quitting...\n";
    try {
        asm_.stopObservingQueue();
        asm_.stopMC();
        asm_.stopDRXPs();
    }  catch (std::exception &e) {
        std::cout << "M&C clean-up error: " << e.what() << "\n";
    }

#if HAVE_XERCES
    xercesc::XMLPlatformUtils::Initialize();
#endif

    return 0;
}


static void sigint_handler(int s)
{
    control_C = true;
    std::cout << "Caught Ctrl-C, stopping soon\n";
}


static void registerSigintHandler()
{
    struct sigaction si;
    si.sa_handler = sigint_handler;
    sigemptyset(&si.sa_mask);
    si.sa_flags = 0;
    sigaction(SIGINT, &si, NULL);
    std::cout << "Registered Ctrl-C handler\n";
}
