#!/bin/bash
#
# Note: version_gittag.sh gets invoked from Makefile !
#
# Gets the git tag of the current working-copy of the source
# and store it in version_gittag.hxx

git describe --all --long | tr "-" " " | awk '{ print "#define GIT_TAG \""  $3 "\"" }' > version_gittag.hxx
git describe --tags HEAD | xargs git show -s --format=format:"%cd" | tail -1 | awk '{ print "#define GIT_DATE \""  $0 "\"" }' >> version_gittag.hxx
cat version_gittag.hxx

