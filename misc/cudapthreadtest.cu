// Trying to test CPU threads and CUDA context,
// as KVN M&C program VDIF data pusher gets alternately
// a SEGFAULT cudaEventRecord() @ CUDA_TIMING_START(cs->evt_kstart, cs->sid),
// or "ERROR: invalid resource handle (CUDA_TIMING_START(cs->evt_kstart, cs->sid))",
// or "ERROR: invalid device ordinal (cudaSetDevice(0))"
// or "ERROR: unknown error"

#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <iomanip>

#include <cuda.h>
#include <helper_cuda.h>

#define BUFSZ 128*1024*1024
#define NGPU  2

// cudaHostAllocPortable : Pinned memory accessible by all CUDA contexts
// cudaHostAllocDefault  : Default page-locked allocation flag

class DataRecipient_IFace {
  public:
    virtual void takeData(void* h_data) = 0;
};

class DataProducer {
  public:
    DataProducer();
    void startProducer(DataRecipient_IFace* dr);
  private:
    void producerThread();
    void handover(void* buf);
    void handoverThread(void* buf);
  private:
    DataRecipient_IFace* rcpt;
    void* h_data;
};

class DataHandler : public DataRecipient_IFace {
  public:
    DataHandler() { m_init_done=false; }
    void alloc();
    void takeData(void* h_data); // DataRecipient_IFace
  private:
    void alloc_gpu(int gpu);
  private:
    void* d_data[NGPU];
    cudaStream_t sid[NGPU];
    cudaEvent_t evt[NGPU];
    volatile bool m_init_done;
    boost::mutex m_cuCtxMutex;
};

////////////////////////////////////////////////////////////////////////////

void DataHandler::alloc()
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    for (int g=0; g < NGPU; g++) { alloc_gpu(g); }
    m_init_done = true;
}

/** Allocate everythign on a specific GPU */
void DataHandler::alloc_gpu(int gpu)
{
    checkCudaErrors( cudaSetDevice(gpu) );
    checkCudaErrors( cudaStreamCreate(&sid[gpu]) );
    checkCudaErrors( cudaEventCreate(&evt[gpu]) );
    checkCudaErrors( cudaMalloc(&d_data[gpu], BUFSZ) );
}

/** Copy data from host to GPU. Invoked by external 'producer' thread */
void DataHandler::takeData(void* h_data)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    static int gpu = -1;
    gpu = (gpu + 1) % NGPU; // round robin

    std::cout << "Thread started, gpu=" << gpu << ", h_data=" << h_data
              << ", d_data=" << d_data[gpu] << ", init_done=" << m_init_done << std::endl;
    if (!m_init_done) {
        std::cout << "  -- skipping copy, target gpu=" << gpu << " not initialized" << std::endl;
        return;
    }

    checkCudaErrors( cudaSetDevice(gpu) );
    checkCudaErrors( cudaEventRecord(this->evt[gpu], this->sid[gpu]) );
    checkCudaErrors( cudaMemcpy(this->d_data[gpu], h_data, BUFSZ, cudaMemcpyHostToDevice) );
    checkCudaErrors( cudaEventSynchronize(this->evt[gpu]) );

    std::cout << "Thread done, gpu=" << gpu << std::endl;
}

////////////////////////////////////////////////////////////////////////////

DataProducer::DataProducer()
{
    checkCudaErrors( cudaMallocHost(&h_data, BUFSZ, cudaHostAllocDefault) );
}

void DataProducer::startProducer(DataRecipient_IFace* dr)
{
    this->rcpt = dr;
    boost::thread *t = new boost::thread(&DataProducer::producerThread, this);
    t->detach();
}

void DataProducer::producerThread()
{
    int iter = 0;
    while (1) {
        sleep(1);
        std::cout << "Iteration " << std::dec << (iter++) << std::endl;
        handover(this->h_data);
        handover(this->h_data);
        handover(this->h_data);
    }
}

void DataProducer::handover(void* buf)
{
    boost::thread *t = new boost::thread(&DataProducer::handoverThread, this, buf);
    t->detach();
}

void DataProducer::handoverThread(void* buf)
{
    rcpt->takeData(buf);
}

////////////////////////////////////////////////////////////////////////////

int main(void)
{
    DataProducer dp;
    DataHandler dh;

    dp.startProducer(&dh);
    sleep(1);
    dh.alloc(); // delayed alloc
    sleep(10);

    return 0;
}
