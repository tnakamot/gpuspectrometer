#include <unistd.h>
#include <sys/time.h>
#include <cuda.h>

#include "SelfTestGPU_memdefs.h"
#include "SelfTestGPU_memtests.h"
#include "SelfTestGPU_memkernels.cu" // or .h?

__thread unsigned int* err_count;

////////////////////////////////////////////////////////////////////////////////////

SelfTestGPUMemTests::SelfTestGPUMemTests(void *d_testArea, int num_blocks)
{
    m_d_testArea = (char*)d_testArea;
    m_d_testArea_end = m_d_testArea + GPU_MEMTEST_BLOCKSIZE*((size_t)num_blocks);
    m_num_blocks = num_blocks;
    m_num_iterations = 1;
    m_error_count = 0;
    cudaMemset(err_count, m_error_count, sizeof(unsigned int));
}

SelfTestGPUMemTests::~SelfTestGPUMemTests()
{
}

////////////////////////////////////////////////////////////////////////////////////
// Helpers from https://sourceforge.net/projects/cudagpumemtest/ file tests.cu
///////////////////////////////////////////////////////////////////////////////////

void SelfTestGPUMemTests::SHOW_PROGRESS(const char* msg, size_t ncurr, size_t ntotal)
{
    return;
}

/** \return True if error detected in latest (or current) memory test */
bool SelfTestGPUMemTests::error_checking(const char*, size_t)
{
     cudaMemcpy((void*)&m_error_count, (void*)err_count, sizeof(unsigned int), cudaMemcpyDeviceToHost);
     return m_error_count != 0;
}

unsigned int SelfTestGPUMemTests::get_random_num(void)
{
    struct timeval t0;
    if (gettimeofday(&t0, NULL) !=0) {
        t0.tv_sec = 1234;
    }

    unsigned int seed = (unsigned int)t0.tv_sec;
    srand(seed);

    return rand_r(&seed);
}

unsigned long SelfTestGPUMemTests::get_random_num_long(void)
{
    struct timeval t0;
    if (gettimeofday(&t0, NULL) !=0){
        fprintf(stderr, "ERROR: gettimeofday() failed\n");
        exit(ERR_GENERAL);
    }


    unsigned int seed= (unsigned int)t0.tv_sec;
    srand(seed);

    unsigned int a = rand_r(&seed);
    unsigned int b = rand_r(&seed);

    unsigned long ret =  ((unsigned long)a) << 32;
    ret |= ((unsigned long)b);

    return ret;
}

unsigned int SelfTestGPUMemTests::move_inv_test(unsigned int p1, unsigned p2)
{
    unsigned int i, err = 0;
    dim3 grid(GPU_MEMTEST_GRIDSIZE);

    for (i= 0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        kernel_move_inv_write<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, p1);
        SHOW_PROGRESS("move_inv_write", i, m_num_blocks);
    }

    for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        kernel_move_inv_readwrite<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, p1, p2, err_count, err_addr, err_expect, err_current, err_second_read);
        err += error_checking("move_inv_readwrite",  i);
        SHOW_PROGRESS("move_inv_readwrite", i, m_num_blocks);
    }

    for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        kernel_move_inv_read<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, p2, err_count, err_addr, err_expect, err_current, err_second_read);
        err += error_checking("move_inv_read",  i);
        SHOW_PROGRESS("move_inv_read", i, m_num_blocks);
    }

    return err;
}

////////////////////////////////////////////////////////////////////////////////////
// Tests from https://sourceforge.net/projects/cudagpumemtest/ file tests.cu
///////////////////////////////////////////////////////////////////////////////////

bool SelfTestGPUMemTests::test0()
{
    unsigned int i;
    dim3 grid(GPU_MEMTEST_GRIDSIZE);

    //test global address
    kernel_test0_global_write<<<1, 1>>>(m_d_testArea, m_d_testArea_end);
    kernel_test0_global_read<<<1, 1>>>(m_d_testArea, m_d_testArea_end, err_count, err_addr, err_expect, err_current, err_second_read);
    error_checking("test0 on global address",  0);

    for(int ite = 0; ite < m_num_iterations; ite++){
        for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
            kernel_test0_write<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end);
            SHOW_PROGRESS("test0 on writing", i, m_num_blocks);
        }

        for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
            kernel_test0_read<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, err_count, err_addr, err_expect, err_current, err_second_read);
            error_checking(__FUNCTION__,  i);
            SHOW_PROGRESS("test0 on reading", i, m_num_blocks);
        }
    }
    return;

    unsigned int i,j;
    dim3 grid(GPU_MEMTEST_GRIDSIZE);

    for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        unsigned int prev_pattern = 0;

        //the first iteration
        unsigned int pattern = 1;
        kernel_test0_write<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, pattern,  err_count, err_addr, err_expect, err_current, err_second_read); CUERR;
        prev_pattern =pattern;

        for (j =1; j < 32; j++){
            pattern = 1 << j;
            kernel_test0_readwrite<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, pattern, prev_pattern, err_count, err_addr, err_expect, err_current, err_second_read); CUERR;
            prev_pattern = pattern;
        }

        error_checking(__FUNCTION__,  i);
        SHOW_PROGRESS(__FUNCTION__, i, m_num_blocks);
    }
}

bool SelfTestGPUMemTests::test1()
{
    unsigned int i;
    dim3 grid(GPU_MEMTEST_GRIDSIZE);

    for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        kernel_test1_write<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, err_count);
        SHOW_PROGRESS("test1 on writing", i, m_num_blocks);

    }

    for (i=0;i < m_num_blocks; i+= GPU_MEMTEST_GRIDSIZE){
        kernel_test1_read<<<grid, 1>>>(m_d_testArea + i*GPU_MEMTEST_BLOCKSIZE, m_d_testArea_end, err_count, err_addr, err_expect, err_current, err_second_read);
        error_checking("test1 on reading",  i);
        SHOW_PROGRESS("test1 on reading", i, m_num_blocks);
    }

    return;
}

bool SelfTestGPUMemTests::test2()
{
    unsigned int p1 = 0;
    unsigned int p2 = ~p1;
    DEBUG_PRINTF("Test2: Moving inversions test, with pattern 0x%x and 0x%x\n", p1, p2);
    move_inv_test(m_d_testArea, m_num_blocks, p1, p2);
    DEBUG_PRINTF("Test2: Moving inversions test, with pattern 0x%x and 0x%x\n", p2, p1);
    move_inv_test(m_d_testArea, m_num_blocks, p2, p1);
}

bool SelfTestGPUMemTests::test3()
{
}
