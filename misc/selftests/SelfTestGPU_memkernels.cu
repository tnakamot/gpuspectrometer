/*
 * Kernels of "CUDA GPU MemTest" from https://sourceforge.net/projects/cudagpumemtest/ file tests.cu
 *
 * The original MemTest is not a library. Memory tests are not easily called from other programs.
 * Rather than update MemTest we just copy out the relevant CUDA kernels and use them directly,
 * without all the verbose wrappings of the MemTest program.
 *
 * Expected external defines: GPU_MEMTEST_BLOCKSIZE, usually ((unsigned long)1024*1024)
 */

#include "SelfTestGPU_memdefs.h"

#define RECORD_ERR(err, p, expect, current) do { atomicAdd(err,1); } while(0)

__global__ void kernel_move_inv_write(char* _ptr, char* end_ptr, unsigned int pattern)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);
    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        ptr[i] = pattern;
    }

    return;
}

__global__ void kernel_move_inv_readwrite(char* _ptr, char* end_ptr, unsigned int p1, unsigned int p2, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);
    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (ptr[i] != p1){
            RECORD_ERR(err, &ptr[i], p1, ptr[i]);
        }
        ptr[i] = p2;

    }

    return;
}


/*
 * Test0 [Walking 1 bit]
 * This test changes one bit a time in memory address to see it
 * goes to a different memory location. It is designed to test
 * the address wires.
 */
__global__ void kernel_test0_global_write(char* _ptr, char* _end_ptr)
{

    unsigned int* ptr = (unsigned int*)_ptr;
    unsigned int* end_ptr = (unsigned int*)_end_ptr;
    unsigned int* orig_ptr = ptr;

    unsigned int pattern = 1;

    unsigned long mask = 4;

    *ptr = pattern;

    while(ptr < end_ptr){

        ptr = (unsigned int*) ( ((unsigned long)orig_ptr) | mask);
        if (ptr == orig_ptr){
            mask = mask <<1;
            continue;
        }
        if (ptr >= end_ptr){
            break;
        }

        *ptr = pattern;

        pattern = pattern << 1;
        mask = mask << 1;
    }
    return;
}

__global__ void kernel_test0_global_read(char* _ptr, char* _end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int* ptr = (unsigned int*)_ptr;
    unsigned int* end_ptr = (unsigned int*)_end_ptr;
    unsigned int* orig_ptr = ptr;

    unsigned int pattern = 1;

    unsigned long mask = 4;

    if (*ptr != pattern){
        RECORD_ERR(err, ptr, pattern, *ptr);
    }

    while(ptr < end_ptr){

        ptr = (unsigned int*) ( ((unsigned long)orig_ptr) | mask);
        if (ptr == orig_ptr){
            mask = mask <<1;
            continue;
        }
        if (ptr >= end_ptr){
            break;
        }

        if (*ptr != pattern){
            RECORD_ERR(err, ptr, pattern, *ptr);
        }

        pattern = pattern << 1;
        mask = mask << 1;
    }
    return;
}

__global__ void kernel_test0_write(char* _ptr, char* end_ptr)
{

    unsigned int* orig_ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);;
    unsigned int* ptr = orig_ptr;
    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int* block_end = orig_ptr + GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int);

    unsigned int pattern = 1;

    unsigned long mask = 4;

    *ptr = pattern;

    while(ptr < block_end){

        ptr = (unsigned int*) ( ((unsigned long)orig_ptr) | mask);
        if (ptr == orig_ptr){
            mask = mask <<1;
            continue;
        }
        if (ptr >= block_end){
            break;
        }

        *ptr = pattern;

        pattern = pattern << 1;
        mask = mask << 1;
    }
    return;
}


__global__ void kernel_test0_read(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{

    unsigned int* orig_ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);;
    unsigned int* ptr = orig_ptr;
    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int* block_end = orig_ptr + GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int);

    unsigned int pattern = 1;

    unsigned long mask = 4;
    if (*ptr != pattern){
        RECORD_ERR(err, ptr, pattern, *ptr);
    }

    while(ptr < block_end){

        ptr = (unsigned int*) ( ((unsigned long)orig_ptr) | mask);
        if (ptr == orig_ptr){
            mask = mask <<1;
            continue;
        }
        if (ptr >= block_end){
            break;
        }

        if (*ptr != pattern){
            RECORD_ERR(err, ptr, pattern, *ptr);
        }

        pattern = pattern << 1;
        mask = mask << 1;
    }
    return;
}


/*********************************************************************************
 * test1
 * Each Memory location is filled with its own address. The next kernel checks if the
 * value in each memory location still agrees with the address.
 *
 ********************************************************************************/

__global__ void kernel_test1_write(char* _ptr, char* end_ptr, unsigned int* err)
{
    unsigned int i;
    unsigned long* ptr = (unsigned long*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned long*) end_ptr) {
        return;
    }


    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned long); i++){
        ptr[i] =(unsigned long) & ptr[i];
    }

    return;
}


__global__ void kernel_test1_read(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned long* ptr = (unsigned long*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned long*) end_ptr) {
        return;
    }


    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned long); i++){
        if (ptr[i] != (unsigned long)& ptr[i]){
            RECORD_ERR(err, &ptr[i], (unsigned long)&ptr[i], ptr[i]);
        }
    }

    return;
}

/************************************************************************************
 * Test 5 [Block move, 64 moves]
 * This test stresses memory by moving block memories. Memory is initialized
 * with shifting patterns that are inverted every 8 bytes.  Then blocks
 * of memory are moved around.  After the moves
 * are completed the data patterns are checked.  Because the data is checked
 * only after the memory moves are completed it is not possible to know
 * where the error occurred.  The addresses reported are only for where the
 * bad pattern was found.
 *
 *
 *************************************************************************************/

__global__ void kernel_test5_init(char* _ptr, char* end_ptr)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int p1 = 1;
    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i+=16){
        unsigned int p2 = ~p1;

        ptr[i] = p1;
        ptr[i+1] = p1;
        ptr[i+2] = p2;
        ptr[i+3] = p2;
        ptr[i+4] = p1;
        ptr[i+5] = p1;
        ptr[i+6] = p2;
        ptr[i+7] = p2;
        ptr[i+8] = p1;
        ptr[i+9] = p1;
        ptr[i+10] = p2;
        ptr[i+11] = p2;
        ptr[i+12] = p1;
        ptr[i+13] = p1;
        ptr[i+14] = p2;
        ptr[i+15] = p2;

        p1 = p1<<1;
        if (p1 == 0){
            p1 = 1;
        }
    }

    return;
}

__global__ void kernel_test5_move(char* _ptr, char* end_ptr)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int half_count = GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int)/2;
    unsigned int* ptr_mid = ptr + half_count;

    for (i = 0;i < half_count; i++){
        ptr_mid[i] = ptr[i];
    }

    for (i=0;i < half_count - 8; i++){
        ptr[i + 8] = ptr_mid[i];
    }

    for (i=0;i < 8; i++){
        ptr[i] = ptr_mid[half_count - 8 + i];
    }

    return;
}


__global__ void kernel_test5_check(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    for (i=0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i+=2){
        if (ptr[i] != ptr[i+1]){
            RECORD_ERR(err, &ptr[i], ptr[i+1], ptr[i]);
        }
    }

    return;
}


/*****************************************************************************************
 * Test 6 [Moving inversions, 32 bit pat]
 * This is a variation of the moving inversions algorithm that shifts the data
 * pattern left one bit for each successive address. The starting bit position
 * is shifted left for each pass. To use all possible data patterns 32 passes
 * are required.  This test is quite effective at detecting data sensitive
 * errors but the execution time is long.
 *
 ***************************************************************************************/



__global__ void kernel_movinv32_write(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int k = offset;
    unsigned pat = pattern;
    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        ptr[i] = pat;
        k++;
        if (k >= 32){
            k=0;
            pat = lb;
        }else{
            pat = pat << 1;
            pat |= sval;
        }
    }

    return;
}

__global__ void kernel_movinv32_readwrite(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset, unsigned int * err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int k = offset;
    unsigned pat = pattern;
    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (ptr[i] != pat){
            RECORD_ERR(err, &ptr[i], pat, ptr[i]);
        }

        ptr[i] = ~pat;

        k++;
        if (k >= 32){
            k=0;
            pat = lb;
        }else{
            pat = pat << 1;
            pat |= sval;
        }
    }

    return;
}

__global__ void kernel_movinv32_read(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset, unsigned int * err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    unsigned int k = offset;
    unsigned pat = pattern;
    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (ptr[i] != ~pat){
            RECORD_ERR(err, &ptr[i], ~pat, ptr[i]);
        }

        k++;
        if (k >= 32){
            k=0;
            pat = lb;
        }else{
            pat = pat << 1;
            pat |= sval;
        }
    }

    return;
}

/******************************************************************************
 * Test 7 [Random number sequence]
 *
 * This test writes a series of random numbers into memory.  A block (1 MB) of memory
 * is initialized with random patterns. These patterns and their complements are
 * used in moving inversions test with rest of memory.
 *
 *
 *******************************************************************************/

__global__ void kernel_test7_write(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);
    unsigned int* start_ptr = (unsigned int*) _start_ptr;

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }


    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        ptr[i] = start_ptr[i];
    }

    return;
}

__global__ void kernel_test7_readwrite(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);
    unsigned int* start_ptr = (unsigned int*) _start_ptr;

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }


    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (ptr[i] != start_ptr[i]){
            RECORD_ERR(err, &ptr[i], start_ptr[i], ptr[i]);
        }
        ptr[i] = ~(start_ptr[i]);
    }

    return;
}

__global__ void kernel_test7_read(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);
    unsigned int* start_ptr = (unsigned int*) _start_ptr;

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }


    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (ptr[i] != ~(start_ptr[i])){
            RECORD_ERR(err, &ptr[i], ~(start_ptr[i]), ptr[i]);
        }
    }

    return;
}

/***********************************************************************************
 * Test 8 [Modulo 20, random pattern]
 *
 * A random pattern is generated. This pattern is used to set every 20th memory location
 * in memory. The rest of the memory location is set to the complimemnt of the pattern.
 * Repeat this for 20 times and each time the memory location to set the pattern is shifted right.
 *
 *
 **********************************************************************************/

__global__ void kernel_modtest_write(char* _ptr, char* end_ptr, unsigned int offset, unsigned int p1, unsigned int p2)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    for (i = offset;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i+=20){
        ptr[i] =p1;
    }

    for (i = 0;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i++){
        if (i % 20 != offset){
            ptr[i] =p2;
        }
    }

    return;
}

__global__ void kernel_modtest_read(char* _ptr, char* end_ptr, unsigned int offset, unsigned int p1, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    unsigned int i;
    unsigned int* ptr = (unsigned int*) (_ptr + blockIdx.x*GPU_MEMTEST_BLOCKSIZE);

    if (ptr >= (unsigned int*) end_ptr) {
        return;
    }

    for (i = offset;i < GPU_MEMTEST_BLOCKSIZE/sizeof(unsigned int); i+=20){
        if (ptr[i] !=p1){
            RECORD_ERR(err, &ptr[i], p1, ptr[i]);
        }
    }

    return;
}

/**************************************************************************************
 * Test10 [memory stress test]
 *
 * Stress memory as much as we can. A random pattern is generated and a kernel of large grid size
 * and block size is launched to set all memory to the pattern. A new read and write kernel is launched
 * immediately after the previous write kernel to check if there is any errors in memory and set the
 * memory to the compliment. This process is repeated for 1000 times for one pattern. The kernel is
 * written as to achieve the maximum bandwidth between the global memory and GPU.
 * This will increase the chance of catching software error. In practice, we found this test quite useful
 * to flush hardware errors as well.
 *
 */

__global__ void test10_kernel_write(char* ptr, int memsize, unsigned long p1)
{
    int i;
    int avenumber = memsize/(gridDim.x*gridDim.y);
    unsigned long* mybuf = (unsigned long*)(ptr + blockIdx.x* avenumber);
    int n = avenumber/(blockDim.x*sizeof(unsigned long));

    for(i=0;i < n;i++){
        int index = i*blockDim.x + threadIdx.x;
        mybuf[index]= p1;
    }
    int index = n*blockDim.x + threadIdx.x;
    if (index*sizeof(unsigned long) < avenumber){
        mybuf[index] = p1;
    }

    return;
}

__global__ void test10_kernel_readwrite(char* ptr, int memsize, unsigned long p1, unsigned long p2,  unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read)
{
    int i;
    int avenumber = memsize/(gridDim.x*gridDim.y);
    unsigned long* mybuf = (unsigned long*)(ptr + blockIdx.x* avenumber);
    int n = avenumber/(blockDim.x*sizeof(unsigned long));
    unsigned long localp;

    for(i=0;i < n;i++){
        int index = i*blockDim.x + threadIdx.x;
        localp = mybuf[index];
        if (localp != p1){
            RECORD_ERR(err, &mybuf[index], p1, localp);
        }
        mybuf[index] = p2;
    }
    int index = n*blockDim.x + threadIdx.x;
    if (index*sizeof(unsigned long) < avenumber){
        localp = mybuf[index];
        if (localp!= p1){
            RECORD_ERR(err, &mybuf[index], p1, localp);
        }
        mybuf[index] = p2;
    }

    return;
}

#undef RECORD_ERR
