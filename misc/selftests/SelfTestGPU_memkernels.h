#ifndef SELFTEST_GPU_MEMKERNELS_H
#define SELFTEST_GPU_MEMKERNELS_H

#include <cuda.h>

__global__ void kernel_move_inv_write(char* _ptr, char* end_ptr, unsigned int pattern);
__global__ void kernel_move_inv_readwrite(char* _ptr, char* end_ptr, unsigned int p1, unsigned int p2, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test0_global_write(char* _ptr, char* _end_ptr);
__global__ void kernel_test0_global_read(char* _ptr, char* _end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test0_write(char* _ptr, char* end_ptr);
__global__ void kernel_test0_read(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test1_write(char* _ptr, char* end_ptr, unsigned int* err);
__global__ void kernel_test1_read(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test5_init(char* _ptr, char* end_ptr);
__global__ void kernel_test5_move(char* _ptr, char* end_ptr);
__global__ void kernel_test5_check(char* _ptr, char* end_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_movinv32_write(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset);
__global__ void kernel_movinv32_readwrite(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset, unsigned int * err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_movinv32_read(char* _ptr, char* end_ptr, unsigned int pattern, unsigned int lb, unsigned int sval, unsigned int offset, unsigned int * err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test7_write(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err);
__global__ void kernel_test7_readwrite(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_test7_read(char* _ptr, char* end_ptr, char* _start_ptr, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void kernel_modtest_write(char* _ptr, char* end_ptr, unsigned int offset, unsigned int p1, unsigned int p2);
__global__ void kernel_modtest_read(char* _ptr, char* end_ptr, unsigned int offset, unsigned int p1, unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);
__global__ void test10_kernel_write(char* ptr, int memsize, unsigned long p1);
__global__ void test10_kernel_readwrite(char* ptr, int memsize, unsigned long p1, unsigned long p2,  unsigned int* err, unsigned long* err_addr, unsigned long* err_expect, unsigned long* err_current, unsigned long* err_second_read);

#endif // SELFTEST_GPU_MEMKERNELS_H
