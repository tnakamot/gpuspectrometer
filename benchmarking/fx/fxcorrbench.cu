// Usage: fxcorrbench [gpu_id] [fx_nchan]

#include <cuda.h>
#include <cufft.h>

#define CHECK_TIMING 0
#include "cuda_utils.cu"
#include "arithmetic_fxcorr_kernels.cu"
#include "decoder_3b32f_kernels.cu"

#define N_NYQ   0  // 1 to keep Nyquist point, 0 to discard it; TODO: 0 introduces erratic issue in first bin of FFTs...
#define N_ANT   4
#define N_BASE  ((N_ANT*(N_ANT-1))/2)
#define N_POL   1
#define DEBUG_ARRAYS  0 // 1 to print out contents of various arrays

size_t nchans_to_test[] = { 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536 };

void usage(void)
{
    printf("Usage: fxcorrbench [gpu_id] [fx_nchan]\n");
}

void dbg_print_device_complexArr(void* d_data, size_t N)
{
    float2* h;
    CUDA_CALL( cudaHostAlloc((void **)&h, N*sizeof(float2), cudaHostAllocDefault) );
    CUDA_CALL( cudaMemcpy(h, d_data, N*sizeof(float2), cudaMemcpyDeviceToHost) );
    printf("dbg_print_device_complexArr(%p, %zu):\n", d_data, N);
    for (size_t n=0; n<N; n++) {
        printf(" %.3f%+.3fi ", h[n].x, h[n].y);
        if ((n+1) % 10 == 0) { printf("\n"); }
    }
    printf("\n");
    CUDA_CALL( cudaFreeHost(h) );
}

void dbg_print_device_realArr(void* d_data, size_t N)
{
    float* h;
    CUDA_CALL( cudaHostAlloc((void **)&h, N*sizeof(float), cudaHostAllocDefault) );
    CUDA_CALL( cudaMemcpy(h, d_data, N*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("dbg_print_device_complexArr(%p, %zu):\n", d_data, N);
    for (size_t n=0; n<N; n++) {
        printf(" %.3f ", h[n]);
        if ((n+1) % 10 == 0) { printf("\n"); }
    }
    printf("\n");
    CUDA_CALL( cudaFreeHost(h) );
}

int main(int argc, char** argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop, gstart, gstop;
    cufftHandle fft_r2c;

    size_t maxphysthreads;
    size_t nsamples, nbyte_raw1antpol;
    float *h_td_data;
    float *d_td_data[N_ANT*N_POL], *d_td_data_block;
    int device = 0;
    int nr_nchans_to_test = sizeof(nchans_to_test)/sizeof(nchans_to_test[0]);

    // Args
    if (argc > 1) {
        device = atoi(argv[1]);
    }
    if (argc > 2) {
        nchans_to_test[0] = atoi(argv[2]);
        nr_nchans_to_test = 1;
    }
    if ((argc > 3) || (device < 0) || (device > 8) || (nchans_to_test[0] < 32) || (argc > 1 && argv[1][0]=='-')) {
        usage();
        return -1;
    }

    // GPU setup
    CUDA_CALL( cudaSetDevice(device) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, device) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        device, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );
    CUDA_CALL( cudaEventCreate( &gstart ) );
    CUDA_CALL( cudaEventCreate( &gstop ) );

    // Input: single pol, 4 antennas, 4 Gs/s and 1 msec = 4000000 real-valued samples per antenna
    nsamples = 4e9 * 48e-3;
    nbyte_raw1antpol = nsamples*sizeof(float);

    printf("Total raw data size : %d antennas x %d pols x %zu samples, ~%.2f GByte in samples total\n", N_ANT, N_POL, nsamples, N_ANT*N_POL*nbyte_raw1antpol*1e-9);
    CUDA_CALL( cudaMalloc((void **)&d_td_data_block, N_ANT*N_POL*nbyte_raw1antpol) );
    for (int a = 0; a < N_ANT*N_POL; a++) {
        d_td_data[a] = &d_td_data_block[a*nsamples];
    }
    CUDA_CALL( cudaHostAlloc((void **)&h_td_data, nbyte_raw1antpol, cudaHostAllocDefault) );

    ///////////////////////////////////////////////////////////////////////////////////////////

    printf("  %7s %7s %10s %10s\n", "Nchan", "thrd/blk", "T [ms]", "R [Ms/s/ant/pol]");

    for (int test_idx=0; test_idx<nr_nchans_to_test; test_idx++) {

        size_t threadsPerBlock, numBlocks;
        float dt_msec;

        float2 *d_fd_data_block;
        float2 *d_vis;
        float2 *h_vis;

        // FFT Size
        const size_t nchan = nchans_to_test[test_idx];
        const size_t nfft = nsamples / (2*nchan);
        const size_t nsamp_eff = nfft*(2*nchan);       // nr of fft input values
        const size_t npoints_eff = nfft*(nchan+N_NYQ); // nr of fft output values

        // FFT Plan Setup
        int dimn[1] = {2*(int)nchan};
        int inembed[1] = {0};         // ignored for 1D xform
        int onembed[1] = {0};         // ignored for 1D xform
        int istride = 1, ostride = 1; // step between in(out) samples
        int idist = 2*nchan;          // in step between FFTs (R2C input = real)
        int odist = nchan + N_NYQ;    // out step between FFTs (x2C output = complex)
        int batch = nfft;
        ostride = N_ANT*N_POL;        // interleave output results of all antennas
        odist = (nchan+N_NYQ)*N_ANT*N_POL; // -"-
        CUFFT_CALL( cufftPlanMany(
                &fft_r2c,
                1, dimn,
                inembed, istride, idist,
                onembed, ostride, odist,
                CUFFT_R2C, batch)
        );
        //printf("cufftPlanMany(plan, 1, [%d], [],%d,%d, [],%d,%d, R2C, %d)\n", dimn[0], istride,idist, ostride,odist, batch);

        // FFT Input: 32-bit real-valued data x N_ANT stations
        for (int a = 0; a < N_ANT*N_POL; a++) {
            //printf("Generating data for antenna %d pol %d...\n", a/N_POL, a%N_POL);
            for (size_t n = 0; n < nsamples; n++) {
                // Option 1: wideband spectra, noting that fft([1 0 0 0 ... 0]) = [1 1 1 .. 1]
                //h_td_data[n] = ((n % (2*nchan)) == 0) ? (1.0f+a) : 0.0f;                         // identical data over time
                h_td_data[n] = ((n % (2*nchan)) == 0) ? (0.0f+a) : 0.0f;                         // identical data over time
                //h_td_data[n] = ((n % (2*nchan)) == 0) ? (1.0f+a+10.0f*int(n/(2*nchan))) : 0.0f;  // different data in each 2chan time slice
                // Option 2: sparse spectra, counter pattern in time domain
                //h_td_data[n] = (n + 0) % (2*nchan); // no lag
                //h_td_data[n] = (n + a) % (2*nchan); // per-antenna fixed lag
            }
            CUDA_CALL( cudaMemcpy(d_td_data[a], h_td_data, nsamples*sizeof(float), cudaMemcpyHostToDevice) );
        }
#if DEBUG_ARRAYS
        for (size_t off = 0; off < nfft*2*nchan; ) {
            for (int a=0; a<N_ANT; a++) {
                printf("at offset of %zu real into FFT input array %d:\n", off, a);
                dbg_print_device_realArr((void*)(d_td_data[a]+off), 8); // 8 samples from somwewhere in the array
            }
            if (getchar() == 'x') { break; }
            off += 2*nchan;
        }
#endif

        // FFT Output: 32-bit complex-valued data x N_ANT stations
        const size_t nbyte_fft = N_ANT*N_POL*nfft*(nchan+1)*sizeof(float2);
        CUDA_CALL( cudaMalloc((void **)&d_fd_data_block, nbyte_fft) );
        CUDA_CALL( cudaMemset(d_fd_data_block, 0x00, nbyte_fft) );

        // Output: 4 antennas -> 6 baselines with Nchan channels, extra space in case we do auto-corrs cross-pols
        const size_t nbyte_vis = sizeof(float2) * (nchan + N_NYQ) * (N_BASE + N_ANT) * (N_POL + 1);
        CUDA_CALL( cudaHostAlloc( (void **)&h_vis, nbyte_vis, cudaHostAllocDefault ) );
        CUDA_CALL( cudaMalloc( (void **)&d_vis, nbyte_vis) );
        CUDA_CALL( cudaMemset(d_vis, 0x00, nbyte_vis) );

        // Layout: cross-multiply accu CUDA grid layout
        prep_cu_fxcorr_4ant_singlepol(numBlocks, threadsPerBlock, maxphysthreads, nchan+N_NYQ);

        // Start benchmarking
        CUDA_CALL( cudaEventRecord(gstart) );

        // Simulate 3-bit int to 32-bit float unpack (if enabled, overwrites the test data!!)
        if (0) {
            size_t nrawbytes = (nsamp_eff*3)/8;
            size_t nr24bitwords = (nsamp_eff*3)/24;
            size_t M = cudaDevProp.warpSize;
            size_t N = div2ceil(nr24bitwords,M);
            //printf(" 3-bit : nsamp=%zu nbyte=%zu n24b=%zu grid %zux%zu\n", nsamp_eff, nrawbytes, nr24bitwords, N,M);
            for (int a=0; a<N_ANT; a++) {
                cu_decode_3bit1ch <<<N,M>>> ((const uint8_t*)d_td_data[(a+1)%N_ANT], (float4*)d_td_data[a], nrawbytes);
            }
        }

        // FFT step : 'N_ANT' of FFTs each with 'nsamp_eff' inputs that produce 'npoints_eff' outputs
        CUDA_TIMING_START(tstart, 0);
        for (int a=0; a<N_ANT; a++) {
            // Strided FFT: input single-pol(!) sample data block from one antenna, output to N_ANT-spaced common results array (interleaved)
            CUFFT_CALL( cufftExecR2C(fft_r2c, (cufftReal*)d_td_data[a], ((cufftComplex*)d_fd_data_block) + a) );
        }
        CUDA_TIMING_STOP(tstop, tstart, 0, "F-step", nsamples);

        // Cross-multiply step
        CUDA_TIMING_START(tstart, 0);
        cu_fxcorr_4ant_singlepol_withauto <<<numBlocks, threadsPerBlock>>> ( d_fd_data_block, d_vis, npoints_eff, nchan+N_NYQ );
        CUDA_TIMING_STOP(tstop, tstart, 0, "X-step", npoints_eff);

        // Performance report
        CUDA_CALL( cudaEventRecord(gstop) );
        CUDA_CALL( cudaEventSynchronize(gstop) );
        CUDA_CALL( cudaEventElapsedTime(&dt_msec, gstart, gstop) );
        printf("  %7zu %7zu %10.3f %10.3f\n", nchan+N_NYQ, threadsPerBlock, dt_msec, 1e-6*nsamp_eff/(1e-3*dt_msec));

#if DEBUG_ARRAYS
        for (size_t off = 0; off < nfft*N_ANT*(nchan+N_NYQ); ) {
            printf("at offset of %zu complex into FFT output array:\n", off);
            dbg_print_device_complexArr((void*)(d_fd_data_block+off), 8*N_ANT); // 8 channels somewhere in between
            if (getchar() == 'x') { break; }
            off += (nchan+N_NYQ)*N_ANT;
        }
#endif

#if DEBUG_ARRAYS
        printf("final cross-corr array:\n");
        dbg_print_device_complexArr((void*)d_vis, (nchan + N_NYQ) * (N_BASE + N_ANT) * N_POL + 8);
        //dbg_print_device_complexArr((void*)d_vis, (nchan + N_NYQ) * (N_BASE) * N_POL + 8);
#endif
        CUDA_CALL( cudaFree(d_fd_data_block) );
        CUDA_CALL( cudaFree(d_vis) );
        CUDA_CALL( cudaFreeHost(h_vis) );
        CUFFT_CALL( cufftDestroy(fft_r2c) );

    } /* for nchan in nchans_to_test[...] */

    return 0;
}
