
Below is a report of memory write bandwidths achieved when writing half, half2, 
float, float2, and float4 types sequentially from CUDA kernels compiled for 
Compute Capability 3.5. Results on Titan X were identical between CC 3.5 and CC 5.2.

Results are in the form of   

  <#values written per thread> * <data type used> : execution time (sec) : total rate (GB/sec)

CUDA Device #1 : GeForce GTX TITAN X, Compute Capability 5.2, 1024 threads/block, warpsize 32

GPU will write 536870912 values (1073.74 MByte of FP16, or 2147.48 MByte of FP32)

  Kernel     :   Blocks,Threads  : Time      : Throughput
  1x_half    : <<<2097152,128>>> : 0.00466 s : 115.172 GB/s
  2x_half    : <<< 524288,256>>> : 0.00236 s : 227.852 GB/s
  2x_half    : <<<1048576,128>>> : 0.00220 s : 243.908 GB/s
  2x_half    : <<<2097152, 64>>> : 0.00440 s : 122.085 GB/s
  2x_half    : <<<2097152, 64>>> : 0.00466 s : 115.113 GB/s
  8x_half    : <<< 262144,128>>> : 0.01328 s :  40.437 GB/s
  8x_half    : <<< 524288, 64>>> : 0.01207 s :  44.474 GB/s
  8x_half    : <<<1048576, 32>>> : 0.01182 s :  45.439 GB/s
  8x_half    : <<<2097152, 16>>> : 0.01051 s :  51.083 GB/s
  1x_half2   : <<<1048576,128>>> : 0.00194 s : 276.205 GB/s
  1x_half2   : <<<2097152, 64>>> : 0.00389 s : 138.048 GB/s
  2x_half2   : <<< 524288,128>>> : 0.00210 s : 256.258 GB/s
  2x_half2   : <<<2097152, 32>>> : 0.00389 s : 138.120 GB/s
  4x_half2   : <<< 262144,128>>> : 0.00467 s : 115.020 GB/s
  4x_half2   : <<< 524288, 64>>> : 0.00474 s : 113.153 GB/s
  4x_half2   : <<<2097152, 16>>> : 0.00419 s : 128.266 GB/s
  1x_float   : <<<2097152,128>>> : 0.00391 s : 274.609 GB/s
  4x_float   : <<< 524288,128>>> : 0.00932 s : 115.257 GB/s
  4x_float   : <<<2097152, 32>>> : 0.00946 s : 113.518 GB/s
  8x_float   : <<< 262144,128>>> : 0.01596 s :  67.293 GB/s
  1x_float2  : <<<1048576,128>>> : 0.00375 s : 286.677 GB/s
  2x_float2  : <<< 524288,128>>> : 0.00392 s : 273.820 GB/s
  4x_float2  : <<< 262144,128>>> : 0.00818 s : 131.257 GB/s
  1x_float4  : <<< 524288,128>>> : 0.00378 s : 284.217 GB/s
  1x_float4  : <<<2097152, 32>>> : 0.00388 s : 276.519 GB/s
  2x_float4  : <<< 262144,128>>> : 0.00408 s : 263.168 GB/s
  2x_float4  : <<<1048576, 32>>> : 0.00421 s : 255.145 GB/s
  2x_float4  : <<<2097152, 16>>> : 0.00388 s : 276.428 GB/s

  Titan X   : 336.0 GB/s theoretical bandwidth

CUDA Device #2 : Tesla K40m, Compute Capability 3.5, 1024 threads/block, warpsize 32

GPU will write 536870912 values (1073.74 MByte of FP16, or 2147.48 MByte of FP32)

  Kernel     :   Blocks,Threads  : Time      : Throughput
  1x_half    : <<<2097152,128>>> : 0.00626 s :  85.720 GB/s
  2x_half    : <<< 524288,256>>> : 0.00344 s : 156.054 GB/s
  2x_half    : <<<1048576,128>>> : 0.00341 s : 157.547 GB/s
  2x_half    : <<<2097152, 64>>> : 0.00626 s :  85.720 GB/s
  8x_half    : <<< 262144,128>>> : 0.01606 s :  33.432 GB/s
  8x_half    : <<< 524288, 64>>> : 0.01593 s :  33.705 GB/s
  8x_half    : <<<1048576, 32>>> : 0.01594 s :  33.675 GB/s
  8x_half    : <<<2097152, 16>>> : 0.01597 s :  33.617 GB/s
  1x_half2   : <<<1048576,128>>> : 0.00313 s : 171.280 GB/s
  1x_half2   : <<<2097152, 64>>> : 0.00626 s :  85.721 GB/s
  2x_half2   : <<< 524288,128>>> : 0.00353 s : 152.080 GB/s
  2x_half2   : <<<2097152, 32>>> : 0.00626 s :  85.718 GB/s
  4x_half2   : <<< 262144,128>>> : 0.00712 s :  75.454 GB/s
  4x_half2   : <<< 524288, 64>>> : 0.00706 s :  76.091 GB/s
  4x_half2   : <<<2097152, 16>>> : 0.00702 s :  76.429 GB/s
  1x_float   : <<<2097152,128>>> : 0.00626 s : 171.438 GB/s
  4x_float   : <<< 524288,128>>> : 0.01422 s :  75.488 GB/s
  4x_float   : <<<2097152, 32>>> : 0.01406 s :  76.377 GB/s
  8x_float   : <<< 262144,128>>> : 0.02965 s :  36.212 GB/s
  1x_float2  : <<<1048576,128>>> : 0.00532 s : 201.865 GB/s
  2x_float2  : <<< 524288,128>>> : 0.00806 s : 133.225 GB/s
  4x_float2  : <<< 262144,128>>> : 0.01799 s :  59.677 GB/s
  1x_float4  : <<< 524288,128>>> : 0.00532 s : 201.726 GB/s
  1x_float4  : <<<2097152, 32>>> : 0.00626 s : 171.447 GB/s
  2x_float4  : <<< 262144,128>>> : 0.01185 s :  90.574 GB/s
  2x_float4  : <<<1048576, 32>>> : 0.00705 s : 152.241 GB/s
  2x_float4  : <<<2097152, 16>>> : 0.00699 s : 153.579 GB/s

  Tesla K40 : 240.0 GB/s theoretical bandwidth

An explanation as to why the write throughput (GB/s) increases when 
wider data types are used:

http://stackoverflow.com/questions/26676806/efficiency-of-cuda-vector-types-float2-float3-float4

"The GPU hardware provides load instructions for 32-bit, 64-bit and 128-bit data, which maps 
to the float, float2, and float4 data types (as well as to the int, int2, and int4 types). The 
data must be naturally aligned for the load instructions to work correctly and in general wider 
loads provide higher peak memory bandwidth. So float4 is preferred over float3 for performance 
reasons."

On Titan X the 'half2' (32-bit) is a bit slower than 'float' (32-bit), not sure why.
However, on K40m the 'half2' and 'float' writes are equally fast, as expected.
