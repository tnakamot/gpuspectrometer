// A test of CUDA 7.x CuFFT with Callbacks to convert FFT input Half2 data into Float2
//
// http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH____HALF2__ARITHMETIC.html#group__CUDA__MATH____HALF2__ARITHMETIC
// http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH____HALF__MISC.html#group__CUDA__MATH____HALF__MISC

#include <stdio.h>
#include <math.h>

#include <cuda.h>
#include <cuda_fp16.h>
#include <cufft.h>
#include <cufftXt.h>

#define MAX_LFFT   (1ULL*1024*1024)
#define MAX_BATCH  128ULL
#define NITER      10  // iterations to use for determining average execution time
#ifndef CUDA_DEVICE_NR
#define CUDA_DEVICE_NR 0
#endif

float mean(float* s, int n)
{
    float m = 0.0f;
    for (int i=0; i<n; i++) {
        m += s[i];
    }
    return m/float(n);
}

float stddev(float* s, int n)
{
    float m = mean(s, n);
    float v = 0.0f;
    for (int i=0; i<n; i++) {
        v += (s[i]-m)*(s[i]-m);
    }
    return sqrtf(v/float(n));
}


#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: error %d\n",
                               pcFile, iLine, iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ cufftComplex cu_loadC(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
    // cufftComplex c = {(float)offset,(float)offset}; return c;
    half2 h = ((half2*)dataIn)[offset];
    float2 f = __half22float2(h);
    return (cufftComplex)f;
}

__device__ void cu_storeC(void *dataOut, size_t offset, cufftComplex element, void *callerInfo, void *sharedPointer)
{
    // ((cufftComplex*) dataOut)[offset] = element; return;
    half2 h = __floats2half2_rn(element.x,element.y);
    ((half2*)dataOut)[offset] = h;
}

__device__ cufftCallbackLoadC cu_loadC_ptr = cu_loadC;
__device__ cufftCallbackStoreC cu_storeC_ptr = cu_storeC;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;

    float *d_idata;
    float *h_idata;
    cufftComplex *d_odata;
    cufftHandle fftplan_r2c;
    cufftHandle fftplan_c2c_fp16;
    cufftHandle fftplan_r2cXt_fp16;

    cudaEvent_t tstart, tstop;
    float dt[NITER], dt_msec, dt_sigma, R_gsps;

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize
    );

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    CUDA_CALL( cudaHostAlloc( (void **)&h_idata, sizeof(cufftComplex)*MAX_LFFT*MAX_BATCH, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_idata, sizeof(cufftComplex)*MAX_LFFT*MAX_BATCH ) );
    CUDA_CALL( cudaMalloc( (void **)&d_odata, sizeof(cufftComplex)*MAX_LFFT*MAX_BATCH ) );
    for (size_t n=0; n<MAX_LFFT*MAX_BATCH*sizeof(cufftComplex)/sizeof(float); n++) {
        h_idata[n] = n % 1234;
    }

    printf("    Nfloat2    Lbatch     T_r2c[ms] R[Gs/s]\n");

    for (size_t fftlen=1024; fftlen <= MAX_LFFT; fftlen *= 2) {

        size_t batch = (MAX_LFFT*MAX_BATCH)/fftlen;

        // Run a normal R2C FFT
        int dimn[1] = {fftlen};         // DFT size
        int inembed[1] = {0};           // ignored for 1D xform
        int onembed[1] = {0};           // ignored for 1D xform
        int istride = 1, ostride = 1;   // step between successive in(out) elements
        int idist = fftlen;             // step between batches (R2C input)
        int odist = fftlen/2+1;         // step between batches (R2C output)
        CUFFT_CALL( cufftPlanMany(&fftplan_r2c, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C,
            batch)
        );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftExecR2C(fftplan_r2c, d_idata, d_odata) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        printf("%10d %10d %6.3f+-%5.3f %4.1f : R2C fp32\n", fftlen, batch, dt_msec, dt_sigma, R_gsps);
        CUFFT_CALL( cufftDestroy(fftplan_r2c) );

#ifdef STATIC_CUFFT
        // Run a C2C FFT with callback on half2 data
        cufftCallbackLoadC h_cuCbLoad;
        cufftCallbackStoreC h_cuCbStore;
        CUFFT_CALL( cufftPlanMany(&fftplan_c2c_fp16, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_C2C,
            batch)
        );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbLoad, cu_loadC_ptr, sizeof(h_cuCbLoad)) );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbStore, cu_storeC_ptr, sizeof(h_cuCbStore)) );
        CUFFT_CALL( cufftXtSetCallback(fftplan_c2c_fp16, (void **)&h_cuCbLoad, CUFFT_CB_LD_COMPLEX, (void**)NULL) );
        CUFFT_CALL( cufftXtSetCallback(fftplan_c2c_fp16, (void **)&h_cuCbStore, CUFFT_CB_ST_COMPLEX, (void**)NULL) );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftExecC2C(fftplan_c2c_fp16, (cufftComplex*)d_idata, d_odata, CUFFT_FORWARD) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        printf("%10d %10d %6.3f+-%5.3f %4.1f : C2C f32 with callback for f16<->f32\n", fftlen, batch, dt_msec, dt_sigma, R_gsps);
        CUFFT_CALL( cufftDestroy(fftplan_c2c_fp16) );
#endif

#ifdef NATIVE_FP16
        // A CuFFT XT R2C plan with 16-bit data
        int gpusXt[2] = { 0,1 };
        cudaLibXtDesc *device_data_input;
        size_t worksizesXt[10];
        long long dimnXt[1] = { fftlen };
        CUFFT_CALL( cufftCreate(&fftplan_r2cXt_fp16) );
        //CUFFT_CALL( cufftXtSetGPUs(fftplan_r2cXt_fp16, 1, gpusXt) );
        //CUFFT_CALL( cufftXtMalloc(fftplan_r2cXt_fp16, &device_data_input, CUFFT_XT_FORMAT_INPUT) );
        CUFFT_CALL( cufftXtMakePlanMany(fftplan_r2cXt_fp16,
            1, dimnXt,
            NULL, 1, fftlen, /* inembed=NULL, ignored istride, ignored idist, */
            CUDA_R_16F,
            NULL, 1, fftlen/2+1, /* onembed=NULL, ignored ostride, ignored odist, */
            CUDA_C_16F,
            batch,
            worksizesXt,
            CUDA_C_16F)
        );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftXtExec(fftplan_r2cXt_fp16, d_idata, d_odata, CUFFT_FORWARD) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        printf("%10d %10d %6.3f+-%5.3f %4.1f : R2C fp16\n", fftlen, batch, dt_msec, dt_sigma, R_gsps);
        CUFFT_CALL( cufftDestroy(fftplan_r2cXt_fp16) );
#endif

    }

    return 0;
}
