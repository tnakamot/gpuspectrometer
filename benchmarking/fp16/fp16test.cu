//
// Compare 16-bit float (CUDA 7.5 'half' and 'half2') and 32-bit float
// data writing to memory.
//
// https://en.wikipedia.org/wiki/Half-precision_floating-point_format
// Integer 32769 should round to a multiple of 32 in float16, but
// be representable directly in float32.
//

#include <cuda.h>
#include <cuda_fp16.h>  // data types 'half' and 'half2' (no 'half4' exists...)
#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>

#define C_FLOAT_VAL 32769.0f

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_write_1x_half(half* dst)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = __float2half(C_FLOAT_VAL);
}

__global__ void cu_write_2x_half(half* dst)
{
    size_t idx = 2*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = __float2half(C_FLOAT_VAL);
    dst[idx+1] = __float2half(C_FLOAT_VAL);
}

__global__ void cu_write_8x_half(half* dst) // 8 * 16-bit = 128-bit cache line
{
    size_t idx = 8*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = __float2half(C_FLOAT_VAL);
    dst[idx+1] = __float2half(C_FLOAT_VAL);
    dst[idx+2] = __float2half(C_FLOAT_VAL);
    dst[idx+3] = __float2half(C_FLOAT_VAL);
    dst[idx+4] = __float2half(C_FLOAT_VAL);
    dst[idx+5] = __float2half(C_FLOAT_VAL);
    dst[idx+6] = __float2half(C_FLOAT_VAL);
    dst[idx+7] = __float2half(C_FLOAT_VAL);
    dst[idx+8] = __float2half(C_FLOAT_VAL);
}

__global__ void cu_write_1x_half2(half2* dst)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    // Note 1: float2 has .x and .y, both both half and half2 have only .x
    // Note 2: assignment float2 tmp={1.0f,2.0f} works, but the equivalent does not work for half2
    // Note 3: half2 createable with __float2half, __halves2half2, and similar, but not with
    //         half2 tmp = (half2) { __float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL) };
    dst[idx] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
}

__global__ void cu_write_2x_half2(half2* dst)
{
    size_t idx = 2*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
    dst[idx+1] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
}

__global__ void cu_write_4x_half2(half2* dst)
{
    size_t idx = 4*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
    dst[idx+1] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
    dst[idx+2] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
    dst[idx+3] = __halves2half2(__float2half(C_FLOAT_VAL), __float2half(C_FLOAT_VAL));
}

__global__ void cu_write_1x_float(float* dst)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = C_FLOAT_VAL;
}

__global__ void cu_write_4x_float(float* dst)
{
    size_t idx = 4*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = C_FLOAT_VAL;
    dst[idx+1] = C_FLOAT_VAL;
    dst[idx+2] = C_FLOAT_VAL;
    dst[idx+3] = C_FLOAT_VAL;
}

__global__ void cu_write_8x_float(float* dst)
{
    size_t idx = 8*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = C_FLOAT_VAL;
    dst[idx+1] = C_FLOAT_VAL;
    dst[idx+2] = C_FLOAT_VAL;
    dst[idx+3] = C_FLOAT_VAL;
    dst[idx+4] = C_FLOAT_VAL;
    dst[idx+5] = C_FLOAT_VAL;
    dst[idx+6] = C_FLOAT_VAL;
    dst[idx+7] = C_FLOAT_VAL;
}

__global__ void cu_write_1x_float2(float2* dst)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
}

__global__ void cu_write_2x_float2(float2* dst)
{
    size_t idx = 2*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
    dst[idx+1] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
}

__global__ void cu_write_4x_float2(float2* dst)
{
    size_t idx = 4*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
    dst[idx+1] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
    dst[idx+2] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
    dst[idx+3] = (float2) { C_FLOAT_VAL, C_FLOAT_VAL };
}

__global__ void cu_write_1x_float4(float4* dst)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = (float4) { C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL };
}

__global__ void cu_write_2x_float4(float4* dst)
{
    size_t idx = 2*(blockIdx.x * blockDim.x + threadIdx.x);
    dst[idx+0] = (float4) { C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL };
    dst[idx+1] = (float4) { C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL, C_FLOAT_VAL };
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)  // checkCudaErrors() from CUDA Examples helper_cuda.h
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_TIMING_START(event) \
   do { CUDA_CALL( cudaEventRecord(event, 0) ); } while(0)

#define CUDA_TIMING_STOP(stop,start,msg,Nblk,Nth,Nbytes,Nsamps) \
   do { \
        float dt_msec = -1.0f; \
        CUDA_CALL( cudaEventRecord(stop, 0) ); \
        CUDA_CALL( cudaEventSynchronize(stop) ); \
        CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
        fprintf(stderr, "  %-10s : <<<%7d,%3d>>> : %7.5f s : %7.3f GB/s : %7.3f GS/s\n", \
            msg, \
            (int)Nblk, (int)Nth, \
            1e-3*dt_msec, \
            1e-9*Nbytes/(1e-3*dt_msec), (1e-9*Nsamps)/(1e-3*dt_msec)); \
   } while(0)

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc, char** argv)
{
    cudaDeviceProp devProp;
    cudaEvent_t    tstart, tstop;

    half*      d_fp16;
    float*     d_fp32;
    half*      h_fp16;
    float*     h_fp32;
    uint32_t*  h_i32;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&devProp, CUDA_DEVICE_NR) );
    printf("\nCUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n\n",
        CUDA_DEVICE_NR, devProp.name, devProp.major, devProp.minor,
        devProp.maxThreadsPerBlock, devProp.warpSize
    );

    const size_t N = 2*1024*1024;
    const size_t M = devProp.warpSize*4;
    const size_t Nsamp = N*M;
    const size_t Nbyte_fp16 = Nsamp*sizeof(half);
    const size_t Nbyte_fp32 = Nsamp*sizeof(float);
    printf("GPU will write %zu values (%.2f MByte of FP16, or %.2f MByte of FP32)\n\n", Nsamp, Nbyte_fp16*1e-6, Nbyte_fp32*1e-6);

    CUDA_CALL( cudaEventCreate(&tstart) );
    CUDA_CALL( cudaEventCreate(&tstop) );

    CUDA_CALL( cudaMalloc((void**)&d_fp16, Nbyte_fp16) );
    CUDA_CALL( cudaMalloc((void**)&d_fp32, Nbyte_fp32) );
    CUDA_CALL( cudaHostAlloc((void**)&h_i32, Nbyte_fp32, cudaHostAllocDefault) );
    h_fp16 = (half*)h_i32;
    h_fp32 = (float*)h_i32;

    #define CHECK_OUTPUT() \
        for (size_t i=0; i<4; i++) { \
            printf("  32-bit word #%d contains: 0x%08X\n", i, h_i32[i]); \
        }
        // TODO: how to printf() the floating point value of a 'half' on the *host* side??


    /////// Header //////////////////////////////////////////////////////////////////////////
   
    printf("  Kernel     :   Blocks,Threads  : Time      : Throughput\n");

    /////// 16-bit floating point ///////////////////////////////////////////////////////////

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_half <<<N,M>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "1x_half", N,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_half <<<N/4,M*2>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "2x_half", N/4,M*2, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_half <<<N/2,M>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "2x_half", N/2,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_half <<<N,M/2>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "2x_half", N,M/2, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_8x_half <<<N/8,M>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "8x_half", N/8,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_8x_half <<<N/4,M/2>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "8x_half", N/4,M/2, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_8x_half <<<N/2,M/4>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "8x_half", N/2,M/4, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_8x_half <<<N,M/8>>> (d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "8x_half", N,M/8, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_half2 <<<N/2,M>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "1x_half2", N/2,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_half2 <<<N,M/2>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "1x_half2", N,M/2, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_half2 <<<N/4,M>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "2x_half2", N/4,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_half2 <<<N,M/4>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "2x_half2", N,M/4, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_half2 <<<N/8,M>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "4x_half2", N/8,M, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_half2 <<<N/4,M/2>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "4x_half2", N/4,M/2, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp16, 0x00, Nbyte_fp16) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_half2 <<<N,M/8>>> ((half2*)d_fp16);
    CUDA_TIMING_STOP(tstop, tstart, "4x_half2", N,M/8, Nbyte_fp16, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp16, d_fp16, Nbyte_fp16, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    /////// 32-bit floating point ///////////////////////////////////////////////////////////

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_float <<<N,M>>> (d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "1x_float", N,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_float <<<N/4,M>>> (d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "4x_float", N/4,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_float <<<N,M/4>>> (d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "4x_float", N,M/4, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_8x_float <<<N/8,M>>> (d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "8x_float", N/8,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_float2 <<<N/2,M>>> ((float2*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "1x_float2", N/2,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_float2 <<<N/4,M>>> ((float2*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "2x_float2", N/4,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_4x_float2 <<<N/8,M>>> ((float2*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "4x_float2", N/8,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    // ---- float4 ----

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_float4 <<<N/4,M>>> ((float4*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "1x_float4", N/4,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_1x_float4 <<<N,M/4>>> ((float4*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "1x_float4", N,M/4, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_float4 <<<N/8,M>>> ((float4*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "2x_float4", N/8,M, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_float4 <<<N/2,M/4>>> ((float4*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "2x_float4", N/2,M/4, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    CUDA_CALL( cudaMemset(d_fp32, 0x00, Nbyte_fp32) );
    CUDA_TIMING_START(tstart);
    cu_write_2x_float4 <<<N,M/8>>> ((float4*)d_fp32);
    CUDA_TIMING_STOP(tstop, tstart, "2x_float4", N,M/8, Nbyte_fp32, Nsamp);
    CUDA_CALL( cudaMemcpy(h_fp32, d_fp32, Nbyte_fp32, cudaMemcpyDeviceToHost) );
    // CHECK_OUTPUT();

    return 0;
}
