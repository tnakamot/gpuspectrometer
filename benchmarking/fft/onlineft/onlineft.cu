#define CHECK_TIMING 1 // 1 to activate CUDA_TIMING_START()/_STOP() timing, 0 otherwise
#define DUMP_DFT 0     // 1 to write first 4 DFT results (Re,Im on alternate lines) into files "onlineft_fft_%d_%d.dat"
#define DUMP_AP 1      // 1 to write the final time-averaged power spectrum into files "onlineft_apspec_%d_%d.dat"

#define MAX_INPUT_SIZE 2*524288  // maximum FFT length to use
//#define BATCH_SIZE   1000    // comment this out to auto-adjust the batch size for fixed amount of input data

#include "utils.cu"

#define m_min(x,y) ((x < y) ? x : y)

#include <stdio.h>
#include <math.h>
#include <cufft.h>

#ifndef BATCH_SIZE
    #define C_INPUT_BATCH_PRODUCT (5*MAX_INPUT_SIZE)
#else
    #define C_INPUT_BATCH_PRODUCT (BATCH_SIZE*MAX_INPUT_SIZE)
#endif

// Converted from Mark5B format into VDIF
//#define INFILE "/home/jwagner/rawdata/sascha/s14db02c_KVNYS_No0006.short.vdif"
//#define INFILE "/scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short"
#define INFILE "/opt/alma/share/synthetic_2bit.vdif"
#define FRAMESIZE   10032UL
#define PAYLOADSIZE 10000UL
#define SAMP_PER_BYTE 4
#define USE_VDIF_LUT  1

#define NUMFRAMES (16 + (C_INPUT_BATCH_PRODUCT/SAMP_PER_BYTE)/PAYLOADSIZE)

__global__ void convert2to32_v2(unsigned char *, float4 *, const size_t, const size_t);
__global__ void online_FFT_integrating(const float4 * __restrict__, float * __restrict__, const size_t, const size_t);

/** Write array into text file */
static void write_float32_to_file(const char* fname, float* d, size_t len)
{
    FILE* fout = fopen(fname, "wb");
    if (NULL == fout) {
        perror("Output file error");
        return;
    }

    for (size_t i = 0; i < len; i++) {
        fprintf(fout, "%zu %e\n", i, d[i]);
    }

    fclose(fout);
}

/** Write array into binary file */
static void write_float32_to_binary_file(const char* fname, float* d, size_t len)
{
    FILE* fout = fopen(fname, "wb");
    if (NULL == fout) {
        perror("Output file error");
        return;
    }
    fwrite((void*)d, len, 1, fout);
    fclose(fout);
}


/** Main */
int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    size_t maxphysthreads;

    cudaEvent_t globalstart, globalstop;
    cudaEvent_t substart, substop;
    float etime, globaltime;

    size_t numThreads;
    size_t numGrids;
    size_t OUTPUT_SIZE;
    size_t INPUT_BYTES;
    size_t INPUT_SAMPS;
#ifndef INPUT_SIZE
    size_t INPUT_SIZE;
#endif
#ifndef BATCH_SIZE
    size_t BATCH_SIZE;
#endif

    // size of memory for digitized samples
    unsigned char *h_samples;
    unsigned char *d_samples;
    float *d_idata;
    float *h_idata;

    float *h_autoPS;
    float *d_autoPS;

    int devNr = 0;
    if (argc == 2) {
        devNr = atoi(argv[1]);
    }

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(devNr) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, devNr) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        devNr, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    CUDA_CALL( cudaEventCreate( &globalstart ) );
    CUDA_CALL( cudaEventCreate( &globalstop ) );
    CUDA_CALL( cudaEventCreate( &substart ) );
    CUDA_CALL( cudaEventCreate( &substop ) );

    // Read one-second data from a file in Mark5B format, strip the 5B headers
    unsigned char *signals;
    unsigned char *header;
    CUDA_CALL( cudaHostAlloc( (void **)&signals,sizeof(char)*PAYLOADSIZE*NUMFRAMES,cudaHostAllocDefault ) );
    header = (unsigned char*)malloc(sizeof(char)*(FRAMESIZE-PAYLOADSIZE));
    FILE* fin = fopen(INFILE, "rb");
    if (fin == NULL) {
            perror("Input file error");
            exit(-1);
    }
    fseek(fin, 0, SEEK_SET);
    for (size_t k = 0; k < NUMFRAMES; k++) {
        fread(header, FRAMESIZE-PAYLOADSIZE, 1, fin);
        fread(signals + k*PAYLOADSIZE, PAYLOADSIZE, 1, fin);
    }
    fclose(fin);


    for (INPUT_SIZE=1024; INPUT_SIZE <= MAX_INPUT_SIZE; INPUT_SIZE += INPUT_SIZE) {
//    if (1) {
//    INPUT_SIZE=32768;

#ifndef BATCH_SIZE
    // if BATCH_SIZE is not hard-coded, we aim to keep the amount of used memory constant
    BATCH_SIZE = C_INPUT_BATCH_PRODUCT / INPUT_SIZE;
#endif

    INPUT_SAMPS = INPUT_SIZE*BATCH_SIZE;
    INPUT_BYTES = (INPUT_SIZE*BATCH_SIZE)/SAMP_PER_BYTE;
    OUTPUT_SIZE = INPUT_SIZE/2;

    // Define memory space for the input data
    CUDA_CALL( cudaHostAlloc( (void **)&h_samples,INPUT_BYTES,cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_samples,INPUT_BYTES) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_idata,sizeof(float)*INPUT_SAMPS,cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_idata,sizeof(float)*INPUT_SAMPS ) );

    // Define memory space for the output data and auto power spectrum
    CUDA_CALL( cudaHostAlloc( (void **)&h_autoPS,sizeof(float)*OUTPUT_SIZE,cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_autoPS, sizeof(float)*OUTPUT_SIZE ) );

    // Initialize memory
    CUDA_CALL( cudaMemset( d_samples, 0x00, INPUT_BYTES ) );
    CUDA_CALL( cudaMemset( d_idata,   0x00, sizeof(float)*INPUT_SAMPS ) );
    CUDA_CALL( cudaMemset( d_autoPS,  0x00, sizeof(float)*OUTPUT_SIZE ) );

    memcpy(h_samples, signals, INPUT_BYTES);

    CUDA_CALL( cudaEventRecord(globalstart, 0) );

    // copy from host to device
    CUDA_TIMING_START(substart, 0);
    CUDA_CALL( cudaMemcpy(d_samples, h_samples, INPUT_BYTES, cudaMemcpyHostToDevice) );
    CUDA_TIMING_STOP(substop, substart, etime, 0, "CPU-->GPU", INPUT_SAMPS);

    // Step 1 -- convert 2-bit samples to 32-bit float
    if (1) {
      //numThreads = cudaDevProp.maxThreadsPerBlock;
      numThreads = cudaDevProp.warpSize;
      numGrids = div2ceil(INPUT_SIZE, numThreads);
      CUDA_TIMING_START(substart, 0);
      convert2to32_v2 <<< numGrids,numThreads >>> (d_samples, (float4 *)d_idata, INPUT_SIZE, BATCH_SIZE); /* 24 Gs/s */
      CUDA_CHECK_ERRORS("convert2to32_v2");
      CUDA_TIMING_STOP(substop, substart, etime, 0, "convert2to32_v2", INPUT_SAMPS);
    }

    // Step 2 -- calculate integrated online Fourier transform
    printf("Running online FFT of %zu points for %zu samples\n", (size_t)OUTPUT_SIZE, (size_t)INPUT_SAMPS);
    CUDA_TIMING_START(substart, 0);
    numThreads = cudaDevProp.maxThreadsPerBlock;
    // numThreads = cudaDevProp.warpSize;
    numGrids = div2ceil(OUTPUT_SIZE,numThreads);
    online_FFT_integrating <<< numGrids,numThreads >>> ((float4 *)d_idata, (float *)d_autoPS, INPUT_SIZE, INPUT_SAMPS);
    CUDA_CHECK_ERRORS("online_FFT_integrating");
    CUDA_TIMING_STOP(substop, substart, etime, 0, "onlineFFT", INPUT_SAMPS);

    // done!

    CUDA_CALL( cudaEventRecord(globalstop, 0) );
    CUDA_CALL( cudaEventSynchronize(globalstop) );
    CUDA_CALL( cudaEventElapsedTime( &globaltime, globalstart, globalstop ) );

    float rate_total = 1e-9*(INPUT_SAMPS)/(globaltime*1e-3);
    printf("%10d %6d %10.5f %10.3f\n",INPUT_SIZE,BATCH_SIZE,globaltime*1e-3,rate_total); // samples | seconds | Gs/s

    CUDA_TIMING_START(substart, 0);
    CUDA_CALL( cudaMemcpy(h_autoPS, d_autoPS, sizeof(float)*OUTPUT_SIZE, cudaMemcpyDeviceToHost) );
    CUDA_TIMING_STOP(substop, substart, etime, 0, "GPU-->Host", OUTPUT_SIZE);

    if (DUMP_AP) {
        char filename[1024];
        sprintf(filename, "onlineft_apspec_%d_%d.dat", INPUT_SIZE, BATCH_SIZE);
        write_float32_to_file(filename, h_autoPS, OUTPUT_SIZE);
    }

    CUDA_CALL( cudaFree(d_samples) );
    CUDA_CALL( cudaFree(d_idata) ); 
    CUDA_CALL( cudaFree(d_autoPS) ); 

    CUDA_CALL( cudaFreeHost(h_samples) );
    CUDA_CALL( cudaFreeHost(h_idata) ); 
    CUDA_CALL( cudaFreeHost(h_autoPS) );

    } // N-loop
}

__global__ void convert2to32_v2(unsigned char *a, float4 *b, const size_t input_size, const size_t batch_size)
{
    const size_t numThreads = blockDim.x * gridDim.x;
    const size_t threadID = blockIdx.x * blockDim.x + threadIdx.x;

#if USE_VDIF_LUT
    const float lut2bit[4] = {-3.3359, -1.0, +1.0, +3.3359}; // VDIF lookup
#else
    const float lut2bit[4] = {-3.3359, +1.0, -1.0, +3.3359}; // Mark5B lookup
#endif

    for (size_t i = threadID; i < (input_size*batch_size)/SAMP_PER_BYTE; i += numThreads)
    {
        unsigned char v = a[i];
        float4 tmp = {
            lut2bit[ v       & 3],
            lut2bit[(v >> 2) & 3],
            lut2bit[(v >> 4) & 3],
            lut2bit[(v >> 6) & 3]
        };
        b[i] = tmp;
    }
}


__global__ void online_FFT_integrating(const float4 * __restrict__ in, float * __restrict__ out, const size_t fft_len, const size_t Nsamps)
{
    const size_t bin = blockIdx.x * blockDim.x + threadIdx.x; // FFT bin 0..(input_size/2)-1, plus possible padding

    /*__shared__*/ float4 sh_samp;

    // Sine-cosine oscillator:
    //   [c_N+1] = [ +cos(phi)  -sin(phi) ] * [c_N]
    //   [s_N+1]   [ +sin(phi)  +cos(phi) ]   [s_N]
    // where c_N and s_N are the current sine and cosine,
    // and phi is the phase step.

    // w[bin,n] = -2i*pi*bin*(n=0...Lfft-1)/Lfft so phase step is 2*pi*bin*1/Lfft
    float phi = (2*M_PI*bin)/fft_len;
    float cos_phi = cosf(phi);
    float sin_phi = sinf(phi);

    float cN = 1.0f, cN1;
    float sN = 0.0, sN1;

    #define OSC_NEXT()  do { \
        cN1 = cN*cos_phi - sN*sin_phi; \
        sN1 = cN*sin_phi + sN*cos_phi; \
        cN = cN1; \
        sN = sN1; } while (0)

    float accu = 0.0f;
    float accu_re = 0.0f;
    float accu_im = 0.0;

    for (size_t i = 0; i < Nsamps/4; i++) {

        sh_samp = in[i];

        accu_re += sh_samp.x * cN;
        accu_im += sh_samp.x * sN;
        OSC_NEXT();

        accu_re += sh_samp.y * cN;
        accu_im += sh_samp.y * sN;
        OSC_NEXT();

        accu_re += sh_samp.z * cN;
        accu_im += sh_samp.z * sN;
        OSC_NEXT();

        accu_re += sh_samp.w * cN;
        accu_im += sh_samp.w * sN;
        OSC_NEXT();

    }

    #undef OSC_NEXT

    accu = (accu_re*accu_re + accu_im*accu_im) / Nsamps;

    if (bin < fft_len/2) {
        out[bin] = accu;
    }
}
