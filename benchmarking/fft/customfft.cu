//
// Simple benchmarking of CuFFT against a custom FFT
//
// Example GPU FFT:
// http://liu.diva-portal.org/smash/get/diva2:617254/FULLTEXT01.pdf
// http://research.microsoft.com/pubs/131400/FftGpuSC08.pdf -- only pseudocode, no GPU code online
//    -"- has somebody elses CPU code at https://github.com/halide/Halide/blob/master/apps/fft/fft.cpp
// http://www.is.titech.ac.jp/research/research-report/C/C-268.pdf  radix-2
// http://www.bealto.com/gpu-fft.html  OpenCL
// http://arxiv.org/pdf/1412.7580v3.pdf --> uses Facebook 'fbcuda' and 'fbfft'
//    https://github.com/facebook/fbcuda/tree/master/fbfft
// ...
//
// Hardware half-precision FFT
// http://www.sjalander.com/research/pdf/sjalander-ssocc2006.pdf

#define CHECK_TIMING 1
#include "cuda_utils.cu"

#include <cuda.h>
#include <cuda_fp16.h>

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <cufft.h>

#include <thrust/complex.h>

#define FFT_LENGTH 2048ULL
#define BATCH_SIZE 1000ULL
#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif

void fft_8pt_dif_r2c(const float* x, thrust::complex<float>* y, thrust::complex<float> Wn)
{
    // http://www.sjalander.com/research/pdf/sjalander-ssocc2006.pdf
    // Wn = exp(-1j*2pi/N)
// TODO: values should be complex!
    thrust::complex<float> stage_1[8];
    thrust::complex<float> stage_2[8];
    thrust::complex<float> Wn2 = Wn*Wn;
    thrust::complex<float> Wn3 = Wn2*Wn;

    stage_1[0] = x[0] + x[4];
    stage_1[1] = x[1] + x[5];
    stage_1[2] = x[2] + x[6];
    stage_1[3] = x[3] + x[7];
    stage_1[4] = x[0] - x[4];
    stage_1[5] = x[1] - x[5]*Wn;
    stage_1[6] = x[2] - x[6]*Wn2;
    stage_1[7] = x[3] - x[7]*Wn3;

    stage_2[0] = stage_1[2] + stage_1[0];
    stage_2[1] = stage_1[3] + stage_1[1];
    stage_2[2] = stage_1[0] - stage_1[2];
    stage_2[3] = stage_1[1] - stage_1[3]*Wn2;

    stage_2[4] = stage_1[6] + stage_1[4];
    stage_2[5] = stage_1[7] + stage_1[5];
    stage_2[6] = stage_1[4] - stage_1[6];
    stage_2[7] = stage_1[5] - stage_1[7]*Wn2;

    y[0] = stage_2[1] + stage_2[0];
    y[4] = stage_2[0] - stage_2[1];

    y[2] = stage_2[3] + stage_2[2];
    y[6] = stage_2[2] - stage_2[3];

    y[1] = stage_2[5] + stage_2[4];
    y[5] = stage_2[4] - stage_2[5];

    y[3] = stage_2[7] + stage_2[6];
    y[7] = stage_2[6] - stage_2[7];
}

int main(void)
{
    cudaDeviceProp cudaDevProp;

    cudaEvent_t tstart, tstop;
    float dt_msec;

    cufftHandle fftplan;
    float *d_idata, *h_idata;
    cufftComplex *d_odata, *h_odata;
    size_t OUTPUT_SIZE, OUTPUT_SIZE_MAX;

    float *d_twiddle32, *h_twiddle32;
    half2 *d_twiddle16, *h_twiddle16;

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize
    );

    // Make CuFFT batched plan
    if (0) {
        OUTPUT_SIZE = FFT_LENGTH/2+0;   // Discard Nyquist
    } else {
        OUTPUT_SIZE = FFT_LENGTH/2+1;   // Keep Nyquist
    }
    OUTPUT_SIZE_MAX = FFT_LENGTH/2+1;
    if (1) {
        int dimn[1] = {FFT_LENGTH};     // DFT size
        int inembed[1] = {0};           // ignored for 1D xform
        int onembed[1] = {0};           // ignored for 1D xform
        int istride = 1, ostride = 1;   // step between successive in(out) elements
        int idist = FFT_LENGTH;         // step between batches (R2C input = real)
        int odist = OUTPUT_SIZE;        // step between batches (R2C output = 1st Nyquist only; let overwrite N/2+1 point!)
        CUFFT_CALL( cufftPlanMany(&fftplan, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C,
            BATCH_SIZE)
        );
        #if (CUDA_VERSION < 8000)
        CUFFT_CALL( cufftSetCompatibilityMode(fftplan, CUFFT_COMPATIBILITY_NATIVE) );
        #endif
    }

    // Define memory space for the FFT input and output data
    size_t in_bytes = sizeof(float)*FFT_LENGTH*BATCH_SIZE;
    size_t out_bytes = sizeof(cufftComplex)*OUTPUT_SIZE_MAX*BATCH_SIZE;
    CUDA_CALL( cudaMalloc( (void **)&d_idata, in_bytes  ) );
    CUDA_CALL( cudaMalloc( (void **)&d_odata, out_bytes ) );
    CUDA_CALL( cudaMalloc( (void **)&d_twiddle32, FFT_LENGTH*FFT_LENGTH*sizeof(cufftComplex) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_twiddle16, FFT_LENGTH*FFT_LENGTH*sizeof(half2) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_idata, in_bytes,  cudaHostAllocPortable ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_odata, out_bytes, cudaHostAllocPortable ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_twiddle32,  FFT_LENGTH*FFT_LENGTH*sizeof(cufftComplex), cudaHostAllocPortable ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_twiddle16,  FFT_LENGTH*FFT_LENGTH*sizeof(half2), cudaHostAllocPortable ) );
    printf("FFT setup   : %zu x %zu-point\n", (size_t)BATCH_SIZE, (size_t)FFT_LENGTH);
    printf("Twiddle size: %.1f Mbyte\n", FFT_LENGTH*FFT_LENGTH*sizeof(cufftComplex)/(1024.0f*1024.0f));
    printf("Input size  : %.1f Mbyte\n", in_bytes/(1024.0f*1024.0f));
    printf("Output siz e: %.1f Mbyte\n", out_bytes/(1024.0f*1024.0f));


    // Generate samples
    for (size_t n=0; n<FFT_LENGTH*BATCH_SIZE; n++) {
        //size_t t = (n % FFT_LENGTH);
        h_idata[n] = sinf(2*M_PI*n*0.012345f);
    }

    // CUDA reference
    CUDA_CALL( cudaMemcpy(d_idata, h_idata, in_bytes, cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaEventRecord(tstart) );
    CUFFT_CALL( cufftExecR2C(fftplan, (cufftReal*)d_idata, d_odata) );
    CUDA_CALL( cudaEventRecord(tstop) );
    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dt_msec, tstart, tstop) );
    printf("  %-18s : %7.5f s : %7.3f Ms/s\n", "cufft r2c", 1e-3*dt_msec, 1e-3*BATCH_SIZE*FFT_LENGTH/dt_msec);
    CUDA_CALL( cudaMemcpy(h_odata, d_odata, out_bytes, cudaMemcpyDeviceToHost) );

    float x[8] = { 0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f };
    thrust::complex<float> y[8];
    fft_8pt_dif_r2c(x, y, thrust::complex<float>(0.707106781186548f,-0.707106781186548f));
    for (size_t n=0; n<8; n++) {
        printf("%zu : %.3f : %.3f %+.3f\n", n, x[n], y[n].real(), y[n].imag());
    }

    return 0;
}
