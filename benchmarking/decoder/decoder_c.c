/////////////////////////////////////////////////////////////////////////////////////
// A set of CPU-based decoders for 2-bit 1-channel sample data.
//
// Assumes the headers of VDIF or Mark5B framing have already been removed
// and any missing frames have been replaced with some fill pattern to maintain
// time continuity of the sample stream.
//
// The main functions are
//
// decode_2bit_1channel_cpu_init   : prepares a parallel decoding task
// decode_2bit_1channel_cpu_submit : starts parallel execution of decoding subtasks
// decode_2bit_1channel_cpu_join   : blocks until all subtasks have completed
//
// decode_2bit_1channel_cpu_blocking : non-threaded decoding
//
/////////////////////////////////////////////////////////////////////////////////////

#include "decoder.h"
#include <pthread.h>
#include <malloc.h>
#include <memory.h>

#define MAX_THREADS  8192
#define ADDR_ALIGN  (16*4096) // for memalign() and best map of CPU core <-> DIMM/memory bus

// Immediately decoding related
int decode_2bit_1channel_cpu_blocking_v1a(const uint32_t* src, float* dst, size_t N);
int decode_2bit_1channel_cpu_blocking_v1b(const uint32_t* src, float* dst, size_t N);
int decode_2bit_1channel_cpu_blocking_v2(const uint32_t* src, float* dst, size_t N);
int decode_2bit_1channel_cpu_blocking_v3(const uint32_t* src, float* dst, size_t N);
int decode_2bit_1channel_cpu_blocking_v4(const uint32_t* src, float* dst, size_t N);

static const float lut2bit_VDIF[4] = {-3.3359, -1.0, +1.0, +3.3359};
static const float lut2bit_Mk5B[4] = {-3.3359, +1.0, -1.0, +3.3359};
#ifdef DECODE_MODE_MARK5B
static const float lut2bit[4] = {-3.3359, +1.0, -1.0, +3.3359};
#else
static const float lut2bit[4] = {-3.3359, -1.0, +1.0, +3.3359};
#endif
static float lookup_2bit[256][4];

// Threading related
struct decode_context_cpu_tt;
struct decode_descriptor_cpu_tt;

typedef struct decode_descriptor_cpu_tt {
	size_t    Nthreads;
	pthread_t tid[MAX_THREADS];
        struct decode_context_cpu_tt* ctx[MAX_THREADS];
} decode_descriptor_cpu_t;

typedef struct decode_context_cpu_tt {
	const uint32_t* src;
	float* dst;
	size_t Nsamples;
	int rank;
	int rc;
} decode_context_cpu_t;


/////////////////////////////////////////////////////////////////////////////////////
// Memory alloc wrapper
/////////////////////////////////////////////////////////////////////////////////////
void* decode_malloc_cpu(size_t bytes)
{
        return memalign(ADDR_ALIGN, bytes);
}


/////////////////////////////////////////////////////////////////////////////////////
// Decoder wrapper
/////////////////////////////////////////////////////////////////////////////////////
int decode_2bit_1channel_cpu_blocking(const uint32_t* src, float* dst, size_t N)
{
	//return decode_2bit_1channel_cpu_blocking_v1a(src, dst, N); // output float32 file passes validation
	return decode_2bit_1channel_cpu_blocking_v1b(src, dst, N); // output float32 file passes validation
	//return decode_2bit_1channel_cpu_blocking_v2(src, dst, N); // output float32 file passes validation
	//return decode_2bit_1channel_cpu_blocking_v3(src, dst, N); // output float32 file passes validation
	//return decode_2bit_1channel_cpu_blocking_v4(src, dst, N); // output float32 file passes validation
}

static void *decode_2bit_1channel_cpu_blocking_pthread(void *v_ctx)
{
	// Get task context
	decode_context_cpu_t* ctx = (decode_context_cpu_t*)v_ctx;

	// Tie to specific CPU core
#if 0
        cpu_set_t set;
        CPU_ZERO(&set);
        CPU_SET(2 + 2*ctx->rank + 1, &set);
	if (pthread_setaffinity_np(pthread_self(), sizeof(set), &set) < 0) {
		printf("Thread %d could not set CPU rank\n", ctx->rank);
	}
#endif

	// Perform calculations
	ctx->rc = decode_2bit_1channel_cpu_blocking(ctx->src, ctx->dst, ctx->Nsamples);
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////
// Local init
/////////////////////////////////////////////////////////////////////////////////////
static void decode_2bit_1channel_cpu_initLUT(void)
{
	const int nbit = 2;
	int val, i;

	for (val = 0; val < 256; val++) {
		for (i = 0; i < (8/nbit); i++) {
			int samp = (val >> (i*nbit)) & 0x3;
			lookup_2bit[val][i] = lut2bit[samp];
		}
	}

	#if 0
	/* Print out the look table to reuse it easily in CUDA code */
	printf("lookup_2bit = [ \n");
	for (val = 0; val < 256; val++) {
		printf("   [%+7.4f,%+7.4f,%+7.4f,%+7.4f ], /* %d */ \n",
			lookup_2bit[val][0],
			lookup_2bit[val][1],
			lookup_2bit[val][2],
			lookup_2bit[val][3],
			val
		);
	}
	printf("];\n");
	#endif

}


/////////////////////////////////////////////////////////////////////////////////////
// Generic threading
//
// decode_2bit_1channel_cpu_init   : prepares a parallel decoding task
// decode_2bit_1channel_cpu_submit : starts parallel execution of decoding subtasks
// decode_2bit_1channel_cpu_join   : blocks until all subtasks have completed
//
/////////////////////////////////////////////////////////////////////////////////////
void* decode_2bit_1channel_cpu_init(const uint32_t** src, float** dst, const size_t Nsamp, const size_t Nth)
{
	size_t i;

	// Overall descriptor of parallel decoding task
	decode_descriptor_cpu_t* d;
	d = (decode_descriptor_cpu_t*)malloc(sizeof(decode_descriptor_cpu_t));
	memset((void*)d, 0, sizeof(decode_descriptor_cpu_t));

	// Task contexts for individual decoding subtasks
	d->Nthreads = Nth;
	for (i = 0; i < Nth; i++) {

		decode_context_cpu_t* ctx = malloc(sizeof(decode_context_cpu_t));
		memset((void*)ctx, 0, sizeof(decode_context_cpu_t));

		ctx->Nsamples = Nsamp;
		ctx->src = src[i];
		ctx->dst = dst[i];
		ctx->rc = -1;
		ctx->rank = i;
		d->ctx[i] = ctx;
		printf("Created context %p : Nsamp=%lu\n", d->ctx[i], d->ctx[i]->Nsamples);
	}

	// Prepare lookup table used by some decoders
	decode_2bit_1channel_cpu_initLUT();

	return (void*)d;
}

int decode_2bit_1channel_cpu_submit(void* descriptor)
{
	decode_descriptor_cpu_t* d = (decode_descriptor_cpu_t*)descriptor;
	size_t i;
	for (i = 0; i < d->Nthreads; i++) {
		(void)pthread_create(&(d->tid[i]), NULL,
			decode_2bit_1channel_cpu_blocking_pthread,
			(void*)(d->ctx[i])
		);
	}
	return 0;
}

int decode_2bit_1channel_cpu_join(void* descriptor)
{
	decode_descriptor_cpu_t* d = (decode_descriptor_cpu_t*)descriptor;
	size_t i;
	for (i = 0; i < d->Nthreads; i++) {
		pthread_join(d->tid[i], NULL);
	}
	return 0;
}




/////////////////////////////////////////////////////////////////////////////////////
// Decoders
/////////////////////////////////////////////////////////////////////////////////////

// Version 1: small memory footprint, more arithmetic
int decode_2bit_1channel_cpu_blocking_v1a(const uint32_t* src, float* dst, size_t N)
{
	const unsigned char* s = (const unsigned char*)src;
	const unsigned char* s_end = s + N/4;
	size_t o = 0;
	for (; s < s_end ; s++,o+=4) {
		unsigned char v = *s;
		dst[o + 0] = lut2bit[v & 3]; v >>= 2;
		dst[o + 1] = lut2bit[v & 3]; v >>= 2;
		dst[o + 2] = lut2bit[v & 3]; v >>= 2;
		dst[o + 3] = lut2bit[v & 3]; v >>= 2;
	}
	return 0;
}

// Version 1b: small memory footprint, more arithmetic
int decode_2bit_1channel_cpu_blocking_v1b(const uint32_t* src, float* dst, size_t N)
{
	const unsigned char* s = (const unsigned char*)src;
	const unsigned char* s_end = s + N/4;
	size_t o = 0;
	for (; s < s_end ; s++,o+=4) {
		dst[o + 0] = lut2bit[((*s) >> 0) & 3];
		dst[o + 1] = lut2bit[((*s) >> 2) & 3];
		dst[o + 2] = lut2bit[((*s) >> 4) & 3];
		dst[o + 3] = lut2bit[((*s) >> 6) & 3];
	}
	return 0;
}

// Version 2: large memory footprint, little arithmetic
int decode_2bit_1channel_cpu_blocking_v2(const uint32_t* src, float* dst, size_t N)
{
	const unsigned char* s = (const unsigned char*)src;
	const unsigned char* s_end = s + N/4;
	size_t o = 0;
	for (; s < s_end ; s++,o+=4) {
		dst[o + 0] = lookup_2bit[*s][0];
		dst[o + 1] = lookup_2bit[*s][1];
		dst[o + 2] = lookup_2bit[*s][2];
		dst[o + 3] = lookup_2bit[*s][3];
	}
	return 0;
}

// Version 3: purely arithmetic, byte-oriented
int decode_2bit_1channel_cpu_blocking_v3(const uint32_t* src, float* dst, size_t N)
{
	const unsigned char* s = (const unsigned char*)src;
	const unsigned char* s_end = s + N/4;
	size_t o = 0;
	for (; s < s_end ; s++,o+=4) {
		// 0,1,2,3 mean is 6/4=1.5 ; levels will be skewed...
		dst[o + 0] = (float)( ((*s) >> 0) & 3) - 1.5;
		dst[o + 1] = (float)( ((*s) >> 2) & 3) - 1.5;
		dst[o + 2] = (float)( ((*s) >> 4) & 3) - 1.5;
		dst[o + 3] = (float)( ((*s) >> 6) & 3) - 1.5;
	}
	return 0;
}

// Version 4: mostly arithmetic, 32-bit -oriented
int decode_2bit_1channel_cpu_blocking_v4(const uint32_t* src, float* dst, size_t N)
{
	size_t o = 0;
	uint32_t s0, s1, s2, s3;

	unsigned char* s0b = (unsigned char*)&s0;
	unsigned char* s1b = (unsigned char*)&s1;
	unsigned char* s2b = (unsigned char*)&s2;
	unsigned char* s3b = (unsigned char*)&s3;

	for (; o < N ; o += 32/2, src++) {

		s0 = (*src >> 0) & 0x03030303; // ---S ---S ---S ---S
		s1 = (*src >> 2) & 0x03030303; // --S- --S- --S- --S-
		s2 = (*src >> 4) & 0x03030303; // -S-- -S-- -S-- -S--
		s3 = (*src >> 6) & 0x03030303; // S--- S--- S--- S---

		dst[o + 0] = lut2bit[s0b[0]];
		dst[o + 1] = lut2bit[s1b[0]];
		dst[o + 2] = lut2bit[s2b[0]];
		dst[o + 3] = lut2bit[s3b[0]];

		dst[o + 4] = lut2bit[s0b[1]];
		dst[o + 5] = lut2bit[s1b[1]];
		dst[o + 6] = lut2bit[s2b[1]];
		dst[o + 7] = lut2bit[s3b[1]];

		dst[o + 8] = lut2bit[s0b[2]];
		dst[o + 9] = lut2bit[s1b[2]];
		dst[o +10] = lut2bit[s2b[2]];
		dst[o +11] = lut2bit[s3b[2]];

		dst[o +12] = lut2bit[s0b[3]];
		dst[o +13] = lut2bit[s1b[3]];
		dst[o +14] = lut2bit[s2b[3]];
		dst[o +15] = lut2bit[s3b[3]];

	}
	return 0;
}
