/////////////////////////////////////////////////////////////////////////////////////////////////
// Trial implementation of real-valued signal lowpass/bandpass filtering (FIR filter)
// that includes sample rate reduction (decimation). For nVidia CUDA.
//
// There are at least four methods to extract at least one subband out of a wideband signal:
//  1) Polyphase filter bank : complex output, multiple subbands, evenly spaced in frequency, Double Sideband (DSB)
//  2) FFT filtering : real output, multiple subbands, evenly spaced in frequency, Single Sideband (SSB)?
//  3) Frequency translating FIR filter : complex output, single subband, arbitrarily centered in frequency, Double Sideband (DSB)
//  4) Decimating FIR bandpass : real output, single subband, bandpass region arbitrarily placed but actual
//                               output band region evenly spaced in frequency, Single Sideband (USB or LSB are alternating)
//
// Here we implement the method 4.
// For an illustrated description, see, http://www.quickfiltertech.com/files/QFAB003%20downsample%20ap%20note.pdf
//
// Main memory speed on TITAN X
//
//   Max 336 GB/s : 336 GB/s / 4byte(float32) = 84 Gs/s
//
// Without sample rate reduction (no decimation),
//
//     1 FIR output sample = N-tap sample reads + 1 write = (N+1) memory I/O amplification
//     in this case for say 9 taps the memory I/O limits processing to 8.4 Gs/s
//
// If we decimate the filter output by 1:R, then we do not need to compute
// only every Rth filter output value and can skip the R-1 other calculations.
//
//     1 FIR output sample = 1 sample out of R = (N+1)/R memory I/O load
//     in this case example when N=16 and R=16 the I/O limits processing to ~80 Gs/s,
//     and other factors (coefficient loading, arithmetics) will dominate
//
// Performance tested on nVidia TITAN X:
//
//  Ntaps = R = 8   : 60897.336 Ms/s
//  Ntaps = R = 16  : 41979.562 Ms/s
//  Ntaps = R = 32  : 14229.508 Ms/s
//  Ntaps = R = 64  :  7380.336 Ms/s
//  Ntaps = R = 128 :  6331.149 Ms/s
//  Ntaps = 32, R = 16 : 22359.842 Ms/s
//  Ntaps = 64, R =  8 : 17175.692 Ms/s
//
/////////////////////////////////////////////////////////////////////////////////////////////////

#include <inttypes.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define CHECK_TIMING 1  // needed by "cuda_utils.cu" to enable throughput benchmarking
#include "cuda_utils.cu"
#include <cuComplex.h>

/////////////////////////////////////////////////////////////////////////////////////////////////

#define FIR_TAPS 64      // number of taps in FIR filter (more taps = narrower bandpass or lowpass)
#define DECIM_R  16      // decimation ratio (larger = narrower output bandwidth)
                         // note: aliasing artefacts in the output signal depend crudely said on
                         // the ratio DECIM_R/FIR_TAPS; smaller ratios yield less aliasing i.e. better signal

#define CUDA_DEVICE_NR 0 // the CUDA device to use (1st installed GPU = 0, 2nd installed GPU = 1, ...)

const char* infile = "bandfilter.in.f32";       // test data file written by bandfilter.m
const char* outfile = "bandfilter.out.f32";     // output from bandfilter.cu GPU program

const char* coefffile = "bandfilter.coeff.f32"; // test FIR coefficients written by bandfilter.m,
                                              // make sure FIR_TAPS here and Ntaps in bandfilter.m are the same!

/////////////////////////////////////////////////////////////////////////////////////////////////

__constant__ float c_fir_coeffs[FIR_TAPS];

__global__ void cu_decimFIR_real(const float* __restrict__ src, float* __restrict__ dst, size_t Niter)
{
    const size_t threadID   = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t numThreads = blockDim.x * gridDim.x;

    size_t n_in  = threadID*DECIM_R;
    size_t n_out = threadID;
    while (Niter-- > 0) {
        float y = 0.0f;
        #pragma unroll
        for (int tap=0; tap<FIR_TAPS; tap++) {
           y += src[n_in + tap] * c_fir_coeffs[tap];
        }
        dst[n_out] = y;
        n_in  += numThreads*DECIM_R;
        n_out += numThreads;
    }
    return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t threadsPerBlock, numBlocks, maxphysthreads;

    size_t nsamples, ntaps;
    float  *h_samples, *d_samples;
    cuFloatComplex *h_filtered, *d_filtered;
    float  *coeffs;

    FILE   *f;
    size_t nrd;

    /* Prepare GPU device */
    printf("Selecting CUDA device %d\n", CUDA_DEVICE_NR);
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );
    printf("Using CUDA device %d : %s\n", CUDA_DEVICE_NR, cudaDevProp.name);

    /* Load precomputed raw data and FIR coefficients */
    struct stat st;
    stat(infile, &st);
    if (st.st_size < 1024) {
       printf("Error with %s\n", infile);
       return -1;
    }
    nsamples = st.st_size / sizeof(float);
    stat(coefffile, &st);
    if (st.st_size < 4) {
       printf("Error with %s\n", coefffile);
       return -1;
    }
    ntaps = st.st_size / sizeof(float);

    printf("Loading %zu samples (32-bit) from %s...\n", nsamples, infile);
    CUDA_CALL( cudaMalloc(&d_samples, nsamples*sizeof(float)) );
    CUDA_CALL( cudaMalloc(&d_filtered, nsamples*sizeof(cuFloatComplex)) );
    CUDA_CALL( cudaMallocHost(&h_samples, nsamples*sizeof(float)) );
    CUDA_CALL( cudaMallocHost(&h_filtered, nsamples*sizeof(cuFloatComplex)) );
    CUDA_CALL( cudaMemset(d_filtered, 0x00, nsamples*sizeof(cuFloatComplex)) );
    f = fopen(infile, "r");
    if (!f) perror("fopen");
    nrd = fread(h_samples, sizeof(float), nsamples, f);
    fclose(f);
    printf("   got %zu samples\n", nrd);
    CUDA_CALL( cudaMemcpy(d_samples, h_samples, nsamples*sizeof(float), cudaMemcpyHostToDevice) );

    printf("Loading FIR coefficients...\n");
    CUDA_CALL( cudaMallocHost(&coeffs, FIR_TAPS*sizeof(float)) );
    for (int i=0; i<FIR_TAPS; i++) {
        coeffs[i] = 1.0f + i;
    }
    f = fopen(coefffile, "r");
    if (!f) perror("fopen");
    nrd = fread(coeffs, sizeof(float), ntaps, f);
    fclose(f);
    printf("   got %zu coefficients\n", nrd);
    CUDA_CALL( cudaMemcpyToSymbol(c_fir_coeffs, coeffs, FIR_TAPS*sizeof(float), 0, cudaMemcpyHostToDevice) );

    /* Run the processing */
    printf("Starting the processing...\n");
    CUDA_CALL( cudaMemset(d_filtered, 0x00, nsamples*sizeof(cuFloatComplex)) );

    // Method:
    // 1 thread takes n==FIR_TAPS input samples to compute 1 output sample
    // and with a decimation ratio R==FIR_TAPS there is no read amplification
    threadsPerBlock = 32;
    numBlocks = 2048; //div2ceil(maxphysthreads, threadsPerBlock);
    size_t niter = (nsamples/(threadsPerBlock*numBlocks)) / DECIM_R;
    printf(" threads=%zu blocks=%zu iters=%zu\n", threadsPerBlock, numBlocks, niter);
    CUDA_TIMING_START(tstart, 0);
    cu_decimFIR_real <<< numBlocks, threadsPerBlock >>> (d_samples, (float*)d_filtered, niter);
    CUDA_TIMING_STOP(tstop, tstart, 0, "FIR", nsamples);

    CUDA_CALL( cudaMemcpy(h_filtered, d_filtered, nsamples*sizeof(cuFloatComplex), cudaMemcpyDeviceToHost) );
    int* test = (int*)h_filtered;
    for (size_t i=0; i<nsamples; i++) {
       if (test[i] == 0) {
          printf("  first zero in h_filtered[%zu]\n", i);
          break;
       }
    }

    /* Store the result */
    printf("Writing output file %s...\n", outfile);
    f = fopen(outfile, "w");
    fwrite(h_filtered, nsamples/DECIM_R, sizeof(float), f);
    fclose(f);

    return 0;
}

