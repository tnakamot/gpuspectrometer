#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <random>
#include <cmath>

#define MAX_ITER  1000000000
#define STEP_ITER 100000

/** Returns sum = sum + y, and stores the running value of compensation. */
template<typename T> T kahanSum(T sum, T x, T& c)
{
   volatile T e = x - c;   // new input minus current compensation
   volatile T t = sum + e; // add "large" sum and new "small" number, looses low-order digits
   c = (t - sum) - e;      // new compensation to recover lost low-order digits
   return t;
}

int main()
{
  std::random_device rd;
  std::mt19937 generator(rd());

  long long n = 0;
  double sum = 0,  sum_Kahan = 0,  comp = 0;
  float  sumf = 0, sumf_Kahan = 0, compf = 0;
  double mean = 0;
  float  meanf = 0;

#if 1
  // Half-normal distribution is abs(normal)
  std::normal_distribution<double> distribution(/*mu*/0.0, /*sigma*/1.0);
  long double theoretical_mean = std::sqrt(M_2_PI); // sigma*sqrt(2/pi)
#else
  // Uniform distrib.
  std::uniform_real_distribution<double> distribution(0, 1);
  long double theoretical_mean = 0.5;
#endif

  std::ofstream outfile;
  outfile.open("accuracy_Kahan.txt");
  while (n < MAX_ITER) {

    // Add another chunk of STEP_ITER new samples to our running sums
    for (long long nsub = 0; nsub < STEP_ITER; nsub++, n++) {
        double v = std::abs(distribution(generator));
        float vf = v;
        sumf += vf; // 32-bit naive summation
        sum  += v;  // 64-bit naive summation
        sumf_Kahan = kahanSum<float>(sumf_Kahan, vf, compf); // 32-bit Kahan summation
        sum_Kahan = kahanSum<double>(sum_Kahan, v, comp);    // 64-bit Kahan summation
        meanf += (vf - meanf)/float(n+1); // 32-bit running mean (D.Knuth)
        mean += (v - mean)/double(n+1);   // 64-bit running mean (D.Knuth)
    }

    long double davg = sum/(long double)n;
    long double davgf = sumf/(long double)n;
    long double davg_Kahan = sum_Kahan/(long double)n;
    long double davgf_Kahan = sumf_Kahan/(long double)n;

    std::ostringstream osummary;
    osummary << n << " " << std::setprecision(18)
      // << sum << " " << sumf << " "
      // << sum_Kahan << " " << sumf_Kahan << " "
      // << comp << " " << compf << " "
      << (theoretical_mean - davg) << " "
      << (theoretical_mean - davgf) << " "
      << (theoretical_mean - davg_Kahan) << " "
      << (theoretical_mean - davgf_Kahan) << " "
      << std::abs(davg - davg_Kahan) << " "
      << std::abs(davg - davgf) << " "
      << std::abs(davg - davgf_Kahan) << " "
      << std::abs(davgf - davgf_Kahan) << " "
      << (theoretical_mean - mean) << " "
      << (theoretical_mean - meanf) << " "
      << std::endl;
    outfile << osummary.str();
    std::cout << "(" << (100.0*n/MAX_ITER) << "%) " << osummary.str();

  }

  outfile.close();

  return 0;
}

