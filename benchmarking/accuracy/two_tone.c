//==============================================================================================
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

//----------------------------------------------------------------------------------------------
#define m_PI	3.14159265358979323846	//��

#define FFT_N	16384	// FFT points 2^14
#define RND_N	(FFT_N / 4)
#define ACC_N	100

#define F_SMPL	1.0e+3
#define T_SMPL	(1.0 / F_SMPL)
#define FRQ_A	50.0	//64.0
#define FRQ_B	55.5	//96.0
#define AMP_A	1.0
#define AMP_B	0.001
#define AMP_NZ	0.01
//----------------------------------------------------------------------------------------------
unsigned char	Rnd_Src[FFT_N];
unsigned int	Gen_Rnd[RND_N];

double	Data[ACC_N][FFT_N];
double	Rnd_NZ[FFT_N];

//****************************************************
#define Lv_1	0.125	// 1/8
#define Lv_2	0.375	// 3/8
#define Lv_3	0.625	// 5/8
#define Lv_4	0.875	// 7/8

static double	cmb_tbl[128] = {
	-Lv_4, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_4
};

//****************************************************
// for random noise generate
static int SR_P;
static unsigned int SR[521];

//----------------------------------------------------------------------------------------------
void noise_gen(void);
int  wr_data(char fname[]);

//++++++ M-series random modules +++++++++++++++++++++++
static void  lfsr521(void);
void         init_rnd(unsigned int seed);
unsigned int irnd(void);
int          load_lfsr(char fname[]);
int          save_lfsr(char fname[]);
//----------------------------------------------------------------------------------------------
main(int argc,char *argv[])
{
	unsigned int	rnd_sd;
	int				ac, dc;
	double			noise, tone_a, tone_b;
	double			t, dph;

	// buffer clear
	memset(Data, 0, sizeof(Data));

	// initialise random sequence
	rnd_sd = 3141592653; // default value
	init_rnd(rnd_sd);

	for(ac = 0; ac < ACC_N; ac++)
	{
		noise_gen();

		for(dc = 0; dc < FFT_N; dc++)
		{
			t = T_SMPL * (ac * FFT_N + dc);
			dph = 2.0 * m_PI * t;

			tone_a = AMP_A * sin(dph * FRQ_A);
			tone_b = AMP_B * sin(dph * FRQ_B);

			Data[ac][dc] = Rnd_NZ[dc] + tone_a + tone_b;
		}
	}

	wr_data("dual_tone_A.dat");

	return EXIT_SUCCESS;
}

//----------------------------------------------------------------------------------------------
void noise_gen(void)
{
	int		dc, k;

	for(dc = 0; dc < RND_N; dc++)
		Gen_Rnd[dc] = irnd();

	memcpy(Rnd_Src, Gen_Rnd, sizeof(Rnd_Src));	// excheng to 8bits-array from 32bits-array

	for(dc = 0; dc < FFT_N; dc++)
	{
		k = Rnd_Src[dc] & 0x7F;
		Rnd_NZ[dc] = AMP_NZ * cmb_tbl[k];
	}
}


//----------------------------------------------------------------------------------------------
int wr_data(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "wb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(Data, sizeof(Data), 1, fp);
	fclose(fp);

	return 0;
}

//==============================================================================================
//	M-series random modules
//==============================================================================================
static void lfsr521(void)
{
	int i;

	for(i =  0; i <  32; i++)
		SR[i] ^= SR[i + 489];

	for(i = 32; i < 521; i++)
		SR[i] ^= SR[i -  32];
}

//----------------------------------------------------------------------------------------------
void init_rnd(unsigned int seed)
{
	int i, j;
	unsigned int u;

	u = 0;

	for(i = 0; i <= 16; i++)
	{
		for(j = 0; j < 32; j++)
		{
			seed = seed * 1566083941UL + 1;

			u = (u >> 1) | (seed & (1UL << 31));
		}
		SR[i] = u;
	}

	SR[16] = (SR[16] << 23) ^ (SR[0] >> 9) ^ SR[15];

	for(i = 17; i <= 520; i++)
		SR[i] = (SR[i-17] << 23) ^ (SR[i-16] >> 9) ^ SR[i-1];

	lfsr521();  lfsr521();  lfsr521();  /* warm up */

	SR_P = 520;
}

//----------------------------------------------------------------------------------------------
unsigned int irnd(void)
{
	if(++SR_P >= 521)
	{
		lfsr521();  SR_P = 0;
	}

	return SR[SR_P];
}

//----------------------------------------------------------------------------------------------
int load_lfsr(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "rb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fread(SR, sizeof(SR), 1, fp);
	fclose(fp);

	SR_P = 520;

	return 0;
}

//----------------------------------------------------------------------------------------------
int save_lfsr(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "wb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(SR, sizeof(SR), 1, fp);
	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------
