#!/usr/bin/python
"""
Polyphase Filterbank Coefficients Calculator

Usage: pfb_coeffs.py <Nchannels> <Ntaps in each channel> [filter type]

Arguments:

   Nchannels    Number of PFB channels
   Ntaps        Number of FIR filter taps in each channel

   Filter type  Optional, can specify 'sinc', 'hsinc', 'bsinc' (default),
                'flattop', or 'hft248d'.

Generates polyphase filter bank coefficients and writes them into coeffs.txt. 

File format: 1st line = prototype filters coefficients, 2nd line = Nchannels,
3rd line = Ntaps, 4th..Nth line = channel# and its FIR coefficients b0..bM in
same order as FIR definition y[n] = b0*x[n] + b1*x[n-1] + b2*x[n-2] + ...

Filter type 'sinc' = sinc(t=-inf..+inf) but truncated to a finite number of 
coefficients. Due to the necessary truncation the response is not quite the
ideal brick wall filter. The windowed 'hsinc' (Hamming) or 'bsinc' (Blackman)
reduce the level of such artefacts (cf. Analog Devices "DSP Book" Chapter 16).

Commercial spectrometers generally use flat-top window functions, for example
the XFFTS2 PFB spectrometer (http://arxiv.org/pdf/1203.3972). The flat-top windows
trade degradation in spectral resolution against improvements in amplitude accuracy.

For PFB intended for channelization (wideband -> multichannel narrowband), steep 
non-overlapping channel responses give by sinc()-based coefficients are preferred.  
For PFB intended for accurate spectrometry (wideband -> time-average spectrum),
overlapping channels with near-unity noise equivalent bandwidths given by flat-top
class of window functions are preferred.
"""

import numpy, sys

ftype_list = ['sinc','hsinc','bsinc','flattop','hft248d']

def create_pfb_coeffs_sinc(Nch, Ntaps, ftype):

	Wn_cutoff = 1/float(Nch)
	Ncoeff = Nch * Ntaps

	# Determine M+1 coefficients where M is even and M+1 <= Ncoeff.
	# These will be zero-padded to get exactly Ncoeff coefficients.

	M = (Ncoeff-1) - (Ncoeff-1)%2
	nn = numpy.linspace(-M/2, +M/2, num=M+1)
	ii = numpy.linspace(0,M, num=M+1)
	M_zeropad = Ncoeff - (M+1)

	# Basic truncated "brick wall" filter
	h = numpy.sinc(2*(Wn_cutoff/2)*nn)

	# Windowing to reduce artefacts
	if (ftype == 'bsinc'):
		wf_blackman = 0.42 - 0.5*numpy.cos(2*numpy.pi*ii/float(M)) 
		wf_blackman += 0.08*numpy.cos(4*numpy.pi*ii/float(M))
		h *= wf_blackman

	if (ftype == 'hsinc'):
		wf_hamming = 0.54 - 0.46*numpy.cos(2*numpy.pi*ii/float(M)) 
		h *= wf_hamming

	# Normalize for unity gain
	h /= numpy.sum(h)

	# Zero-pad to Ncoeff coefficients (none to front, and M_zeropad to back)
	h = numpy.pad(h, (0,M_zeropad), 'constant')

	# Rearrange coefficients, the FIR in each PFB channel has a subset of these
	# Note: Matlab uses fortran order, Numpy uses C order
	H = numpy.reshape(h, (Ntaps,Nch), order='C')
	H = numpy.fliplr(H)

	return (h,H)

def create_pfb_coeffs_flattop(Nch, Ntaps, ftype, windowedSinc=True):
	# Flat-top windows in general are summations of consines, i.e.,
	#    h = a[0] - a[1]*numpy.cos(nn) + a[2]*numpy.cos(2*nn) - a[3]*numpy.cos(3*nn) + a[4]*numpy.cos(4*nn) - ...
	#         where nn = 2pi*n/(N-1) with n=0..N-1

	Wn_cutoff = 1/float(Nch)

	# Number of coefficients
	N = Nch * Ntaps

	# Determine M+1 coefficients where M is even and M+1 <= N.
	# These will be zero-padded to get exactly N coefficients.
	M = (N-1) - (N-1)%2
	nn = numpy.linspace(-M/2, +M/2, num=M+1)
	ii = numpy.linspace(0,M, num=M+1)
	M_zeropad = N - (M+1)

	# Basic truncated "brick wall" filter
	hsinc = numpy.sinc(2*(Wn_cutoff/2)*nn)

	# Zero-pad to Ncoeff coefficients (none to front, and M_zeropad to back)
	hsinc = numpy.pad(hsinc, (0,M_zeropad), 'constant')

	# Two options: symmetric window, or periodic window?
	# For filters the symmetric window is best (compute length N window). For FFT-based
	# analysis the periodic window is best (compute length N+1 window and use first N points).
	# Since our coefficients will be used for PFB (filter) we choose a symmetric window.

	nn = (2.0*numpy.pi/(N-1)) * numpy.linspace(0, N-1, N)

	# Desired coefficients
	if ftype == 'flattop':
		# See http://de.mathworks.com/help/signal/ref/flattopwin.html
		a = [0.21557895, 0.41663158, 0.277263158, 0.083578947, 0.006947368]

	if ftype == 'hft248d':
		# See G. Heinzel et al. (2002), http://pubman.mpdl.mpg.de/pubman/item/escidoc:152164:1
		# HFT248D (10-term cosine), 248.4 dB, differentiable, 0.0009 dB flatness, needs 80% FFT overlap		
		a = [1.0, 1.985844164102, 1.791176438506, 1.282075284005, 0.667777530266, 0.240160796576, 0.056656381764, 0.008134974479, 0.000624544650, 0.000019808998, 0.000000132974]

	# Calculate window function
	h = a[0] * numpy.ones_like(nn)
	for ii in range(1,len(a)):
		h += (pow(-1,ii) * a[ii]) * numpy.cos(float(ii)*nn)

	# Window the sinc() function with it
	if windowedSinc:
		h = hsinc * h

	# Normalize
	h /= numpy.max(h)  # to the peak as in Matlab
	#h /= numpy.sum(h)  # to the sum for unity gain

	# Rearrange coefficients, the FIR in each PFB channel has a subset of these
	# Note: Matlab uses fortran order, Numpy uses C order
	H = numpy.reshape(h, (Ntaps,Nch), order='C')
	H = numpy.fliplr(H)

	return (h,H)

def write_array(f, arr):

	# Write out all coefficients, if short
	if (len(arr) <= 128*128):

		# Option 1: write entire array without 'summarization' using numpy (slow!)
		# numpy.set_printoptions(threshold=len(arr)+10)
		# f.write(numpy.array_str(arr, max_line_width=1e99) + '\n')

		# Option 2: write entire array explicitly (quite fast)
		f.write(' [ ')
		for ii in xrange(len(arr)):
			f.write('%.10e ' % (arr[ii]))
		f.write(' ];\n')
	# Write just a summarized list of coefficients 
	else:
		numpy.set_printoptions(threshold=1000)
		f.write(numpy.array_str(arr, max_line_width=1e99) + '\n')

def main(argv=sys.argv):

	if len(argv) not in [3,4]:
		print __doc__
		sys.exit(-1)

	ftype = 'bsinc'
	Nch   = int(sys.argv[1])
	Ntaps = int(sys.argv[2])
	fn    = 'coeff_%d_%d.txt' % (Nch,Ntaps)
	if len(sys.argv)==4:
		ftype = sys.argv[3].lower()

	if (ftype not in ftype_list) or (Nch < 1) or (Ntaps < 2):
		print __doc__
		sys.exit(-1)

	if ftype in ['sinc','hsinc','bsinc']:
		(h,H) = create_pfb_coeffs_sinc(Nch, Ntaps, ftype)
	else:
		(h,H) = create_pfb_coeffs_flattop(Nch, Ntaps, ftype)

	# Optional: store copy of coeffs for easy plotting
	if True:
		f = open('gnuplot.dat', 'w')
		for v in h:
			f.write('%.10e\n' % (v))
		f.close()
		print ('Wrote prototype filter into gnuplot.dat for easy inspection.')

	# Store PFB coefficients file
	f = open(fn, 'w')
	f.write('# prototype %s h = ' % (ftype))
	write_array(f, h)
	f.write('%5d # channels\n' % (Nch))
	f.write('%5d # FIR taps per channel\n' % (Ntaps))
	for ch in range(Nch):
		h_fir = H[:,ch]
		numpy.set_printoptions(threshold=len(h_fir)+10)
		s = numpy.array_str(h_fir, max_line_width=1e99)
		s = s[1:-1].strip()
		f.write('%d %s\n' % (ch,s))
	print ('Wrote coefficients into %s' % (fn))
	f.close()

main(sys.argv)
