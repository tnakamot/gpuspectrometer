% Polyphase Filterbank FIR Coefficients design based on windowed sinc(x).
%
% function [H,Hq,h] = pfb_coeffs(<args>)
%
% pfb_coeffs(Nch)
% pfb_coeffs(Nch, Ntaps)
% pfb_coeffs(Nch, Ntaps, coeff_file_name)
% pfb_coeffs(Nch, Ntaps, coeff_file_name, R)
% pfb_coeffs(Nch, Ntaps, [], R)
% pfb_coeffs(Nch, Ntaps, coeff_file_name, R, designtype)
% pfb_coeffs(Nch, Ntaps, [], R, designtype)
%
% Arguments:
%
% Nch   : the number of PFB output channels
% Ntaps : if unspecified, it defaults to the number of channels Nch
%
% coeff_file_name : file to store PFB filter coefficients in text format.
%    The file has a 3-row header followed by Nch rows of FIR coefficients.
%    Each coefficient row contains the channel number (0..Nch-1) followed
%    by Ntaps of real-valued coefficients that can be input to dfilt.dffir().
%    If the file name is set to [] or '', no file will be generated.
%
% R : the decimation ratio, defaults to critical sampling i.e. R = Nch.
%    Smaller (or larger) values widen (or narrow) the channel responses.
%
% designtype : specifies the design type of the prototype filter
%    'sinc'    = truncated sinc function
%    'hsinc'   = Hamming windowed sinc function
%    'bsinc'   = Blackman windowed sinc function (default)
%    'fir1'    = Matlab-generated coeffs from fir1() (usually same as Hamming)
%    'flattop' = Matlab-generated flat-top window (flattopwin()) as used 
%                e.g. for calibration purposes. Ignores decimation ratio 'R'.
%    'hft248d' = flat-top window HFT248D from G. Heinzel, A. Rudiger, and R. Schilling
%                Ignores decimation ratio 'R'.
%
%    The prototype filter is automatically zero-padded.
%
% Returns:
%
% H  : matrix of real-valued coefficients (H: Nch x Ntaps)
% Hq : cell array of dfilt.dffir FIR filter objects (H: 1 x Nchannels)
% h  : the original prototype filter (h: 1 x Nch*Ntaps)
%
% Note that coefficients are quantized to a 32-bit float.
%
function [H,Hq,h] = pfb_coeffs(varargin)
    
    Nch = 32;        % Default number of channels in the FIR filter bank
    coeff_file = ''; % Default coefficients file for export
    do_plot = 1;     % Set to 1 to show frequency response of prototype filter

    %% Parse arguments
    if length(varargin)>=1,
        Nch = varargin{1};
        Ntap = Nch;
    end
    if length(varargin)>=2,
        Ntap = varargin{2};
    end
    if length(varargin)>=3,
        coeff_file = varargin{3};
    end
    if length(varargin)>=4,
        R = varargin{4};
    else
        R = Nch;
    end
    if length(varargin)>=5,
        designtype = varargin{5};
    else
        designtype = 'bsinc';
    end
    
    %% Design the filterbank
    % also see http://radio.feld.cvut.cz/matlab/toolbox/filterdesign/quant10c.html
        
    % Create prototype lowpass filters
    Wn_cutoff = 1/R;
    Ncoeff = Nch*Ntap;
    h_proto = {};
    h_names = {};
    h_designtypes = {};

    % Determine M+1 coefficients, where M is even, and M+1 <= Ncoeff.
    % These will be zero-padded later to get a total of Ncoeff coefficients.
    % Indices for coefficients:
    M = (Ncoeff-1) - mod((Ncoeff-1),2);
    M_zeropad = Ncoeff - (M+1);
 
    % Basic truncated sinc() i.e. approximation of "brick wall" filter
    nn = -(M/2):+(M/2); 
    h_brick = sinc(2*(Wn_cutoff/2)*nn); % sinc brick lowpass coeffs
    h_brick = h_brick / sum(h_brick);   % normalize for unity gain
    
    % Unwindowed filter
    h_proto{end+1} = [h_brick, zeros(1,M_zeropad)];
    h_names{end+1} = 'Unwindowed sinc';
    h_designtypes{end+1} = 'sinc';

    % Hamming windowed filter (same as Matlab fir1() function)
    h = h_brick .* hamming(M+1)';
    h = h / sum(h);
    h_proto{end+1} = [h, zeros(1,M_zeropad)];
    h_names{end+1} = 'Hamming-windowed sinc';
    h_designtypes{end+1} = 'hsinc';

    % Blackman windowed filter
    h = h_brick .* blackman(M+1)';
    h = h / sum(h);
    h_proto{end+1} = [h, zeros(1,M_zeropad)];
    h_names{end+1} = 'Blackman-windowed sinc';
    h_designtypes{end+1} = 'bsinc';
    
    % Other: FIR1 from Matlab
    h = fir1(Ncoeff-2, Wn_cutoff);
    h = [h, zeros(1, Ncoeff-length(h))];
    h = h / sum(h);
    h_proto{end+1} = h;
    h_names{end+1} = 'Matlab FIR1';
    h_designtypes{end+1} = 'fir1';

    % Other: flattop() from Matlab
    % Flat top windows spread the energy of a sine wave across multiple(!) bins,
    % ensuring that the unattenuated (exact) amplitude is found in at least one bin,
    % with the drawback that frequency resolution is ~2.5 poorer.
    h = flattopwin(Ncoeff-2).';
    h = [h, zeros(1, Ncoeff-length(h))];
    h = h / sum(h);
    h_proto{end+1} = h;
    h_names{end+1} = 'Matlab flattop()';
    h_designtypes{end+1} = 'flattop';

    % Other: HFT248D from http://edoc.mpg.de/395068 (needs 84% overlap)
    % calculated using flatwin.m available at
    % http://www.mathworks.com/matlabcentral/fileexchange/46092-window-utilities/content/flatwin.m
    h = flatwin(Ncoeff-2,18).';
    h = [h, zeros(1, Ncoeff-length(h))];
    h = h / sum(h);
    h_proto{end+1} = h;
    h_names{end+1} = 'HFT248D';
    h_designtypes{end+1} = 'hft248d';

    %% Distribute coefficients    
    % Choose one of the filters (Blackman-windowed sinc is good)     
    idx = find(ismember(h_designtypes, designtype));
    if isempty(idx),
        fprintf(1, 'error: user specified an unknown design type %s\n', designtype);
        H = []; Hq = {}; h = [];
        return;
    end
    h = h_proto{idx};
    
    % Distribute coefficients into polyphase structure
    H = reshape(h, [Nch Ntap]); % size(H) = Nch x Ntap
    H = flipud(H); 
    
    % Quantize to 32-bit float
    H = single(H);

    % Create the FIR filters, check stability of each quantized filter
    Hq = cell(Nch,1);
    for k=1:Nch,
        Hq{k} = dfilt.dffir(H(k,:));      % dffir = Direct-form FIR
        set(Hq{k},'arithmetic','single'); % quantize again
        if ~isstable(Hq{k}),
            fprintf(1, 'Warning: filter block %d in single precision is not stable!\n', k);
        end
    end

    %% Export to text file
    if numel(coeff_file)>0,
        fd = fopen(coeff_file, 'w');
        str = num2str(h, ' %e');
        fprintf(fd, '# prototype h = %s\n', str);
        fprintf(fd, '%5d # channels\n', Nch);
        fprintf(fd, '%5d # FIR taps per channel\n', Ntap);
        for k=1:Nch,
            str = num2str(H(k,:), ' %e');
            fprintf(fd, '%d %s\n', k-1, str);
        end
        fclose(fd);
    end
    
    %% Plotting
    % Response of selected prototype filter
    if do_plot && 1,
        figure(), freqz(h, 1, 512);
    end

    % Coefficients of each filter type
    if do_plot && 1,
        figure(), set(gca,'Color','w');
        Nflt = numel(h_proto);
        cm = jet(Nflt);
        for k=1:Nflt,
            plot(h_proto{k},'x:','color',cm(k,:)), hold on;
        end
        legend(h_names);
        axis tight;
        xlabel('Tap number'); ylabel('Weight');
        title(sprintf('Coefficients for %d-tap prototype filters allowing a %d:1 sample rate reduction', Ntap,R));
    end
    
    % Responses of each filter type
    if do_plot && 1,
        figure(),
        hdl = fvtool(h_proto{1},1, h_proto{2},1, h_proto{3},1, h_proto{4},1, h_proto{5},1, h_proto{6},1);
        %set(hdl, 'NumberofPoints',2*8192);
        set(gca,'Color','w');
        title(sprintf('Responses of %d-tap prototype filters allowing a %d:1 sample rate reduction', Ntap,R));

        % Modify the view: make symmetric plot, zoom into 0 Hz region
        wvec = linspace(-16*pi/R, 16*pi/R, 8192);
        set(hdl,'FrequencyVector',wvec);
        set(hdl,'FrequencyRange','Specify freq. vector');
        
        % Show correct names
        legend(h_names);        
    end

end

