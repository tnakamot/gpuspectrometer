function check_pfb_8b(fname)

   N = 64*1024;
   fn_TeX = strrep(fname, '_', '\_');
   Niter = 10000;

   Fd = zeros([1 N]);

   % Read input file. It has 8-bit chars in {Re,Im} pairs.
   fd = fopen(fname, 'rb');
   Nstacked = 0;
   for n=1:Niter,

      din = fread(fd, [2 N], 'schar');
      if ~(size(din)==[2 N]), break; end

      % Convert to complex
      din = single(din);
      dd = din(1,:) + 1i*din(2,:);
      clear din;
      % dd(1:10)  % note that PFB Channel 0 should have imag part==0
      sc = 1;

   % Could take just real part
   dd = real(dd);
   sc = 2;

      Fd = Fd + abs(fft(dd));
      Nstacked = Nstacked + 1;

   end
   fprintf(1, 'Averaged spectra from %d segments.\n', Nstacked);
   fclose(fd);

   % Show spectrum
   Nnyq = round(numel(Fd)/sc);
   Fd = Fd / Nstacked;
   Fd = Fd(1:Nnyq);

   figure(1), clf;
   semilogy(Fd, 'kx');
   xlabel('Frequency (bin)');
   ylabel('Fourier Amplitude (log10|F(t)|)')
   title(['Spectrum of complex signed 8-bit file ' fn_TeX]);
   axis tight;

   % Show histograms
   bins = linspace(-128,+128, 512);
   figure(2), clf;
   subplot(3,1,1), hist(abs(dd),bins),  title('Histogram of  |f(t)|');
   subplot(3,1,2), hist(real(dd),bins), title('Histogram of Re{f(t)}');
   subplot(3,1,3), hist(imag(dd),bins), title('Histogram of Im{f(t)}');

end

