#ifndef QUANTIZER_KERNELS_CU
#define QUANTIZER_KERNELS_CU

////////////////////////////////////////////////////////////////////////////////////////////////
// Data re-quantizer from float to signed 8-bit
////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_quantize_cf32_to_int8_v1 (
    const float* __restrict__ src, const float* __restrict__ variance,
    char* dst, size_t Nsamp, size_t nPFB)
{
    const float* __restrict__ src_end = src + Nsamp;

    size_t off_in  = blockIdx.x * blockDim.x + threadIdx.x;
    size_t off_out = off_in;

    size_t stride_out = nPFB*PFB_HC_NCHAN;
    size_t stride_in  = stride_out;

    // Complex samples are treated as 2 x float, both to be quantized the same way...

    // Skip the redundant half of Nch/2-symmetric input data via GPU thread indexing change
    // See cu_stats_complex_v1 for the details.
    off_in = PFB_HC_NCHAN*(size_t)(off_in/(PFB_HC_NCHAN/2)) + (off_in%(PFB_HC_NCHAN/2));
    stride_in *= 2;

    // Try to choose quantization level based on standard deviation in our channel
    size_t ch = off_in % PFB_HC_NCHAN;
    float eph_inv = 64.0f / (sqrtf(variance[ch]));

    src += off_in;
    dst += off_out;

    while (src < src_end) {
        float x = (*src) * eph_inv;

#if 0
        // Use round-to even (~36 Gs/s), symmetric errors, not biased
        // http://developer.amd.com/resources/documentation-articles/articles-whitepapers/new-round-to-even-technique-for-large-scale-data-and-its-application-in-integer-scaling/
        int   int_part  = __float2int_rd(x); // round-down : 23.5 becomes 23, -23.5 becomes -24
        float frac_part = x - int_part;      // 23.5-23 = +0.5, -23.5-(-24) = +0.5
        int C = (frac_part == 0.5) && ((int_part&1) == 0); // C = "frac part is 0.5" && "last digit is even"
        int B = truncf(x + 0.5);                           // B = "truncate(x+0.5)"
        int r2even = B - C;
        *dst = (char)r2even;
#else
        // Just round-towards-zero (~40 Gs/s), asymmetric errors, slightly biased?
        int r2zero = __float2int_rz(x);
        *dst = (char)r2zero;
#endif

        src += stride_in;
        dst += stride_out;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////
// Data re-quantizer from float to 2-bit
////////////////////////////////////////////////////////////////////////////////////////////////

// TODO, can be tricky (non-uniform levels & sub-byte addressing)


#endif // QUANTIZER_KERNELS_CU

