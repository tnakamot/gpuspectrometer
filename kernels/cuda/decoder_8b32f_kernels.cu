/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for decoding 2-bit VDIF sample data into 32-bit float.
//
// (C) 2015 Jan Wagner
//
// The Titan X memory bus bandwidth is 336 GB/s maximum. 
// In the ideal case this allows 336 GB/s / (sizeof(float) + 1/4 byte) = 79 Gs/s (base2)
// or about 84 Gs/s (decimal), but since each thread reads one byte and then writes
// sixteen bytes, there will be a lot of overhead...
//
// There are several decoder versions. They are experimenting with various approaches
// at making fast decoder implementations.
//
// The fastest decoder is 'cu_decode_2bit1ch_1D_v2' utilizing a small 4 x float lookup.
// It decodes VDIF samples. The Mark5B counterpart is cu_decode_2bit1ch_1D_mark5b_v2().
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef DECODER_8B32F_KERNELS_CU
#define DECODER_8B32F_KERNELS_CU

#include <stdint.h>
#include <inttypes.h>

__global__ void cu_decode_8bit1ch_ALMA_1D(const uchar4* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes);

/**
 * Decode 4 bytes in each thread, each byte containing one 3-bit sample (and '0'-bits)
 * data in ALMA encoding, such that byte value 0 = sample -7/2 and byte value 7 = sample +7/2.
 * Throughput on Titan X is
 *    ~56.5 Gs/s (240 GB/s in+out) when checking for "if (i >= Nbytes) { return; }"
 *    ~59.2 Gs/s when using global __constant__ instead of local const float
 */
__global__ void cu_decode_8bit1ch_ALMA_1D(const uchar4* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i >= 4*Nbytes) { return; }
        uchar4 v = src[i];

	float4 tmp = {
            -7.0f + 2.0f*__uint2float_rn(v.x),
            -7.0f + 2.0f*__uint2float_rn(v.y),
            -7.0f + 2.0f*__uint2float_rn(v.z),
            -7.0f + 2.0f*__uint2float_rn(v.w)
	};
        dst[i] = tmp;

        return;
}

#endif // DECODER_8B32F_KERNELS_CU
