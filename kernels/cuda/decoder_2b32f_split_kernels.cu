/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for decoding 2-bit VDIF sample data into 32-bit float.
//
// Applies special "splitting" of multi-channel input data during decoding.
//
// Currently the kernel(s) are intended for 2 subband data only.
//
// For input 2-bit packed like [x0 y0 x1 y1 ... xn-1 yn-1], samples are
// first converted to 32-bit and then written out into two separate arrays
// [x0 x1 ... xn-1] and [y0 y1 .. yn-1].
//
// Performance is around 57.7 Gs/s on Titan X, quite good compared
// to the non-splitting decoder that reaches 59.2 Gs/s.
//
// (C) 2015 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////
// $ nvcc -DBENCH=1 -gencode=arch=compute_52,code=sm_52 decoder_2b32f_split_kernels.cu
/////////////////////////////////////////////////////////////////////////////////////

#ifndef DECODER_2B32F_SPLIT_KERNELS_CU
#define DECODER_2B32F_SPLIT_KERNELS_CU

#include <stdint.h>
#include <inttypes.h>

#ifndef DECODER_LUT_2BIT_TO_4FLOAT_VDIF
    #define DECODER_LUT_2BIT_TO_4FLOAT_VDIF   {-3.3359, -1.0, +1.0, +3.3359}
    #define DECODER_LUT_2BIT_TO_4FLOAT_MARK5B {-3.3359, +1.0, -1.0, +3.3359}
    __constant__ float c_lut2bit_vdif[4]   = DECODER_LUT_2BIT_TO_4FLOAT_VDIF;
    __constant__ float c_lut2bit_mark5b[4] = DECODER_LUT_2BIT_TO_4FLOAT_MARK5B;
#endif

/**
 * Decode one byte in each thread.
 * One byte contains four 2-bit samples in VDIF or Mark5B order.
 */
__global__ void cu_decode_2bit2ch_split(
     const uint8_t* __restrict__ src,
     float2* __restrict__ dst_ch0,  float2* __restrict__ dst_ch1,
     const size_t Nbytes
)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        //const float lc_lut2bit_vdif[4] = DECODER_LUT_2BIT_TO_4FLOAT;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float2 ch0 = { c_lut2bit_vdif[(v >> 0) & 3], c_lut2bit_vdif[(v >> 4) & 3] };
        float2 ch1 = { c_lut2bit_vdif[(v >> 2) & 3], c_lut2bit_vdif[(v >> 6) & 3] };
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

__global__ void cu_decode_2bit2ch_testdata(uint8_t* dst, const size_t N)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N) { return; }
        uint8_t v = (i & 3);      // xxxxxxAA
        v = (v<<2) | ((i+1) & 3); // xxxxAABB
        v = (v<<4) | v;           // AABBAABB
        dst[i] = v;
}

int main(void)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t threadsPerBlock, numBlocks;
    size_t maxphysthreads;
    size_t nsamples, nrawbytes;
    uint8_t *d_raw_xy;
    float *d_x;
    float *d_y;
    float *h_d;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input
    nsamples = 512*1024*1024;
    nrawbytes = 2*nsamples*sizeof(uint8_t)/4; // 2 subbands, 4 samples per byte
    CUDA_CALL( cudaMalloc( (void **)&d_raw_xy, nrawbytes ) );
    CUDA_CALL( cudaMalloc( (void **)&d_x, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_y, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_raw_xy, 0x00, nrawbytes ) );
    CUDA_CALL( cudaMemset( d_x, 0xFF, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_y, 0xFF, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_d, 1*nsamples*sizeof(float), cudaHostAllocDefault ) );
    printf("Pointers: d_raw_xy=%p, d_x=%p, d_y=%p, h_d=%p\n", d_raw_xy, d_x, d_y, h_d);
    /////////////////////////////////////////////////////////////////////////////////

    //const int lst_threadsPerBlock[11] = { 32, 8, 16, 24, 32, 56, 64, 96, 128, 256, 512 };
    const int lst_threadsPerBlock[3] = {32, 32, 64};
    for (size_t i = 0; i < sizeof(lst_threadsPerBlock)/sizeof(int); i++) {
        printf("Speed with %d threads/block:\n", lst_threadsPerBlock[i]);

        // Data generator kernel
        threadsPerBlock = lst_threadsPerBlock[i]; // cudaDevProp.warpSize;
        numBlocks = div2ceil(nrawbytes,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_decode_2bit2ch_testdata <<< numBlocks, threadsPerBlock>>> ( d_raw_xy, nrawbytes );
        CUDA_CHECK_ERRORS("cu_decode_2bit2ch_testdata");
        CUDA_TIMING_STOP(tstop, tstart, 0, "testdata", 2*nsamples);

        // Data decode-and-split kernel
        numBlocks = div2ceil(nrawbytes,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_decode_2bit2ch_split <<< numBlocks, threadsPerBlock>>> ( d_raw_xy, (float2*)d_x, (float2*)d_y, nrawbytes );
        CUDA_CHECK_ERRORS("cu_decode_2bit2ch_split");
        CUDA_TIMING_STOP(tstop, tstart, 0, "2bit2ch_split", 2*nsamples);
    }

    /////////////////////////////////////////////////////////////////////////////////

    // Some extra info
    printf("Layout: %zd blocks x %zd threads\n", numBlocks, threadsPerBlock);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );
    printf("nsamples = %zd\n", nsamples);

    CUDA_CALL( cudaMemcpy(h_d, d_raw_xy, 32, cudaMemcpyDeviceToHost) );
    printf("raw xy : %2X %2X %2X %2X ...\n", ((uint8_t*)h_d)[0], ((uint8_t*)h_d)[1], ((uint8_t*)h_d)[2], ((uint8_t*)h_d)[3]);

    CUDA_CALL( cudaMemcpy(h_d, d_x, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("x  : %.1f %.1f %.1f %.1f ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("%.1f %.1f %.1f %.1f\n", h_d[nsamples-4],h_d[nsamples-3],h_d[nsamples-2],h_d[nsamples-1]);

    CUDA_CALL( cudaMemcpy(h_d, d_y, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("y  : %.1f %.1f %.1f %.1f ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("%.1f %.1f %.1f %.1f\n", h_d[nsamples-4],h_d[nsamples-3],h_d[nsamples-2],h_d[nsamples-1]);

    return 0;
}

#endif // BENCH

#endif // DECODER_2B32F_SPLIT_KERNELS_CU
