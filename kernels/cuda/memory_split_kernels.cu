/////////////////////////////////////////////////////////////////////////////////////
//
// Kernel to "split" interleaved sample data [x0 y0 x1 y1 x2 y2 ... xn yn] into
// two output arrays [x0 x1 x2 .. xn] and [y0 y1 y2 ... yn].
//
// (C) 2015 Jan Wagner
//
// cu_split_2chan_f2_2f  : 29 Gs/s on Titan X for >=32 th/blk (faster 33 Gs/s after 1st run)
// cu_split_2chan_f4_2f2 : 33 Gs/s on Titan X for >=16 th/blk (same 33 Gs/s after 1st run)
//
// Note that 33Gsamp/s * 4 byte/samp * 2 r/w = 264 GB/s of semi-random I/O,
// which is 78% of the theoretical max. sequential(!) 336.5 GB/s
//
/////////////////////////////////////////////////////////////////////////////////////
// $ nvcc -DBENCH=1 -gencode=arch=compute_52,code=sm_52 memory_split.cu # test
/////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <inttypes.h>

/**
 * cu_split_2chan_f4_2f2
 * @param src Pointer to float4 of [x0 y0 x1 y1 ...] data
 * @param dst_x Pointer to float2 to receive [x0 x1 ...] data
 * @param dst_y Pointer to float2 to receive [y0 y1 ...] data
 * @param N The expected length (in sample) of dst_x or dst_y
 */
__global__ void cu_split_2chan_f4_2f2(const float4* __restrict__ src, float2* __restrict__ dst_x, float2* __restrict__ dst_y, const size_t N)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N/2) { return; }
        float4 v = src[i];
        float2 x = {v.x, v.z};
        float2 y = {v.y, v.w};
        __syncthreads();
        dst_x[i] = x;
        dst_y[i] = y;
        return;
}

/**
 * cu_split_2chan_f2_2f
 * @param src Pointer to float2 of [x0 y0 x1 y1 ...] data
 * @param dst_x Pointer to float to receive [x0 x1 ...] data
 * @param dst_y Pointer to float to receive [y0 y1 ...] data
 * @param N The expected length (in sample) of dst_x or dst_y
 */
__global__ void cu_split_2chan_f2_2f(const float2* __restrict__ src, float* __restrict__ dst_x, float* __restrict__ dst_y, const size_t N)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N) { return; }
        float2 v = src[i];
        dst_x[i] = v.x;
        dst_y[i] = v.y;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

__global__ void cu_split_2chan_testdata(float2* dst, const size_t N)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N) { return; }
        float2 v = { __int2float_rz(2*i+1), __int2float_rz(2*i+2) };
        dst[i] = v;
}

int main(void)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t threadsPerBlock, numBlocks;
    size_t maxphysthreads;
    size_t nsamples;
    float *d_xy;
    float *d_x;
    float *d_y;
    float *h_d;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input
    nsamples = 512*1024*1024;
    CUDA_CALL( cudaMalloc( (void **)&d_xy,  2*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_x,   1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_y,   1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_xy, 0x00, 2*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_x,  0x00, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_y,  0x00, 1*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_d, 2*nsamples*sizeof(float), cudaHostAllocDefault ) );
    printf("Pointers: d_xy=%p, d_x=%p, d_y=%p, h_d=%p\n", d_xy, d_x, d_y, h_d);

    /////////////////////////////////////////////////////////////////////////////////


    const int lst_threadsPerBlock[11] = { 32, 8, 16, 24, 32, 56, 64, 96, 128, 256, 512 };
    for (int i = 0; i < 9; i++) {
        printf("Speed with %d threads/block:\n", lst_threadsPerBlock[i]);

        // Data generator kernel
        threadsPerBlock = lst_threadsPerBlock[i]; // cudaDevProp.warpSize;
        numBlocks = div2ceil(nsamples,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_split_2chan_testdata <<< numBlocks, threadsPerBlock>>> ( (float2*)d_xy, nsamples );
        CUDA_CHECK_ERRORS("cu_split_2chan_testdata");
        CUDA_TIMING_STOP(tstop, tstart, 0, "testdata", 2*nsamples);

        // Data splitter kernel
#if 1
        numBlocks = div2ceil(nsamples,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_split_2chan_f2_2f <<< numBlocks, threadsPerBlock>>> ( (float2*)d_xy, (float*)d_x, (float*)d_y, nsamples );
        CUDA_CHECK_ERRORS("cu_split_2chan_f2_2f");
        CUDA_TIMING_STOP(tstop, tstart, 0, "split_2chan_float2", 2*nsamples);
#else
        numBlocks = div2ceil(nsamples/2,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_split_2chan_f4_2f2 <<< numBlocks, threadsPerBlock>>> ( (float4*)d_xy, (float2*)d_x, (float2*)d_y, nsamples );
        CUDA_CHECK_ERRORS("cu_split_2chan_f4_2f2");
        CUDA_TIMING_STOP(tstop, tstart, 0, "split_2chan_float4", 2*nsamples);
#endif
    }

    /////////////////////////////////////////////////////////////////////////////////

    // Some extra info
    printf("Layout: %zd blocks x %zd threads\n", numBlocks, threadsPerBlock);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );
    printf("nsamples = %zd, 2*nsamples = %zd\n", nsamples, 2*nsamples);

    CUDA_CALL( cudaMemcpy(h_d, d_xy, 2*nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("xy : %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f ... %.1f %.1f\n",
           h_d[0],h_d[1],h_d[2],h_d[3],h_d[4],h_d[5],h_d[6],h_d[7],
           h_d[2*nsamples-2], h_d[2*nsamples-1]
    );

    CUDA_CALL( cudaMemcpy(h_d, d_x, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("x  : %.1f %.1f %.1f %.1f ... %.1f\n", h_d[0],h_d[1],h_d[2],h_d[3],h_d[nsamples-1]);

    CUDA_CALL( cudaMemcpy(h_d, d_y, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("y  : %.1f %.1f %.1f %.1f ... %.1f\n", h_d[0],h_d[1],h_d[2],h_d[3],h_d[nsamples-1]);

    return 0;
}

#endif

