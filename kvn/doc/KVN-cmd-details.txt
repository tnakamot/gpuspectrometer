
*** Telescope setup files ***************************************************

The configuration file format is described in 2006KVN-SPC0007_KVN_LOC_ConfigFile_ver100_JK.doc

Multiple "Device Name" are available. For a GPU spectrometer the important ones are "PFB" (Digital
Filter) and "DSM" (Digital Spectrometer), and their monitoring port counterparts "mPFB" and "mDSM".

$ENTRY:DEVICE
	$DEV:PFB
		IPAD=192.168.1.1, PORT=5653, VSIID=FFFF, CNTL=ON, TOUT=3
	$DEV:DSM
		IPAD=192.168.1.2, PORT=5653, VSIID=FFFD, CNTL=ON, TOUT=3
$END
$ENTRY:KEYWORD
        $SECT:DFB_Pnl
                $PLST:REFARENCE_SELECT
                $PLST:TVR_INSEL
                $PLST:MODE
                $PLST:DATA_SELECT
                $PLST:SAMPLE_PHASE
                $PLST:FILTER_COEFFICIENT
                $PLST:TAP_MODE
                $PLST:LSB_POSITION
                $PLST:ID-1_OUT_ASSIGNE
        $END
        $SECT:DSM_Pnl
                $PLST:TVR_INSEL
                $PLST:NARROW_MODE_MODE
                $PLST:NARROW_MODE_DATA_SELECT
                $PLST:NARROW_MODE_BAND_WIDTH
                $PLST:REFARENCE_SELECT
                $PLST:WINDOW_FUNCTION
                $PLST:IP_LENGTH
        $END
$END

In current KVN FieldSystem operation however the config file (and macros
on the config files) are being used less, and are replaced by DCMD direct
device commands over the M&C interface.


*** Spectrometer related DCMD commands **************************************

The commands currently used for the hardware DSM and hardware DFB are below.
Integration of a new software-based spectral backend into the current telescope
control system is easiest if the existing hardware command set is supported.

  kvnuser@KVNTN:/home/kvnuser/KVN/Log/KCS/  das.log

  Setup using a default config file and running the macro 'kvn_64' defined
  in the config file:

    STOP
    PCMD,*,kvn_64
    STRT

  Setup via direct commands:
 
    STOP
    DCMD,DSM,SEL_CORIPLEN=512;       # set SEL_CORIPLEN to 512; same "<cmd,var>=<x>;" syntax also for other commands
    DCMD,DSM,SEL_CORIPLEN?;          # return current setting; similar "<cmd,var>?;" syntax also for all other commands 
    DCMD,DSM,SEL_MAINPORT=1;
    DCMD,DSM,SEL_CORWINDOW=HAMMING;
    DCMD,DSM,SET_CORIPNUM=0;
    STRT

    STOP
    DCMD,DSM,SEL_CORMODE=NARROW;
    DCMD,DSM,SEL_NRWSTREAM1=2;           # filter output #2 to Signal 1
    DCMD,DSM,SEL_NRWSTREAM2=6;           # filter output #6 to Signal 2
    ...
    DCMD,DSM,SEL_COROUTSTREAM=ALL:OFF;   # de-select all spectral outputs, i.e., produce no output data
    DCMD,DSM,SEL_COROUTSTREAM=Auto1:ON;  # select auto-corr of Signal 1
    DCMD,DSM,SEL_COROUTSTREAM=Auto2:ON;  # select (also) auto-corr of Signal 2
    DCMD,DSM,SEL_COROUTSTREAM=Cros12:ON; # select (also) the cross-corr between Signal 1 and Signal 2
    DCMD,DSM,SEL_COROUTSTREAM=Auto3:ON;  # ...
    DCMD,DSM,SEL_COROUTSTREAM=Auto4:ON;
    DCMD,DSM,SEL_COROUTSTREAM=Cros34:ON;
    STRT

    DCMD,DFB,SET_FILFILE16=W016C280.FCF;
    DCMD,DFB,LOAD_FILFILE;

  SEL_CORIPLEN    integration time in milliseconds; apparently supported are 10.24, 102.4, 512, 1024
  SET_CORIPNUM    0 for infite run time, other values N>0 to auto-stop after N spectra
  SEL_MAINPORT    (the sampler device, one out of ADS#1 to ADS#4?)
  SEL_CORMODE     NARROW to get spectrometer input from filters (DFB, 8/16/32/64/128/256 MHz slice out of 512 MHz),
                  WIDE to get the unfiltered full bandwidth (512 MHz)


*** Coefficients files ******************************************************

The filter coefficients are in a file, generated with utilities in e.g.

  kvnuser@KVNTN:/home/kvnuser/KVN/DAS/DFB/coef_tools/

The hardware supports filter bandwidths of 8/16/32/64/128/256 MHz,
and filter center frequencies at 
   fc = (BW/2) + n*BW   where n=0...max
                        such that fc <= 512MHz-BW/2
For a 8 MHz bandwidth, centers can be 4 MHz, 12 MHz, 20 MHz, ..., 492 MHz,
and for 16 MHz, centers can be at 8 MHz, 24 MHz, 40 MHz, ..., 504 MHz,
and for 256 MHz, centers at 128 MHz or 384 MHz.

Filter coeffs appear to be cosine-shifted Hamming-windowed sinc() with 
N=1024 filter points over 512 MHz.

Coefficients are time-reversed and one element is dropped, resulting in a
total of M = N-1 = 1023 coefficients. For example, direct coefficients
of [0,1,2,3,4,5,6,7,8,9] would be stored as [9,8,7,6,5,4,3,2,1].

Ready coeff files have 1023 coefficients, one per text file line,

  kvnuser@KVNTN:~/KVN/DAS/DFB/coef_tools/COEF0$ wc -l *.freq
    1023 W008C004.freq
    1023 W008C012.freq
    1023 W008C020.freq
    1023 W008C028.freq
    1023 W008C036.freq
    1023 W008C044.freq
    1023 W008C052.freq
    1023 W008C060.freq

  kvnuser@KVNTN:~/KVN/DAS/DFB/coef_tools/COEF0$ less W016C248.freq
    -7.678743069630578037e-06
    3.117473946472959848e-04
    6.896546667434750948e-05
    -6.144556766227660333e-04
    -1.901114289731868298e-04
    ...
    ...
    -1.901114289731868298e-04
    -6.144556766227660333e-04
    6.896546667434750948e-05
    3.117473946472959848e-04
    -7.678743069630578037e-06

For hardware DSM these are converted into some internal binary format
and are stored in files *.fcf. The format depends on the firmware PFB/DFB
implementation and is not relevat to the GPU spectrometer.


