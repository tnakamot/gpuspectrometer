#include "logger.h"
#include "commands/common.hxx"

#include <stdlib.h>

#include <algorithm>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(CONF);

static bool parseCorOutstream(SpectralSettings* cfg, std::string darg);

static const int valid_cross_pairs[] = { 12, 34, 56, 78, 910, 1112, 1314, 1516 };


int MCMsgCommandResponse_CONF::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    if (args.size() != 1) {
        L_(lerror) << "CONF had " << args.size() << " instead of expected 1 parameter";
        out.push_back(CMD_RET_PARAMERROR);
        out.push_back("CONF");
        return -1;
    }

    // Decode; examples are "CONF,SEL_CORIPLEN?;" for a query and "CONF,SEL_CORIPLEN=512;" for a request
    const std::string& command = args[0];

    // Ugly way to decode the command string
    std::string darg, ccmd = command;
    ccmd.erase(std::remove(ccmd.begin(), ccmd.end(), '?'), ccmd.end());
    ccmd.erase(std::remove(ccmd.begin(), ccmd.end(), ';'), ccmd.end());
    size_t p = ccmd.find('=');
    if (p != std::string::npos) {
        darg = ccmd.substr(p+1);
        ccmd = ccmd.substr(0, p);
    }
    std::transform(ccmd.begin(), ccmd.end(), ccmd.begin(), ::toupper);
    L_(linfo) << "got CONF cmd='" << ccmd << "', arg='" << darg << "', isQuery=" << isQuery;

    // Get a copy of current editable config
    SpectralSettings newcfg = k.spectralCfg();

    // Apply command to the copy of config
    bool valid = true;
    std::stringstream dresp;
    if (ccmd == "SEL_CORIPLEN") {
        if (!isQuery && !darg.empty()) {
            newcfg.Tint_wish_s = /*msec*/atof(darg.c_str()) * 1e-3;
        }
        dresp << ccmd << "=" << std::fixed << std::setw(2) << (newcfg.Tint_wish_s*1000);
    } else if (ccmd == "SEL_CORLEN") {
        if (!isQuery && !darg.empty()) {
            newcfg.nchan = atoi(darg.c_str());
        }
        dresp << ccmd << "=" << newcfg.nchan;
    } else if (ccmd == "SEL_CORWINDOW") {
        if (!isQuery && !darg.empty()) {
            newcfg.window_type = SpectralSettings::windowType(darg.c_str());
        }
        dresp << ccmd << "=" << SpectralSettings::windowType(newcfg.window_type);
    } else if (ccmd == "SEL_COROUTSTREAM") {
        if (!isQuery && !darg.empty()) {
            valid = parseCorOutstream(&newcfg, darg);
            dresp << ccmd << "=" << darg;
            // TODO: what to reply on query "SEL_COROUTSTREAM?"
        } else {
            dresp << ccmd << "=" <<k.spectralCfg().strEnabledStreams();
        }
    } else {
        L_(lerror) << "CONF with unknown target " << ccmd;
        dresp << ccmd << "=" << darg;
        valid = false;
    }

    // Check validity of config
    valid = newcfg.validate() && valid;
    if (!valid) {
        out.push_back(CMD_RET_PARAMERROR);
        out.push_back("CONF");
        return -1;
    }

    // Update internals and copy config into storage
    newcfg.recalculate();
    k.spectralCfg() = newcfg; // TODO: might need mutex?
    L_(ldebug) << "CONF updated spectral config to " << newcfg;

    // Response
    out.push_back(CMD_RET_DONE);
    out.push_back("CONF");
    out.push_back(dresp.str());

    return 0;
}

static bool parseCorOutstream(SpectralSettings* cfg, std::string darg)
{
    std::transform(darg.begin(), darg.end(), darg.begin(), ::toupper); // TODO: go back to case-sensitive?

    std::string::iterator sep = std::find(darg.begin(), darg.end(), ':');
    if (sep == darg.end() || (sep + 1) == darg.end()) {
        L_(lerror) << "Syntax error in SEL_COROUTSTREAM argument, expected format <ALL|AUTOx|CROSSxy>:<ON|OFF> but got '" << darg << "'";
        return false;
    }

    std::string channel(darg.begin(), sep);
    std::string onoff(sep + 1, darg.end());
    bool isOn = (onoff == "ON");

    const int Nmaxchan = sizeof(cfg->enableAutocorr)/sizeof(bool);
    const int Nmaxcross = sizeof(cfg->enableCrosscorr)/sizeof(bool);

    if (channel == "ALL") {

        for (size_t n = 0; n < Nmaxchan; n++) {
            cfg->enableAutocorr[n] = isOn;
        }
        for (size_t n = 0; n < Nmaxcross; n++) {
            cfg->enableCrosscorr[n] = isOn;
        }

    } else if ((channel.length() >= 5) && (channel.substr(0,4) == "AUTO")) {

        int ch = atoi(channel.substr(4,std::string::npos).c_str());
        if (ch <= 0 || ch > Nmaxchan) {
            L_(lerror) << "The output stream argument " << channel << ", parsed as channel " << ch << " is out of range (1.." << Nmaxchan << ")!";
            return false;
        }
        cfg->enableAutocorr[ch-1] = isOn;

    } else if ((channel.length() >= 7) && (channel.substr(0,5) == "CROSS")) {

        // TODO: DSM supports only ch 1 x ch 2, ch 3 x ch 4, ...
        //       however the spectrometer could support ch 1 x ch 4 or other combinations
        // TODO: DSM CROSSxy syntax supports only up to 9 channels, what about syntax for VDIF with 16/32 channels?

        int chpair = atoi(channel.substr(5,std::string::npos).c_str());
        size_t productnr = -1;
        for (size_t n=0; n<sizeof(valid_cross_pairs)/sizeof(valid_cross_pairs[0]); n++) {
            if (chpair == valid_cross_pairs[n]) {
                productnr = n;
            }
        }

        if (productnr < 0 || productnr >= Nmaxcross) {
            L_(lerror) << "The output stream argument " << channel << ", parsed as stream product '" << chpair
                << " is not a valid channel pair! Only 12, 34, ..., 78, 910, 1112, ..., 1516 are allowed!";
            return false;
        }

        cfg->enableCrosscorr[productnr] = isOn;
        if (isOn) {
            cfg->enableAutocorr[2*productnr] = true;
            cfg->enableAutocorr[2*productnr+1] = true;
        }

    } else {

        L_(lwarning) << "Unknown or incomplete output stream argument " << darg << ", parsed channel name '" << channel << "' with on=" << isOn;
        return false;

    }

    return true;
}

