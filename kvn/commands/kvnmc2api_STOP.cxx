#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(STOP);

int MCMsgCommandResponse_STOP::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    bool success;

    // Stop GPU first
    success = k.stopGPU();

    // Always stop VDIF RX even if GPU not successfully stopped
    k.stopRX();

    // Response
    if (success) {
        out.push_back(CMD_RET_DONE);
    } else {
        out.push_back(CMD_RET_CMDERROR);
    }
    out.push_back("STOP");

    return 0;
}

