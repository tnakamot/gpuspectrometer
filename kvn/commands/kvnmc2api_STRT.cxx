#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(STRT);

int MCMsgCommandResponse_STRT::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    bool success;

    // Launch VDIF capture first (blocks until VDIF properties have been detected, or until timeout)
    success = k.startRX();

    // Launch GPU processing, based on current settings and detected VDIF properties
    if (success) {
        success = k.startGPU();
    }

    // Response
    if (success) {
        out.push_back(CMD_RET_DONE);
    } else {
        out.push_back(CMD_RET_CMDERROR);
        //out.push_back(CMD_RET_FAIL);  // not DSM compatible, but more appropriate? i.e. correct command but exec failed
    }
    out.push_back("STRT");

    return 0;
}

