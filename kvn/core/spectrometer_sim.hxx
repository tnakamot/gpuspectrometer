/**
 * Plain implemetation of SpectrometerInterface that receives
 * only M&C commands but does not really do anything on GPU.
 *
 */
#ifndef SPECTROMETER_SIM_HXX
#define SPECTROMETER_SIM_HXX

#include "core/common.hxx"
#include "core/spectrometerinterface.hxx"
#include "network/udpvdifreceiver.hxx"
#include "datarecipients/spectraloutput.hxx"
#include "datarecipients/commandrecipient.hxx"

#include <boost/thread/mutex.hpp>

#include <string>

class SpectrometerSim : public SpectrometerInterface
{
    friend class SpectrometerTCPServer;

    private:
        SpectrometerSim();

    public:
        /** C'stor with TCP/UDP port numbers to listen on */
        SpectrometerSim(int cmdPort, int monPort, int outputPort, int vdifPort);

        /** Method that starts the spectrometer M&C listener loop (default: non-blocking, background) */
        void startMC(bool blocking=false);

        /** Handle a command string and return a response string. */
        std::string handleCommand(std::string cmd); // impl. CommandRecipient::handleCommand() iface

        /** Stop the M&C listener loop; dummy call for now */
        void stopMC() { }

    public:
        /** Set/change UDP port **/
        void setVDIFPort(int vdifPort) { m_udpPort = vdifPort; }

        /** Set/change VDIF frame offset (PSN length in bytes) **/
        void setVDIFOffset(int vdifOffset) { m_vdifOffset = vdifOffset; }

        /** Start background VDIF data receiption pump */
        bool startRX();

        /** Stop background VDIF data receiption pump */
        bool stopRX();

    public:
        /**
         * Start GPU processing.
         * The current spectral settings are copied into "active" settings
         * and spectral processing is initialized and started with that copy.
         * Note that active settings can not be changed on the fly, i.e.,
         * settings changed while GPU processing is already ongoing will
         * become activated only after stopRX() followed by startRX().
         */
        bool startGPU();

        /** Stop GPU processing */
        bool stopGPU();

        /** Underallocation of GPU memory to allow multiple instances without OutOfMemory */
        void setUnderallocationFactor(int factor) {
            if (factor >= 1 && factor <= 512) {
                m_cfg.pcfg.underallocfactor = factor;
            }
        }

        // TODO
        bool startProcessing() { return true; }
        bool stopProcessing() { return true; }

        /** Enable or disable spectral output to a file. Output to TCP clients is not affected. */
        void enableFileOutput(bool enable) { /* no SpectralOutput object so no output to enable/disable */ }

   public:
        /** Direct read/write accessor to spectral settings */
        SpectralSettings& spectralCfg();

        /** Direct read/write accessor to processing settings */
        ProcessingSettings& processingCfg();

    private:
        /** Pin or un-pin memory buffers */
        void memoryLockBuffers(bool do_pin);

    private:
        boost::mutex m_cmdmutex;
        SpectrometerMonCtrl m_mciface;
        SpectrometerTCPServer m_ctrlListener;
        SpectrometerTCPServer m_monitorListener;

        UDPVDIFReceiver m_vdifRx;
        int m_udpPort;
        int m_vdifOffset;
        bool m_rx_started;

    private:
        boost::mutex m_cfgmutex; // for safely copying m_cfg --> m_cfg_active
        SpectrometerConfig m_cfg;
        SpectrometerConfig m_cfg_active;

};

#endif // SPECTROMETER_SIM_HXX
