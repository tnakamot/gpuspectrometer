#ifndef BACKEND_GPU_IMPL_HXX
#define BACKEND_GPU_IMPL_HXX

#include "core/defines.hxx"
#include "core/spectrometerconfig.hxx"
#include "arithmetic_winfunc_kernels.h"     // def. cu_window_cb_params_t
#include "histogram_kernels.h"              // def. PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_BIN_COUNT, hist_t

class ResultRecipient;

#include <cuda.h>
#include <cufft.h>

#include <sys/time.h>

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

#define CUDA_CFG_POOL_MAXWORKERS 8*16

/** Data pointers and auxiliary data for a single specific GPU and Stream */
struct cuda_worker_t : private boost::noncopyable
{

    /* GPU and Stream and FFT setup */
    int            g;          // CUDA GPU device number
    int            s;          // CUDA stream number
    int            n;          // Worker slot index
    cudaDeviceProp devprop;    // Properties of the target GPU device
    cudaStream_t   sid;        // CUDA stream handle on the target GPU device
    cudaStream_t   d2h;        // CUDA stream handle for device-to-host mem copy
    cudaStream_t   h2d;        // CUDA stream handle for host-to-device mem copy (overlapped bidirectional xfer)
    cufftHandle    cufftplan;  // Handle to CUFFT plan on the target GPU device, assigned to target stream

    /* Raw input sample data (headerless, with a fill pattern for missing frames) */
    struct timeval rawbuf_midtime; // Timestamps (from VDIF) at midpoint of integration period
    unsigned char* d_rawbuf;   // Pointer to raw data on GPU
    unsigned char* h_rawbuf;   // Pointer to raw data on host side (will be updated by VDIF data pusher to point to newest block)
    size_t         rawbuflen;  // Length in bytes for raw input data of one integration period

    /* On-GPU arrays */
    hist_t*        d_histogram256;
    //float*         d_fft_multiband_in;            // input to r2c FFT with all bands interleaved same way as in VDIF payload  // NOTE: failed because CUDA FFT r2c input ptr must be 8-byte aligned not 4-byte (damn!!)
    float*         d_fft_bands_in[MAX_SUBBANDS];    // inputs to r2c FFT with bands separated out into own arrays at 2-bit decode stage
    float*         d_fft_out[MAX_SUBBANDS];         // output of r2c FFT nchan+1 nyquist (type is recast cufftComplex) for each selected/enabled VDIF band
    cu_window_cb_params_t  h_cufft_userparams;      // extra user params in cuFFT Callback
    cu_window_cb_params_t* d_cufft_userparams;      // device copy of -"-, one copy per GPU is enough
    hist_t*        d_partial_histogram256;

    /* Output arrays */
    hist_t*        h_histogram256;
    float*         d_powspecs;  // accumulated auto&cross power spectra (interleaved {XX,Re XY,Im XY, YY) of all FFTSPEC_MAX_SUBBANDS-pairs side-by-side
    float*         h_powspecs;  // -"- on host
    double         spec_weight; // weights of each spectrum = valid data / total data; -1 if invalid spectrum
    size_t         powspecslen; // length of d_powspecs in byte; all auto&cross power spectra included

    /* Performance tracking */
    cudaEvent_t evt_start;    // Event for wallclock timestamp when processing of segment started
    cudaEvent_t evt_stop;     // -"- finished
    cudaEvent_t evt_raw_overwriteable; // Event happens after raw input data area can be freely overwritten
    cudaEvent_t evt_spectrum_avail;    // Event happens after spectrum completely copied to host
    cudaEvent_t evt_kstart;            // kernel timing
    cudaEvent_t evt_kstop;             // kernel timing
    cudaEvent_t evt_sync_with_yield;   // generic sync-up, without timing, and busy-poll replaced by yield (flag cudaEventBlockingSync)

    /* Settings applicable to the data (private copy) */
    SpectrometerConfig cfg;

    /* Output object (shared) */
    ResultRecipient* recipient;

    /* CPU thread that handles the processing */
    boost::thread *workerthread;
    boost::mutex   statemutex;
    boost::mutex   recipientmutex;
    boost::condition_variable newdatacond;
    boost::condition_variable newdataonGPUcond;
    enum WorkerState { Idle=0, Reserved=1, InputCopied=2, WorkLaunched=3, Done=4 };
    WorkerState state;
    bool initialized;

    /* Defaults */
    cuda_worker_t() {
        g = -1; s = -1;
        d_rawbuf = NULL;
        rawbuflen = 0;
        workerthread = NULL;
        recipient = NULL;
        initialized = false;
    }

};

struct cuda_config_pool_t : private boost::noncopyable
{
    /* Mutex for changes to the overall pool; pool entries have own mutexes */
    boost::mutex mutex;

    /* Individual workers and their settings */
    int nworkers;
    cuda_worker_t pool[CUDA_CFG_POOL_MAXWORKERS];

    /* Output object (shared). The default to use when workers are created. */
    ResultRecipient* common_recipient;

    /* Defaults */
    cuda_config_pool_t()
    {
        nworkers = 0;
        common_recipient = NULL;
        for (int i = 0; i < CUDA_CFG_POOL_MAXWORKERS; i++) {
            pool[i].state = cuda_worker_t::Idle;
        }
    }
};

#endif // BACKEND_GPU_IMPL_HXX
