#ifndef DEFINES_HXX
#define DEFINES_HXX

#ifndef WINDOW_FUNCTION_NONE
    #define WINDOW_FUNCTION_NONE 0
    #define WINDOW_FUNCTION_HANN 1
    #define WINDOW_FUNCTION_HAMMING 2
    #define WINDOW_FUNCTION_HFT248D 3
    #define WINDOW_FUNCTION_CUSTOM  4
#endif

#define MAX_GPUS      2   // Max nr of GPUs to use in parallel
#define MAX_STREAMS   2   // Max nr of CUDA streams to create on each GPU
#define MAX_SUBBANDS 16   // Input data, maximum nr of subbands (aka IFs aka channels) to support

#define SPEC_INVALID      -1.0

#define TCP_CONTROL_PORT_DEFAULT  60100
#define TCP_MONITOR_PORT_DEFAULT  60101
#define TCP_SPECTRAL_PORT_DEFAULT 60450
#define UDP_VDIF_PORT_DEFAULT     46227

#define OBS_ROOT_DIR       "/home/DAS_LOC/OBS/"

#endif // DEFINES_HXX
