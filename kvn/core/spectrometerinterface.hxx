#ifndef SPECTROMETERINTERFACE_HXX
#define SPECTROMETERINTERFACE_HXX

#include "core/common.hxx"
#include "network/udpvdifreceiver.hxx"
#include "datarecipients/spectraloutput.hxx"
#include "datarecipients/commandrecipient.hxx"

#include <boost/thread/mutex.hpp>

#include <string>

class SpectrometerInterface : public CommandRecipient
{
    friend class SpectrometerTCPServer;

    public:
        SpectrometerInterface() { }

        /** C'stor with TCP/UDP port numbers to listen on */
        SpectrometerInterface(int cmdPort, int monPort, int outputPort, int vdifPort) { }

        virtual ~SpectrometerInterface() { }

        /** Method that starts the spectrometer M&C listener loop (default: non-blocking, background) */
        virtual void startMC(bool blocking=false) = 0;

        /** Stop the M&C listener loop; dummy call for now */
        virtual void stopMC() = 0;

        /** Handle a command string and return a response string. */
        virtual std::string handleCommand(std::string cmd) = 0; // impl. CommandRecipient::handleCommand() iface

    public:

        /** Set/change UDP port **/
        virtual void setVDIFPort(int vdifPort) = 0;

        /** Set/change VDIF frame offset (PSN length in bytes) **/
        virtual void setVDIFOffset(int vdifOffset) = 0;

        /** Start background VDIF data receiption pump */
        virtual bool startRX() = 0;

        /** Stop background VDIF data receiption pump */
        virtual bool stopRX() = 0;

    public:
        /**
         * Set up the GPU(s) for data processing.
         * The current spectral settings are copied into "active" settings
         * and spectral processing is initialized and started with that copy.
         * Note that active settings can not be changed on the fly, i.e.,
         * settings changed while GPU processing is already ongoing will
         * become activated only after stopRX() followed by startRX().
         */
        virtual bool startGPU() = 0;

        /** Stop GPU processing and release all resources */
        virtual bool stopGPU() = 0;

        /** Start routing data to already started GPU(s) for processing */
        virtual bool startProcessing() = 0;

        /** Underallocation of GPU memory to allow multiple instances without OutOfMemory */
        virtual void setUnderallocationFactor(int) = 0;

        /** Stop routing data to the started GPU(s). */
        virtual bool stopProcessing() = 0;

        /** Enable or disable spectral output to a file. Output to TCP clients is not affected. */
        virtual void enableFileOutput(bool) = 0;

   public:
        /** Direct read/write accessor to spectral settings */
        virtual SpectralSettings& spectralCfg() = 0;

        /** Direct read/write accessor to processing settings */
        virtual ProcessingSettings& processingCfg() = 0;

};

#endif // SPECTROMETERINTERFACE_HXX
