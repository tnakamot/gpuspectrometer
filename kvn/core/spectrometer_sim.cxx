#include "logger.h"

#include "core/common.hxx"
#include "core/spectrometer_sim.hxx"
#include "kvnmc2api/kvnmc_defs.hxx"
#include "network/udpvdifreceiver.hxx"

#include <iostream>
#include <iterator>
#include <vector>

/** C'stor with TCP port numbers to listen on */
SpectrometerSim::SpectrometerSim(int cmdPort, int monPort, int outputPort, int vdifPort)
  : m_ctrlListener(*this,cmdPort), m_monitorListener(*this,monPort), m_udpPort(vdifPort)
{
    m_rx_started = false;
    m_vdifOffset = 0;
}

/** Direct read/write accessor to spectral settings */
SpectralSettings& SpectrometerSim::spectralCfg()
{
    return m_cfg.scfg;
}

/** Direct read/write accessor to spectral settings */
ProcessingSettings& SpectrometerSim::processingCfg()
{
    return m_cfg.pcfg;
}

/** Handle a command string and return a response string. Invoked from SpectrometerTCPServer listener only. */
std::string SpectrometerSim::handleCommand(std::string cmd)
{
    boost::mutex::scoped_lock cmdlock(m_cmdmutex);

    // Let the M&C class parse the command
    m_mciface.load(cmd);
    L_(linfo) << m_mciface.getCommandName() << " : handling command";

    // Let the M&C class now execute the command (factory method)
    std::vector<std::string> respItems;
    int rc = m_mciface.handle(respItems, *this);

    // Collect the M&C response to send back
    std::stringstream resp;
    std::copy(respItems.begin(), respItems.end()-1, std::ostream_iterator<std::string>(resp,","));
    resp << respItems.back();
    L_(linfo) << m_mciface.getCommandName() << " : handler returned rc=" << rc << ", response=" << resp.str();

    return resp.str();
}

/** Method that starts the spectrometer M&C listener loop (default: non-blocking, background) */
void SpectrometerSim::startMC(bool blocking)
{
    L_(linfo) << "Started version " << KVNMC2API_VERSION_STR;

    L_(linfo) << "Control acceptor is listening on TCP/IPv4 port " << m_ctrlListener.getPort();
    m_ctrlListener.run(blocking);

    L_(linfo) << "Monitor acceptor is listening on TCP/IPv4 port " << m_monitorListener.getPort();
    m_monitorListener.run(blocking);

    L_(linfo) << "Spectraloutput acceptor not available in SpectrometerSim implementation.";
}

/** Start background VDIF data receiption pump */
bool SpectrometerSim::startRX(void)
{
    int timeout_sec = 2;

    if (m_rx_started) {
        L_(lwarning) << "StartRX(): Reception was already running! Restarting...";
        stopRX();
    }

    int Nbufs; // should be >= 2 * nstreams * nGPUs
    Nbufs = 2 * m_cfg.pcfg.nGPUs * m_cfg.pcfg.nstreams;
    if (Nbufs < 8) {
        Nbufs = 8;
    }

    if (!m_cfg.scfg.validate()) {
        L_(lerror) << "Cannot start because spectral settings (integration time, #channels) are invalid";
        return false;
    }
    if (!m_cfg.pcfg.validate()) {
        L_(lerror) << "Cannot start because processing settings (observation details, GPU infos) are invalid";
        return false;
    }

    // Open port
    if (!m_vdifRx.open(m_udpPort, m_vdifOffset, timeout_sec)) {
        L_(lerror) << "Failed to open VDIF UDP port " << m_udpPort;
        return false;
    }

    // Check details of incoming VDIF
    if (!m_vdifRx.analyze()) {
        L_(lerror) << "Error determing UDP VDIF information from UDP stream";
        m_vdifRx.close();
        return false;
    }
    m_cfg.vdifinfo = m_vdifRx.getVDIFDetails();

    // Apply VDIF details to derive the actual integration time
    m_cfg.recalculate();
    if (m_cfg.pcfg.Tint_s < 10e-3) {
        L_(lerror) << "Cannot start, requested Tint=" << std::setprecision(3) << m_cfg.scfg.Tint_wish_s << "s "
                   << "under " << m_cfg.vdifinfo.framespersec << " fps results in integer-frame "
                   << "Tint=" << std::setprecision(3) << m_cfg.pcfg.Tint_s << "s. "
                   << "The minimum Tint is 0.010s.";
        m_vdifRx.close();
        return false;
    }
    L_(linfo) << "From VDIF infos determined most fitting Tint=" << std::setprecision(3) << std::fixed << m_cfg.pcfg.Tint_s << "s, "
        << m_cfg.pcfg.rawbufsize << "-byte buffer, " << m_cfg.pcfg.rawbufsize/(double)m_cfg.vdifinfo.payloadsize << " frames/buf";

    // Allocate buffers for VDIF reception
    if (!m_vdifRx.allocate(Nbufs, m_cfg.pcfg.rawbufsize)) {
        L_(lerror) << "Error allocating " << std::setprecision(1)
                   << (m_cfg.pcfg.Tint_s*1e3) << "msec receive buffers "
                   << "(" << m_cfg.pcfg.rawbufsize << " byte)";
        m_vdifRx.close();
        return false;
    }

    // Start reception
    m_rx_started = m_vdifRx.startRx();

    return m_rx_started;
}

/** Stop background VDIF data receiption pump */
bool SpectrometerSim::stopRX()
{
    if (!m_rx_started) {
        return true;
    }
    m_vdifRx.stopRx();
    m_vdifRx.deallocate();
    m_vdifRx.close();
    m_rx_started = false;
    return true;
}

/**
 * Start GPU processing.
 * The current spectral settings are copied into "active" settings
 * and spectral processing is initialized and started with that copy.
 * Note that active settings can not be changed on the fly, i.e.,
 * settings changed while GPU processing is already ongoing will
 * become activated only after stopRX() followed by startRX().
 */
bool SpectrometerSim::startGPU()
{
    if (!m_cfg.scfg.validate()) {
        L_(lerror) << "Cannot start GPU because spectral settings are invalid";
        return false;
    }

    lockscope {
        boost::mutex::scoped_lock cmdlock(m_cfgmutex);
        m_cfg_active = m_cfg;
    }

    L_(linfo) << "Started dummy processing, all VDIF data will be discarded";

    return true;
}

/** Stop GPU processing */
bool SpectrometerSim::stopGPU()
{
    L_(linfo) << "Stopped dummy processing";

    return true;
}

/** Pin or un-pin memory buffers */
void SpectrometerSim::memoryLockBuffers(bool do_pin)
{
    int Nbuffers;
    size_t buflen;
    void** buffers;
    buffers = m_vdifRx.getBufferPtrs(Nbuffers, buflen);
    if (buffers != NULL) {
        L_(lwarning) << "Could not vdif::getBufferPtrs()!";
    }
}
