% Generate VDIF file (2-bit, 10000 byte per frame = 40k samples per frame)
% with data from a synthesized spectrum, based on a template spectrum
%
% TODO: fill out VDIF header properly, currently it is all zeros
%
outfile = '../fft/synthetic_2bit.vdif';

%% Template spectrum
% Length
N = 40e3 / 2;
% Noise
sref = 1e-1*(randn(N,1));
% Baseline
sref = sref + 1.5;
% Spectral components
sref = sref + circshift(0.5*gausswin(N,10), +round(0.1*N));
sref = sref + circshift(2.8*gausswin(N,50), -round(0.15*N));
sref = sref + circshift(3*gausswin(N,500), -round(0.25*N));
sref = sref + circshift(15*gausswin(N,500), -round(0.255*N));
% Discard negative power spectral components
sref(sref < 0) = std(sref);
% Randomize the phase
sref = sref .* exp(-1i*random('unif',-pi,pi,N,1));

%% Generate simple time domain signal : compare its spectrum to the reference
R = real(ifft(sref,2*N));
dR = std(R);
if 1,
    figure(1),clf,
    subplot(3,1,1), plot(abs(sref)), title('Reference');
    subplot(3,1,2), plot(R), title('Time domain');
    subplot(3,1,3), plot(abs(fft(R))), title('Resynthesized');
end

%% Generate signal : for initial test
wf = hanning(2*N);
M = 20*(2*N);
sig = R(1:N);
while numel(sig)<M,
    % Randomize phase again, derive time domain signal
    R = sref .* exp(-1i*random('unif',-pi,pi,N,1));
    R = real(ifft(R,2*N));
    sig = [sig; R];
end
if 1,
    K = 8192;
    L = floor(numel(sig)/K);
    s = reshape(sig(1:K*L), [K, L]);
    S = fft(s,[],1);
    S = sum(abs(S),2);
    figure(2),clf,
    subplot(2,1,1), plot(abs(sref)), title('Reference');
    subplot(2,1,2), plot(S), title('Synthesized, average of several spectra');
end

%% Generate output file : 2-bit quantized single-pol 
m = 0;
M = 100e-3 * 2*1e9; % 100ms, 1 Gs/s
nlev = 8;
fdo = fopen(outfile,'w');
vdif_hdr = zeros(32,1);
while m<M,

    mleft = M-m;
    if (mleft <= 0), break, end;
    mleft = min(mleft, 2*N);

    % Randomize phase again, derive time domain signal
    R = sref .* exp(-1i*random('unif',-pi,pi,N,1));
    R = real(ifft(R,2*N));

    % Quantize
    sdev = std(R);
    Rn = round(R / sdev);
    Q = 0*(Rn>=0 & Rn<sdev) ...
                  + 1*(Rn>=sdev) ...
                  + 2*(Rn>-sdev & Rn<0) ...
                  + 3*(Rn<=-sdev);
    
    % Write VDIF header
    fwrite(fdo, vdif_hdr, 'uint8'); 
    
    % Write data block
    fwrite(fdo, Q, 'ubit2'); 
    
    m = m + numel(R);
end
fclose(fdo);

