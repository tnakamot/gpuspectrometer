#!/usr/bin/python
"""
copy_kfftspec.py Version 1.0  20160320 Jan Wagner
Usage: copy_kfftspec.py infile outfile N
"""
import argparse, os, sys, struct
from kfftspecReader import kfftspecReader

if not(len(sys.argv)==4):
    print __doc__
    sys.exit(1)

inname = sys.argv[1]
outname = sys.argv[2]
N = int(sys.argv[3])
kfftspec = kfftspecReader(inname)

of = open(outname, 'w')
for n in range(N):
   raw = kfftspec.getraw_next_spectrum()
   if len(raw)<1:
       break
   of.write(raw)
