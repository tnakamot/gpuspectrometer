#!/bin/bash

########################################################################################

echoerr () {
   echo "$@" 1>&2; 
}

# Func run_gpu(Ngpu, Nstream, Nch, Tint, extra_args)
run_gpu_1ch () {

   rm -f expt001_KVN_Yonsei_2016*.kfftspec

   CMD1="./kfftspec -E expt001 -O jwagner -A KVN_Yonsei $5 "
   CMD2=" /scratch/jwagner/s14db02c_KVNYS_No0006.vdif VDIF_10000-2048-1-2"

   if [ "$1" == "1" ]; then
      CMD="$CMD1 --devices=0 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "2" ]; then
      CMD="$CMD1 --devices=0,1 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "3" ]; then
      CMD="$CMD1 --devices=0,1,2 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "4" ]; then
      CMD="$CMD1 --devices=0,1,2,3 --nstreams=$2 $3 $4 $CMD2"
   fi

   echoerr "" 
   echoerr " ---- Run with $1 GPU, $2 streams, $3 channels, $4 seconds avg --- "
   echoerr "      cmd: $CMD"
   echoerr ""
   $CMD
}

########################################################################################

run_gpu_2ch () {

   rm -f expt001_KVN_Yonsei_2016*.kfftspec

   CMD1="./kfftspec -E expt001 -O jwagner -A KVN_Yonsei $5 "
   CMD2=" /scratch/jwagner/s14db02c_KVNYS_No0006.vdif VDIF_10000-2048-2-2"

   if [ "$1" == "1" ]; then
      CMD="$CMD1 --devices=0 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "2" ]; then
      CMD="$CMD1 --devices=0,1 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "3" ]; then
      CMD="$CMD1 --devices=0,1,2 --nstreams=$2 $3 $4 $CMD2"
   elif [ "$1" == "4" ]; then
      CMD="$CMD1 --devices=0,1,2,3 --nstreams=$2 $3 $4 $CMD2"
   fi

   echoerr ""
   echoerr " ---- Run with $1 GPU, $2 streams, $3 channels, $4 seconds avg --- "
   echoerr "      cmd: $CMD"
   echoerr ""
   $CMD
}

########################################################################################

echo "######### `hostname` `date -u`" >> perf_run.log
TINT=0.048 # 48ms for ALMA, other for KVN
LFFTs=(1024 2048 4096 8192 16384 32768 65536 131072 262144 524288)
#LFFTs=(1024 2048 4096 8192 16384 32768)
#LFFTs=(65536 131072 262144 524288)

for lfft in ${LFFTs[@]}; do
	# [ run_gpu(Ngpu, Nstream, Nch, Tint, extra_args) ]
	run_gpu_1ch 1 1 $lfft $TINT "" >> perf_run.log
	run_gpu_1ch 1 2 $lfft $TINT "" >> perf_run.log
	run_gpu_1ch 1 4 $lfft $TINT "" >> perf_run.log
	run_gpu_1ch 2 1 $lfft $TINT "" >> perf_run.log
	run_gpu_1ch 2 2 $lfft $TINT "" >> perf_run.log
	run_gpu_1ch 2 4 $lfft $TINT "" >> perf_run.log

	run_gpu_2ch 1 1 $lfft $TINT "" >> perf_run.log
	run_gpu_2ch 1 2 $lfft $TINT "" >> perf_run.log
	run_gpu_2ch 1 4 $lfft $TINT "" >> perf_run.log
	run_gpu_2ch 2 1 $lfft $TINT "" >> perf_run.log
	run_gpu_2ch 2 2 $lfft $TINT "" >> perf_run.log
	run_gpu_2ch 2 4 $lfft $TINT "" >> perf_run.log

	run_gpu_2ch 1 1 $lfft $TINT "--cross" >> perf_run.log
	run_gpu_2ch 1 2 $lfft $TINT "--cross" >> perf_run.log
	run_gpu_2ch 1 4 $lfft $TINT "--cross" >> perf_run.log
	run_gpu_2ch 2 1 $lfft $TINT "--cross" >> perf_run.log
	run_gpu_2ch 2 2 $lfft $TINT "--cross" >> perf_run.log
	run_gpu_2ch 2 4 $lfft $TINT "--cross" >> perf_run.log
done

