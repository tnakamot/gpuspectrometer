/////////////////////////////////////////////////////////////////////////////////////
//
// A VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
// Starts the data receiver into the background. Returns segments of raw VDIF sample
// data for a given integration period. The VDIF headers are removed. Any missing
// packets (missing VDIF frames) are replaced with zero-valued samples (TODO: random
// noise).
//
// Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before the
// actual VDIF frame. The receive function can skip these preceding bytes.
//
// The raw data stripping routines work only on a full-frame level. The requested
// duration of segments to return must fit an integer number of complete VDIF frames.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "RingBuffer.h"
#include "vdif_header_util.h"
#include "vdif_receiver_rb.h"
#include "math_ext.h" // for fmod_prec(), a reduced-precision fmod() variant

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <assert.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static float (*testsource_vdif_receiver_generateSample)(double fs, size_t n, size_t Lfft, size_t Nint);
static float default_testsource_vdif_receiver_generateSample(double fs, size_t n, size_t Lfft, size_t Nint);
static float m_gaussrandf(void);
static void quantize_float_to_2bit(const float* s, unsigned char* x, size_t n, float std);

/////////////////////////////////////////////////////////////////////////////////////

/* Conversion from VDIF Ref. Epoch into Unix time, calculated with ../script/vdif_epochs_unixtime.py */
static const time_t C_vdif_epoch_to_timeval[] = {
   946684800,  962409600,  978307200,  993945600, 1009843200,
  1025481600, 1041379200, 1057017600, 1072915200, 1088640000,
  1104537600, 1120176000, 1136073600, 1151712000, 1167609600,
  1183248000, 1199145600, 1214870400, 1230768000, 1246406400,
  1262304000, 1277942400, 1293840000, 1309478400, 1325376000,
  1341100800, 1356998400, 1372636800, 1388534400, 1404172800,
  1420070400, 1435708800, 1451606400, 1467331200, 1483228800,
  1498867200, 1514764800, 1530403200, 1546300800, 1561939200,
  1577836800, 1593561600, 1609459200, 1625097600, 1640995200,
  1656633600, 1672531200, 1688169600, 1704067200, 1719792000,
  1735689600, 1751328000, 1767225600, 1782864000, 1798761600,
  1814400000, 1830297600, 1846022400, 1861920000, 1877558400,
  1893456000, 1909094400, 1924992000
};

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Default function to use for generating a signal, in case the user-provided C
 * code fails to compile cleanly.
 */
static float default_testsource_vdif_receiver_generateSample(double fs, size_t n, size_t Lfft, size_t Nint)
{
    double wtone = 2.0*M_PI/16.0;  // angular freq of strong tone
    double min_snr = 1;            // minimum signal to noise ratio (time domain, not averaged)
    double dynrange = 100;         // "dynamic range" in ALMA definition: DR = peak of strong tone / peak of simultaneous weak tone
    double wtone2 = 2.0*M_PI/20.0; // angular freq of weaker tone
    float s = 0.0f;
    // Gaussian noise (mean 0, std 1.0, var 1.0)
    s += m_gaussrandf();
    // Constant tone
    s += min_snr*dynrange*sin(wtone*((double)n));
    // Neighbouring tone, jumping farther away once per second; try to start in same FFT bin initially
    //s += (min_snr/dynrange)*sin((wtone+(nsecs+1.0)*wdelta)*t);
    // Neighbouring tone, fixed frequency
    s += min_snr*sin(wtone2*((double)n));
    return s;
}

/** testource_vdif_receiver_thread()
 * Worker thread that generates and inserts test data into a ring buffer.
 * The data are a 2-bit quantized signal intended for testing the accuracy
 * of spectrometer software, and it need not be generated quickly.
 */
static void *testsource_vdif_receiver_thread(void* targ)
{
    vdif_rx_t *rx = (vdif_rx_t*)targ;
    assert(targ != NULL);

    size_t n, nsamples = 0, nframes = 0, nsecs = 0, vdif_ep = 29;
    size_t nsamples_per_frame = (rx->frame_size-VDIF_HDRLEN)*8/32; // 32-bit samples
    float* signal = memalign(4096, nsamples_per_frame*sizeof(float));
    float  fs = nsamples_per_frame*rx->rate_fps;
    size_t Lfft = 8192, Nint = 8192; // TODO: how to get actual spectral settings of spectrometer side passed back into input side!?

    double min_snr = 1;            // minimum signal to noise ratio (time domain, not averaged)
    double dynrange = 100;         // "dynamic range" in ALMA definition: DR = peak of strong tone / peak of simultaneous weak tone
    double sigma;

    pthread_mutex_lock(&rx->mtx);
    pthread_cond_wait(&rx->go, &rx->mtx);
    pthread_mutex_unlock(&rx->mtx);

    time_t now = time(NULL);
    for (n = 0; n < sizeof(C_vdif_epoch_to_timeval)/sizeof(time_t); n++) {
        if (now > C_vdif_epoch_to_timeval[n]) {
            vdif_ep = n;
        }
    }
    nsecs = now - C_vdif_epoch_to_timeval[vdif_ep];

    while (1) {

        vdif_header_t* hdr;
        unsigned char* data;
        size_t nfree;

        /* Make sure an entire frame fits */
        void* free_at = RBappendable(rx->rb, &nfree);
        if (nfree < rx->frame_size) {
            usleep(100);
            continue;
        }

        /* Write header fields into RingBuffer space */
        hdr = (vdif_header_t*)free_at;
        memset(hdr, 0x00, sizeof(vdif_header_t));
        hdr->epoch = vdif_ep;
        hdr->framelength8 = rx->frame_size/8;
        hdr->seconds = nsecs;
        hdr->frame = nframes;
        hdr->nbits = 32-1;

        /* Make data */
        for (n = 0; n < nsamples_per_frame; n++, nsamples++) {
            signal[n] = testsource_vdif_receiver_generateSample(fs, nsamples, Lfft, Nint);
        }

        sigma = 1.0;            // noise with var of 1.0
        sigma += min_snr*min_snr/2; // var of tone with ampl. A (std = A/sqrt(2))
        sigma += min_snr*min_snr/(2*dynrange*dynrange); // var of neighbouring tone with ampl. A/dynrange
        sigma = sqrtf(sigma);

        /* Quantize and write into RingBuffer space */
        data = ((unsigned char*)free_at) + VDIF_HDRLEN;
        // TODO:  quantize_to_N_level(signal, data, nsamples_per_frame, sigma); // float to float
        memcpy(data, signal, nsamples_per_frame*sizeof(float));

#ifdef TESTSOURCE_FILE_COPY
        FILE* ftmp = fopen("synth.vdif", "ab");
        fwrite(hdr,  sizeof(vdif_header_t), 1, ftmp);
        fwrite(data, rx->frame_size-VDIF_HDRLEN, 1, ftmp);
        fclose(ftmp);
        ftmp = fopen("synth.32bit", "ab");
        fwrite(signal, nsamples_per_frame*sizeof(float), 1, ftmp);
        fclose(ftmp);
#endif

        /* Mark data as written */
        RBappended(rx->rb, rx->frame_size);

        /* Advance the time stamp */
        nframes++;
        if (nframes >= rx->rate_fps) {
            nframes = 0;
            nsecs++;
        }
    }

    pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////

/** testsource_vdif_receiver()
 *
 * Creates a test data generator source (2-bit samples), then returns
 * an object that the user can "receive" those data from.
 *
 * @param testcode Location and name of C source code defining the synthetic signal.
 * @param vdif_framesize Exact length of the VDIF frames in bytes.
 * @param vdif_offset Not used.
 * @param rate_Mbps Not used.
 * @return A new buffered receiver object used to read incoming data, or NULL on error.
 */
vdif_rx_t* testsource_vdif_receiver(const char* testcode, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps)
{
    vdif_rx_t *rx;
    char *dyngcc, *dlerr;

    assert((vdif_framesize >= VDIF_HDRLEN) && (vdif_framesize <= VDIF_MAXLEN));
    assert(vdif_offset >= 0 && vdif_offset < (vdif_framesize-VDIF_HDRLEN));
    assert(vdif_offset == 0); // current file receiver does not strip PSN or other non-VDIF extra data

    /* Precompile the external C signal synthesizer code */
    dyngcc = malloc(4096);
    snprintf(dyngcc, 4095, "/usr/bin/cat %s %s %s > %s",
             TESTSOURCE_C_PRE,
             testcode,
             TESTSOURCE_C_POST,
             TESTSOURCE_C_OUT
    );
    system(dyngcc);
    snprintf(dyngcc, 4095, "gcc %s -o %s -shared -fPIC",
             TESTSOURCE_C_OUT, TESTSOURCE_SO_OUT
    );
    system(dyngcc);
    free(dyngcc);

    /* Receiver object */
    rx = calloc(1, sizeof(vdif_rx_t));
    rx->frame_size     = vdif_framesize;
    rx->frame_offset   = vdif_offset;

    /* Create the ring buffer */
    rx->rate_Mbps      = rate_Mbps;
    rx->rate_fps       = ((rate_Mbps * 1000000)/8) / (rx->frame_size - VDIF_HDRLEN);
    rx->bufsize_frames = rx->rate_fps;
    rx->bufsize_bytes  = rx->bufsize_frames * rx->frame_size;
    fprintf(stderr,
            "Creating ring buffer to hold %zu frames (%zu bytes) of self-generated test data.\n",
            rx->bufsize_frames, rx->bufsize_bytes
    );
    rx->rb = RBcreate(rx->bufsize_bytes, "spectrometerRBtestRx");
    rx->workbuf = memalign(4096, 2*rx->frame_size);
    rx->is_file = 0;
    rx->is_network = 0;

    /* Load the compiled synthesizer function */
    rx->dlhandle = dlopen(TESTSOURCE_SO_OUT, RTLD_NOW);
    if (!rx->dlhandle) {
        fprintf(stderr, "Error: failed to compile %s and dlopen(%s) returned %s\n",
                TESTSOURCE_C_OUT, TESTSOURCE_SO_OUT, dlerror()
        );
    }
    testsource_vdif_receiver_generateSample = dlsym(rx->dlhandle, "testsource_vdif_receiver_generateSample");
    dlerr = dlerror();
    if (dlerr != NULL) {
        fprintf(stderr, "Error: failed to load signal generator function symbol from %s\n",
                TESTSOURCE_SO_OUT
        );
        testsource_vdif_receiver_generateSample = &default_testsource_vdif_receiver_generateSample;
    }

    /* Start the background receiver thread */
    pthread_mutex_init(&rx->mtx, NULL);
    pthread_cond_init(&rx->go, NULL);
    pthread_create(&rx->tid, NULL, testsource_vdif_receiver_thread, (void*)rx);

    return rx;
}

/////////////////////////////////////////////////////////////////////////////////////

/** gaussrandf()
 * Generate normal distributed random numbers with mean 0 and standard deviation 1
 * using the Knuth/Marsaglia method. Code adapted from http://c-faq.com/lib/gaussian.html.
 */
static float m_gaussrandf(void)
{
    static float V1, V2, S;
    static int phase = 0;
    float X;
    if (phase == 0) {
        do {
            float U1 = ((float)rand()) / RAND_MAX;
            float U2 = ((float)rand()) / RAND_MAX;
            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        } while(S >= 1 || S == 0);
        X = V1 * sqrtf(-2 * logf(S) / S);
    } else {
        X = V2 * sqrtf(-2 * logf(S) / S);
    }
    phase = 1 - phase;
    return X;
}

/////////////////////////////////////////////////////////////////////////////////////

/** quantize_float_to_2bit()
 * Quantize a 32-bit float signal s[0..n-1] into 2-bit samples in VDIF encoding.
 * A zero point (e.g., mean) is removed from s[i] first.
 * The 2-bit sample encoding is such that
 *     q[i] = 0b00 when  s[i] < -v0
 *     q[i] = 0b01 when  -v0 <= s[i] < 0
 *     q[i] = 0b10 when    0 <= s[i] < v0
 *     q[i] = 0b11 when  v0 <= s[i]
 * and four 2-bit samples are packed into each output byte,
 * The newest sample is in the MSBs, the oldest in the LSBs.
 *
 * Also see, for example, pages 40 and 69 of Sasao & Fletcher (2006)
 * "Introduction to VLBI Systems, Lecture Notes for KVN Students, Version 1"
 * http://www.astro.sci.yamaguchi-u.ac.jp/jvn/reduction/kvnlecnote/kchap4.pdf
 *
 * @param s Input data
 * @param x Output for packed 2-bit quantized data
 * @param n Number of input samples
 * @param sigma Standard deviation of input data (v0=sigma*0.982)
 */
static void quantize_float_to_2bit(const float* s, unsigned char* x, size_t n, float sigma)
{
    size_t i;
    const float v0 = 0.982*sigma; // 0.982 should result in coherence factor of 0.883

    for (i = 0; i < n; i++) {

        const float v = s[i];

        // Quantize and pack 4 samples into one byte
#if 1
        x[i/4] = x[i/4]>>2;
        if (v < -v0) {
            // q = 0;
        } else if (v < 0) {
            x[i/4] |= 64;  // q=1; q<<6 = 64
        } else if (v < v0) {
            x[i/4] |= 128; // q=2; q<<6 = 128
        } else {
            x[i/4] |= 192; // q=3; q<<6 = 192
        }
#else
        x[i/4] = x[i/4]<<2;
        if (v < -v0) {
        } else if (v < 0) {
            x[i/4] |= 1;
        } else if (v < v0) {
            x[i/4] |= 2;
        } else {
            x[i/4] |= 3;
        }
#endif


    }
}

/////////////////////////////////////////////////////////////////////////////////////
