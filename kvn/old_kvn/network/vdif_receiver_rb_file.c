/////////////////////////////////////////////////////////////////////////////////////
//
// A VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
// Starts the data receiver into the background. Returns segments of raw VDIF sample
// data for a given integration period. The VDIF headers are removed. Any missing
// packets (missing VDIF frames) are replaced with zero-valued samples (TODO: random
// noise).
//
// Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before the
// actual VDIF frame. The receive function can skip these preceding bytes.
//
// The raw data stripping routines work only on a full-frame level. The requested
// duration of segments to return must fit an integer number of complete VDIF frames.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "RingBuffer.h"
#include "vdif_header_util.h"
#include "vdif_receiver_rb.h"
#include "math_ext.h" // for fmod_prec(), a reduced-precision fmod() variant

#include <sys/time.h>
#include <assert.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/////////////////////////////////////////////////////////////////////////////////////

/** filesource_vdif_receiver_thread()
 *
 * Worker thread that fills a ring buffer using a file as the source.
 * This simulates continuous (non-bursty) FPGA-generated network traffic,
 * but without taking into account line rate limitations.
 */
static void *filesource_vdif_receiver_thread(void* targ)
{
    vdif_rx_t *rx = (vdif_rx_t*)targ;
    assert(targ != NULL);

#ifdef THROTTLE_FILE_READS
    struct timeval tv_curr, tv_prev;
    double dT_usec_goal = 1e6 / rx->rate_fps, dT_accu = 0.0, dT_usec;
#endif

    pthread_mutex_lock(&rx->mtx);
    pthread_cond_wait(&rx->go, &rx->mtx);
    pthread_mutex_unlock(&rx->mtx);

#ifdef THROTTLE_FILE_READS
    gettimeofday(&tv_prev, NULL);
#endif
    while (rx->fd) {
        size_t ntotal = 0, nfree;

        // Note: cannot use RBread() here because it does not handle "short"
        // reads from source file, and may even do "short" writes into RB if
        // there is less space available than the requested file read size.

        /* Make sure an entire frame fits */
        void* free_at = RBappendable(rx->rb, &nfree);
        if (nfree < rx->frame_size) {
            usleep(100);
            continue;
        }

        /* Read from file into ringbuffer space */
        while (ntotal < rx->frame_size) {
            ssize_t nrd = read(rx->fd, free_at+ntotal, rx->frame_size-ntotal);
            if (nrd < 0) {
                fprintf(stderr, "Failed to read file source, %s\n", strerror(errno));
                RBstatus(rx->rb, "file 'rx' error");
                rx->at_eof = 1;
                pthread_exit(NULL);
            } else if (nrd == 0) {
                RBstatus(rx->rb, "file 'rx' reached EOF");
                rx->at_eof = 1;
                pthread_exit(NULL);
            }
            ntotal += nrd;
        }

        /* Mark data as written */
        RBappended(rx->rb, rx->frame_size);

        /* Throttle to target rate */
#ifdef THROTTLE_FILE_READS
        if (dT_accu > 0) {
           usleep(dT_accu);
        }
        gettimeofday(&tv_curr, NULL);
        dT_usec = (tv_curr.tv_sec - tv_prev.tv_sec)*1e6 + (tv_curr.tv_usec - tv_prev.tv_usec);
        dT_accu += (dT_usec_goal - dT_usec);
        tv_prev = tv_curr;
#endif
    }

    pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////

/** filesource_vdif_receiver()
 *
 * Open the source file (used in place of actual incoming network data), then return
 * an object that the user can "receive" those incoming file data from.
 *
 * @param filepath The path and name of the input VDIF file to read.
 * @param vdif_framesize Exact length of the VDIF frames in bytes.
 * @param vdif_offset Number of bytes at beginning of "packets" to ignore.
 * @param rate_Mbps Goodput payload data rate (Mbit/second) excluding overhead like VDIF headers.
 * @return A new buffered receiver object used to read incoming data, or NULL on error.
 */
vdif_rx_t* filesource_vdif_receiver(const char* filepath, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps)
{
    vdif_rx_t *rx;
    int rc;

    assert((vdif_framesize >= VDIF_HDRLEN) && (vdif_framesize <= VDIF_MAXLEN));
    assert(vdif_offset >= 0 && vdif_offset < (vdif_framesize-VDIF_HDRLEN));
    assert(vdif_offset == 0); // current file receiver does not strip PSN or other non-VDIF extra data

    /* Receiver object */
    rx = calloc(1, sizeof(vdif_rx_t));
    rx->frame_size     = vdif_framesize;
    rx->frame_offset   = vdif_offset;
    rx->fd = open(filepath, O_RDONLY);
    if (rx->fd == -1) {
        perror("Failed to open input file");
        return NULL;
    }
    if ((rc = fdatasync(rx->fd)) != 0) {
        perror("Failed to fdatasync() on the input file");
    }
    if ((rc = posix_fadvise(rx->fd, 0, 0, POSIX_FADV_SEQUENTIAL)) != 0) {
        errno = rc;
        perror("Failed to fadvise() on the input file");
    }

    /* Create the ring buffer */
    rx->rate_Mbps      = rate_Mbps;
    rx->rate_fps       = ((rate_Mbps * 1000000)/8) / (rx->frame_size - VDIF_HDRLEN);
    rx->bufsize_frames = rx->rate_fps;
    rx->bufsize_bytes  = rx->bufsize_frames * rx->frame_size;
    fprintf(stderr,
            "Creating ring buffer to hold %zu frames (%zu bytes) at assumed %zu frames/sec from file %s.\n",
            rx->bufsize_frames, rx->bufsize_bytes, rx->rate_fps, filepath
    );
    rx->rb = RBcreate(rx->bufsize_bytes, "spectrometerRBfileRx");
    rx->workbuf = memalign(4096, 2*rx->frame_size);
    rx->is_file = 1;
    rx->is_network = 0;

#ifdef THROTTLE_FILE_READS
    fprintf(stderr, " *** Note: file reading will be throttled to %zu Mbit/s to simulate network! ***\n", rate_Mbps);
#endif

    /* Start the background receiver thread */
    pthread_mutex_init(&rx->mtx, NULL);
    pthread_cond_init(&rx->go, NULL);
    pthread_create(&rx->tid, NULL, filesource_vdif_receiver_thread, (void*)rx);

    return rx;
}

/////////////////////////////////////////////////////////////////////////////////////
