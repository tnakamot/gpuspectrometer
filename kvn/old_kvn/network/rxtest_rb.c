/////////////////////////////////////////////////////////////////////////////////////
//
// Test for VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "RingBuffer.h"
#include "vdif_header_util.h"
#include "vdif_receiver_rb.h"
#include "math_ext.h" // for fmod_prec(), a reduced-precision fmod() variant

#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#define USE_NETWORK yes // Define to use UDP/IP as source. Otherwise a file is the source.

int main(void)
{
    vdif_rx_t* rx;
    double T_int, T_wish;

    void*  raw_data_buf;
    size_t raw_data_len;
    size_t i;

#ifdef USE_NETWORK
    // Use typical settings for FILA10G and for R2DBE(?), 8192B payload @ 2048 Mbps = 31250 fps
    rx = udp_vdif_receiver(
        // port, frame, PSN, goodput Mbps
        "46227", 8192+32, 8, 2048
    );
#else
    rx = filesource_vdif_receiver(
        // filename, frame, PSN, goodput Mbps
        "/mnt/disks/capture.bin", 8192+32, 0, 2048
    );
#endif

    //T_wish = 0.128; // 31250 fps x 0.128 = 4000 frames, at n+0.0s, n+0.128s, ..., n+1.024s, ...
    T_wish = 0.25;   // 31250 fps x 0.100 = 3125 frames, at n+0.0s, n+0.100s, ..., n+1.000s, ...
    T_wish = 1.25;

    T_int        = vdif_receiver_fit_segmenttime(rx, T_wish);
    raw_data_len = vdif_receiver_get_segmentsize(rx, T_int);
    raw_data_buf = memalign(4096, raw_data_len);
    fprintf(stderr,
            "Desired segmentation time : %f, fitted to %zd frames/sec input rate\n"
            "Best-fit segmentation time: %f with %zd frames per segment\n",
            T_wish, rx->rate_fps, T_int, (size_t)(T_int*rx->rate_fps)
    );

    memset(raw_data_buf, 0x00, raw_data_len);

    vdif_receiver_start(rx);

    for (i = 0; i < 10000; i++) {
        struct timeval tv_mid;
        struct tm* tm_mid;

        size_t nwanted = vdif_receiver_get_segmentsize(rx, T_int);
        size_t nrx = vdif_receiver_get_segment(rx, raw_data_buf, T_int, &tv_mid);
        ssize_t nlost = nwanted - nrx;

        tm_mid = gmtime((const time_t*)&tv_mid.tv_sec);
        fprintf(stderr, "Segment with mid-time %04d/%02d/%02d %02d:%02d:%06.3f : %zu byte wanted, got %zu, lost %zd\n",
                1900 + tm_mid->tm_year, tm_mid->tm_mon, tm_mid->tm_mday, tm_mid->tm_hour, tm_mid->tm_min,
                tv_mid.tv_usec*1e-6 + tm_mid->tm_sec,
                nwanted, nrx, nlost
        );
    }

    return 0;
}
