/////////////////////////////////////////////////////////////////////////////////////
//
// A multi-threading capable buffer system that receives UDP/IP.
//
// Multiple large buffers are continuously refilled by a receiver thread.
// Each buffer contains time-continuous UDP/IP data. The receiver thread (re)-fills
// empty or already processed old buffers with new data.
//
// For spectrometer applications, each buffer would typically contain the data
// of one full integration period. Zero data loss inside a buffer allows to 
// form spectra without corruption. Data loss (if any) happens only between
// spectra, i.e., there can be time gaps between complete spectra, but no
// corruption of the spectral data itself.
//
// rx_buffers_t* udp_receiver_init() : initializes buffer system
// udp_receiver_start()              : starts the receiver thread
//
// User source code can "pthread_cond_wait(&rx->data_available, &rx->lock);"
// to wait for and then grab one of the recently filled data buffers.
//
// When user code is complete it can mark the grabbed buffer as processed
// and release it for re-filling by the asynchronous network receiver thread.
//
// If user code is too slow to process data, the UDP/IP receiver thread
// waits (and drops packets) until the user releases an empty buffer again.
//
// This ensures that data inside a buffer is always in an increasing-in time order,
// and that any data loss caused by slow consumption has a large granularity, i.e.,
// the loss inside any given buffer will be zero/minimal, and any loss will most
// likely be "between" buffers.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "udp_receiver.h"

#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <malloc.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

static void* udp_receiver_thread(void *ptr);

///////////////////////////////////////////////////////////////////////////////////
// Initialize receiver buffer,
// return readied structure.
///////////////////////////////////////////////////////////////////////////////////
rx_buffers_t* udp_receiver_init(const char* port, size_t Nbuffers, size_t Lbuffers)
{
	struct addrinfo  hints;
	struct addrinfo* res = 0;
	struct timeval tv_timeout;
	size_t i;
	int sd, val;

	/* Receiver socket */
	memset(&hints,0,sizeof(hints));
	hints.ai_family   = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = 0;
	hints.ai_flags    = AI_PASSIVE | AI_ADDRCONFIG;
	getaddrinfo(NULL, port, &hints, &res);

	sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (bind(sd, res->ai_addr, res->ai_addrlen) == -1) {
		perror("Failed to bind socket");
		return NULL;
	}
	freeaddrinfo(res);

	tv_timeout.tv_sec  = RX_TIMEOUT_SEC;
	tv_timeout.tv_usec = 0;
	setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval));

	val = 64*1024*1024;
	setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val));

	/* Initialize buffers */
	rx_buffers_t* rx = malloc(sizeof(rx_buffers_t));
	rx->do_terminate = 0;
	rx->sock_d       = sd;
	rx->Nbuffers     = Nbuffers;
	rx->Lbuffers     = Lbuffers;
	rx->Nunconsumed  = 0;
	rx->buffer_starttimes  = malloc(Nbuffers * sizeof(struct timeval));
	rx->buffer_stoptimes   = malloc(Nbuffers * sizeof(struct timeval));
	rx->buffer_is_consumed = malloc(Nbuffers * sizeof(char));
	rx->buffer_is_held     = malloc(Nbuffers * sizeof(char));
	rx->buffer_nbytes      = malloc(Nbuffers * sizeof(size_t));
	rx->buffers = malloc(Nbuffers * sizeof(unsigned char*));
	for (i = 0; i < rx->Nbuffers; i++) {
		rx->buffers[i] = memalign(16*4096, rx->Lbuffers + MAX_RECEIVE_SIZE);
		rx->buffer_is_consumed[i] = 1;
		rx->buffer_is_held[i] = 0;
		rx->buffer_nbytes[i] = 0;
		gettimeofday(rx->buffer_starttimes + i, NULL);
		gettimeofday(rx->buffer_stoptimes + i, NULL);
	}
	pthread_mutex_init(&rx->lock, NULL);
	pthread_cond_init(&rx->data_available, NULL);

	return rx;
}

///////////////////////////////////////////////////////////////////////////////////
// Start network reception
///////////////////////////////////////////////////////////////////////////////////
void udp_receiver_start(rx_buffers_t* rx)
{
	pthread_create(&rx->rxthread, NULL, udp_receiver_thread, (void*)rx);
}

///////////////////////////////////////////////////////////////////////////////////
// Thread that fills available buffers one-by-one.
// When the current buffer is full it signals possible waiters,
// then switches to a new (already consumed) buffer if available.
///////////////////////////////////////////////////////////////////////////////////
static void* udp_receiver_thread(void *v_rx)
{
	rx_buffers_t* rx = (rx_buffers_t*)v_rx;
	int buf_idx = 0;

	while (!rx->do_terminate) {
		int i, found = 0;
		unsigned char* wptr;
		ssize_t nrd;

		/* Grab new buffer */
		pthread_mutex_lock(&rx->lock);
		for (i = 0; i < rx->Nbuffers; i++) {
			buf_idx = (buf_idx + 1) % rx->Nbuffers;
			if (rx->buffer_is_consumed[buf_idx]) {
				found = 1;
				rx->buffer_nbytes[buf_idx] = 0;
				gettimeofday(rx->buffer_starttimes + buf_idx, NULL);
				break;
			}
		}
		pthread_mutex_unlock(&rx->lock);
		if (!found) {
			printf("udp_receiver_thread: currently no free buffers to receiver more UDP data\n");
			sleep(1);
			continue;
		}

		/* Receive UDP until buffer is full */
		wptr = rx->buffers[buf_idx];
		while (1) {

			// Receiver more
			nrd = recv(rx->sock_d, wptr, MAX_RECEIVE_SIZE, 0);
			if ((nrd == -1) && (errno == EAGAIN)) {
				printf("No UDP packets received in the last %d seconds...\n", RX_TIMEOUT_SEC);
				continue;
			} else if (nrd == -1) {
				perror("Receive error");
				pthread_exit((void*)-1);
			}

			// Stop when buffer is filled
			if ((rx->buffer_nbytes[buf_idx] + nrd) >= rx->Lbuffers) {
				gettimeofday(rx->buffer_stoptimes + buf_idx, NULL);
				break;
			}

			// Continue
			wptr += nrd;
			rx->buffer_nbytes[buf_idx] += nrd;
		}

		/* Signal any waiters to wake up */
		pthread_mutex_lock(&rx->lock);
		rx->buffer_is_consumed[buf_idx] = 0;
		rx->Nunconsumed++;
		pthread_mutex_unlock(&rx->lock);
		pthread_cond_signal(&rx->data_available);

	}

	pthread_exit((void*)0);
}

