/////////////////////////////////////////////////////////////////////////////////////
//
// A VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
// Starts the data receiver into the background. Returns segments of raw VDIF sample
// data for a given integration period. The VDIF headers are removed. Any missing
// packets (missing VDIF frames) are replaced with zero-valued samples (TODO: random
// noise). Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before
// the actual VDIF frame. The receive function can skip these preceding bytes.
//
// Three VDIF receivers are available:
//   udp_vdif_receiver        :  UDP/IP VDIF reception
//   filesource_vdif_receiver :  file VDIF readout
//   testsource_vdif_receiver :  synthetic sample generator (32-bit float)
//
// Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before the
// actual VDIF frame. The receive function can skip these preceding bytes.
//
// The raw data stripping routines work only on a full-frame level. The requested
// duration of segments to return must fit an integer number of complete VDIF frames.
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef VDIF_RECEIVER_RB_H
#define VDIF_RECEIVER_RB_H

#include "RingBuffer.h"

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

/////////////////////////////////////////////////////////////////////////////////////

#define DBG_PRINT 0                // Define as 1 to print some extra infos
//-----------------------------------------------------------------------------------
#define VDIF_HDRLEN 32             // Length of Non-Legacy VDIF header
#define VDIF_MAXLEN 32768          // Max frame size for UDP/IP-carried VDIF
//-----------------------------------------------------------------------------------
#define UDP_TIMEOUT_SEC 5          // Timeout in seconds for UDP/IP packet recv()
#define UDP_RXBUF_LEN 64*1024*1024 // Size of the socket buffer
//-----------------------------------------------------------------------------------
//#define THROTTLE_FILE_READS        // Comment/undefine to read files at max speed
//-----------------------------------------------------------------------------------
#define TESTSOURCE_FILE_COPY       // Synthetic VDIF generator writes "synth.vdif" and "synth.32bit"
#define TESTSOURCE_C_PRE   "/etc/kfftspec/kfftspec_synth_dyn.c.pre"
#define TESTSOURCE_C_POST  "/etc/kfftspec/kfftspec_synth_dyn.c.post"
#define TESTSOURCE_C_OUT   "/tmp/kfftspec_synth_dyn.c"
#define TESTSOURCE_SO_OUT  "/tmp/kfftspec_synth_dyn.so"
//-----------------------------------------------------------------------------------
#define SEGMENT_START_THRESH 0.25  // At what granularity to assume a new segment
                                   // started (default: 25%). A segment starts if
                                   // the VDIF time T and T_seg are, e.g.,
                                   //   when   fmod(T, T_seg) < 25% * T_seg
                                   // This basically sets how much data can be lost.

/////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

typedef struct vdif_rx_tt {
    pthread_t       tid;
    pthread_mutex_t mtx;
    pthread_cond_t  go;
    int         bind_cpu;
    int         sd, fd;
    RingBuffer *rb;
    size_t      frame_size;
    size_t      frame_offset;
    size_t      bufsize_bytes;
    size_t      bufsize_frames;
    size_t      rate_Mbps;
    size_t      rate_fps;
    int         is_file;
    int         is_network;
    volatile int at_eof;
    void*       workbuf;
    void*       dlhandle;
} vdif_rx_t;

// Receiver implementations (UDP/IP VDIF, File VDIF, Test VDIF)
vdif_rx_t* udp_vdif_receiver(const char* port, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps);
vdif_rx_t* filesource_vdif_receiver(const char* filepath, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps);
vdif_rx_t* testsource_vdif_receiver(const char* testcode, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps);

// General helper functions
double vdif_receiver_fit_segmenttime(const vdif_rx_t* const, const double T_seg_wish);
size_t vdif_receiver_get_segmentsize(const vdif_rx_t* const, const double T_seg);
size_t vdif_receiver_get_segment(vdif_rx_t* const, unsigned char* d, double T_seg, struct timeval* tv_mid);
void   vdif_receiver_start(vdif_rx_t* rx);
int    vdif_receiver_bind_cpu(vdif_rx_t* const rx, int cpu);

#ifdef __cplusplus
}
#endif

#endif // VDIF_RECEIVER_RB_H
