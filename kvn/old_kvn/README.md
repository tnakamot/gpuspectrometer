
 Multi-GPU Spectometer
 kfftspec v0.6.7


## Contents ###################################################################

1. Intro
2. Details
3. Special Notes
4. Installation
5. Files
6. Spectrometer Usage
7. Performance Test
8. Spectral Accuracy
9. Known Issues

Last updated 18 Nov 2015


## Intro ######################################################################

This repository contains a work in progress towards a (multi-)GPU -based
spectrometer with file and network UDP/IP input. Currently supports FFT mode
with or without windowing.

The spectrometer input data are expected to be in VDIF format. 
The spectrometer writes *.kfftspec files, in addition to KVN DSM -like files.

A PFB spectrometer mode and a PFB zoom band mode still need to be integrated
into a full spectrometer framework, currently they exist only in a separate
benchmarking program.

The current performance on a nVidia TITAN X card is between 3--8 Gs/s (6--16 Gbit/s), 
depending on spectrometer settings. The performance scales quite linearly
by the number of cards added to the system. Multi-10G (40G,100G,IB FDR)
connectivity is required for full performance.


## Details ####################################################################

The repository contains a basic working spectrometer implementation that 
currently is able to process single-subband and dual-subband data, can
optionally calculate the cross-power spectrum, optionally with windowing.

The time averaging of power spectra uses compensated summation (Kahan
summation, see, e.g., https://en.wikipedia.org/wiki/Kahan_summation_algorithm 
The kernel for cross-power currently uses numerically inferior direct summation.

The repository includes computing kernels for input data unpacking (2/3-bit
to float), polyphase filter bank (PFB) processing (set of FIR filters in a
naive convolution implementation without FFT), Fourier transform related
processing such as window functions and power and cross-power spectrum,
data statistics, and re-quantization. In some cases "cuFFT Callback"
functions can be used. Some kernels have own benchmarking programs.

The computing kernels and the Fourier transform currently rely on CUDA 6.5+ and
cuFFT and thus require nVidia graphics cards. A more portable (OpenCL)
implementation doesn't really exist yet, there is only a 2-bit sample decoder kernel,
but this might change in the future especially for a correlator
implementation (for <4096 spectral channels OpenCL 'clFFT' beats 'CUFFT'
noticeably).


## Special Notes ##############################################################

The output format is suitable for KVN use. For ALMA/ACA-TP and ASTE FPA/multi-beam
some other yet-to-be-determined format is necessary.

The VDIF file reader mode defaults to throttled! reading. This simulates real-time
input from an actual sampler system. For (un)throttled reading grep for THROTTLE_FILE_READS
under ./network/ and comment (out) the #define there accordingly.

The 'kfftspec' spectrometer is an FFT based spectrometer.
A future 'kpfbspec' will use the already existing polyphase filter bank (PFB) code.

The current PFB code is standalone and for channelization purposes (time domain output,
without the trivial complex magnitude averaging needed for making power spectra).
The PFB implementation is a naive implementation (FIR). Improved implementations (IIR FB,
FFT convolution based FIR, etc, etc) are still TODO.

## Installing #################################################################

First get a copy of the source code. Packaged versions of the source code
repository can be downloaded from

  https://bitbucket.org/jwagner313/gpuspectrometer/downloads

or the entire current repository can be checked out via

  $ git clone https://bitbucket.org/jwagner313/gpuspectrometer.git

In either case the package contains the current trunk version of the
source code, as well as tagged versions packaged under ./dist-packages/.

To unpack source code from a tagged version (optional):

$ cd dist-packages ; tar xjvf kspec-0.6.7.tar.bz2 ; cd kspec-0.6.7

To compile and install:

$ make
$ make install


## Files ######################################################################

./Makefile.top   : list of GPU platforms to compile for, NVCC settings

./spectrometers/ : source for multi-GPU FFT spectrometer, by J.Wagner, 
                   integrates several of the components below

./kernels/       : computing kernel sources, by J.Wagner and JS.Kim

./ringbuffer/    : sources for libRingBuffer library, by DG.Roh

./network/       : sources for libvdifrx_rb.so library that uses the RingBuffer
                   and extracts VDIF payload in "integration period" -sized
                   segments (lost frames accounted for), by J.Wagner

./pfb/           : sources for Polyphase Filter Bank processing benchmark
                   utilizing FIR kernels from ./kernels/cuda/, by J.Wagner

./fft/           : sources for FFT processing benchmark, by JS.Kim

./helpers/       : sources for KVN DSM format output, timestamp formatting
                   and time presentation, etc, by J.Wagner

./decoder/       : sources for 2-bit -> float decoder testing, by J.Wagner

./fp16/          : sources for testing 16-bit float ('half') memory access

./script/        : some scripts, including PFB coefficient generators

./scripts/plot_kfftspec.py   : plots spectra from *.kfftspec output files
./helpers/kfftspec_outfile.h : definition of the simple *.kfftspec output format
./scripts/pfb_coeffs.py      : calculates PFB coefficients via window method


## Spectrometer Usage #########################################################

kfftspec ver. 0.6.6   J.Wagner, JS.Kim, DG.Roh  20151028

A GPU-based wideband FFT spectrometer with data from the network or a file.
Currently supports single-IF non-overlapped FFT procesing.

Usage: kfftspec [--udpoffset=n] [--cross] [--devices=list] [--nstreams=n]
                [--winfunc=hann|hamm|hft248d] [--winoverlap=percentage]
                [--exp=experimentname] [--obs=observername] [--ant=stationname]
                <nchan> <Tint (sec)> <inputsrc> <inputformat>

   <nchan> is the number of spectral output channels

   <Tint> is the desired integration time in seconds

   <inputsrc> is either an input UDP/IP port number, or a file name

   <inputformat> should be of the form: <FORMAT>-<Mbps>-<nchan>-<nbit>,
      Mark5B-2048-1-2
      VDIF_8192-2048-1-2 (here 8192 is payload size in bytes)

The following options are supported:

   --udpoffset=n   Discard the first n bytes from start of UDP payload
   --cross         Also form cross-power spectra
   --devices=list  Ordered list of CUDA Devices to use (e.g., --devices=0,2,1,3)
   --nstreams=n    Number of CUDA Streams to use per CUDA Device
   --winfunc=name, --winoverlap=pct   Window function to use and amount of overlap
   --exp=name, --obs=name, --ant=name Names of experiment, observer, and antenna



Usage example:

      jwagner@mars:~$ kfftspec -E expt001 -O jwagner -A KVN_Yonsei \
        --devices=0,1 --nstreams=2 --winfunc=hamming \
         1024 0.05 \
         /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-4096-1-2


## Performance Test ###########################################################

Grating GPU spectrometer
https://github.com/jayanthc/grating

jwagner@mars trunk grating> ./bin/grating --nfft 32768 -d 0 --nacc 3906 --nsub 1 ~/rawdata/grating_testdata2.bin
* Benchmarking run commencing...
CUDA Device 0: GeForce GTX TITAN X, Compute Capability 5.2, 49152 physical threads (selected)
INFO: Total memory requested on GPU is 33.5 of a possible 12287.8 MB. Memory request break-up:
    Input data buffer: 32 MB
    FFT in array: 0.5 MB
    FFT out array: 0.5 MB
    Stokes output array: 0.5 MB
Data read done! Read count = 31
    Total elapsed time for
            31 calls to cudaMemcpy(Host2Device)          :  110.076ms, 16%; Average = 3.551ms
          7936 calls to CopyDataForFFT()                 :  165.613ms, 24%; Average = 0.021ms
          7936 calls to DoFFT()                          :  267.493ms, 39%; Average = 0.034ms
          7936 calls to Accumulate()                     :  127.282ms, 18%; Average = 0.016ms
             2 calls to cudaMemcpy(Device2Host)          :    0.419ms,  0%; Average = 0.210ms
    Average throughput: 369.662 Msps = 1478.648 MBps

kfftspec v0.6.6

jwagner@mars:~$ kfftspec -E expt001 -O jwagner -A KVN_Yonsei \
   --devices=0 --nstreams=2 --winfunc=boxcar \
   32768 0.256 /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

  Computational setup  : 1 GPUs x 2 streams x 1 sub-integrations, without cuFFT Callbacks.
  Estimated memory req.: 5.84 GB per GPU, 0.36 GB on host
  Spectral setup       : 32768 spectral points, 1 subbands, 0.250s integration of 3906 spectra
                         boxcar windowing with 0.0% overlap
  Spectral output      : 1 spectra per integration, with no cross-power spectra
  Card 0: CUDA device 0: GeForce GTX TITAN X, CC 5.2, 1024 threads/block, warpsize 32
  ...
  Got result [0][0]  : mid-time 2016/10/19 08:40:02.125: weight 1.0000 : computing 6.80 Gs/s
  Got result [0][1]  : mid-time 2016/10/19 08:40:02.375: weight 1.0000 : computing 6.79 Gs/s
  Got result [0][0]  : mid-time 2016/10/19 08:40:02.625: weight 1.0000 : computing 6.80 Gs/s
  Got result [0][1]  : mid-time 2016/10/19 08:40:02.875: weight 1.0000 : computing 6.79 Gs/s
    Readout cancelled, no data in buffer
  Got result [0][0]  : mid-time 2016/10/19 08:40:03.125: weight 0.7503 : computing 6.80 Gs/s
  Produced 17 spectra from 1072020000/1088000000 bytes (1.47% loss, or early EOF)
  Took 1.054 seconds. 4.13 Gs/s overall throughput.

  If we enable Hamming windowing of input data to reduce artefacts,

jwagner@mars:~$ kfftspec -E expt001 -O jwagner -A KVN_Yonsei \
   --devices=0 --nstreams=2 --winfunc=hamming \
   32768 0.256 /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

  Got result [0][1]  : mid-time 2016/10/19 08:40:02.875: weight 1.0000 : computing 5.64 Gs/s 
  Got result [0][0]  : mid-time 2016/10/19 08:40:03.125: weight 0.7503 : computing 5.63 Gs/s
  ...

If cuFFT Callbacks are enabled ('kfftspec_cb'), but no windowing is applied,

  Got result [0][1]  : mid-time 2016/10/19 08:40:02.875: weight 1.0000 : computing 7.25 Gs/s

and with Hamming windowing, curiously Callback performance is lower than without,

  Got result [0][1]  : mid-time 2016/10/19 08:40:02.875: weight 1.0000 : computing 4.75 Gs/s


## Spectral Accuracy ##########################################################

To Do:

 - Use test data made as in PolariS spectrometer documentation,
 - Use test data as described in HFT248D/others flat-top window function paper


## Known Issues ###############################################################

Current issues

 - the normalization of spectra is currently wrong by factor "*nsubint" when
   computation is split (automatically) into 'nsubint' sub-integrations

 - performance with cuFFT Callbacks is unexpected:

   - with windowing disabled, the Callback that writes power data
     rather than 'raw' FFT complex output data gives a nice speed-up

   - with windowing enabled, the Callback that loads a FFT input sample
     and applies window coefficients to it leads to *slower* performance
     than pre-windowing the data with a separate kernel call (i.e. three
     memory accesses rather than just one somehow give better performance)

     The FFT Load callback that applies window coefficients is slow regardless 
     of whether hardware intrinsic __cosf() or software counterpart cosf() are
     used... Performance loss still unexplained...
