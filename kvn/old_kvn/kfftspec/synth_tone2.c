double wtone = 2.0*M_PI/16.0;  // angular freq of strong tone
double min_snr = 1;            // minimum signal to noise ratio (time domain, not averaged)
double dynrange = 100;         // "dynamic range" in ALMA definition: DR = peak of strong tone / peak of simultaneous weak tone
double wtone2 = 2.0*M_PI/20.0; // angular freq of weaker tone
double nn = (double)n;

// Gaussian noise (mean 0, std 1.0, var 1.0)
x_n = gaussrandf();

// Constant tone
x_n += min_snr*dynrange*sin(wtone*nn);

// Neighbouring tone, fixed frequency
x_n += min_snr*sin(wtone2*nn);
