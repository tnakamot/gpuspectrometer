#!/usr/bin/python
"""
plot_kfftspec.py Version 1.1  20160203 Jan Wagner
"""
import argparse, os, sys, struct
from kfftspecReader import kfftspecReader

XUNITS_CH = 0
XUNITS_HZ = 1
startch = 0   # default starting channel (inclusive) for plotting
stopch = -1   # default ending channel (exclusive) for plotting

parser = argparse.ArgumentParser(description=__doc__, epilog='Raw spectra from the file are plotted on screen one by one, with no processing or calibration.')
parser.add_argument('files',   metavar='infile',     type=str, nargs='+', help='file produced by the kfftspec software spectrometer')
parser.add_argument('--zoom',  metavar='start,stop', type=str, help='range to zoom into, given by start and stop channels')
parser.add_argument('--nspec', metavar='N',          type=int, help='show only the first N spectra of the file')
parser.add_argument('--freq',  dest='XUNITS', action='store_const', const=XUNITS_HZ, default=XUNITS_CH, help='use frequency as unit of horizontal axis')
parser.add_argument('--lsb',   dest='XLSB', action='store_const', const=True, default=False, help='flip the spectrum by relabeling frequencies or channels')
parser.add_argument('--png',   dest='PLDEV', action='store_const', const='png', default='x11', help='store spectrum into PNG image file')
parser.add_argument('--npeaks',metavar='N',          type=int, help='label the first N peaks (default: 0)')
parser.add_argument('--avg'   ,dest='doAvg', action='store_true', help='average all spectra over time')

# Check args, show usage if necessary (before trying X11 display)
args = parser.parse_args()
XUNITS = args.XUNITS
XLSB   = args.XLSB
PLDEV  = args.PLDEV
doAvg  = args.doAvg
if args.zoom != None:
	zoom = args.zoom.split(',')
	if len(zoom)!=2:
		parser.print_help()
		sys.exit(1)
	startch = int(zoom[0])
	stopch = int(zoom[1])

#import matplotlib as mpl
#if True:
#    # Disable path simplifcation to allow fringe peaks to be seen even in dense plots
#    # http://stackoverflow.com/questions/15795720/
#    #   matplotlib-major-display-issue-with-dense-data-sets
#    mpl.rcParams['path.simplify'] = False
#import pylab # must be after import of matplotlib
import numpy

def plot_spectrum(x,s,title):
    #print(title)
    xformatter = mpl.ticker.ScalarFormatter(useOffset=False)
    #pylab.gcf().set_facecolor('white')
    #pylab.subplot(111)
    #pylab.semilogy(x,s,'k-')
    ##pylab.plot(s,'k-')
    #if XUNITS==XUNITS_CH:
    #    #pylab.xlabel('Channel')
    #else:
    #    #pylab.xlabel('Frequency (MHz)')
    #pylab.ylabel('Spectral Power')
    #pylab.gca().xaxis.set_major_formatter(xformatter)
    #pylab.title(title)
    #pylab.axis('tight')

def plot_cross_spectrum(x,s,title):
    #print(title)
    A  = numpy.abs(s)
    ph = numpy.angle(s, deg=True)
    #pylab.gcf().set_facecolor('white')
    #pylab.subplot(211)
    #pylab.semilogy(x,A,'k-')
    ##pylab.plot(A,'k-')
    #pylab.ylabel('Spectral Cross-Power')
    #pylab.title(title)
    #pylab.axis('tight')

    #pylab.subplot(212)
    #pylab.plot(x,ph,'kx')
    #pylab.ylabel('Phase (deg)')
    #if XUNITS==XUNITS_CH:
    #    #pylab.xlabel('Channel')
    #else:
    #    #pylab.xlabel('Frequency (MHz)')
    #pylab.xlabel('Channel')
    #pylab.axis('tight')

def mad(v):
    med = numpy.median(v)
    return numpy.median(numpy.abs(v - med))

def indicate_peaks(x,s,N,sigma=10.0):
    n = 0
    dx = numpy.mean(numpy.diff(x))
    stmp = numpy.abs(s.copy())
    dcut = sigma*mad(stmp)
    cut  = dcut + numpy.median(stmp)
    while (n < N):

        # Find next significant peak
        imax = numpy.argmax(stmp)
        if (stmp[imax] < cut):
            break
        n = n + 1

        t = '#%d: A=%.3e x=%f delta_cut=%f : A/dcut=%f' % (n, stmp[imax],x[imax],dcut, stmp[imax]/dcut)
        print t
        # #pylab.text(x[imax], stmp[imax], t)

        # Erase all sidelobes left and right of the peak
        smax = stmp[imax]
        i = imax-1
        while (i>0) and ((stmp[i-1]-stmp[i])<=dcut):
            stmp[i] = 0.0
            i = i - 1
        i = imax+1
        while (i<(len(s)-1)) and ((stmp[i+1]-stmp[i])<=dcut):
            stmp[i] = 0.0
            i = i + 1
        stmp[imax] = 0.0
        # #pylab.semilogy(x,stmp,'xr:') # for debugging, indicate what has been retained in for the next peak search

def process_kfftspec_file(fname,startch,stopch,Nmax):

    kfftspec = kfftspecReader(fname)
    Nshown = 0

    avg_specs = {}
    avg_counts = {}
    avg_weights = {}

    while True:

        (h,s) = kfftspec.get_next_spectrum()
        if h == None or s == None:
            print ('EOF')
            break
        N = len(s)
        key = '%s%s' % (h['signal_src_1'],h['signal_src_2'])
        if doAvg:
           if float(h['weight'])<0.4:
              # print('Skip %s w=%.3f' % (key,h['weight']))
              continue
           if not(key in avg_specs):
              avg_specs[key] = s
              avg_counts[key] = 1
              avg_weights[key] = float(h['weight'])
           else:
              avg_specs[key] = avg_specs[key] + s * float(h['weight'])
              avg_counts[key] = avg_counts[key] + 1
              avg_weights[key] = avg_weights[key] + float(h['weight'])
           s = avg_specs[key]  / avg_weights[key]
           h['Tint'] = h['Tint'] * avg_counts[key]
           h['weight'] = avg_weights[key] / avg_counts[key]
           if (avg_counts[key] % 50)>0:
               # print key, avg_counts[key]
               continue

        t = ('%s\nSignal %d x Signal %d, Tint=%.4f sec, bw=%.3f kHz, weight %.3f\nMid-time %s'
            % ( fname,
                h['signal_src_1'],h['signal_src_2'],h['Tint'], 1e-3*h['bandwidth'],
                h['weight'],h['timestamp_str']) )
        x = numpy.linspace(0,N-1,N)
        if XUNITS==XUNITS_HZ:
            x = 1e-6*x * h['bandwidth']/float(N)
            if XLSB:
                x = 2*1e-6*h['bandwidth'] - x
        elif XLSB:
             x = (N-1) - x

	x = x[startch:stopch]
	s = s[startch:stopch]

        #if numpy.iscomplexobj(s):
        #    plot_cross_spectrum(x,s,t)
        #else:
        #    plot_spectrum(x,s,t)

        #print t
        if (args.npeaks>0):
            indicate_peaks(x,s,args.npeaks)

        if PLDEV=='png':
            pname = os.path.splitext(fname)[0] + '.png'
            ##pylab.savefig(pname)
            #print ('Wrote plot into %s' % (pname))
            ##pylab.clf()
        else:
            ##pylab.show()
            pass

        Nshown += 1
        if (Nmax != None) and (Nshown >= Nmax):
            break

# Main
for fn in args.files:
	print fn
	process_kfftspec_file(fn,startch,stopch,args.nspec)
