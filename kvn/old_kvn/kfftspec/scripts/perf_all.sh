
#### SINGLE GPU ##########################################################

## 1-pol, 32k channels, no windowing, no callback:

./kfftspec -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

## 1-pol, 32k channels, no windowing, cuFFT Callback:

./kfftspec_cb -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

## 1-pol, 32k channels, Hamming, no callback:

./kfftspec -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    --winfunc=hamming 32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

## 1-pol, 32k channels, Hamming, cuFFT callback:

./kfftspec_cb -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    --winfunc=hamming 32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2


## 2-pol, 32k channels, no windowing, no callback, no cross-spectra:

./kfftspec -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-2-2

## 2-pol, 32k channels, no windowing, no callback, with cross-spectra:

./kfftspec -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    --cross 32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-2-2

## 2-pol, 32k channels, no windowing, cuFFT callback, with cross-spectra:

./kfftspec_cb -E expt001 -O jwagner -A KVN_Yonsei --devices=0 --nstreams=2 \
    --cross 32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-2-2



## Dual GPU, 1-pol, 32k channels, no windowing, no callback:

./kfftspec -E expt001 -O jwagner -A KVN_Yonsei --devices=0,1 --nstreams=2 \
    32768 0.256 \
    /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2

