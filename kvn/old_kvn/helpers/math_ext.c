#include "math_ext.h"

/**
 * Floating point modulo similar to standard C fmod(), but
 * with reduced precision. For example.
 *   fmod(1.0, 0.1) == 0.0999999... ~= 0.1, whereas
 *   fmod_prec(1.0, 0.1, 3) == 0
 *
 * @param numer Numerator
 * @param denom Denomerator
 * @param Ndecimals Number of decimals in precision
 */
double fmod_prec(double numer, double denom, int Ndecimals)
{
    // fmod() gives "unexpected" results
    //   fmod(n*0.1,0.1) = 0.1 when n=10 or n=60.
    //   See, e.g., http://stackoverflow.com/questions/4218961/why-fmod1-0-0-1-1
    // The unexpected result is correct.
    //
    // For time segmentation of VDIF data we would however want
    //   fmod(n*dT, dT) == 0 for all n, to some low-precision dT
    // Didn't prove it, but *probably* the above can be achieved
    // for all practically occurring n and dT by switching to
    // integer modulo and then back to floating point:
    double scale = pow10(Ndecimals);
    ssize_t N = round(numer*scale);
    ssize_t n = round(denom*scale);
    double m = (N % n) / scale;
    return m;
}


void fmod_prec_test(const double T_int)
{
    const int ndec = 4;
    double T, m;
    size_t i;
    for (i=0; i<60; i++) {
        T  = i*T_int;
        T += pow10(-ndec)*0.25;
        m  = fmod_prec(T,T_int,ndec);
        fprintf(stderr, " T=%f with mod to ndec=%d mod=%f : %s\n", T, ndec, m, (fabs(m) < 1e-7) ? "ok" : "fail");
    }
}

/**
 * Greatest common divisor
 */
size_t gcd(size_t a, size_t b)
{
  size_t t;
  while ( a != 0 ) {
     t = a;
     a = b % a;
     b = t;
  }
  return b;
}

