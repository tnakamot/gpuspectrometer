#ifndef MATH_EXT_H
#define MATH_EXT_H

#include <math.h>
#include <stdio.h>
#include <sys/types.h>

/**
 * Floating point modulo similar to standard C fmod(), but
 * with reduced precision. For example.
 *   fmod(1.0, 0.1) == 0.0999999... ~= 0.1, whereas
 *   fmod_prec(1.0, 0.1, 3) == 0
 *
 * @param numer Numerator
 * @param denom Denomerator
 * @param Ndecimals Number of decimals in precision
 */
double fmod_prec(double numer, double denom, int Ndecimals);

void fmod_prec_test(double denom);

/**
 * Greatest common divisor
 */
size_t gcd(size_t a, size_t b);

#endif
