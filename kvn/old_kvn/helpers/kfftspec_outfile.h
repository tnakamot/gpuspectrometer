#ifndef KFFTSPEC_OUTFILE_H
#define KFFTSPEC_OUTFILE_H

#include <stdint.h>
#include <time.h>
#include <sys/time.h>

/////////////////////////////////////////////////////////////////////////////////////
//
// KFFTSPEC Output File Format
//
// The output file has no special *globa*l header.
//
// Instead, every new spectrum in the file begins with an own header.
// The header describes the origin and length in bytes of the spectrum.
// The actual spectral data (a single power or cross-power spectrum) follows
// after the header, with all values in 32-bit floating point format.
//
// The header contains a field for sampler statistics, reserved for future use.
// Details on the sampler statistics are as follows:
//
// When 2 <= nlevels <= 16 (1-bit to 4-bit quantization), the start of the array
// i.e. plevels[0..nlevels-1] contains the normalized state counts of each level,
// and the remaining plevels[nlevels..15] are zero. The state counts are normalized
// such that sum(plevels)==1.0.
//
// When nlevels > 16 (5-bit quantization or better), the array plevels[0..15]
// contains a histogram of sample values, binned into 16 bins, with the
// zero voltage (0V) landing exactly between plevels[7] and plevels[8].
//
/////////////////////////////////////////////////////////////////////////////////////

#define KFFTSPEC_OUTFILE_DLAYOUT_RE        0
#define KFFTSPEC_OUTFILE_DLAYOUT_NxREIM    1
#define KFFTSPEC_OUTFILE_DLAYOUT_NxRE_NxIM 2

typedef struct kfftspec_outfile_spectrumheader_v1_tt {

    /* Header size */
    uint32_t   headerlen;    // Length of header in bytes

    /* Actual data, appended after header */
    uint32_t   datalen;      // Length in bytes of the spectrum that follows
    uint32_t   datalayout;   // 0: Re, 1: nchan*{Re,Im}, 2: {nchan*Re},{nchan*Im}

    /* Version (in case header is expanded in the future) */
    uint32_t   header_version; // kfftspec 0.6.x has version 0

    /* Data description */
    uint32_t   nchan;        // Number of spectral channels
    uint32_t   signal_src_1; // The source (some IF or subband number) of 1st and 2nd signal
    uint32_t   signal_src_2; // Power spectrum if src_1==src__2, cross-power if src_1!=src_2.
    double     Tint;         // Integration time in seconds
    double     bandwidth;    // Bandwidth of spectrum in Hz
    double     weight;       // 0 <= weight <= 1.0 is the amount valid data in this integration
    char       winfunc[8];   // Short name of window function (default: boxcar)
    double     winoverlap;   // 0 <= overlap < 1.0  amount of overlap during windowing

    /* Sampler statistics (quantizier state counts i.e. sample distribution; 0 <= p <= 1.0) */
    uint32_t   nlevels_src_1;     // Number of digitizer levels in 1st signal
    uint32_t   nlevels_src_2;     // Number of digitizer levels in 2nd signal
    double     plevels_src_1[16]; // Probabilities in 16 or fewer bins, 1st signal
    double     plevels_src_2[16]; // Probabilities in 16 or fewer bins, 2nd signal

    /* Computing info (help to trace errors in spectra to certain GPU) */
    uint8_t    GPU_device;
    uint8_t    GPU_stream;
    uint8_t    GPU_datasource; // 0=file, 1=network (10G), 2=network (IB), 3=network (ALMA'ish "OC-192")
    uint8_t    _struct_padding_1;

    /* Timestamp in multiple formats */
    double     timestamp_secondsofday;
    double     timestamp_tm_seconds;
    char       timestamp_str[20]; // YYYYDDDhhmmss.ffffff  = 13 + 1 ('.') + 6
    struct tm  timestamp_tm;

} kfftspec_outfile_spectrumheader_v1_t;

#endif
