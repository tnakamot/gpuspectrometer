/**
 * \class DataRecipient
 *
 * An interface invoked by UDPVDIFReceiver upon each new received
 * chunk of 2-bit raw data (VDIF header -less, properly time segmented).
 * Interface needs to be implemented by CPU or GPU spectral processor class.
 */

#ifndef DATARECIPIENT_HXX
#define DATARECIPIENT_HXX

#include "rawdatabuffer.hxx"

#include <stdlib.h>
#include <sys/time.h>

class DataRecipient {
    public:
        DataRecipient() {}
        virtual void takeData(RawDataBuffer* data) = 0;
};

/** Dummy no-op implementation of DataRecipient */
class DataRecipientNoop : public DataRecipient {
    public:
        DataRecipientNoop() {}
        void takeData(RawDataBuffer* data) { /* discard data */ }
};

#endif // DATARECIPIENT_HXX
