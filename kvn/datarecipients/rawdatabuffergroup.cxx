#include "rawdatabuffer.hxx"
#include "rawdatabuffergroup.hxx"
#include "logger.h"

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/time.h>

#ifndef PAGE_SZ
    #define PAGE_SZ 4096
#endif

RawDataBufferGroup::RawDataBufferGroup(int nbuffers, size_t nbytes, int nchan, int streamId, int verbosity)
{
    this->buffers = NULL;
    this->nbuffers = nbuffers;
    this->bufsize = nbytes;
    this->bufcapacity = nbytes - (nbytes%PAGE_SZ) + PAGE_SZ;   // rounding to full pages;
    this->nchan = nchan;
    this->streamId = streamId;
    this->verbosity = verbosity;
    allocate();
}

RawDataBufferGroup::~RawDataBufferGroup()
{
    if (buffers != NULL) {
        deallocate();
    }
}

void RawDataBufferGroup::allocate()
{
    if (buffers != NULL) {
        deallocate();
    }

    // Allocate buffers
    buffers = (RawDataBuffer**)calloc(nbuffers, sizeof(RawDataBuffer**));
    if (!buffers) {
        perror("calloc buffers");
        return;
    }

    // Initialise
    for (int n = 0; n < nbuffers; n++) {
        void* area = memalign(PAGE_SZ, bufcapacity);
        if (!area) {
            perror("memalign buffers[n]");
            continue;
        }
        buffers[n] = new RawDataBuffer(area, bufcapacity, nchan, streamId);
        buffers[n]->state = RawDataBuffer::Free;
        buffers[n]->prevstate = RawDataBuffer::Free;
        // for async DMA would need this somewhere: cudaHostRegister(buffers[n], buflen_ceil, cudaHostRegisterPortable);
        // here just mem-lock for now
        //if (mlock(buffers[n]->data, buffers[n]->capacity) == -1) {
        //    perror("mlock buffers[n]");
        //}
    }
}

void RawDataBufferGroup::deallocate()
{
    if (buffers != NULL) {
        for (int n = 0; n < nbuffers; n++) {
            delete buffers[n];
        }
        free(buffers);
        buffers = NULL;
    }
}

/** Find and reserve the first slot with given state, starting from slot n. */
int RawDataBufferGroup::reserveSlot(const RawDataBuffer::BufState state, int n)
{
    boost::mutex::scoped_lock grplock(mutex);

    // debug: print status of all slots
    if (verbosity > 2) {
        std::stringstream ss;
        ss << "Slot statuses: ";
        for (int i = 0; i < nbuffers; i++) {
            ss << i << ":" << RawDataBuffer::stateToStr(buffers[i]->state) << " ";
        }
        L_(ldebug2) << ss.str();
    }

    // Find first match to 'state'
    for (int i = 0; i < nbuffers; i++) {
        n = (n + i) % nbuffers;
        if ((buffers[n] != NULL) && (buffers[n]->state == state)) {
            buffers[n]->prevstate = buffers[n]->state;
            buffers[n]->state = RawDataBuffer::Busy;
            return n;
        }
    }

    return -1;
}

/** Find and reserve the first slot *not* in the given state, starting from slot n. */
int RawDataBufferGroup::reserveSlotInv(const RawDataBuffer::BufState state, int n)
{
    boost::mutex::scoped_lock grplock(mutex);

    // debug: print status of all slots
    if (verbosity > 2) {
        std::stringstream ss;
        ss << "Slot statuses: ";
        for (int i = 0; i < nbuffers; i++) {
            ss << i << ":" << RawDataBuffer::stateToStr(buffers[i]->state) << " ";
        }
        L_(ldebug2) << ss.str();
    }

    // Now look for first non-match to 'state'
    for (int i = 0; i < nbuffers; i++) {
        n = (n + i) % nbuffers;
        if ((buffers[n] != NULL) && (buffers[n]->state != state)) {
            buffers[n]->prevstate = buffers[n]->state;
            buffers[n]->state = RawDataBuffer::Busy;
            return n;
        }
    }

    return -1;
}

/** Mark slot as full (or Overflowed, depending on earlier state) */
void RawDataBufferGroup::occupySlot(int slotnr)
{
    boost::mutex::scoped_lock grplock(mutex);
    RawDataBuffer* slot = buffers[slotnr];

    switch (slot->prevstate)
    {
        case RawDataBuffer::Free:
            slot->state = RawDataBuffer::Full;
            break;
        case RawDataBuffer::Full:
            slot->state = RawDataBuffer::Overflowed;
            break;
        case RawDataBuffer::Overflowed:
            // allow an overflow condition to clear, rather than sticking around permanently?
            slot->state = RawDataBuffer::Full;
            break;
        case RawDataBuffer::Busy:
        case RawDataBuffer::Transferring:
            slot->state = RawDataBuffer::Full;
            L_(lerror) << "Unexpected previous VDIF receiver slot state of 'Busy' or 'Xfer'";
            break;
        default:
            slot->state = RawDataBuffer::Full;
            L_(lerror) << "Unexpected previous VDIF receiver slot state";
            break;
    }
}

/** Free up a buffer slot */
void RawDataBufferGroup::releaseSlot(int slotnr)
{
    boost::mutex::scoped_lock grplock(mutex);
    buffers[slotnr]->state = RawDataBuffer::Free;
}

/** Set slot state */
void RawDataBufferGroup::setSlotState(const RawDataBuffer::BufState state, int slotnr)
{
    boost::mutex::scoped_lock grplock(mutex);
    buffers[slotnr]->state = state;
}
