#ifndef TCPSERVER_HXX
#define TCPSERVER_HXX

#include <boost/asio.hpp>
#include <string>

#include <boost/thread.hpp>

using boost::asio::ip::tcp;

class TCPSimpleServer
{
    public:
        TCPSimpleServer(int port=1337, bool binary=true) : m_port(port),m_binaryMode(binary) {}

    private:

        /** Worker that acts on received data; implement in a derived class!
         * \return false if TCP iostream went stale, out of sync, or otherwise corrupt
         */
        virtual bool work(boost::asio::ip::tcp::iostream& s) = 0;

        /** Receive data and pass it to work()
         * Invoking run() in non-blocking mode launches this function as a thread (background),
         * while in blocking mode this function is entered directly.
         */
        void run_thread();

    public:
        /** \return port number the object listens on */
        int getPort() const { return m_port; }

        /** Start receiver */
        void run(bool blocking=false);

    public:
        /** Set binary vs text mode */
        void setMode(bool binary) { m_binaryMode=binary; }

        /** Receive data in the pre-set mode */
        bool receive(boost::asio::ip::tcp::iostream& s, std::string& out) {
             if (m_binaryMode) {
                 return receive_bin(s, out);
             } else {
                 return receive_txt(s, out);
             }
        }

        /** Send data in the pre-set mode */
        bool send(boost::asio::ip::tcp::iostream& s, const std::string& msg) const {
             if (m_binaryMode) {
                 return send_bin(s, msg);
             } else {
                 return send_txt(s, msg);
             }
        }

    private:
        bool receive_txt(boost::asio::ip::tcp::iostream& s, std::string& out);
        bool receive_bin(boost::asio::ip::tcp::iostream& s, std::string& out);
        bool send_txt(boost::asio::ip::tcp::iostream& s, const std::string& msg) const;
        bool send_bin(boost::asio::ip::tcp::iostream& s, const std::string& msg) const;

    private:
        boost::thread* m_clientAcceptorThread;
        const int m_port;
        bool m_binaryMode;
};

#endif // TCPSERVER_HXX

