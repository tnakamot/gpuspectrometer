
#include "logger.h"

#include "core/common.hxx"
#include "network/spectrometertcpserver.hxx"

#include <boost/asio.hpp>
#include <boost/algorithm/string/trim_all.hpp>

/** C'stor, link to spectrometer object, listen on port, accept text or binary */
SpectrometerTCPServer::SpectrometerTCPServer(CommandRecipient& cmdrecipient, int tcp_port, bool binaryMode)
  : TCPSimpleServer(tcp_port,binaryMode), m_cmdrecipient(cmdrecipient)
{
    // nothing
}

void SpectrometerTCPServer::trim(std::string& str, bool safe=false)
{
    str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
    str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());
    str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
    if (!safe) {
        boost::algorithm::trim_all(str);
    }
}

bool SpectrometerTCPServer::work(boost::asio::ip::tcp::iostream& tcp_io)
{
    // Receive a command
    std::string cmd;
    try {
        bool ok = receive(tcp_io, cmd);
        SpectrometerTCPServer::trim(cmd);
        if (!ok || (cmd.size() < 1)) {
            return false;
        }
        L_(ldebug) << "server::work() got KVN command : " << cmd;
    } catch (...) {
        L_(lerror) << "server::work() data reception failed";
        return false;
    }

    // Process the command and reply
    std::string resp;
    try {
        resp = m_cmdrecipient.handleCommand(cmd);
        tcp_io << resp << "\r\n";
        L_(linfo) <<  "server::work() sent response : " << resp;
    } catch (std::exception &e) {
        L_(lerror) << "server::work() handling error : " << e.what() << ", str = " << resp;
        return false;
    }

    return true;
}
