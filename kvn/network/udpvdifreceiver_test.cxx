
// usage: udpRxTest [<port> [<psn bytes> [<'s' for simulate>]]]

#include "udpvdifreceiverinterface.hxx"
#include "udpvdifreceiver_sim.hxx"
#include "udpvdifreceiver.hxx"

#include <signal.h>
#include <stdlib.h>

static volatile bool ctrlC_pressed = false;

void sigintHandler(int)
{
    ctrlC_pressed = true;
}

int main(int argc, char** argv)
{
    const int Nbufs = 8;
    int port = 46227;
    int psnbytes = 0;
    int verbosity = 3;

    DataRecipientNoop rcv;
    UDPVDIFReceiverInterface* udpRx;

    if (argc >= 2) {
        port = atoi(argv[1]);
    }
    if (argc >= 3) {
        psnbytes = atoi(argv[2]);
    }

    if (argc >= 4 && argv[3][0]=='s') {
        udpRx = new UDPVDIFReceiverSim(verbosity);
    } else {
        udpRx = new UDPVDIFReceiver(verbosity);
    }

    std::cout << "Opening UDP/IP port " << port << "\n";
    if (!udpRx->open(port, psnbytes)) {
        std::cout << "Error opening UDP port\n";
        return 0;
    }

    std::cout << "Inspecting UDP/IP VDIF traffic on port " << port << "\n";
    if (!udpRx->analyze()) {
        std::cout << "Error determing UDP VDIF information from UDP stream\n";
        return 0;
    }

    size_t framespersec = udpRx->getVDIFFramesPerSec();
    size_t payloadsize = udpRx->getVDIFPayloadSize();
    size_t bufsize = 0;
    double buftime = 0;
    for (int div=10; div>0; div--){
        if ((framespersec % div) == 0) {
            bufsize = (framespersec / div) * payloadsize;
            buftime = 1 / (double)div;
            break;
        }
    }
    if (!udpRx->allocate(Nbufs, bufsize)) {
        std::cout << "Error allocating " << std::fixed << buftime*1e3 << "ms receive buffers\n";
        return 0;
    }
    std::cout << "Allocated " << std::fixed << buftime*1e3 << "ms receive buffers\n";

    std::cout << "\n### Receiving for 5 sec then stoppping for 5 sec\n";
    udpRx->startRx();
    sleep(5);

    std::cout << "\n### Stopping for 5 sec\n";
    udpRx->stopRx();
    sleep(5);

    std::cout << "\n### Receiving again 5 sec, then adding recipient\n";
    std::cout << "### Hit Ctrl-C to stop receiver loop\n";
    udpRx->startRx();
    sleep(5);

    std::cout << "\n### Registering recipient\n";
    udpRx->registerRecipient(&rcv);
    //udpRx->startRx();

    std::cout << "\n### Continue receiving, into recipient\n";
    if (0) {
        signal(SIGINT, sigintHandler);
        while (!ctrlC_pressed) sleep(1);
    } else if(0) {
        while (1) sleep(1);
    } else {
        sleep(20);
    }

    udpRx->stopRx();

    udpRx->deallocate();
    udpRx->close();

    return 0;
}


