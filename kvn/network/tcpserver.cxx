#include "logger.h"
#include "network/tcpserver.hxx"

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <string>
#include <iostream>

#include <unistd.h>    // fork()
#include <string.h>    // memset()
#include <stdint.h>    // int32_t
#include <arpa/inet.h> // ntohl(), htonl()

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** A getline() for various line terminations.
 * Copied from http://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
 */
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\0':
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void TCPSimpleServer::run(bool blocking)
{
    using namespace boost::asio::ip;
    if (!blocking) {
        m_clientAcceptorThread = new boost::thread(&TCPSimpleServer::run_thread, this);
    } else {
        run_thread();
    }
}

void TCPSimpleServer::run_thread()
{
    boost::asio::io_service ios;
    tcp::endpoint endpoint(tcp::v4(), m_port);
    tcp::acceptor acceptor(ios, endpoint);
    acceptor.set_option(tcp::acceptor::reuse_address(true));

    for (;;) {

        tcp::iostream stream;
        tcp::endpoint peer;
        boost::system::error_code ec;

        // Get a new client
        acceptor.accept(*stream.rdbuf(), peer, ec);
        if (ec) {
            L_(lerror) << "Could not accept client on TCP port " << m_port << ", " << ec.message();
            continue;
        } else {
            L_(linfo) << "New client from " << peer.address().to_string()
                      << " port " << peer.port();
        }

        // Handle client until TCP connection breaks down
        bool ok = true;
        while (ok) {
            ok &= this->work(stream);
            ok &= !(!stream);
        }
        L_(linfo) << "Client at port " << m_port
                  << " from " << peer.address().to_string()
                  << " port " << peer.port()
                  << " disconnected";

        // Done, next client can connect now
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool TCPSimpleServer::receive_txt(boost::asio::ip::tcp::iostream& s, std::string& cmd)
{
    std::istream is(s.rdbuf());
    safeGetline(is, cmd);
    // std::cerr << "s.good()=" << s.good() << ", s.fail()=" << s.fail() << ", sstream() got '" << cmd << "', len=" << cmd.size() << "\n";
    bool ok = s.good() && (!s.fail()) && (cmd.size() > 0);
    return ok;
}

bool TCPSimpleServer::send_txt(boost::asio::ip::tcp::iostream& s, const std::string& msg) const
{
    s << msg << '\n';
    s.flush();
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool TCPSimpleServer::receive_bin(boost::asio::ip::tcp::iostream& s, std::string& cmd)
{
    // First get 4-byte binary integer with the length of KVN command to follow
    int32_t nbyte;
    s.read(reinterpret_cast<char*>(&nbyte), sizeof(nbyte));
    nbyte = ntohl(nbyte);
    if ((nbyte < 1) || (nbyte > 512*1024)) {
        std::cerr << "Error: client sent unexpected command length field of " << nbyte << " bytes (not in 1B--512kB range)\n";
        cmd = "";
        return false;
    }

    // Now get N-byte text containing KVN command without null termination
    char rx[nbyte+32];
    s.read(rx, nbyte);
    rx[nbyte] = 0;
    std::string rxs(rx);
    cmd = rxs;

    bool ok = s && ((int)rxs.size() == nbyte);
    return ok;
}

bool TCPSimpleServer::send_bin(boost::asio::ip::tcp::iostream& s, const std::string& msg) const
{
    // First send 4-byte binary integer with the length of KVN command to follow
    int32_t nbyte = htonl(msg.size());
    s.write(reinterpret_cast<char*>(&nbyte), sizeof(nbyte));

    // Now send the KVN response without null termination
    s.write(msg.c_str(), msg.size());
    s.flush();

    return true;
}
