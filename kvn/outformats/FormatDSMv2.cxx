/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

#include "outformats/FormatDSMv2.hxx"
#include "core/spectrometerconfig.hxx"
#include "logger.h"

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <memory.h>
#include <malloc.h>

#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>

/////////////////////////////////////////////////////////////////////////////////////

static void str2bcd(char* io);

float FormatDSMv2::m_standard_DSM_Tints_s[5] = { 10.24e-3, 51.2e-3, 102.4e-3, 512e-3, 1024e-3 };

/////////////////////////////////////////////////////////////////////////////////////

// C'stor
FormatDSMv2::FormatDSMv2()
{
    m_serialNum = 0;
    m_ipNum = 0;
}

/** Convert GPU spectrometer data into KVN DSM format. Allocates new 'out' buffer */
int FormatDSMv2::convertData(char** out, size_t& nbyte_out, const char* in, size_t nbyte_in, double w, struct timeval tref, const SpectrometerConfig* cfg)
{
    // Mask of streams
    KVN_OutStreams_t streammask;
    streammask.word32 = 0;
    streammask.bit.auto_f1 = cfg->scfg.enableAutocorr[0];
    streammask.bit.auto_f2 = cfg->scfg.enableAutocorr[1];
    streammask.bit.auto_f3 = cfg->scfg.enableAutocorr[2];
    streammask.bit.auto_f4 = cfg->scfg.enableAutocorr[3];
    streammask.bit.auto_f5 = cfg->scfg.enableAutocorr[4];
    streammask.bit.auto_f6 = cfg->scfg.enableAutocorr[5];
    streammask.bit.auto_f7 = cfg->scfg.enableAutocorr[6];
    streammask.bit.auto_f8 = cfg->scfg.enableAutocorr[7];
    streammask.bit.auto_f9  = cfg->scfg.enableAutocorr[8];
    streammask.bit.auto_f10 = cfg->scfg.enableAutocorr[9];
    streammask.bit.auto_f11 = cfg->scfg.enableAutocorr[10];
    streammask.bit.auto_f12 = cfg->scfg.enableAutocorr[11];
    streammask.bit.auto_f13 = cfg->scfg.enableAutocorr[12];
    streammask.bit.auto_f14 = cfg->scfg.enableAutocorr[13];
    streammask.bit.auto_f15 = cfg->scfg.enableAutocorr[14];
    streammask.bit.auto_f16 = cfg->scfg.enableAutocorr[15];
    streammask.bit.cross_f1f2_re = cfg->scfg.enableCrosscorr[0];
    streammask.bit.cross_f1f2_im = cfg->scfg.enableCrosscorr[0];
    streammask.bit.cross_f3f4_re = cfg->scfg.enableCrosscorr[1];
    streammask.bit.cross_f3f4_im = cfg->scfg.enableCrosscorr[1];
    streammask.bit.cross_f5f6_re = cfg->scfg.enableCrosscorr[2];
    streammask.bit.cross_f5f6_im = cfg->scfg.enableCrosscorr[2];
    streammask.bit.cross_f7f8_re = cfg->scfg.enableCrosscorr[3];
    streammask.bit.cross_f7f8_im = cfg->scfg.enableCrosscorr[3];
    streammask.bit.cross_f9f10_re = cfg->scfg.enableCrosscorr[4];
    streammask.bit.cross_f9f10_im = cfg->scfg.enableCrosscorr[4];
    streammask.bit.cross_f11f12_re = cfg->scfg.enableCrosscorr[5];
    streammask.bit.cross_f11f12_im = cfg->scfg.enableCrosscorr[5];
    streammask.bit.cross_f13f14_re = cfg->scfg.enableCrosscorr[6];
    streammask.bit.cross_f13f14_im = cfg->scfg.enableCrosscorr[6];
    streammask.bit.cross_f15f16_re = cfg->scfg.enableCrosscorr[7];
    streammask.bit.cross_f15f16_im = cfg->scfg.enableCrosscorr[7];

    // Allocate output buffer
    nbyte_out = sizeof(FormatDSMv2::KVN_DSM_Header_t);
    nbyte_out += cfg->scfg.nenabledbands * cfg->scfg.nchan*sizeof(float); // auto
    nbyte_out += (cfg->scfg.nspectra - cfg->scfg.nenabledbands) * cfg->scfg.nchan*2*sizeof(float); // cross Re,Im parts
    *out = (char*)memalign(4096, nbyte_out);

    // Convenience pointers : input data have spectra in order of [auto1 auto2 auto3 auto4 auto5 .... cross12 cross34 ...]
    const float* in_autos = (float*)in;
    const float* in_crosses = ((float*)in) + cfg->scfg.nenabledbands*cfg->scfg.nchan;
    float* payload_out = (float*)(*out + sizeof(FormatDSMv2::KVN_DSM_Header_t));

    // Header details
    FormatDSMv2::KVN_DSM_Header_t* h = (FormatDSMv2::KVN_DSM_Header_t*)(*out);
    memset(h, 0, sizeof(FormatDSMv2::KVN_DSM_Header_t));
    h->header_length = sizeof(FormatDSMv2::KVN_DSM_Header_t);
    h->sequence_number = m_ipNum % KVN_DSMv2_MAX_IP_PER_FILE;
    if (m_ipNum >= KVN_DSMv2_MAX_IP_PER_FILE) {
        m_ipNum = m_ipNum % KVN_DSMv2_MAX_IP_PER_FILE;
        m_serialNum++;
    }
    timeval2bcd(tref, h->start_time);
    h->bandwidth = cfg->vdifinfo.bw;
    h->output_streams = streammask.word32; // 32bit: filled out bitwise earlier above
    h->valid_samples = w * cfg->pcfg.nfft * 2*cfg->scfg.nchan;
    h->invalid_samples = (uint32_t)(cfg->pcfg.nfft * 2*cfg->scfg.nchan) - h->valid_samples;
    h->integration_length = getIntegrationLengthID(cfg->pcfg.Tint_s);
    h->actual_integration = cfg->pcfg.Tint_s * 1e3; // millisec
    h->spectral_channels = cfg->scfg.nchan;
    h->start_channel = 0;
    h->stop_channel = cfg->scfg.nchan - 1;

    // File name (if used externally)
    std::ostringstream ss;
    ss << cfg->scfg.obsPath << m_serialNum << ".SPC.dat";
    m_currFilename = ss.str();

    // Fill the 2-bit statistics of up to 16 IFs
    // TODO : h->sampler_stats[<if>][<4 levels>] = ...

    // Append all spectra in the correct order; place crosses immediately after their auto pair
    const int max_bands = sizeof(cfg->scfg.enableAutocorr) / sizeof(cfg->scfg.enableAutocorr[0]);
    for (int n=0; n<max_bands/2; n++) {
        appendSpectrum_if(cfg->scfg.enableAutocorr[2*n],   in_autos,   payload_out, cfg->scfg.nchan);
        appendSpectrum_if(cfg->scfg.enableAutocorr[2*n+1], in_autos,   payload_out, cfg->scfg.nchan);
        appendSpectrum_if(cfg->scfg.enableCrosscorr[n],    in_crosses, payload_out, 2*cfg->scfg.nchan);
    }

    // printf(" seq = %u   valid = %u  invalid = %u\n", h->sequence_number, h->valid_samples, h->invalid_samples);

    // Prepare for next batch
    m_ipNum++;

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

void FormatDSMv2::appendSpectrum_if(bool enabled, const float*& in, float*& out, int nfloats)
{
    if (!enabled) {
        return;
    }
    memcpy(out, in, sizeof(float)*nfloats);
    in += nfloats;
    out += nfloats;
}

int FormatDSMv2::getIntegrationLengthID(double T_int_s)
{
    int id = 0;
    // 1: 10.24ms, 2: 51.2ms, 3: 102.4ms, 4: 512ms, 5: 1024ms
    for (size_t n=0; n<sizeof(m_standard_DSM_Tints_s)/sizeof(m_standard_DSM_Tints_s[0]); n++) {
        if (std::abs(m_standard_DSM_Tints_s[n] - T_int_s) < 1e-3) {
            id = n;
        }
    }
    if (id == 0) {
        L_(lwarning) << "non-DSM-standard integration length of " << std::fixed << std::setprecision(2) << (T_int_s*1e3) << "ms";
    }
    return id;
}

void FormatDSMv2::timeval2bcd(struct timeval& tref, unsigned char* out)
{
    char dtime[20];
    timeval2YYYYDDDhhmmss_fmt(dtime, sizeof(dtime), &tref, "%04d%03d%02d%02d%02.0f");
    str2bcd(dtime);
    memcpy(out, dtime, sizeof(FormatDSMv2::KVN_DSM_Header_t::start_time));
    out[6] &= 0xF0;
}

/** Converts a 'struct timeval' into a string representation. The output format is specified in 'fmt'. */
void FormatDSMv2::timeval2YYYYMMDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_mon, tm_val->tm_mday,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/** Converts a 'struct timeval' into a string representation, with Day-of-Year. The output format is specified in 'fmt'. */
void FormatDSMv2::timeval2YYYYDDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_yday + 1,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/////////////////////////////////////////////////////////////////////////////////////

static void str2bcd(char* io)
{
    char* out = io;
    while (1) {
        int v = toupper(*(io++));
        if (v == '\0') { *out = 0x00; out++; break; }
        *out = (v>='0' && v<='9') ? (v-'0')<<4 : (10+v-'A')<<4; // TODO: check for illegal inputs
        v = toupper(*(io++));
        if (v == '\0') { *out &= 0xF0; out++; break; }
        *out |= (v>='0' && v<='9') ? (v-'0')&0x0F : (10+v-'A')&0x0F; // TODO: check for illegal inputs
        out++;
    }
    while (out <= io) {
        *out = '\0';
        out++;
    }
}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef TEST_BCD

int main(int argc, char** argv)
{

    if (argc > 1) {
       char* s = strdup(argv[1]);
       str2bcd(s);
       for (size_t i = 0; i < (strlen(argv[1])+1)/2; i++) {
           printf("%02x", (unsigned char)s[i]);
       }
       printf("\n");
    }


    FormatDSMv2::KVN_OutStreams_t streammask;
    streammask.word32 = 0;
    printf("all unset, mask : %08X\n", streammask.word32);
    streammask.bit.auto_f2 = true;
    printf("f2 set, mask : %08X\n", streammask.word32);
    streammask.bit.auto_f1 = true;
    streammask.bit.auto_f2 = true;
    streammask.bit.cross_f1f2_re = true;
    streammask.bit.cross_f1f2_im = true;
    streammask.bit.auto_f15 = true;
    streammask.bit.auto_f16 = true;
    streammask.bit.cross_f15f16_re = true;
    streammask.bit.cross_f15f16_im = true;
    printf("first 4 last 4 set, mask : %08X\n", streammask.word32);

    return 0;
}

#endif
