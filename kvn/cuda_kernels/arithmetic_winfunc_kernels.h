/////////////////////////////////////////////////////////////////////////////////////
//
// Windowing functions. Applied in-place to data.
//
// (C) 2015 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_WINFUNC_KERNELS_H
#define ARITHMETIC_WINFUNC_KERNELS_H

#include <cufft.h>
#include <cufftXt.h>

// cuFFT Load callback parameter passed via "void* callerInfo".
// See CUDA example simpleCUFFT_callback.cu
typedef struct cu_window_cb_params_tt {
        size_t fftlen;
} cu_window_cb_params_t;

#endif // ARITHMETIC_WINFUNC_KERNELS_H
