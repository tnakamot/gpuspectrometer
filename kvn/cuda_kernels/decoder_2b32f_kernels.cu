/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for decoding 2-bit VDIF sample data into 32-bit float.
//
// (C) 2015 Jan Wagner
//
// The Titan X memory bus bandwidth is 336 GB/s maximum. 
// In the ideal case this allows 336 GB/s / (sizeof(float) + 1/4 byte) = 79 Gs/s (base2)
// or about 84 Gs/s (decimal), but since each thread reads one byte and then writes
// sixteen bytes, there will be a lot of overhead...
//
// There are several decoder versions. They are experimenting with various approaches
// at making fast decoder implementations.
//
// The fastest decoder is 'cu_decode_2bit1ch_1D_v2' utilizing a small 4 x float lookup.
// It decodes VDIF samples. The Mark5B counterpart is cu_decode_2bit1ch_1D_mark5b_v2().
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef DECODER_2B32F_KERNELS_CU
#define DECODER_2B32F_KERNELS_CU

#include "decoder_2b32f_large_lookup.cu" // contains float4 c_lookup_2bit[256] of ~4kB in size
#include "cuda_utils.cu"                 // contains operator* for float4*float

#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

#ifndef DECODER_LUT_2BIT_TO_4FLOAT_VDIF
    #define DECODER_LUT_2BIT_TO_4FLOAT_VDIF   {-3.3359, -1.0, +1.0, +3.3359}
    #define DECODER_LUT_2BIT_TO_4FLOAT_MARK5B {-3.3359, +1.0, -1.0, +3.3359}
    __constant__ float c_lut2bit_vdif[4]   = DECODER_LUT_2BIT_TO_4FLOAT_VDIF;
    __constant__ float c_lut2bit_mark5b[4] = DECODER_LUT_2BIT_TO_4FLOAT_MARK5B;
#endif

// Functions
__global__ void cu_decode_2bit1ch(const uint8_t* src, float4* dst, const size_t Nbytes);
__global__ void cu_decode_2bit1ch_complex(const uint8_t* src, float4* dst, const size_t Nbytes);
__global__ void cu_decode_2bit2ch(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes);
__global__ void cu_decode_2bit4ch(const uint8_t* __restrict__ src, float* __restrict__ dst_ch0, float* __restrict__ dst_ch1, float* __restrict__ dst_ch2, float* __restrict__ dst_ch3, const size_t Nbytes);
__global__ void cu_decode_2bit4Xch(const uint8_t* __restrict__ src, float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch, const size_t Nbytes, const size_t Nch);

// Functions with windowing
__global__ void cu_decode_2bit1ch_Hann(const uint8_t* src, float4* dst, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit2ch_Hann(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit4ch_Hann(const uint8_t* __restrict__ src, float* __restrict__ dst_ch0, float* __restrict__ dst_ch1, float* __restrict__ dst_ch2, float* __restrict__ dst_ch3, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit4Xch_Hann(const uint8_t* __restrict__ src, float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch, const size_t Nbytes, const size_t Lfft, const size_t Nch);
__global__ void cu_decode_2bit1ch_Hamming(const uint8_t* src, float4* dst, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit2ch_Hamming(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit4ch_Hamming(const uint8_t* __restrict__ src, float* __restrict__ dst_ch0, float* __restrict__ dst_ch1, float* __restrict__ dst_ch2, float* __restrict__ dst_ch3, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_2bit4Xch_Hamming(const uint8_t* __restrict__ src, float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch, const size_t Nbytes, const size_t Lfft, const size_t Nch);
__global__ void cu_decode_2bit1ch_CustomWindow(const uint8_t* src, float4* dst, const size_t Nbytes, const size_t Lfft, const float4* __restrict__ wf);

// Wrappers CPU --> CUDA
void decode_2bitNch(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src, float** __restrict__ dst_chs,
    const size_t Nbytes, const size_t Nch);
void decode_2bitNch_Hann(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src, float** __restrict__ dst_chs,
    const size_t Nbytes, const size_t Nch);
void decode_2bitNch_Hamming(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src, float** __restrict__ dst_chs,
    const size_t Nbytes, const size_t Nch);

// Old functions (performance testing only)
__global__ void cu_decode_2bit1ch_1D_v1b(const uint8_t* src, float4* dst, size_t Nbytes, size_t bytes_per_thread);
__global__ void cu_decode_2bit1ch_1D_v2(const uint8_t* src, float4* dst, size_t Nbytes, size_t dummy);
__global__ void cu_decode_2bit1ch_1D_v3(const uint8_t* src, float4* dst, size_t Nbytes, size_t bytes_per_thread);
__global__ void cu_decode_2bit1ch_1D_v4(const uint8_t* src, float4* dst, size_t Nbytes, size_t bytes_per_thread);
__global__ void cu_decode_2bit1ch_1D_mark5b_v2(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, size_t dummy);

/////////////////////////////////////////////////////////////////////////////////////
// DECODER KERNELS without windowing
/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_decode_2bit1ch(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; } // warp divergence...
        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_vdif[(v >> 0) & 3],
	    c_lut2bit_vdif[(v >> 2) & 3],
	    c_lut2bit_vdif[(v >> 4) & 3],
	    c_lut2bit_vdif[(v >> 6) & 3]
	};
        dst[i] = tmp;
        return;
}

__global__ void cu_decode_2bit1ch_complex(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i >= Nbytes) { return; } // warp divergence...
    uint8_t v = src[i];
#if 0
    float4 tmp1 = {
        c_lut2bit_vdif[(v >> 0) & 3], 0.0f,
        c_lut2bit_vdif[(v >> 2) & 3], 0.0f

    };
    float4 tmp2 = {
        c_lut2bit_vdif[(v >> 4) & 3], 0.0f,
        c_lut2bit_vdif[(v >> 6) & 3], 0.0f
    };
    dst[2*i] = tmp1;
    dst[2*i+1] = tmp2;
#else
    float* dstf = (float*)dst;
    dstf[0] = c_lut2bit_vdif[(v >> 0) & 3];
    dstf[2] = c_lut2bit_vdif[(v >> 2) & 3];
    dstf[4] = c_lut2bit_vdif[(v >> 4) & 3];
    dstf[6] = c_lut2bit_vdif[(v >> 6) & 3];
#endif
    return;
}

__global__ void cu_decode_2bit2ch(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float2 ch0 = { c_lut2bit_vdif[(v >> 0) & 3], c_lut2bit_vdif[(v >> 4) & 3] };
        float2 ch1 = { c_lut2bit_vdif[(v >> 2) & 3], c_lut2bit_vdif[(v >> 6) & 3] };
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        return;
}

__global__ void cu_decode_2bit4ch(const uint8_t* __restrict__ src,
     float* __restrict__ dst_ch0, float* __restrict__ dst_ch1,
     float* __restrict__ dst_ch2, float* __restrict__ dst_ch3,
     const size_t Nbytes)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float ch0 = c_lut2bit_vdif[(v >> 0) & 3];
        float ch1 = c_lut2bit_vdif[(v >> 4) & 3];
        float ch2 = c_lut2bit_vdif[(v >> 2) & 3];
        float ch3 = c_lut2bit_vdif[(v >> 6) & 3];
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        dst_ch2[i] = ch2;
        dst_ch3[i] = ch3;
        return;
}

__global__ void cu_decode_2bit4Xch(const uint8_t* __restrict__ src,
    float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch,
    const size_t Nbytes, const size_t Nch)
{
        // Must have Nch >= 4
        const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        const size_t base_ch = (4*i) % Nch;
        const size_t samplenr = (4*i) / Nch;
        const size_t out_chA = base_ch;
        const size_t out_chB = (base_ch + 1) % Nch;
        const size_t out_chC = (base_ch + 2) % Nch;
        const size_t out_chD = (base_ch + 3) % Nch;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float s0 = c_lut2bit_vdif[(v >> 0) & 3];
        float s1 = c_lut2bit_vdif[(v >> 4) & 3];
        float s2 = c_lut2bit_vdif[(v >> 2) & 3];
        float s3 = c_lut2bit_vdif[(v >> 6) & 3];
        (dst_chs_base + dst_delta_next_ch*out_chA)[samplenr] = s0;
        (dst_chs_base + dst_delta_next_ch*out_chB)[samplenr] = s1;
        (dst_chs_base + dst_delta_next_ch*out_chC)[samplenr] = s2;
        (dst_chs_base + dst_delta_next_ch*out_chD)[samplenr] = s3;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////
// DECODER KERNELS with HANN WINDOWING
/////////////////////////////////////////////////////////////////////////////////////

#ifdef M_2PI_f32
    #undef M_2PI_f32
#endif
#ifdef m_cosf
    #undef m_cosf
#endif

#define M_2PI_f32 6.283185307179586f
#define m_cosf(x) __cosf(x)  // use cosf(x) or __cosf(x)

__global__ void cu_decode_2bit1ch_Hann(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; } // warp divergence...
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = (4*i) % Lfft;
        float4 wf = {
            (1.0f - m_cosf(omega*(n_win+0))) / 2.0f,
            (1.0f - m_cosf(omega*(n_win+1))) / 2.0f,
            (1.0f - m_cosf(omega*(n_win+2))) / 2.0f,
            (1.0f - m_cosf(omega*(n_win+3))) / 2.0f
        };
        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_vdif[(v >> 0) & 3] * wf.x,
	    c_lut2bit_vdif[(v >> 2) & 3] * wf.y,
	    c_lut2bit_vdif[(v >> 4) & 3] * wf.z,
	    c_lut2bit_vdif[(v >> 6) & 3] * wf.w
	};
        dst[i] = tmp;
        return;
}

__global__ void cu_decode_2bit2ch_Hann(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = (2*i) % Lfft;
        float2 wf = {
            (1.0f - m_cosf(omega*(n_win+0))) / 2.0f,
            (1.0f - m_cosf(omega*(n_win+1))) / 2.0f
        };
        uint8_t v = src[i];
        float2 ch0 = { c_lut2bit_vdif[(v >> 0) & 3] * wf.x, c_lut2bit_vdif[(v >> 4) & 3] * wf.y };
        float2 ch1 = { c_lut2bit_vdif[(v >> 2) & 3] * wf.x, c_lut2bit_vdif[(v >> 6) & 3] * wf.y };
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        return;
}

__global__ void cu_decode_2bit4ch_Hann(const uint8_t* __restrict__ src,
     float* __restrict__ dst_ch0, float* __restrict__ dst_ch1,
     float* __restrict__ dst_ch2, float* __restrict__ dst_ch3,
     const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = i % Lfft;
        float wf = (1.0f - m_cosf(omega*(n_win))) / 2.0f;
        uint8_t v = src[i];
        float ch0 = c_lut2bit_vdif[(v >> 0) & 3] * wf;
        float ch1 = c_lut2bit_vdif[(v >> 4) & 3] * wf;
        float ch2 = c_lut2bit_vdif[(v >> 2) & 3] * wf;
        float ch3 = c_lut2bit_vdif[(v >> 6) & 3] * wf;
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        dst_ch2[i] = ch2;
        dst_ch3[i] = ch3;
        return;
}

__global__ void cu_decode_2bit4Xch_Hann(const uint8_t* __restrict__ src,
    float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch,
    const size_t Nbytes, const size_t Lfft, const size_t Nch)
{
        // Must have Nch >= 4
        const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        const size_t base_ch = (4*i) % Nch;
        const size_t samplenr = (4*i) / Nch;
        const float omega = M_2PI_f32 / (Lfft - 1);
        const float n_win = samplenr % Lfft;
        const float wf = (1.0f - m_cosf(omega*(n_win))) / 2.0f;
        const size_t out_chA = base_ch;
        const size_t out_chB = (base_ch + 1) % Nch;
        const size_t out_chC = (base_ch + 2) % Nch;
        const size_t out_chD = (base_ch + 3) % Nch;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float s0 = c_lut2bit_vdif[(v >> 0) & 3] * wf;
        float s1 = c_lut2bit_vdif[(v >> 4) & 3] * wf;
        float s2 = c_lut2bit_vdif[(v >> 2) & 3] * wf;
        float s3 = c_lut2bit_vdif[(v >> 6) & 3] * wf;
        (dst_chs_base + dst_delta_next_ch*out_chA)[samplenr] = s0;
        (dst_chs_base + dst_delta_next_ch*out_chB)[samplenr] = s1;
        (dst_chs_base + dst_delta_next_ch*out_chC)[samplenr] = s2;
        (dst_chs_base + dst_delta_next_ch*out_chD)[samplenr] = s3;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////
// DECODER KERNELS with HAMMING WINDOWING
/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_decode_2bit1ch_Hamming(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; } // warp divergence...
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = (4*i) % Lfft;
        float4 wf = {
            (0.54f - 0.46f*m_cosf(omega*(n_win+0))),
            (0.54f - 0.46f*m_cosf(omega*(n_win+1))),
            (0.54f - 0.46f*m_cosf(omega*(n_win+2))),
            (0.54f - 0.46f*m_cosf(omega*(n_win+3)))
        };
        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_vdif[(v >> 0) & 3] * wf.x,
	    c_lut2bit_vdif[(v >> 2) & 3] * wf.y,
	    c_lut2bit_vdif[(v >> 4) & 3] * wf.z,
	    c_lut2bit_vdif[(v >> 6) & 3] * wf.w
	};
        dst[i] = tmp;
        return;
}

__global__ void cu_decode_2bit2ch_Hamming(const uint8_t* __restrict__ src, float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1, const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = (2*i) % Lfft;
        float2 wf = {
            (0.54f - 0.46f*m_cosf(omega*(n_win+0))),
            (0.54f - 0.46f*m_cosf(omega*(n_win+1)))
        };
        uint8_t v = src[i];
        float2 ch0 = { c_lut2bit_vdif[(v >> 0) & 3] * wf.x, c_lut2bit_vdif[(v >> 4) & 3] * wf.y };
        float2 ch1 = { c_lut2bit_vdif[(v >> 2) & 3] * wf.x, c_lut2bit_vdif[(v >> 6) & 3] * wf.y };
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        return;
}

__global__ void cu_decode_2bit4ch_Hamming(const uint8_t* __restrict__ src,
     float* __restrict__ dst_ch0, float* __restrict__ dst_ch1,
     float* __restrict__ dst_ch2, float* __restrict__ dst_ch3,
     const size_t Nbytes, const size_t Lfft)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float omega = M_2PI_f32 / (Lfft - 1);
        float n_win = i % Lfft;
        float wf = (0.54f - 0.46f*m_cosf(omega*(n_win)));
        uint8_t v = src[i];
        float ch0 = c_lut2bit_vdif[(v >> 0) & 3] * wf;
        float ch1 = c_lut2bit_vdif[(v >> 4) & 3] * wf;
        float ch2 = c_lut2bit_vdif[(v >> 2) & 3] * wf;
        float ch3 = c_lut2bit_vdif[(v >> 6) & 3] * wf;
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        dst_ch2[i] = ch2;
        dst_ch3[i] = ch3;
        return;
}

__global__ void cu_decode_2bit4Xch_Hamming(const uint8_t* __restrict__ src,
    float* __restrict__ dst_chs_base, const size_t dst_delta_next_ch,
    const size_t Nbytes, const size_t Lfft, const size_t Nch)
{
        // Must have Nch >= 4
        const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        const size_t base_ch = (4*i) % Nch;
        const size_t samplenr = (4*i) / Nch;
        const float omega = M_2PI_f32 / (Lfft - 1);
        const float n_win = samplenr % Lfft;
        const float wf = (0.54f - 0.46f*m_cosf(omega*(n_win)));
        const size_t out_chA = base_ch;
        const size_t out_chB = (base_ch + 1) % Nch;
        const size_t out_chC = (base_ch + 2) % Nch;
        const size_t out_chD = (base_ch + 3) % Nch;
        if (i >= Nbytes) { return; }
        uint8_t v = src[i];
        float s0 = c_lut2bit_vdif[(v >> 0) & 3] * wf;
        float s1 = c_lut2bit_vdif[(v >> 4) & 3] * wf;
        float s2 = c_lut2bit_vdif[(v >> 2) & 3] * wf;
        float s3 = c_lut2bit_vdif[(v >> 6) & 3] * wf;
        (dst_chs_base + dst_delta_next_ch*out_chA)[samplenr] = s0;
        (dst_chs_base + dst_delta_next_ch*out_chB)[samplenr] = s1;
        (dst_chs_base + dst_delta_next_ch*out_chC)[samplenr] = s2;
        (dst_chs_base + dst_delta_next_ch*out_chD)[samplenr] = s3;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////
// DECODER KERNELS with USER-PROVIDED WINDOW in global memory
/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_decode_2bit1ch_CustomWindow(const uint8_t* src, float4* dst, const size_t Nbytes, const size_t Lfft, const float4* __restrict__ wf)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x; // index of 4-sample raw byte
        if (i >= Nbytes) { return; } // warp divergence...
        float4 w = wf[i % (Lfft/4)];
        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_vdif[(v >> 0) & 3] * w.x,
	    c_lut2bit_vdif[(v >> 2) & 3] * w.y,
	    c_lut2bit_vdif[(v >> 4) & 3] * w.z,
	    c_lut2bit_vdif[(v >> 6) & 3] * w.w
	};
        dst[i] = tmp;
        return;
}

__global__ void cu_decode_2bit2ch_CustomWindow(const uint8_t* __restrict__ src,
    float2* __restrict__ dst_ch0, float2* __restrict__ dst_ch1,
    const size_t Nbytes, const size_t Lfft, const float2* __restrict__ wf)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float2 w = wf[i % (Lfft/2)];
        uint8_t v = src[i];
        float2 ch0 = { c_lut2bit_vdif[(v >> 0) & 3] * w.x, c_lut2bit_vdif[(v >> 4) & 3] * w.y };
        float2 ch1 = { c_lut2bit_vdif[(v >> 2) & 3] * w.x, c_lut2bit_vdif[(v >> 6) & 3] * w.y };
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        return;
}

__global__ void cu_decode_2bit4ch_CustomWindow(const uint8_t* __restrict__ src,
     float* __restrict__ dst_ch0, float* __restrict__ dst_ch1,
     float* __restrict__ dst_ch2, float* __restrict__ dst_ch3,
     const size_t Nbytes, const size_t Lfft, const float* __restrict__ wf)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= Nbytes) { return; }
        float w = wf[i % Lfft];
        uint8_t v = src[i];
        float ch0 = c_lut2bit_vdif[(v >> 0) & 3] * w;
        float ch1 = c_lut2bit_vdif[(v >> 4) & 3] * w;
        float ch2 = c_lut2bit_vdif[(v >> 2) & 3] * w;
        float ch3 = c_lut2bit_vdif[(v >> 6) & 3] * w;
        dst_ch0[i] = ch0;
        dst_ch1[i] = ch1;
        dst_ch2[i] = ch2;
        dst_ch3[i] = ch3;
        return;
}

/////////////////////////////////////////////////////////////////////////////////////
// CPU -- CUDA wrappers
/////////////////////////////////////////////////////////////////////////////////////

void decode_2bitNch(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src,
    float** __restrict__ dst_chs,
    const size_t Nbytes,
    const size_t Nch)
{
    // Simplifies the selection of the correct kernel
    // Note: CUDA does not support "float** arg" since the pointer is *CPU-side* to an array of GPU-side data,
    //       hence instead must expand it into Nch x (float* dev_ptr_n) arguments...
    size_t dst_delta_next_ch = dst_chs[1] - dst_chs[0];
    switch (Nch) {
        case 1:
            cu_decode_2bit1ch<<<nblk,nthrd,0,sid>>>(src, (float4*)(dst_chs[0]), Nbytes);
            break;
        case 2:
            cu_decode_2bit2ch<<<nblk,nthrd,0,sid>>>(src, (float2*)(dst_chs[0]), (float2*)(dst_chs[1]), Nbytes);
            break;
        case 4:
            cu_decode_2bit4ch<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_chs[1], dst_chs[2], dst_chs[3], Nbytes);
            break;
        case 8:
        case 16:
        case 32:
        case 64:
            cu_decode_2bit4Xch<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_delta_next_ch, Nbytes, Nch);
            break;
        default:
            printf("Decode for 2-bit %d-ch not implemented!\n", (int)Nch);
            exit(1);
    }
}

void decode_2bitNch_Hann(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src,
    float** __restrict__ dst_chs,
    const size_t Nbytes,
    const size_t Lfft,
    const size_t Nch)
{
    // Simplifies the selection of the correct kernel
    // Note: CUDA does not support "float** arg" since the pointer is *CPU-side* to an array of GPU-side data,
    //       hence instead must expand it into Nch x (float* dev_ptr_n) arguments...
    size_t dst_delta_next_ch = dst_chs[1] - dst_chs[0];
    switch (Nch) {
        case 1:
            cu_decode_2bit1ch_Hann<<<nblk,nthrd,0,sid>>>(src, (float4*)(dst_chs[0]), Nbytes, Lfft);
            break;
        case 2:
            cu_decode_2bit2ch_Hann<<<nblk,nthrd,0,sid>>>(src, (float2*)(dst_chs[0]), (float2*)(dst_chs[1]), Nbytes, Lfft);
            break;
        case 4:
            cu_decode_2bit4ch_Hann<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_chs[1], dst_chs[2], dst_chs[3], Nbytes, Lfft);
            break;
        case 8:
        case 16:
        case 32:
        case 64:
            cu_decode_2bit4Xch_Hann<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_delta_next_ch, Nbytes, Lfft, Nch);
            break;
        default:
            printf("Decode for 2-bit %d-ch not implemented!\n", (int)Nch);
            exit(1);
    }
}

void decode_2bitNch_Hamming(const size_t nblk, const size_t nthrd, cudaStream_t sid,
    const uint8_t* __restrict__ src,
    float** __restrict__ dst_chs,
    const size_t Nbytes,
    const size_t Lfft,
    const size_t Nch)
{
    // Simplifies the selection of the correct kernel
    // Note: CUDA does not support "float** arg" since the pointer is *CPU-side* to an array of GPU-side data,
    //       hence instead must expand it into Nch x (float* dev_ptr_n) arguments...
    size_t dst_delta_next_ch = dst_chs[1] - dst_chs[0];
    switch (Nch) {
        case 1:
            cu_decode_2bit1ch_Hamming<<<nblk,nthrd,0,sid>>>(src, (float4*)(dst_chs[0]), Nbytes, Lfft);
            break;
        case 2:
            cu_decode_2bit2ch_Hamming<<<nblk,nthrd,0,sid>>>(src, (float2*)(dst_chs[0]), (float2*)(dst_chs[1]), Nbytes, Lfft);
            break;
        case 4:
            cu_decode_2bit4ch_Hamming<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_chs[1], dst_chs[2], dst_chs[3], Nbytes, Lfft);
            break;
        case 8:
        case 16:
        case 32:
        case 64:
            cu_decode_2bit4Xch_Hamming<<<nblk,nthrd,0,sid>>>(src, dst_chs[0], dst_delta_next_ch, Nbytes, Lfft, Nch);
            break;
        default:
            printf("Decode for 2-bit %d-ch not implemented!\n", (int)Nch);
            exit(1);
    }
}

/////////////////////////////////////////////////////////////////////////////////////
// Older functions, for speed testing of various decode methods
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Decode several bytes in each thread.
 * One byte contains four 2-bit samples.
 * Memory layout:  [byte 0, byte 1, ... byte 31, byte 32, byte 33, ... ]
 * Access pattern: [bk0 t0, bk0 t1, ... bk0 t31, bk1  t0, bk1  t1, ... ]
 * Lookup is from a very small float[4] table.
 * Throughput on Titan X is 
 *    ~45.0 Gs/s (191.3 GB/s in+out) when using lookup in __constant__,
 *    ~41.0 Gs/s (174.3 GB/s in+out) when using 'const float[4]' inside kernel,
 *    ~45.0 Gs/s (191.3 GB/s in+out) when using preloaded __shared__ inside kernel,
 *                                no gain with sh_lut2bit[threadIdx.x][4] to avoid collisions
 */
__global__ void cu_decode_2bit1ch_1D_v1b(const uint8_t* __restrict__ src, float4* __restrict__ dst, size_t Nbytes, size_t bytes_per_thread)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        size_t nthreads = blockDim.x * gridDim.x;

        __shared__ float sh_lut2bit[4];
        sh_lut2bit[0] = -3.3359; sh_lut2bit[1] = -1.0; 
        sh_lut2bit[2] = +1.0; sh_lut2bit[3] = +3.3359;
        //const float lc_lut2bit_vdif[4] = DECODER_LUT_2BIT_TO_4FLOAT_VDIF;

        for (size_t nbyte = 0; nbyte < bytes_per_thread && i < Nbytes; nbyte++) {
                uint8_t v = src[i];
                float4 tmp = {
                    sh_lut2bit[(v >> 0) & 3],
                    sh_lut2bit[(v >> 2) & 3],
                    sh_lut2bit[(v >> 4) & 3],
                    sh_lut2bit[(v >> 6) & 3]
                };
                dst[i] = tmp;
                i += nthreads;
	}
        return;
}

/**
 * Decode one byte in each thread.
 * One byte contains four 2-bit samples in VDIF or Mark5B order.
 * Throughput on Titan X is 
 *    ~56.5 Gs/s (240 GB/s in+out) when checking for "if (i >= Nbytes) { return; }"
 *    ~59.2 Gs/s when using global __constant__ instead of local const float
 */
__global__ void cu_decode_2bit1ch_1D_v2(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, size_t dummy)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;

        //const float lc_lut2bit_vdif[4] = DECODER_LUT_2BIT_TO_4FLOAT;

        if (i >= Nbytes) { return; } // warp divergence...

        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_vdif[(v >> 0) & 3],
	    c_lut2bit_vdif[(v >> 2) & 3],
	    c_lut2bit_vdif[(v >> 4) & 3],
	    c_lut2bit_vdif[(v >> 6) & 3]
	};
        dst[i] = tmp;

        return;
}

__global__ void cu_decode_2bit1ch_1D_mark5b_v2(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, size_t dummy)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i >= Nbytes) { return; }

        uint8_t v = src[i];
	float4 tmp = {
	    c_lut2bit_mark5b[(v >> 0) & 3],
	    c_lut2bit_mark5b[(v >> 2) & 3],
	    c_lut2bit_mark5b[(v >> 4) & 3],
	    c_lut2bit_mark5b[(v >> 6) & 3]
	};
        dst[i] = tmp;

        return;
}

/**
 * Decode several bytes in each thread.
 * One byte contains four 2-bit samples in VDIF bit order.
 * Memory layout:  [byte 0, byte 1, ... byte 31, byte 32, byte 33, ... ]
 * Access pattern: [bk0 t0, bk0 t1, ... bk0 t31, bk1  t0, bk1  t1, ... ]
 * Lookup is from a large float4[255] lookup table of ~4kB in size.
 * Throughput on Titan X is 
 *     ~16.2 Gs/s ( 68.8 GB/s) when using __constant__ for the lookup
 *     ~44.6 Gs/s (189.6 GB/s) when using shared memory for the lookup
 */
__global__ void cu_decode_2bit1ch_1D_v3(const uint8_t* src, float4* dst, size_t Nbytes, size_t bytes_per_thread)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        size_t nthreads = blockDim.x * gridDim.x;

	__shared__ float4 sh_lookup_2bit[256];
        // Prefetch the lookup table into shared fast memory.
        // When using constant memory for lookup, throughput is ~6 Gs/s on Titan X
        // When using shared memory for lookup, throughout is ~25 Gs/s on Titan X
        // For 32 threads in a warp, each thread loads 8 tuples out of 256-tuple lookup (256/32=8)
        size_t sh_i = 8*threadIdx.x; 
        sh_lookup_2bit[sh_i + 0] = c_lookup_2bit_VDIF[sh_i+0]; 
        sh_lookup_2bit[sh_i + 1] = c_lookup_2bit_VDIF[sh_i+1]; 
        sh_lookup_2bit[sh_i + 2] = c_lookup_2bit_VDIF[sh_i+2]; 
        sh_lookup_2bit[sh_i + 3] = c_lookup_2bit_VDIF[sh_i+3]; 
        sh_lookup_2bit[sh_i + 4] = c_lookup_2bit_VDIF[sh_i+4]; 
        sh_lookup_2bit[sh_i + 5] = c_lookup_2bit_VDIF[sh_i+5]; 
        sh_lookup_2bit[sh_i + 6] = c_lookup_2bit_VDIF[sh_i+6]; 
        sh_lookup_2bit[sh_i + 7] = c_lookup_2bit_VDIF[sh_i+7]; 
        __syncthreads();

        for (size_t nbyte = 0; nbyte < bytes_per_thread && i < Nbytes; nbyte++) {
                uint8_t v = src[i];  // 66 Gs/s if not reading from main memory
		//dst[i] = c_lookup_2bit_VDIF[v];
		dst[i] = sh_lookup_2bit[v];
                i += nthreads;
        }
        return;
}

/**
 * Decode several bytes in each thread.
 * One byte contains four 2-bit samples in VDIF bit order.
 * Memory layout:  [byte 0, byte 1, ... byte 31, byte 32, byte 33, ... ]
 * Access pattern: [bk0 t0, bk0 t1, ... bk0 t31, bk1  t0, bk1  t1, ... ]
 * Lookup is replaced by a Lagrange interpolating polynomial,
 * trading locally serialized memory access against parallel arithmetic.
 * Throughput on Titan X is 
 *     ~45.0 Gs/s ( 191.3 GB/s) 
 */
__global__ void cu_decode_2bit1ch_1D_v4(const uint8_t* src, float4* dst, size_t Nbytes, size_t bytes_per_thread)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        size_t nthreads = blockDim.x * gridDim.x;

        const float4 c1_c4 = {0.55598333f, 0.55598333f, 0.55598333f, 0.55598333f}; // =3.3359/6
        const float4 c2_c3 = {-0.5f, -0.5f, -0.5f, -0.5f};

        for (size_t nbyte = 0; nbyte < bytes_per_thread && i < Nbytes; nbyte++) {
                uint8_t v = src[i];
                float4 xx = { (v>>0)&3, (v>>2)&3, (v>>4)&3, (v>>6)&3 };
                float4 xm1 = {xx.x - 1.0f, xx.y - 1.0f, xx.z - 1.0f, xx.w - 1.0f };
                float4 xm2 = {xx.x - 2.0f, xx.y - 2.0f, xx.z - 2.0f, xx.w - 2.0f };
                float4 xm3 = {xx.x - 3.0f, xx.y - 3.0f, xx.z - 3.0f, xx.w - 3.0f };
                float4 xm2xm3 = xm2 * xm3;
                dst[i] = c1_c4*xm1*xm2xm3 + xx * (c2_c3*xm2xm3 + xm1 * ( c1_c4*xm2 + c2_c3*xm3 ) );
                i += nthreads;
	}
        return;
}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

int main(int argc, char** argv)
{
    const int max_channels = 32;
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    unsigned char *d_data_uint8;
    float *d_data_f32;
    float *h_data_f32;
    float *d_window_f32;
    float *h_window_f32;
    float *d_channels_f32[max_channels];

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    // size_t maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    size_t Lfft = 64; // fake for windowed unpacker
    size_t nsamples  = 32*1024*1024;
    size_t nrawbytes = (nsamples*2)/8;

    if (argc == 2) {
        Lfft = atoi(argv[1]);
        if ((Lfft >= nsamples) || (Lfft < 64)) {
            printf("Error: specified bad Lfft of %zu\n", Lfft);
            return -1;
        }
    }

    CUDA_CALL( cudaMalloc( (void **)&d_data_uint8, nrawbytes ) );
    CUDA_CALL( cudaMalloc( (void **)&d_data_f32,   nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_window_f32, Lfft*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_f32,   nsamples*sizeof(float), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_window_f32, Lfft*sizeof(float), cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_channels_f32[0], max_channels*(nsamples+8)*sizeof(float)) );
    for (int n=1; n<max_channels; n++) {
        size_t step = nsamples;
        if ((step % 8) != 0) {
            step = step + (8 - (step % 8));
        }
        d_channels_f32[n] = d_channels_f32[n-1] + step;
    }

    // Fill input with 0x1B = 0b0001.1011 = { +3, +1, -1, -3 }
    CUDA_CALL( cudaMemset( d_data_uint8, 0x1B, nrawbytes ) );

    // Prepare a custom window
    for (size_t n=0; n<Lfft; n++) {
        float omega = M_2PI_f32 / (Lfft - 1);
        h_window_f32[n] = 0.54f - 0.46f*cosf(omega*n);
    }
    CUDA_CALL( cudaMemcpy(d_window_f32, h_window_f32, Lfft*sizeof(float), cudaMemcpyHostToDevice) );

    /////////////////////////////////////////////////////////////////////////////////

    size_t threadsPerBlock = cudaDevProp.warpSize;
    size_t numBlocks = div2ceil(nrawbytes, threadsPerBlock);

    printf("\nDecoder performance, window func length %zu\n", Lfft);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes );
    CUDA_CHECK_ERRORS("2bit1ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit1ch", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch_complex <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes/2 );
    CUDA_CHECK_ERRORS("2bit1ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit1chCX", nsamples/2);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch_Hann <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("2bit1ch_Hann");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit1ch_Hann", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch_Hamming <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("2bit1ch_Hamming");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit1ch_Hamming", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch_CustomWindow <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes, Lfft, (float4*)d_window_f32 );
    CUDA_CHECK_ERRORS("2bit1ch_CustomWf");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit1ch_CustomWf", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit2ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float2*)d_data_f32, ((float2*)d_data_f32) + nsamples/4, nrawbytes);
    CUDA_CHECK_ERRORS("2bit2ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit2ch", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit2ch_Hann <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float2*)d_data_f32, ((float2*)d_data_f32) + nsamples/4, nrawbytes, Lfft);
    CUDA_CHECK_ERRORS("2bit2ch_Hann");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit2ch_Hann", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit2ch_Hamming <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float2*)d_data_f32, ((float2*)d_data_f32) + nsamples/4, nrawbytes, Lfft);
    CUDA_CHECK_ERRORS("2bit2ch_Hamming");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit2ch_Hamming", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit2ch_CustomWindow <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float2*)d_data_f32, ((float2*)d_data_f32) + nsamples/4,
        nrawbytes, Lfft, (float2*)d_window_f32
      );
    CUDA_CHECK_ERRORS("2bit2ch_CustomWf");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit2ch_CustomWf", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit4ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8,
        ((float*)d_data_f32), ((float*)d_data_f32) + nsamples/4, ((float*)d_data_f32) + 2*nsamples/4, ((float*)d_data_f32) + 3*nsamples/4, nrawbytes
      );
    CUDA_CHECK_ERRORS("2bit4ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit4ch", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit4ch_Hann <<<numBlocks, threadsPerBlock>>> ( d_data_uint8,
        ((float*)d_data_f32), ((float*)d_data_f32) + nsamples/4, ((float*)d_data_f32) + 2*nsamples/4, ((float*)d_data_f32) + 3*nsamples/4, nrawbytes, Lfft
      );
    CUDA_CHECK_ERRORS("2bit4ch_Hann");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit4ch_Hann", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit4ch_Hamming <<<numBlocks, threadsPerBlock>>> ( d_data_uint8,
        ((float*)d_data_f32), ((float*)d_data_f32) + nsamples/4, ((float*)d_data_f32) + 2*nsamples/4, ((float*)d_data_f32) + 3*nsamples/4, nrawbytes, Lfft
      );
    CUDA_CHECK_ERRORS("2bit4ch_Hamming");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit4ch_Hamming", nsamples);

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit4ch_CustomWindow <<<numBlocks, threadsPerBlock>>> ( d_data_uint8,
        ((float*)d_data_f32), ((float*)d_data_f32) + nsamples/4, ((float*)d_data_f32) + 2*nsamples/4, ((float*)d_data_f32) + 3*nsamples/4,
        nrawbytes, Lfft, d_window_f32
      );
    CUDA_CHECK_ERRORS("2bit4ch_CustomWf");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit4ch_CustomWf", nsamples);

    /////////////////////////////////////////////////////////////////////////////////

    for (int nch = 1; nch <= max_channels; nch *= 2) {
        fprintf(stderr, "decode_2bitNch         nch=%2d : ", nch);
        CUDA_TIMING_START(tstart, 0);
        decode_2bitNch(numBlocks, threadsPerBlock, 0,
            d_data_uint8, (float**)(&d_channels_f32),
            nrawbytes, nch);
        CUDA_CHECK_ERRORS("2bitNch");
        CUDA_TIMING_STOP(tstop, tstart, 0, "2bitNch", nsamples);
    }

    for (int nch = 1; nch <= max_channels; nch *= 2) {
        fprintf(stderr, "decode_2bitNch Hann    nch=%2d : ", nch);
        CUDA_TIMING_START(tstart, 0);
        decode_2bitNch_Hann(numBlocks, threadsPerBlock, 0,
            d_data_uint8, (float**)(&d_channels_f32),
            nrawbytes, Lfft, nch);
        CUDA_CHECK_ERRORS("2bitNch_Hann");
        CUDA_TIMING_STOP(tstop, tstart, 0, "2bitNch_Hann", nsamples);
    }

    for (int nch = 1; nch <= max_channels; nch *= 2) {
        fprintf(stderr, "decode_2bitNch Hamming nch=%2d : ", nch);
        CUDA_TIMING_START(tstart, 0);
        decode_2bitNch_Hamming(numBlocks, threadsPerBlock, 0,
            d_data_uint8, (float**)(&d_channels_f32),
            nrawbytes, Lfft, nch);
        CUDA_CHECK_ERRORS("2bitNch_Hamming");
        CUDA_TIMING_STOP(tstop, tstart, 0, "2bitNch_Hamming", nsamples);
    }

    /////////////////////////////////////////////////////////////////////////////////

    printf("\nDecoder output data\n");

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit1ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float4*)d_data_f32, nrawbytes );
    CUDA_CHECK_ERRORS("cu_decode_2bit1ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2b1c", nsamples);
    CUDA_CALL( cudaMemcpy(h_data_f32, d_data_f32, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("kernel out: %5.3f %5.3f %5.3f %5.3f  ... %5.3f %5.3f %5.3f %5.3f\n",
            h_data_f32[0], h_data_f32[1], h_data_f32[2], h_data_f32[3],
            h_data_f32[nsamples-4], h_data_f32[nsamples-3], h_data_f32[nsamples-2], h_data_f32[nsamples-1]
    );

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit2ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, (float2*)d_data_f32, ((float2*)d_data_f32) + nsamples/4, nrawbytes);
    CUDA_CHECK_ERRORS("cu_decode_2bit2ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2b2c", nsamples);
    CUDA_CALL( cudaMemcpy(h_data_f32, d_data_f32, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("kernel out: %5.3f %5.3f %5.3f %5.3f  ... %5.3f %5.3f %5.3f %5.3f\n",
            h_data_f32[0], h_data_f32[1], h_data_f32[2], h_data_f32[3],
            h_data_f32[nsamples-4], h_data_f32[nsamples-3], h_data_f32[nsamples-2], h_data_f32[nsamples-1]
    );

    CUDA_TIMING_START(tstart, 0);
    cu_decode_2bit4ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8,
        ((float*)d_data_f32),
        ((float*)d_data_f32) + nsamples/4,
        ((float*)d_data_f32) + 2*nsamples/4,
        ((float*)d_data_f32) + 3*nsamples/4,
        nrawbytes
      );
    CUDA_CHECK_ERRORS("cu_decode_2bit4ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2b24", nsamples);
    CUDA_CALL( cudaMemcpy(h_data_f32, d_data_f32, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("kernel out: %5.3f %5.3f %5.3f %5.3f  ... %5.3f %5.3f %5.3f %5.3f\n",
            h_data_f32[0], h_data_f32[1], h_data_f32[2], h_data_f32[3],
            h_data_f32[nsamples-4], h_data_f32[nsamples-3], h_data_f32[nsamples-2], h_data_f32[nsamples-1]
    );

    //  2b1c               : 0.00059 s : 56888.887 Ms/s
    //     kernel out: 3.336 1.000 -1.000 -3.336  ... 3.336 1.000 -1.000 -3.336
    //  2b2c               : 0.00059 s : 56594.129 Ms/s
    //     kernel out: 3.336 -1.000 3.336 -1.000  ... 1.000 -3.336 1.000 -3.336
    //  2b24               : 0.00059 s : 56597.185 Ms/s
    //     kernel out: 3.336 3.336 3.336 3.336  ... -3.336 -3.336 -3.336 -3.336

    /////////////////////////////////////////////////////////////////////////////////

    return 0;
}

#endif


#endif // DECODER_2B32F_KERNELS_CU
