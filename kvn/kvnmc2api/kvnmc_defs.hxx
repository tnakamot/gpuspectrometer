#ifndef KVNMC_DEFS_HXX
#define KVNMC_DEFS_HXX

#define CMD_INVALID_CMD    "-na-"
#define CMD_RET_DONE       "DONE"
#define CMD_RET_CMDERROR   "CERR"
#define CMD_RET_PARAMERROR "PERR"
#define CMD_RET_BUSY       "BUSY"
#define CMD_RET_FAIL       "FAIL"  // note: not part of DSM behaviour

#define KVNMC2API_VERSION_IFACE    1  // incremented when an interface to the outside world is changed, e.g., command format, data format, timing, restrictions etc
#define KVNMC2API_VERSION_FUNCTION 1  // incremented (1..inf) when the function of the spectrometer program is changed, e.g., improving the calculation accuracy etc
#define KVNMC2API_VERSION_DEBUG    1  // incremented (1..inf) when a known bug is fixed after a new version of the spectrometer program is released
#define KVNMC2API_VERSION_REFACTOR 1  // incremented (1..inf) when the internal structure of hardware or software is changed with no change in the interface or in the function
#define VER_STRINGIZER(a) #a
#define VER_2STR(a) VER_STRINGIZER(a)

#define KVNMC2API_VERSION_STR VER_2STR(KVNMC2API_VERSION_IFACE) "." VER_2STR(KVNMC2API_VERSION_FUNCTION) "." VER_2STR(KVNMC2API_VERSION_DEBUG) "." VER_2STR(KVNMC2API_VERSION_REFACTOR)

#endif
