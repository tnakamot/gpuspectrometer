/**
 * \class SpectrometerMonCtrl
 *
 * \brief Class to handle KVN commands. Also implements a command class factory.
 *
 * \author $Author: janw $
 *
 */

#ifndef KVNMC2API_HXX
#define KVNMC2API_HXX

#include <iostream>
#include <set>
#include <vector>
#include <string>

class SpectrometerInterface;

class SpectrometerMonCtrl
{
    public:
        /** C'stor, initialize list of commands (whether actually implemented or not depends on ./commands/cmdname.cxx) */
        SpectrometerMonCtrl();

    public:
        void load(std::istream &sstream);
        void load(std::string &s);

        bool isNominallySupported(const std::string &command) const;
        bool isCommandImplemented(const std::string &command) const;
        bool isCommand() const { return m_isCommand; }
        std::string getCommandName() const { return m_commandName; }
        unsigned int getCommandSeqNr() const { return m_commandSeqNr; }

        int  handle(const std::vector<std::string> args, std::vector<std::string>& out, SpectrometerInterface& spectrometer);

        int  handle(std::vector<std::string>& out, SpectrometerInterface& spectrometer) {
            return handle(m_commandArgs, out, spectrometer);
        }

    public:
        std::string dbg_ListAllCommands(bool do_color=true) const;

    private:
        void parse(std::string& s);

    private:
        bool m_isCommand;
        unsigned int m_commandSeqNr;
        std::string m_commandName;
        bool m_commandIsQuery;
        std::vector<std::string> m_commandArgs;
        std::set<std::string> m_mcCommands;
        std::set<std::string> m_mcResponses;

};

#endif // KVNMC2API_HXX
