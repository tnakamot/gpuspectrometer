#include "logger.h"
#include "kvnmc2api/kvnmc2api_factory.hxx"

#include <map>
#include <string>

std::map<std::string, factoryMethod>& MCMsgCommandResponseFactory::m_registeredNames() {
    // note: first call to m_registeredNames() will make sure the _map contained here is
    // properly initialized (singleton pattern), and we avoid the "static initialization fiasco"
    static std::map<std::string, factoryMethod> _map;
    return _map;
}

bool MCMsgCommandResponseFactory::registerClass(std::string name, factoryMethod createMethod)
{
    //L_(ldebug2) << "Registering handler for '" << name << "'";
    std::pair<std::map<std::string, factoryMethod>::iterator, bool> registeredPair =
        MCMsgCommandResponseFactory::m_registeredNames().insert(std::make_pair(name.c_str(), createMethod));
    return registeredPair.second;
}

MCMsgCommandResponse* MCMsgCommandResponseFactory::createObject(std::string name)
{
    std::map<std::string, factoryMethod>::iterator registeredPair =
        MCMsgCommandResponseFactory::m_registeredNames().find(name);
    if(registeredPair == MCMsgCommandResponseFactory::m_registeredNames().end()) { return NULL; } // oops
    return registeredPair->second();
}
