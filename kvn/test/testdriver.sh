#!/bin/bash

HOST=localhost

rm -rf /home/DAS_LOC/OBS/TEST/

./sendCommand.py --host=$HOST PCMD,hello

./sendCommand.py --host=$HOST OBNM,test
./sendCommand.py --host=$HOST OBNM

./sendCommand.py --host=$HOST CONF,SEL_CORIPLEN=256
./sendCommand.py --host=$HOST CONF,SEL_CORLEN=32768
./sendCommand.py --host=$HOST CONF,SEL_CORWINDOW=NONE

./sendCommand.py --host=$HOST CONF,SEL_CORIPLEN?
./sendCommand.py --host=$HOST CONF,SEL_CORLEN?
./sendCommand.py --host=$HOST CONF,SEL_CORWINDOW?

./sendCommand.py --host=$HOST CONF,SEL_COROUTSTREAM=ALL:ON

./sendCommand.py --host=$HOST CONF,SEL_COROUTSTREAM=CROSS12:ON

./sendCommand.py --host=$HOST ACQD,SAVE

sleep 2

./sendCommand.py --host=$HOST STRT

sleep 10

./sendCommand.py --host=$HOST STOP

sleep 1

./sendCommand.py --host=$HOST QUIT




