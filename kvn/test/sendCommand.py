#!/usr/bin/python
"""
Usage: sendCommand.py [--host=<hostname|ip>] <cmd1> <cmd2> <cmd3>

Sends one or more commands to the specified host (default: localhost).
Does not disconnect between commands.
"""

import re, socket, struct, sys, time
remotePort = 60100
remoteHost = ('localhost',remotePort)

def connect(host):
        try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect(remoteHost)
                return sock
        except:
                print ('Could not connect to %s' % (str(remoteHost)))
                sys.exit(-1)

def send(s,cmd):
	s.sendall(cmd.upper())
	s.sendall('\r\n')

def recv(s):
	resp = str(s.makefile().readline())
	return resp.strip()

if len(sys.argv)<=1:
	print __doc__
	sys.exit(-1)

args = sys.argv[1:]
while (len(args)>0) and (args[0][0:2] == '--'):
	if args[0][0:7]=='--host=':
		remoteHost = (args[0][7:],remotePort)
	args = args[1:]
cmds = args

if (len(cmds) <= 0):
        print __doc__
        sys.exit(-1)

sock = connect(remoteHost)
for cmd in args:
	print ('request  : %s' % (cmd.upper()))
	send(sock,cmd)
	#time.sleep(0.5)
	resp = recv(sock)
	print ('response : %s' % (resp))
	print ('')
