#include "core/spectrometerconfig.hxx"

#include <stdio.h>
#include <stdlib.h>
#include <cmath>

static int Tint_msec_candidates[] = {
    10, 16, 20, 32, 40, 48, 50, 64, 100, 120, 128, 150, 200, 250, 256, 500, 512, 1000, 1024
};

void usage(void)
{
    printf(
        "\n"
        "Usage: calculateIntegrationTimes <VDIF payload length> <VDIF rate Mbit/s> <num spectral channels>\n"
        "\n"
        "Provides a list of a few usual integration times between 10ms and 1024ms that are possible\n"
        "for the given length of VDIF frame payload size in bytes, goodput in Mbit/s, and\n"
        "number of channels requested from a spectrometer. Shows only integration times with\n"
        "an integer milliseconds.\n"
        "\n"
    );
}

int main(int argc, char** argv)
{
    if (argc != 4) {
        usage();
        return 0;
    }

    SpectrometerConfig cfg;

    cfg.vdifinfo.payloadsize = atoi(argv[1]);
    cfg.vdifinfo.framespersec = (atoi(argv[2])*1e6)/(8.0*cfg.vdifinfo.payloadsize);
    cfg.scfg.nchan = atoi(argv[3]);

    cfg.vdifinfo.framesize = cfg.vdifinfo.payloadsize + 32;
    cfg.vdifinfo.nbits = 2;
    cfg.vdifinfo.nbands = 8;

    printf("'Integer' integration times possible with %zu-byte payloads, %zu frames/sec, %d spectral channels:\n",
        cfg.vdifinfo.payloadsize, cfg.vdifinfo.framespersec, cfg.scfg.nchan
    );

    for (size_t n=0; n<sizeof(Tint_msec_candidates)/sizeof(Tint_msec_candidates[0]); n++) {

        cfg.scfg.Tint_wish_s = 1e-3*Tint_msec_candidates[n];
        cfg.recalculate();

        double Tint_msec = 1e3*cfg.pcfg.Tint_s;
        double fract = Tint_msec - round(Tint_msec);
        if ((fabs(fract) < 1e-7) && (cfg.pcfg.nfft > 0)) {
            printf("%7d x %d-point FFT : Tint %11.6f msec\n",
                cfg.pcfg.nfft, 2*cfg.scfg.nchan, Tint_msec);
        }

    }

}
