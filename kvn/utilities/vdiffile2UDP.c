#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>

#define handle_error(msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

void usage()
{
    printf("\nUtility vdiffile2udp v1.0.0\n\n"
           "Usage: vdiffile2udp <file> <rate_Mbit/s> [<port> [<peer>]]\n\n"
           "  file   Input file in VDIF format\n"
           "  rate   Total transfer rate (rate of VDIF Threads * #Threads)\n"
           "  port   Optional UDP destination port (default: 42267)\n"
           "  peer   Optional UDP destination host (default: 127.0.0.1)\n\n"
    );
}

int main(int argc, char** argv)
{
    char *fname;
    char *udp_port = "46227";
    char *udp_host = "127.0.0.1";

    int fd, sd, rate_Mbps, so_arg, repcount = 0;
    struct stat st;
    unsigned char* mm;

    struct timeval tv_prev, tv_curr;
    double ipd_curr_usec, ipd_target_usec, ipd_delta_usec = 0.0;

    size_t frame_size, payload_size, rate_fps, rdpos = 0;

    struct addrinfo hints;
    struct addrinfo *peer = NULL;

    // Args
    if ((argc < 3) || (argc > 5)) {
        usage();
        exit(EXIT_FAILURE);
    }
    fname = argv[1];
    rate_Mbps = atoi(argv[2]);
    if (argc >= 4) {
        udp_port = argv[3];
    }
    if (argc >= 5) {
        udp_host = argv[4];
    }

    // Prepare UDP transmit socket
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_ADDRCONFIG;
    if (getaddrinfo(udp_host, udp_port, &hints, &peer) != 0) {
        handle_error("resolving peer name");
    }
    sd = socket(peer->ai_family, peer->ai_socktype, peer->ai_protocol);
    if (sd == -1) {
        handle_error("making UDP transmit socket");
    }
    so_arg = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_NO_CHECK, &so_arg, sizeof(so_arg)) == -1) {
        perror("setsockopt UDP checksums off");
    }

    // Open VDIF file and map it
    fd = open(fname, O_RDONLY);
    if (fd == -1) {
        handle_error("open VDIF file");
    }
    if (fstat(fd, &st) == -1) {
        handle_error("get VDIF file size");
    }
    mm = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE|MAP_NORESERVE, fd, 0);
    if (mm == MAP_FAILED) {
        handle_error("mapping VDIF file into memory");
    }

    // Determine VDIF frame rate and IPD
    frame_size = 8*(mm[8] + mm[9]*256L + mm[10]*65536L);
    payload_size = ((mm[3] & 0x40) != 0) ? (frame_size - 16) : (frame_size - 32);
    rate_fps = (1e6*rate_Mbps/8.0) / payload_size;
    ipd_target_usec = 1e6/rate_fps;
    printf("VDIF frame size   : %zu byte\n", frame_size);
    printf("VDIF payload size : %zu byte\n", payload_size);
    printf("VDIF frames/sec   : %zu\n", rate_fps);
    printf("Inter-frame delay : %.3f usec\n", ipd_target_usec);
    printf("Remote recipient  : %s port %s\n", udp_host, udp_port);

    // Transmit loop; restart from beginning of file when hitting EOF
    gettimeofday(&tv_prev, NULL);
    while (1) {

        gettimeofday(&tv_curr, NULL);

        if ((rdpos + frame_size) > st.st_size) {
            rdpos = 0;
            repcount++;
            printf("Entire %.2f GB were sent, restarting (count %d)\n", st.st_size/(1024.0*1024.0*1024.0), repcount);
        }

        if (sendto(sd, mm+rdpos, frame_size, 0, peer->ai_addr, peer->ai_addrlen) == -1) {
            perror("sendto");
        }

        ipd_curr_usec = (tv_curr.tv_sec - tv_prev.tv_sec)*1e6 + (tv_curr.tv_usec - tv_prev.tv_usec);
        ipd_delta_usec += (ipd_target_usec - ipd_curr_usec);
        if (ipd_delta_usec > 0) {
            struct timespec ts;
            ts.tv_sec = (int)(ipd_delta_usec*1e-6);
            ts.tv_nsec = (ipd_delta_usec - ts.tv_sec*1e6)*1e3;
            nanosleep(&ts, NULL);
        }

        tv_prev = tv_curr;
        rdpos += frame_size;
    }

    munmap(mm, st.st_size);
    return 0;
}
