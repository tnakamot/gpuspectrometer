import struct
#import numarray
import numpy
import time

# KVN DSM documentation:
# the header might be 84 bytes long (or 82 bytes if stream bitmask is 16-bit like in Section 5.1.1, versus 32-bit as in Section 5.2)
# [uint32 seqnr | char8 starttime[7] | char8 inputmode[1] | uint16/32 stream bitmask | uint32 valids[8] | uint32 invalids[8] | char8 Tint | char8 outmode | int16 startChIdx ]
#HEADER_FORMAT = 'I7BBI4Q4QBBh' 
HEADER_FORMAT = 'I7BBI8I8IBBh' # should decode into 9 fields, some of them are arrays
                               # -> 29 entries total

								# C format, Python struct format
FLD_IP_NUMBER = 0			# unsigned int[1], I
FLD_START_TIME = 1				# char[7], 7B
FLD_INPUT_MODE = 8				# char[1], B
FLD_OUTPUT_STREAMS = 9			# unsigned int[1], I
								# refer to OUTPUT_STREAMS_INFO
FLD_VALID_SAMPLES = 10			# unsigned int[8], 4Q
FLD_INVALID_SAMPLES = 18		# unsigned int[9], 4Q
FLD_IP_LENGTH  = 26 	# char[1], B
								# 1:10.24, 2:51.2, 3:102.4, 4:512, 5:1024 ms
								# refer to IP_LENGTH_INFO
FLD_OUTPUT_MODE = 27			# char[1], B
								# 1:Channel Select Mode, 2:Channel Bind Mode
								# refer to OUTPUT_MODE_INFO
FLD_START_CHANNEL_INDEX = 28	# 16 bit short[1], h

FLDLEN_START_TIME = 7			
FLDLEN_VALID_SAMPLES = 8		
FLDLEN_INVALID_SAMPLES = 8		

HEADER_LENGTH = 84
MAX_NR_STREAM = 16 

INPUT_MODE_INFO = ['Narrow','Wide']
OUTPUT_MODE_INFO = ['Select','Bind']
OUTPUT_STREAMS_INFO = [
	'Auto1','Auto2','CR12','CI12',
	'Auto3','Auto4','CR34','CI34',
	'Auto5','Auto6','CR56','CI56',
	'Auto7','Auto8','CR78','CI78',]
#	'Auto1','Auto2','CR12 ','CI12 ',
#	'Auto3','Auto4','CR34 ','CI34 ',
#	'Auto5','Auto6','CR56 ','CI56 ',
#	'Auto7','Auto8','CR78 ','CI78 ',]
IP_LENGTH_INFO = [10.24, 51.2, 102.4, 512, 1024] # in ms

#DATA_TYPE = numarray.Float32
DATA_TYPE = numpy.float32
DATA_LENGTH = 4
NCHANNEL = 4096
STREAM_DATA_SIZE = DATA_LENGTH*NCHANNEL
STREAM_SIZE = HEADER_LENGTH+STREAM_DATA_SIZE

class HeaderSection:
	def __init__(self):
		self.h = None
		self.IPNum = None
		self.StartTime = None
		self.InputMode = None
		self.OutputStreams = []
		self.ValidSamples = None
		self.InvalidSamples = None
		self.IPLength = None
		self.OutputMode = None
		self.StartChannelIndex = None

	def read(self,repository):
		''' read header section on the repository place.
		The type of repository is either file or string type.
		For file type, the file position must be set to the start position of header section in advance. 
		'''
		if isinstance(repository,file):
			fp = repository
			buf = fp.read(HEADER_LENGTH)
		else:
			buf = repository
		self.hbuf = buf[:HEADER_LENGTH]
		self.unpack(self.hbuf)

	def unpack(self,buf):
		ssize = struct.calcsize(HEADER_FORMAT)
		#print 'Format expected len:', ssize, '  Header len:', HEADER_LENGTH
		self.hbuf = buf[:HEADER_LENGTH]
		self.h = struct.unpack(HEADER_FORMAT, self.hbuf)
		shex = ":".join("{:02x}".format(ord(c)) for c in self.hbuf)
		print 'Raw header:', shex
		#print 'Unpacked:', self.h
		h = self.h

		self.IPNum = h[FLD_IP_NUMBER]
		self.StartTime = self.getStartTime()
#		self.InputMode = h[FLD_INPUT_MODE]
		self.InputMode = self.getInputMode()
		self.OutputStreams = self.getOutputStreams()
		self.ValidSamples = self.getValidSamples()
		self.InvalidSamples = self.getInvalidSamples()
		self.IPLength = self.getIPLength()
		self.OutputMode = self.getOutputMode()
		self.StartChannelIndex = h[FLD_START_CHANNEL_INDEX]
		self.uttime = time.mktime(self.StartTime)\
			+float(self.IPLength*self.IPNum)/1000

	def getIPLength(self):
		#print 'getIPLength, self.h=', self.h, ' value=', (int(self.h[FLD_IP_LENGTH])-1)
		i = int(self.h[FLD_IP_LENGTH])-1
		if (i >= len(IP_LENGTH_INFO)):
			return 1.00
		return IP_LENGTH_INFO[int(self.h[FLD_IP_LENGTH])-1]

	def getStartTime(self):
		import time
		st = []
		mask = 0x0f
		for i in range(FLDLEN_START_TIME):
			t = (self.h[FLD_START_TIME+i]>>4) & mask
			if t > 0x09: t -= 0x0a
			st.append(t)
			t = (self.h[FLD_START_TIME+i]) & mask
			if t > 0x09: t -= 0x0a
			st.append(t)

		yr = st[0]*1000+st[1]*100+st[2]*10+st[3]
		day = st[4]*100+st[5]*10+st[6]
		hour = st[7]*10+st[8]
		min = st[9]*10+st[10]
		sec = st[11]*10+st[12]

		tfmt = "%Y %j %H %M %S"
		tstr = "%d %d %d %d %d"%(yr,day,hour,min,sec)

		return time.strptime(tstr,tfmt)

	def getValidSamples(self):
		return self.h[FLD_VALID_SAMPLES:FLD_VALID_SAMPLES+8]
			
	def getInvalidSamples(self):
		return self.h[FLD_INVALID_SAMPLES:FLD_INVALID_SAMPLES+8]
			
	def getInputMode(self):
		i = self.h[FLD_INPUT_MODE]
		mode =  INPUT_MODE_INFO[i-1]
#		print 'Input Mode :', mode
		return mode

	def getOutputMode(self):
		i = self.h[FLD_OUTPUT_MODE]
		mode =  OUTPUT_MODE_INFO[i-1]
#		print 'Output Mode :', mode
		return mode

	def getOutputStreams(self):
		outputstreams = []
		ostm = self.h[FLD_OUTPUT_STREAMS]
		mask = 0x01
		for i in range(16):
			if (ostm >> i) & mask:
				outputstreams.append(OUTPUT_STREAMS_INFO[i])
		return outputstreams

	def show(self):
		if not self.StartTime: 
			print "No header section read."
			return
		print "Sequence Number     :", self.IPNum
		print "Start Time          :", self.StartTime
		print "Input Mode          :", self.InputMode
		print "Output Streams      :", self.OutputStreams
		print "Valid Samples       :", self.ValidSamples
		print "Invalid Samples     :", self.InvalidSamples
		print "Integration Length  :", self.IPLength
		print "Output Mode         :", self.OutputMode
		print "Start Channel Index :", self.StartChannelIndex

class DSMHeader(HeaderSection):
	def __init__(self):
		HeaderSection.__init__(self)
		self.IPNum = []
		self.ValidSamples = []
		self.InvalidSamples = []

	def append(self,header_section):
		self.IPNum.append(header_section.IPNum)
		self.ValidSamples.append(header_section.ValidSamples)
		self.InvalidSamples.append(header_section.InvalidSamples)

		self.StartTime = header_section.StartTime
		self.InputMode = header_section.InputMode
		self.OutputStreams = header_section.OutputStreams
		self.IPLength = header_section.IPLength
		self.OutputMode = header_section.OutputMode
		self.StartChannelIndex = header_section.StartChannelIndex

class DSMData:
	def __init__(self):
		self.header_section = HeaderSection()
		self.header = DSMHeader()
		self.streams = None
		self.fp = None
		self.buf = None
		self.file_size = 0
		self.stream_size = 0
		self.nstreams = 0
		self.num_ip = 0

	def clear(self):
		self.fp.close()
		self.__init__()
	
	def isOpen(self):
		if self.fp == None: return False
		else: return not self.fp.closed

	def open(self,file):
		'''open DSM file and read the first header section to extract 
		StartTime, InputMode, OutputStreams, IPLength, OutputMode, and StartChannelIndex'''

		fp = open(file,'r')
		fp.seek(0,2)
		file_size = fp.tell() 
		fp.seek(0)

		if file_size < HEADER_LENGTH:
			raise IOError

		self.header_section.read(fp)
		self.fp = fp
		self.file_size = file_size

		outputstreams = self.header_section.OutputStreams
		self.nstreams = len(outputstreams)
		self.stream_size = (HEADER_LENGTH+NCHANNEL*DATA_LENGTH*self.nstreams)
		self.num_ip = file_size/self.stream_size

	def getNStreams(self): return self.nstreams
	def getSequenceSize(self): return self.stream_size
	def getNumIP(self): return self.num_ip
	def getFileSize(self): return self.file_size
	def getFileName(self): return self.fp.name

	def getStartTime(self): return self.header_section.StartTime
	def getInputMode(self): return self.header_section.InputMode
	def getOutputStreams(self): return self.header_section.OutputStreams
	def getIPLength(self): return self.header_section.IPLength
	def getOutputMode(self): return self.header_section.OutputMode
	def getStartChannelIndex(self): return self.header_section.StartChannelIndex

	def show(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name           :", self.getFileName()
		print "File Size           :", self.getFileSize()
		print "File Num of IP      :", self.getNumIP()
		print "File NStreams       :", self.getNStreams()
		print "# Start DSM Header"
		self.header.show()

	def showHeaderSection(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name           :", self.getFileName()
		print "File Size           :", self.getFileSize()
		print "File Num of IP      :", self.getNumIP()
		print "File NStreams       :", self.getNStreams()
		print "# Start Header Section"
		self.header_section.show()

	def read(self,isequence):
		'''Return header section and data stream in a given sequence number.
		'''
		if isequence > self.num_ip:
			raise IOError

		offset = self.stream_size*isequence
		self.fp.seek(offset)
		header_section = HeaderSection()
		buf = self.fp.read(self.stream_size)
		header_section.read(buf)
#		streams = numarray.fromstring(buf[HEADER_LENGTH:],DATA_TYPE,(self.nstreams, NCHANNEL))
		_streams = numpy.fromstring(buf[HEADER_LENGTH:],dtype=DATA_TYPE)
		streams = numpy.resize(_streams,(self.nstreams, NCHANNEL))

		return header_section, streams

	def readHeader(self):
		'''Read all header sections in the DSM file.
		'''
		fp = self.fp
		header_section = HeaderSection()
		for isequence in range(self.num_ip):
			offset = self.stream_size*isequence
			fp.seek(offset)
			header_section.read(fp)
			self.header.append(header_section)
			
	def readChannel(self,*channels):
		'''Read all data of given IF channel name or IF channel number in DSM file.
		'''
		ichs = []
		output_streams = []
#		streams = numarray.zeros((self.num_ip,len(channels), NCHANNEL),DATA_TYPE)
		streams = numpy.zeros((self.num_ip,len(channels), NCHANNEL),DATA_TYPE)
		header = DSMHeader()

		for ch in channels:
			if isinstance(ch, int):
				if ch < self.nstreams:
					ichs.append(ch)
				else: 
					raise IOError
			elif ch in OUTPUT_STREAMS_INFO:
				ichs.append(OUTPUT_STREAMS_INFO.index(ch))
			else:
				raise IOError
			print ch
			output_streams.append(ichs[-1])

		for isequence in range(self.num_ip):
			h,s = self.read(isequence)
			header.append(h)
			for i, ich in enumerate(ichs):
				streams[isequence][i]=s[ich].copy()

		header.OutputStreams = output_streams

#		return header, streams
		return header, streams
				
	def readChannelAvg(self,*channels):
		'''Read all data of given IF channel name or IF channel number in DSM file.
		'''
		ichs = []
		output_streams = []
#		streams = numarray.zeros((len(channels), NCHANNEL),DATA_TYPE)
		streams = numpy.zeros((len(channels), NCHANNEL),DATA_TYPE)
		header = DSMHeader()

		for ch in channels:
			if isinstance(ch, int):
				if ch < self.nstreams:
					ichs.append(ch)
				else: 
					raise IOError
			elif ch in OUTPUT_STREAMS_INFO:
				ichs.append(OUTPUT_STREAMS_INFO.index(ch))
			else:
				raise IOError
			print ch
			output_streams.append(ichs[-1])

		for isequence in range(self.num_ip):
			h,s = self.read(isequence)
			header.append(h)
			for i, ich in enumerate(ichs):
				streams[i] += s[ich]

		header.OutputStreams = output_streams

		return header, streams

class StreamHeader(HeaderSection):
	def __init__(self):
		HeaderSection.__init__(self)
		self.StreamNum = -1
	
	def read(self,buf,i=0):
		''' read header section on the repository place.
		The type of repository is either file or string type.
		For file type, the file position must be set to the start position of header section in advance. 
		'''
		hlen = HEADER_LENGTH
		if i >= 0:
			self.hbuf = buf[hlen*i:hlen*(i+1)]
		else:
			size = len(buf)
			self.hbuf = buf[size+hlen*i:size+hlen*(i+1)]
			
		self.unpack(self.hbuf)
		
	def unpack(self,buf):
		self.hbuf = buf[:HEADER_LENGTH]
		self.h = struct.unpack(HEADER_FORMAT, self.hbuf)
		h = self.h
		self.IPNum = h[FLD_IP_NUMBER]
		self.StartTime = self.getStartTime()
#		self.InputMode = h[FLD_INPUT_MODE]
		self.InputMode = self.getInputMode()
		self.OutputStreams = self.getOutputStreams()
		self.ValidSamples = self.getValidSamples()
		self.InvalidSamples = self.getInvalidSamples()
		self.IPLength = self.getIPLength()
		self.OutputMode = self.getOutputMode()
		self.StartChannelIndex = h[FLD_START_CHANNEL_INDEX]
		self.uttime = time.mktime(self.StartTime)\
			+float(self.IPLength*self.IPNum)/1000

	'''
	def show(self):
		if not self.StartTime: 
			print "No header section read."
			return
		print "Integration Period Number:", self.IPNum
		print "Start Time (in UT)       :", self.StartTime
		print "Data Time                :", self.uttime
		print "Input Mode               :", self.InputMode
		print "Output Streams           :", self.OutputStreams
		print "Valid Samples            :", self.ValidSamples
		print "Invalid Samples          :", self.InvalidSamples
		print "Integration Length       :", self.IPLength
		print "Output Mode              :", self.OutputMode
		print "Start Channel Index      :", self.StartChannelIndex
	'''

class DSMStream(DSMData):
	def __init__(self):
		self.header = StreamHeader()
		self.streams = None
		self.fp = None
		self.buf = None
		self.file_size = 0
		self.stream_size = STREAM_SIZE
		self.num_ip = 0

	def open(self,file):
		'''open DSM file and read the first header section to extract 
		StartTime, InputMode, OutputStreams, IPLength, OutputMode, and StartChannelIndex'''

		fp = open(file,'r')
		fp.seek(0,2)
		file_size = fp.tell() 
		fp.seek(0)

		if file_size < STREAM_SIZE:
			raise IOError

		buf = fp.read()
		self.header.read(buf,i=0)
		self.fp = fp
		self.file_size = file_size

		outputstreams = self.header.OutputStreams
		self.stream_size = STREAM_SIZE
		self.num_ip = file_size/self.stream_size

	def show(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name                 :", self.getFileName()
		print "File Size                 :", self.getFileSize()
		print "Num of Integraiton Period :", self.getNumIP()
		print "# Start Header Section"
		self.header.show()

	def read(self,ipnum=-1):
		'''Return header section and data stream in a given sequence number.
		'''
		if ipnum > self.num_ip:
			raise IOError
		elif ipnum >= 0 :
			offset = self.stream_size*ipnum
		elif ipnum < 0:
			offset = self.stream_size*(self.num_ip+ipnum)
		self.fp.seek(offset)
		header = StreamHeader()
		buf = self.fp.read(self.stream_size)
		header.read(buf,i=0)
		data = numpy.fromstring(buf[HEADER_LENGTH+2:],dtype=DATA_TYPE)
		return header, data

	def readBuf(self,buf):
		header = StreamHeader()
		streams = []
		num_ip = len(buf)/STREAM_SIZE
		header.read(buf,i=0)
		for seq in range(num_ip):
			offset = STREAM_SIZE*seq
			_i0 = offset + HEADER_LENGTH
			_i1 = _i0+STREAM_DATA_SIZE
			_stream = numpy.fromstring(buf[_i0:_i1],dtype=DATA_TYPE)
			streams.append(_stream)
		return header, numpy.array(streams)

if __name__ == '__main__':
	d = DSMData()
	filename = '2007123123456.dat'
	d.open(filename)
	hs, s = d.read(0)
	hs.show()

	d.readHeader()
	h.showHeader()

	h, ch = d.readChannel(0,4,8,12)
