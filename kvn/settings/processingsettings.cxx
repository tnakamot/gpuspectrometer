#include "settings/processingsettings.hxx"
#include "kvnmc2api/kvnmc_defs.hxx"
#include "core/defines.hxx"

#include <algorithm>
#include <string>
#include <cassert>
#include <cstring>
#include <iomanip>

/** Initialize to default values */
ProcessingSettings::ProcessingSettings()
{
    // Default GPU mapping
    nGPUs = MAX_GPUS;
    nstreams = MAX_STREAMS;
    for (size_t g = 0; g < sizeof(mapGPUs)/sizeof(int); g++) {
        mapGPUs[g] = g;
    }

    // Default other
    Tint_s = 0.0;
    nfft = 0;
    nsubints = 1;
    nfft_subint = 0;
    rawbufsize = 0;
    nbuffers = nGPUs * nstreams;
    underallocfactor = 1;
}

/** \return True if the spectral settings look fine */
bool ProcessingSettings::validate() const
{
    bool ok = true;

    ok &= (nGPUs > 0);
    assert(nGPUs > 0);

    ok &= (nfft % nsubints) == 0;
    assert((nfft % nsubints) == 0);

    return ok;
}

/** Recalculate any internal derived settings, call if parameters have been changed */
void ProcessingSettings::recalculate()
{
    if (nsubints <= 0) {
        nsubints = 1;
    }
    nfft_subint = nfft / nsubints;
    return;
}

std::ostream& operator<<(std::ostream& os, const ProcessingSettings& s)
{
    os << "Backend: " << s.nGPUs << " devices x " << s.nstreams << " streams\n";
    os << "Backend: device map";
    for (int i=0; i<s.nGPUs; i++) {
        os << " dev[" << i << "]=" << s.mapGPUs[i];
    }
    os << "\n";
    os << "Backend: Tint=" << std::fixed << s.Tint_s << "s actual, "
        << s.nfft << " x " << std::fixed << std::setprecision(1) << s.dfttime_s*1e6 << "us DFTs in "
        << s.nsubints << " subintegrations\n";
    os << "Backend: " << s.nbuffers << " x " << s.rawbufsize << "-byte long raw input buffers\n";
    return os;
}
