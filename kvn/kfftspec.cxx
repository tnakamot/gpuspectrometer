#include "logger.h"
#include "core/spectrometer.hxx"
#include "core/spectrometer_sim.hxx"

#include <boost/program_options.hpp>

#include <iostream>
#include <unistd.h>


int main(int argc, char **argv)
{
    //initLogger("service.log", ldebug);
    SpectrometerInterface* spectrometer = NULL;
    int ctrlPort = TCP_CONTROL_PORT_DEFAULT;
    int monPort  = TCP_MONITOR_PORT_DEFAULT;
    int outputPort = TCP_SPECTRAL_PORT_DEFAULT;
    int vdifPort = UDP_VDIF_PORT_DEFAULT;
    int vdifframeoffset = 0;
    int nparallel = 1;

    // Command line options
    boost::program_options::options_description opts("Command line options");
    opts.add_options()
        ("help,h", "")
        ("ctrl,c",      boost::program_options::value<int>(), "TCP port number to listen on for M&C commands")
        ("monitor,m",  	boost::program_options::value<int>(), "TCP port number for monitoring (unused?)")
        ("spectral,o",  boost::program_options::value<int>(), "TCP port number for client(s) to receive spectra from")
        ("vdif,u",      boost::program_options::value<int>(), "UDP port number to listen on for single-stream VDIF")
        ("vdifoffset,P",boost::program_options::value<int>(), "UDP packet sequence number length (OCTA-D: 0 byte, DBBC3: 8-byte PSN)")
        ("nparallel,n", boost::program_options::value<int>(), "Number of kfftspec instances that user expects to start (default: 1)")
        ("sim,s",           "Simulator mode without GPU nor data output")
        ("immediate,i",     "Start spectral processing immediately using default config (512ms, 4k)")
    ;
    boost::program_options::variables_map parsed;
    try {
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, opts), parsed);
    } catch (...) {
        std::cout << "Failed to parse command line options.\n";
        return 0;
    }

    // Grab the optional cmd line args
    if (parsed.count("help")) {
        std::cout << "\n" << opts << "\n";
        return 0;
    }
    if (parsed.count("ctrl")) {
        ctrlPort = parsed["ctrl"].as<int>();
    }
    if (parsed.count("monitor")) {
        monPort = parsed["monitor"].as<int>();
    }
    if (parsed.count("monitor")) {
        outputPort = parsed["output"].as<int>();
    }
    if (parsed.count("vdif")) {
        vdifPort = parsed["vdif"].as<int>();
    }
    if (parsed.count("vdifoffset")) {
        vdifframeoffset = parsed["vdifoffset"].as<int>();
    }
    if (parsed.count("nparallel")) {
        nparallel = parsed["nparallel"].as<int>();
    }
    bool simulate = (parsed.count("sim") > 0);
    bool immediate_start = (parsed.count("immediate") > 0);

    // Launch the Spectrometer
    try
    {
        // Create a spectrometer on given M&C and VDIF data port
        if (!simulate) {
            spectrometer = new Spectrometer(ctrlPort, monPort, outputPort, vdifPort);
        } else {
            spectrometer = new SpectrometerSim(ctrlPort, monPort, outputPort, vdifPort);
        }

        // Modify some settings if specified
        if (nparallel > 1) {
            spectrometer->setUnderallocationFactor(nparallel);
        }
        if (vdifframeoffset != 0) {
            spectrometer->setVDIFPort(vdifPort);
            spectrometer->setVDIFOffset(vdifframeoffset);
        }

        // Accept M&C connections
        spectrometer->startMC(/*blocking=*/false);

        // Immediately begin with data processing, without M&C-assigned config?
        if (immediate_start) {
            if (spectrometer->startRX()) {
                spectrometer->startGPU();
            }
        }

        // Keep running forever
        while (1) {
            sleep(10);
            L_(ldebug) << "Spectrometer service still alive";
            //break; // for gdb
        }

    }
    catch (std::exception &e)
    {
        std::cout << "Error: " << e.what() << "\n";
    }

    delete spectrometer;
    return 0;
}
